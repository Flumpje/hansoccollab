--------------------------------------------------------------------
--! \file      constantkey.vhd
--! \date      see top of 'Version History'
--! \brief     Showkey
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |20-3-2015  |WLGRW   |Initial version
--! 002    |28-3-2015  |WLGRW   |Corrections in output key and dig2 and dig 3
--! 003    |2-4-2015   |WLGRW   |Corrections to states to meet data stream from keyboard.
--! 004    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--!
--! # Opdracht 2: Deelopdracht constantkey
--!
--! 
--! 
--! \verbatim
--!
--! \ENDverbatim
--!
--! \todo Complete documentation
--!
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
--------------------------------------------------------------------
ENTITY constantkey IS
   PORT (
      reset     : IN  STD_LOGIC;                    --! reset signal active low '0'
      clk       : IN  STD_LOGIC;                    --! clock (50 MHz)
      scancode  : IN  STD_LOGIC_VECTOR(7 DOWNTO 0); --! received byte
      byte_read : IN  STD_LOGIC;                    --! Set if byte is received 
      key       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0); --! show key pressed
      dig2, 
      dig3      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)  --! debug info, current and previous key pressed
   );
END constantkey;
--------------------------------------------------------------------
ARCHITECTURE LogicFunction OF constantkey IS

   -- Modify "typedef" for enum state_type to meet your needs
   -- Add addiditonal SIGNALs for this architecture 
   
   --! "typedef" for enum state_type.
   TYPE state_type is (
      state_no_key,
      state_key_pressed,
		state_key_released,
		state_key_reminder,
		state_key_reminder_2
   );
   SIGNAL current_state, 
          next_state     : state_type; -- signals of the type state_type to hold current- and next-state
   SIGNAL byte_current, 
          dig_buffer     : STD_LOGIC_VECTOR(7 DOWNTO 0); -- temporary buffers
BEGIN

   --! The state dcoder is a PROCESS that is PROCESSing state changes 
   --! each clk and executing async reset. It will also PROCESS debug 
   --! outputs for the component.
   state_decoder: PROCESS (clk, reset) IS
   BEGIN
		--! reset all output signals of FSM
		--! set FSM to inital state
		IF reset = '0' THEN -- Reset (async).
			current_state <= state_no_key;
		ELSIF rising_edge(clk) THEN
			current_state <= next_state;
		END IF;
   END PROCESS; -- END PROCESS input_decoder

   --! The input decoder is a PROCESS that contains the tests and 
   --! conditions for each state
   input_decoder : PROCESS (current_state, scancode, byte_read)
   BEGIN
		CASE current_state IS
			WHEN state_no_key =>
				IF byte_read = '1' THEN
					next_state <= state_key_pressed;
				ELSE
					next_state <= current_state;
				END IF;
			WHEN state_key_pressed =>
				IF byte_read = '1' and scancode = X"F0" THEN
					next_state <= state_key_released;
				ELSE
					next_state <= current_state;
				END IF;
			WHEN state_key_released =>
				IF byte_read = '1' and scancode = X"F0" THEN
					next_state <= state_key_reminder;
				ELSE
					next_state <= current_state;
				END IF;
			WHEN state_key_reminder =>
				IF byte_read = '0' and scancode = X"F0" THEN
					next_state <= state_key_reminder_2;
				ELSE
					next_state <= current_state;
				END IF;
			WHEN state_key_reminder_2 =>
				IF byte_read = '0' THEN
					next_state <= state_no_key;
				ELSE
					next_state <= current_state;
				END IF;
			WHEN OTHERS =>
				next_state <= current_state;
		END CASE;
   END PROCESS; -- END PROCESS state_decoder

   --! The output decoder ia a PROCESS that is executing actions that 
   --! apply to each individual state
   output_decoder : PROCESS (current_state, byte_current)
   BEGIN   
		CASE current_state IS
			WHEN state_no_key =>
				byte_current <= X"00";
			WHEN state_key_pressed =>
				byte_current <= scancode;
			WHEN state_key_released =>
				byte_current <= X"F0";
			WHEN state_key_reminder =>
				byte_current <= X"F0";
			WHEN state_key_reminder_2 =>
				byte_current <= X"00";
			WHEN OTHERS =>
				byte_current <= X"00";
		END CASE;
		
		key <= byte_current;
		dig2 <= byte_current;
		dig3 <= dig_buffer;
		dig_buffer <= byte_current;
   END PROCESS;
   
END LogicFunction;
--------------------------------------------------------------------
