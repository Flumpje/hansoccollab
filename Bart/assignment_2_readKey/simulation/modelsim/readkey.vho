-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "05/19/2020 21:52:30"

-- 
-- Device: Altera 10M02DCU324A6G Package UFBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_J7,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_J8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_H3,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_H4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_H8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	readkey IS
    PORT (
	clk : IN std_logic;
	reset : IN std_logic;
	kbclock : IN std_logic;
	kbdata : IN std_logic;
	key : OUT std_logic_vector(7 DOWNTO 0);
	dig2 : OUT std_logic_vector(7 DOWNTO 0);
	dig3 : OUT std_logic_vector(7 DOWNTO 0)
	);
END readkey;

-- Design Ports Information
-- key[0]	=>  Location: PIN_K17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[1]	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[2]	=>  Location: PIN_T16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[3]	=>  Location: PIN_L16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[4]	=>  Location: PIN_K12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[5]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[6]	=>  Location: PIN_H18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[7]	=>  Location: PIN_P14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[0]	=>  Location: PIN_K18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[1]	=>  Location: PIN_L11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[2]	=>  Location: PIN_T10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[3]	=>  Location: PIN_L15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[4]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[5]	=>  Location: PIN_R18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[6]	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[7]	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[0]	=>  Location: PIN_L17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[1]	=>  Location: PIN_L12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[2]	=>  Location: PIN_U11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[3]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[4]	=>  Location: PIN_K11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[5]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[6]	=>  Location: PIN_J18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[7]	=>  Location: PIN_P17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_L8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- kbclock	=>  Location: PIN_M17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- kbdata	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF readkey IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_kbclock : std_logic;
SIGNAL ww_kbdata : std_logic;
SIGNAL ww_key : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_dig2 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_dig3 : std_logic_vector(7 DOWNTO 0);
SIGNAL \L_ClockDomainCrossing|c3|Q~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \reset~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \key[0]~output_o\ : std_logic;
SIGNAL \key[1]~output_o\ : std_logic;
SIGNAL \key[2]~output_o\ : std_logic;
SIGNAL \key[3]~output_o\ : std_logic;
SIGNAL \key[4]~output_o\ : std_logic;
SIGNAL \key[5]~output_o\ : std_logic;
SIGNAL \key[6]~output_o\ : std_logic;
SIGNAL \key[7]~output_o\ : std_logic;
SIGNAL \dig2[0]~output_o\ : std_logic;
SIGNAL \dig2[1]~output_o\ : std_logic;
SIGNAL \dig2[2]~output_o\ : std_logic;
SIGNAL \dig2[3]~output_o\ : std_logic;
SIGNAL \dig2[4]~output_o\ : std_logic;
SIGNAL \dig2[5]~output_o\ : std_logic;
SIGNAL \dig2[6]~output_o\ : std_logic;
SIGNAL \dig2[7]~output_o\ : std_logic;
SIGNAL \dig3[0]~output_o\ : std_logic;
SIGNAL \dig3[1]~output_o\ : std_logic;
SIGNAL \dig3[2]~output_o\ : std_logic;
SIGNAL \dig3[3]~output_o\ : std_logic;
SIGNAL \dig3[4]~output_o\ : std_logic;
SIGNAL \dig3[5]~output_o\ : std_logic;
SIGNAL \dig3[6]~output_o\ : std_logic;
SIGNAL \dig3[7]~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \kbclock~input_o\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c2|Q~feeder_combout\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \reset~inputclkctrl_outclk\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c2|Q~q\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c3|Q~feeder_combout\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c3|Q~q\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c3|Q~clkctrl_outclk\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[0]~1_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[0]~2\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[1]~1_combout\ : std_logic;
SIGNAL \L_Showkey|Equal2~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[3]~2\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[4]~1_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[4]~q\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[4]~2\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[5]~1_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[5]~q\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[5]~2\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[6]~1_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[6]~q\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[6]~2\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[7]~2_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[7]~q\ : std_logic;
SIGNAL \L_Showkey|Equal0~0_combout\ : std_logic;
SIGNAL \L_Showkey|scancode[0]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[7]~1_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[1]~q\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[1]~2\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[2]~1_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[2]~q\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[2]~2\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[3]~1_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[3]~q\ : std_logic;
SIGNAL \L_Showkey|counter~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:counter[0]~q\ : std_logic;
SIGNAL \L_Showkey|Decoder0~0_combout\ : std_logic;
SIGNAL \L_Showkey|Decoder0~1_combout\ : std_logic;
SIGNAL \kbdata~input_o\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c0|Q~feeder_combout\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c0|Q~q\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c1|Q~feeder_combout\ : std_logic;
SIGNAL \L_ClockDomainCrossing|c1|Q~q\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[0]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[0]~q\ : std_logic;
SIGNAL \L_Showkey|byte_read~0_combout\ : std_logic;
SIGNAL \L_Showkey|byte_read~1_combout\ : std_logic;
SIGNAL \L_Showkey|byte_read~q\ : std_logic;
SIGNAL \L_Showkey|Decoder0~4_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[3]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[3]~q\ : std_logic;
SIGNAL \L_Showkey|Decoder0~3_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[2]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[2]~q\ : std_logic;
SIGNAL \L_Showkey|Decoder0~2_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[1]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[1]~q\ : std_logic;
SIGNAL \L_Showkey|Decoder0~5_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[4]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[4]~q\ : std_logic;
SIGNAL \L_Constantkey|Equal0~1_combout\ : std_logic;
SIGNAL \L_Showkey|Decoder0~6_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[5]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[5]~q\ : std_logic;
SIGNAL \L_Showkey|Decoder0~7_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[6]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[6]~q\ : std_logic;
SIGNAL \L_Showkey|Decoder0~8_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[7]~0_combout\ : std_logic;
SIGNAL \L_Showkey|COUNT:byte_current[7]~q\ : std_logic;
SIGNAL \L_Constantkey|Equal0~0_combout\ : std_logic;
SIGNAL \L_Constantkey|Equal0~2_combout\ : std_logic;
SIGNAL \L_Constantkey|current_state.state_key_released~0_combout\ : std_logic;
SIGNAL \L_Constantkey|current_state.state_key_released~q\ : std_logic;
SIGNAL \L_Constantkey|Selector3~0_combout\ : std_logic;
SIGNAL \L_Constantkey|current_state.state_key_reminder~q\ : std_logic;
SIGNAL \L_Constantkey|current_state.state_key_reminder_2~0_combout\ : std_logic;
SIGNAL \L_Constantkey|current_state.state_key_reminder_2~q\ : std_logic;
SIGNAL \L_Constantkey|Selector0~0_combout\ : std_logic;
SIGNAL \L_Constantkey|current_state.state_no_key~q\ : std_logic;
SIGNAL \L_Constantkey|Selector1~0_combout\ : std_logic;
SIGNAL \L_Constantkey|current_state.state_key_pressed~q\ : std_logic;
SIGNAL \L_Constantkey|dig2[0]~0_combout\ : std_logic;
SIGNAL \L_Constantkey|dig2[1]~1_combout\ : std_logic;
SIGNAL \L_Constantkey|dig2[2]~2_combout\ : std_logic;
SIGNAL \L_Constantkey|dig2[3]~3_combout\ : std_logic;
SIGNAL \L_Constantkey|Selector8~0_combout\ : std_logic;
SIGNAL \L_Constantkey|Selector7~0_combout\ : std_logic;
SIGNAL \L_Constantkey|Selector6~0_combout\ : std_logic;
SIGNAL \L_Constantkey|Selector5~0_combout\ : std_logic;
SIGNAL \L_Showkey|scancode\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_reset <= reset;
ww_kbclock <= kbclock;
ww_kbdata <= kbdata;
key <= ww_key;
dig2 <= ww_dig2;
dig3 <= ww_dig3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\L_ClockDomainCrossing|c3|Q~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \L_ClockDomainCrossing|c3|Q~q\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);

\reset~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \reset~input_o\);
\L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\ <= NOT \L_ClockDomainCrossing|c3|Q~clkctrl_outclk\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X11_Y9_N16
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X18_Y8_N16
\key[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[0]~0_combout\,
	devoe => ww_devoe,
	o => \key[0]~output_o\);

-- Location: IOOBUF_X18_Y3_N2
\key[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[1]~1_combout\,
	devoe => ww_devoe,
	o => \key[1]~output_o\);

-- Location: IOOBUF_X18_Y1_N9
\key[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[2]~2_combout\,
	devoe => ww_devoe,
	o => \key[2]~output_o\);

-- Location: IOOBUF_X18_Y4_N9
\key[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[3]~3_combout\,
	devoe => ww_devoe,
	o => \key[3]~output_o\);

-- Location: IOOBUF_X18_Y6_N23
\key[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector8~0_combout\,
	devoe => ww_devoe,
	o => \key[4]~output_o\);

-- Location: IOOBUF_X18_Y5_N9
\key[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector7~0_combout\,
	devoe => ww_devoe,
	o => \key[5]~output_o\);

-- Location: IOOBUF_X18_Y9_N23
\key[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector6~0_combout\,
	devoe => ww_devoe,
	o => \key[6]~output_o\);

-- Location: IOOBUF_X18_Y1_N16
\key[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector5~0_combout\,
	devoe => ww_devoe,
	o => \key[7]~output_o\);

-- Location: IOOBUF_X18_Y8_N9
\dig2[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[0]~0_combout\,
	devoe => ww_devoe,
	o => \dig2[0]~output_o\);

-- Location: IOOBUF_X18_Y3_N16
\dig2[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[1]~1_combout\,
	devoe => ww_devoe,
	o => \dig2[1]~output_o\);

-- Location: IOOBUF_X16_Y0_N2
\dig2[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[2]~2_combout\,
	devoe => ww_devoe,
	o => \dig2[2]~output_o\);

-- Location: IOOBUF_X18_Y4_N23
\dig2[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[3]~3_combout\,
	devoe => ww_devoe,
	o => \dig2[3]~output_o\);

-- Location: IOOBUF_X18_Y6_N2
\dig2[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector8~0_combout\,
	devoe => ww_devoe,
	o => \dig2[4]~output_o\);

-- Location: IOOBUF_X18_Y5_N23
\dig2[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector7~0_combout\,
	devoe => ww_devoe,
	o => \dig2[5]~output_o\);

-- Location: IOOBUF_X18_Y9_N9
\dig2[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector6~0_combout\,
	devoe => ww_devoe,
	o => \dig2[6]~output_o\);

-- Location: IOOBUF_X18_Y2_N9
\dig2[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector5~0_combout\,
	devoe => ww_devoe,
	o => \dig2[7]~output_o\);

-- Location: IOOBUF_X18_Y8_N23
\dig3[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[0]~0_combout\,
	devoe => ww_devoe,
	o => \dig3[0]~output_o\);

-- Location: IOOBUF_X18_Y3_N23
\dig3[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[1]~1_combout\,
	devoe => ww_devoe,
	o => \dig3[1]~output_o\);

-- Location: IOOBUF_X16_Y0_N16
\dig3[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[2]~2_combout\,
	devoe => ww_devoe,
	o => \dig3[2]~output_o\);

-- Location: IOOBUF_X18_Y4_N2
\dig3[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|dig2[3]~3_combout\,
	devoe => ww_devoe,
	o => \dig3[3]~output_o\);

-- Location: IOOBUF_X18_Y6_N16
\dig3[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector8~0_combout\,
	devoe => ww_devoe,
	o => \dig3[4]~output_o\);

-- Location: IOOBUF_X18_Y5_N2
\dig3[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector7~0_combout\,
	devoe => ww_devoe,
	o => \dig3[5]~output_o\);

-- Location: IOOBUF_X18_Y8_N2
\dig3[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector6~0_combout\,
	devoe => ww_devoe,
	o => \dig3[6]~output_o\);

-- Location: IOOBUF_X18_Y2_N2
\dig3[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_Constantkey|Selector5~0_combout\,
	devoe => ww_devoe,
	o => \dig3[7]~output_o\);

-- Location: IOIBUF_X0_Y4_N15
\clk~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G3
\clk~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X18_Y6_N8
\kbclock~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_kbclock,
	o => \kbclock~input_o\);

-- Location: LCCOMB_X17_Y6_N18
\L_ClockDomainCrossing|c2|Q~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_ClockDomainCrossing|c2|Q~feeder_combout\ = \kbclock~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbclock~input_o\,
	combout => \L_ClockDomainCrossing|c2|Q~feeder_combout\);

-- Location: IOIBUF_X0_Y4_N22
\reset~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: CLKCTRL_G1
\reset~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \reset~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \reset~inputclkctrl_outclk\);

-- Location: FF_X17_Y6_N19
\L_ClockDomainCrossing|c2|Q\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_ClockDomainCrossing|c2|Q~feeder_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_ClockDomainCrossing|c2|Q~q\);

-- Location: LCCOMB_X17_Y6_N16
\L_ClockDomainCrossing|c3|Q~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_ClockDomainCrossing|c3|Q~feeder_combout\ = \L_ClockDomainCrossing|c2|Q~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_ClockDomainCrossing|c2|Q~q\,
	combout => \L_ClockDomainCrossing|c3|Q~feeder_combout\);

-- Location: FF_X17_Y6_N17
\L_ClockDomainCrossing|c3|Q\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_ClockDomainCrossing|c3|Q~feeder_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_ClockDomainCrossing|c3|Q~q\);

-- Location: CLKCTRL_G9
\L_ClockDomainCrossing|c3|Q~clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \L_ClockDomainCrossing|c3|Q~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \L_ClockDomainCrossing|c3|Q~clkctrl_outclk\);

-- Location: LCCOMB_X16_Y5_N14
\L_Showkey|COUNT:counter[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[0]~1_combout\ = \L_Showkey|COUNT:counter[0]~q\ $ (VCC)
-- \L_Showkey|COUNT:counter[0]~2\ = CARRY(\L_Showkey|COUNT:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|COUNT:counter[0]~q\,
	datad => VCC,
	combout => \L_Showkey|COUNT:counter[0]~1_combout\,
	cout => \L_Showkey|COUNT:counter[0]~2\);

-- Location: LCCOMB_X16_Y5_N16
\L_Showkey|COUNT:counter[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[1]~1_combout\ = (\L_Showkey|COUNT:counter[1]~q\ & (!\L_Showkey|COUNT:counter[0]~2\)) # (!\L_Showkey|COUNT:counter[1]~q\ & ((\L_Showkey|COUNT:counter[0]~2\) # (GND)))
-- \L_Showkey|COUNT:counter[1]~2\ = CARRY((!\L_Showkey|COUNT:counter[0]~2\) # (!\L_Showkey|COUNT:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|COUNT:counter[1]~q\,
	datad => VCC,
	cin => \L_Showkey|COUNT:counter[0]~2\,
	combout => \L_Showkey|COUNT:counter[1]~1_combout\,
	cout => \L_Showkey|COUNT:counter[1]~2\);

-- Location: LCCOMB_X16_Y5_N4
\L_Showkey|Equal2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Equal2~0_combout\ = (!\L_Showkey|COUNT:counter[0]~q\ & !\L_Showkey|COUNT:counter[2]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_Showkey|COUNT:counter[0]~q\,
	datad => \L_Showkey|COUNT:counter[2]~q\,
	combout => \L_Showkey|Equal2~0_combout\);

-- Location: LCCOMB_X16_Y5_N20
\L_Showkey|COUNT:counter[3]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[3]~1_combout\ = (\L_Showkey|COUNT:counter[3]~q\ & (!\L_Showkey|COUNT:counter[2]~2\)) # (!\L_Showkey|COUNT:counter[3]~q\ & ((\L_Showkey|COUNT:counter[2]~2\) # (GND)))
-- \L_Showkey|COUNT:counter[3]~2\ = CARRY((!\L_Showkey|COUNT:counter[2]~2\) # (!\L_Showkey|COUNT:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|COUNT:counter[3]~q\,
	datad => VCC,
	cin => \L_Showkey|COUNT:counter[2]~2\,
	combout => \L_Showkey|COUNT:counter[3]~1_combout\,
	cout => \L_Showkey|COUNT:counter[3]~2\);

-- Location: LCCOMB_X16_Y5_N22
\L_Showkey|COUNT:counter[4]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[4]~1_combout\ = (\L_Showkey|COUNT:counter[4]~q\ & (\L_Showkey|COUNT:counter[3]~2\ $ (GND))) # (!\L_Showkey|COUNT:counter[4]~q\ & (!\L_Showkey|COUNT:counter[3]~2\ & VCC))
-- \L_Showkey|COUNT:counter[4]~2\ = CARRY((\L_Showkey|COUNT:counter[4]~q\ & !\L_Showkey|COUNT:counter[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|COUNT:counter[4]~q\,
	datad => VCC,
	cin => \L_Showkey|COUNT:counter[3]~2\,
	combout => \L_Showkey|COUNT:counter[4]~1_combout\,
	cout => \L_Showkey|COUNT:counter[4]~2\);

-- Location: FF_X16_Y5_N23
\L_Showkey|COUNT:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:counter[4]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_Showkey|counter~0_combout\,
	ena => \L_Showkey|COUNT:counter[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:counter[4]~q\);

-- Location: LCCOMB_X16_Y5_N24
\L_Showkey|COUNT:counter[5]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[5]~1_combout\ = (\L_Showkey|COUNT:counter[5]~q\ & (!\L_Showkey|COUNT:counter[4]~2\)) # (!\L_Showkey|COUNT:counter[5]~q\ & ((\L_Showkey|COUNT:counter[4]~2\) # (GND)))
-- \L_Showkey|COUNT:counter[5]~2\ = CARRY((!\L_Showkey|COUNT:counter[4]~2\) # (!\L_Showkey|COUNT:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[5]~q\,
	datad => VCC,
	cin => \L_Showkey|COUNT:counter[4]~2\,
	combout => \L_Showkey|COUNT:counter[5]~1_combout\,
	cout => \L_Showkey|COUNT:counter[5]~2\);

-- Location: FF_X16_Y5_N25
\L_Showkey|COUNT:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:counter[5]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_Showkey|counter~0_combout\,
	ena => \L_Showkey|COUNT:counter[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:counter[5]~q\);

-- Location: LCCOMB_X16_Y5_N26
\L_Showkey|COUNT:counter[6]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[6]~1_combout\ = (\L_Showkey|COUNT:counter[6]~q\ & (\L_Showkey|COUNT:counter[5]~2\ $ (GND))) # (!\L_Showkey|COUNT:counter[6]~q\ & (!\L_Showkey|COUNT:counter[5]~2\ & VCC))
-- \L_Showkey|COUNT:counter[6]~2\ = CARRY((\L_Showkey|COUNT:counter[6]~q\ & !\L_Showkey|COUNT:counter[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|COUNT:counter[6]~q\,
	datad => VCC,
	cin => \L_Showkey|COUNT:counter[5]~2\,
	combout => \L_Showkey|COUNT:counter[6]~1_combout\,
	cout => \L_Showkey|COUNT:counter[6]~2\);

-- Location: FF_X16_Y5_N27
\L_Showkey|COUNT:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:counter[6]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_Showkey|counter~0_combout\,
	ena => \L_Showkey|COUNT:counter[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:counter[6]~q\);

-- Location: LCCOMB_X16_Y5_N28
\L_Showkey|COUNT:counter[7]~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[7]~2_combout\ = \L_Showkey|COUNT:counter[6]~2\ $ (\L_Showkey|COUNT:counter[7]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \L_Showkey|COUNT:counter[7]~q\,
	cin => \L_Showkey|COUNT:counter[6]~2\,
	combout => \L_Showkey|COUNT:counter[7]~2_combout\);

-- Location: FF_X16_Y5_N29
\L_Showkey|COUNT:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:counter[7]~2_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_Showkey|counter~0_combout\,
	ena => \L_Showkey|COUNT:counter[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:counter[7]~q\);

-- Location: LCCOMB_X16_Y5_N2
\L_Showkey|Equal0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Equal0~0_combout\ = (!\L_Showkey|COUNT:counter[4]~q\ & (!\L_Showkey|COUNT:counter[5]~q\ & (!\L_Showkey|COUNT:counter[6]~q\ & !\L_Showkey|COUNT:counter[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[4]~q\,
	datab => \L_Showkey|COUNT:counter[5]~q\,
	datac => \L_Showkey|COUNT:counter[6]~q\,
	datad => \L_Showkey|COUNT:counter[7]~q\,
	combout => \L_Showkey|Equal0~0_combout\);

-- Location: LCCOMB_X16_Y5_N8
\L_Showkey|scancode[0]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|scancode[0]~0_combout\ = (\L_Showkey|COUNT:counter[3]~q\ & (\L_Showkey|COUNT:counter[1]~q\ & (\L_Showkey|Equal2~0_combout\ & \L_Showkey|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[3]~q\,
	datab => \L_Showkey|COUNT:counter[1]~q\,
	datac => \L_Showkey|Equal2~0_combout\,
	datad => \L_Showkey|Equal0~0_combout\,
	combout => \L_Showkey|scancode[0]~0_combout\);

-- Location: LCCOMB_X16_Y5_N0
\L_Showkey|COUNT:counter[7]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[7]~1_combout\ = (\L_Showkey|scancode[0]~0_combout\) # (!\L_Showkey|counter~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_Showkey|scancode[0]~0_combout\,
	datad => \L_Showkey|counter~0_combout\,
	combout => \L_Showkey|COUNT:counter[7]~1_combout\);

-- Location: FF_X16_Y5_N17
\L_Showkey|COUNT:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:counter[1]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_Showkey|counter~0_combout\,
	ena => \L_Showkey|COUNT:counter[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:counter[1]~q\);

-- Location: LCCOMB_X16_Y5_N18
\L_Showkey|COUNT:counter[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:counter[2]~1_combout\ = (\L_Showkey|COUNT:counter[2]~q\ & (\L_Showkey|COUNT:counter[1]~2\ $ (GND))) # (!\L_Showkey|COUNT:counter[2]~q\ & (!\L_Showkey|COUNT:counter[1]~2\ & VCC))
-- \L_Showkey|COUNT:counter[2]~2\ = CARRY((\L_Showkey|COUNT:counter[2]~q\ & !\L_Showkey|COUNT:counter[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[2]~q\,
	datad => VCC,
	cin => \L_Showkey|COUNT:counter[1]~2\,
	combout => \L_Showkey|COUNT:counter[2]~1_combout\,
	cout => \L_Showkey|COUNT:counter[2]~2\);

-- Location: FF_X16_Y5_N19
\L_Showkey|COUNT:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:counter[2]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_Showkey|counter~0_combout\,
	ena => \L_Showkey|COUNT:counter[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:counter[2]~q\);

-- Location: FF_X16_Y5_N21
\L_Showkey|COUNT:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:counter[3]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_Showkey|counter~0_combout\,
	ena => \L_Showkey|COUNT:counter[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:counter[3]~q\);

-- Location: LCCOMB_X16_Y5_N10
\L_Showkey|counter~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|counter~0_combout\ = ((\L_Showkey|COUNT:counter[3]~q\ & ((\L_Showkey|COUNT:counter[1]~q\) # (\L_Showkey|COUNT:counter[2]~q\)))) # (!\L_Showkey|Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[3]~q\,
	datab => \L_Showkey|COUNT:counter[1]~q\,
	datac => \L_Showkey|COUNT:counter[2]~q\,
	datad => \L_Showkey|Equal0~0_combout\,
	combout => \L_Showkey|counter~0_combout\);

-- Location: FF_X16_Y5_N15
\L_Showkey|COUNT:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:counter[0]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_Showkey|counter~0_combout\,
	ena => \L_Showkey|COUNT:counter[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:counter[0]~q\);

-- Location: LCCOMB_X16_Y5_N6
\L_Showkey|Decoder0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~0_combout\ = (\L_Showkey|Equal0~0_combout\ & (\L_Showkey|COUNT:counter[3]~q\ $ (((\L_Showkey|COUNT:counter[1]~q\) # (!\L_Showkey|Equal2~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[3]~q\,
	datab => \L_Showkey|COUNT:counter[1]~q\,
	datac => \L_Showkey|Equal2~0_combout\,
	datad => \L_Showkey|Equal0~0_combout\,
	combout => \L_Showkey|Decoder0~0_combout\);

-- Location: LCCOMB_X16_Y5_N12
\L_Showkey|Decoder0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~1_combout\ = (\L_Showkey|COUNT:counter[0]~q\ & (!\L_Showkey|COUNT:counter[1]~q\ & (!\L_Showkey|COUNT:counter[2]~q\ & \L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[0]~q\,
	datab => \L_Showkey|COUNT:counter[1]~q\,
	datac => \L_Showkey|COUNT:counter[2]~q\,
	datad => \L_Showkey|Decoder0~0_combout\,
	combout => \L_Showkey|Decoder0~1_combout\);

-- Location: IOIBUF_X18_Y5_N15
\kbdata~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_kbdata,
	o => \kbdata~input_o\);

-- Location: LCCOMB_X15_Y5_N2
\L_ClockDomainCrossing|c0|Q~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_ClockDomainCrossing|c0|Q~feeder_combout\ = \kbdata~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbdata~input_o\,
	combout => \L_ClockDomainCrossing|c0|Q~feeder_combout\);

-- Location: FF_X15_Y5_N3
\L_ClockDomainCrossing|c0|Q\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_ClockDomainCrossing|c0|Q~feeder_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_ClockDomainCrossing|c0|Q~q\);

-- Location: LCCOMB_X15_Y5_N28
\L_ClockDomainCrossing|c1|Q~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_ClockDomainCrossing|c1|Q~feeder_combout\ = \L_ClockDomainCrossing|c0|Q~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_ClockDomainCrossing|c0|Q~q\,
	combout => \L_ClockDomainCrossing|c1|Q~feeder_combout\);

-- Location: FF_X15_Y5_N29
\L_ClockDomainCrossing|c1|Q\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_ClockDomainCrossing|c1|Q~feeder_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_ClockDomainCrossing|c1|Q~q\);

-- Location: LCCOMB_X15_Y5_N12
\L_Showkey|COUNT:byte_current[0]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:byte_current[0]~0_combout\ = (\L_Showkey|Decoder0~1_combout\ & ((\L_ClockDomainCrossing|c1|Q~q\))) # (!\L_Showkey|Decoder0~1_combout\ & (\L_Showkey|COUNT:byte_current[0]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|Decoder0~1_combout\,
	datac => \L_Showkey|COUNT:byte_current[0]~q\,
	datad => \L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_Showkey|COUNT:byte_current[0]~0_combout\);

-- Location: FF_X15_Y5_N13
\L_Showkey|COUNT:byte_current[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:byte_current[0]~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:byte_current[0]~q\);

-- Location: FF_X17_Y5_N9
\L_Showkey|scancode[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_Showkey|COUNT:byte_current[0]~q\,
	clrn => \reset~inputclkctrl_outclk\,
	sload => VCC,
	ena => \L_Showkey|scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|scancode\(0));

-- Location: LCCOMB_X14_Y5_N10
\L_Showkey|byte_read~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|byte_read~0_combout\ = (\L_Showkey|COUNT:counter[3]~q\ & ((\L_Showkey|COUNT:counter[0]~q\) # ((\L_Showkey|COUNT:counter[1]~q\) # (\L_Showkey|COUNT:counter[2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[3]~q\,
	datab => \L_Showkey|COUNT:counter[0]~q\,
	datac => \L_Showkey|COUNT:counter[1]~q\,
	datad => \L_Showkey|COUNT:counter[2]~q\,
	combout => \L_Showkey|byte_read~0_combout\);

-- Location: LCCOMB_X14_Y5_N4
\L_Showkey|byte_read~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|byte_read~1_combout\ = (\L_Showkey|byte_read~0_combout\ & (((\L_Showkey|byte_read~q\) # (\L_Showkey|scancode[0]~0_combout\)))) # (!\L_Showkey|byte_read~0_combout\ & (!\L_Showkey|Equal0~0_combout\ & ((\L_Showkey|byte_read~q\) # 
-- (\L_Showkey|scancode[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|byte_read~0_combout\,
	datab => \L_Showkey|Equal0~0_combout\,
	datac => \L_Showkey|byte_read~q\,
	datad => \L_Showkey|scancode[0]~0_combout\,
	combout => \L_Showkey|byte_read~1_combout\);

-- Location: FF_X14_Y5_N5
\L_Showkey|byte_read\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|byte_read~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|byte_read~q\);

-- Location: LCCOMB_X15_Y5_N6
\L_Showkey|Decoder0~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~4_combout\ = (!\L_Showkey|COUNT:counter[1]~q\ & (\L_Showkey|COUNT:counter[2]~q\ & (!\L_Showkey|COUNT:counter[0]~q\ & \L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[1]~q\,
	datab => \L_Showkey|COUNT:counter[2]~q\,
	datac => \L_Showkey|COUNT:counter[0]~q\,
	datad => \L_Showkey|Decoder0~0_combout\,
	combout => \L_Showkey|Decoder0~4_combout\);

-- Location: LCCOMB_X15_Y5_N30
\L_Showkey|COUNT:byte_current[3]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:byte_current[3]~0_combout\ = (\L_Showkey|Decoder0~4_combout\ & ((\L_ClockDomainCrossing|c1|Q~q\))) # (!\L_Showkey|Decoder0~4_combout\ & (\L_Showkey|COUNT:byte_current[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|Decoder0~4_combout\,
	datac => \L_Showkey|COUNT:byte_current[3]~q\,
	datad => \L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_Showkey|COUNT:byte_current[3]~0_combout\);

-- Location: FF_X15_Y5_N31
\L_Showkey|COUNT:byte_current[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:byte_current[3]~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:byte_current[3]~q\);

-- Location: FF_X17_Y5_N31
\L_Showkey|scancode[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_Showkey|COUNT:byte_current[3]~q\,
	clrn => \reset~inputclkctrl_outclk\,
	sload => VCC,
	ena => \L_Showkey|scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|scancode\(3));

-- Location: LCCOMB_X15_Y5_N24
\L_Showkey|Decoder0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~3_combout\ = (\L_Showkey|COUNT:counter[1]~q\ & (!\L_Showkey|COUNT:counter[2]~q\ & (\L_Showkey|COUNT:counter[0]~q\ & \L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[1]~q\,
	datab => \L_Showkey|COUNT:counter[2]~q\,
	datac => \L_Showkey|COUNT:counter[0]~q\,
	datad => \L_Showkey|Decoder0~0_combout\,
	combout => \L_Showkey|Decoder0~3_combout\);

-- Location: LCCOMB_X15_Y5_N8
\L_Showkey|COUNT:byte_current[2]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:byte_current[2]~0_combout\ = (\L_Showkey|Decoder0~3_combout\ & ((\L_ClockDomainCrossing|c1|Q~q\))) # (!\L_Showkey|Decoder0~3_combout\ & (\L_Showkey|COUNT:byte_current[2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|Decoder0~3_combout\,
	datac => \L_Showkey|COUNT:byte_current[2]~q\,
	datad => \L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_Showkey|COUNT:byte_current[2]~0_combout\);

-- Location: FF_X15_Y5_N9
\L_Showkey|COUNT:byte_current[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:byte_current[2]~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:byte_current[2]~q\);

-- Location: FF_X17_Y5_N29
\L_Showkey|scancode[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_Showkey|COUNT:byte_current[2]~q\,
	clrn => \reset~inputclkctrl_outclk\,
	sload => VCC,
	ena => \L_Showkey|scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|scancode\(2));

-- Location: LCCOMB_X15_Y5_N10
\L_Showkey|Decoder0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~2_combout\ = (\L_Showkey|COUNT:counter[1]~q\ & (!\L_Showkey|COUNT:counter[2]~q\ & (!\L_Showkey|COUNT:counter[0]~q\ & \L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[1]~q\,
	datab => \L_Showkey|COUNT:counter[2]~q\,
	datac => \L_Showkey|COUNT:counter[0]~q\,
	datad => \L_Showkey|Decoder0~0_combout\,
	combout => \L_Showkey|Decoder0~2_combout\);

-- Location: LCCOMB_X15_Y5_N22
\L_Showkey|COUNT:byte_current[1]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:byte_current[1]~0_combout\ = (\L_Showkey|Decoder0~2_combout\ & ((\L_ClockDomainCrossing|c1|Q~q\))) # (!\L_Showkey|Decoder0~2_combout\ & (\L_Showkey|COUNT:byte_current[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|Decoder0~2_combout\,
	datac => \L_Showkey|COUNT:byte_current[1]~q\,
	datad => \L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_Showkey|COUNT:byte_current[1]~0_combout\);

-- Location: FF_X15_Y5_N23
\L_Showkey|COUNT:byte_current[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:byte_current[1]~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:byte_current[1]~q\);

-- Location: FF_X17_Y5_N5
\L_Showkey|scancode[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_Showkey|COUNT:byte_current[1]~q\,
	clrn => \reset~inputclkctrl_outclk\,
	sload => VCC,
	ena => \L_Showkey|scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|scancode\(1));

-- Location: LCCOMB_X15_Y5_N4
\L_Showkey|Decoder0~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~5_combout\ = (!\L_Showkey|COUNT:counter[1]~q\ & (\L_Showkey|COUNT:counter[2]~q\ & (\L_Showkey|COUNT:counter[0]~q\ & \L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[1]~q\,
	datab => \L_Showkey|COUNT:counter[2]~q\,
	datac => \L_Showkey|COUNT:counter[0]~q\,
	datad => \L_Showkey|Decoder0~0_combout\,
	combout => \L_Showkey|Decoder0~5_combout\);

-- Location: LCCOMB_X15_Y5_N20
\L_Showkey|COUNT:byte_current[4]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:byte_current[4]~0_combout\ = (\L_Showkey|Decoder0~5_combout\ & ((\L_ClockDomainCrossing|c1|Q~q\))) # (!\L_Showkey|Decoder0~5_combout\ & (\L_Showkey|COUNT:byte_current[4]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|Decoder0~5_combout\,
	datac => \L_Showkey|COUNT:byte_current[4]~q\,
	datad => \L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_Showkey|COUNT:byte_current[4]~0_combout\);

-- Location: FF_X15_Y5_N21
\L_Showkey|COUNT:byte_current[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:byte_current[4]~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:byte_current[4]~q\);

-- Location: FF_X17_Y5_N1
\L_Showkey|scancode[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_Showkey|COUNT:byte_current[4]~q\,
	clrn => \reset~inputclkctrl_outclk\,
	sload => VCC,
	ena => \L_Showkey|scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|scancode\(4));

-- Location: LCCOMB_X17_Y5_N4
\L_Constantkey|Equal0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Equal0~1_combout\ = (!\L_Showkey|scancode\(3) & (!\L_Showkey|scancode\(2) & (!\L_Showkey|scancode\(1) & \L_Showkey|scancode\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|scancode\(3),
	datab => \L_Showkey|scancode\(2),
	datac => \L_Showkey|scancode\(1),
	datad => \L_Showkey|scancode\(4),
	combout => \L_Constantkey|Equal0~1_combout\);

-- Location: LCCOMB_X15_Y5_N18
\L_Showkey|Decoder0~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~6_combout\ = (\L_Showkey|COUNT:counter[1]~q\ & (\L_Showkey|COUNT:counter[2]~q\ & (!\L_Showkey|COUNT:counter[0]~q\ & \L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[1]~q\,
	datab => \L_Showkey|COUNT:counter[2]~q\,
	datac => \L_Showkey|COUNT:counter[0]~q\,
	datad => \L_Showkey|Decoder0~0_combout\,
	combout => \L_Showkey|Decoder0~6_combout\);

-- Location: LCCOMB_X15_Y5_N14
\L_Showkey|COUNT:byte_current[5]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:byte_current[5]~0_combout\ = (\L_Showkey|Decoder0~6_combout\ & ((\L_ClockDomainCrossing|c1|Q~q\))) # (!\L_Showkey|Decoder0~6_combout\ & (\L_Showkey|COUNT:byte_current[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|Decoder0~6_combout\,
	datac => \L_Showkey|COUNT:byte_current[5]~q\,
	datad => \L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_Showkey|COUNT:byte_current[5]~0_combout\);

-- Location: FF_X15_Y5_N15
\L_Showkey|COUNT:byte_current[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:byte_current[5]~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:byte_current[5]~q\);

-- Location: FF_X17_Y5_N23
\L_Showkey|scancode[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_Showkey|COUNT:byte_current[5]~q\,
	clrn => \reset~inputclkctrl_outclk\,
	sload => VCC,
	ena => \L_Showkey|scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|scancode\(5));

-- Location: LCCOMB_X15_Y5_N16
\L_Showkey|Decoder0~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~7_combout\ = (\L_Showkey|COUNT:counter[1]~q\ & (\L_Showkey|COUNT:counter[2]~q\ & (\L_Showkey|COUNT:counter[0]~q\ & \L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[1]~q\,
	datab => \L_Showkey|COUNT:counter[2]~q\,
	datac => \L_Showkey|COUNT:counter[0]~q\,
	datad => \L_Showkey|Decoder0~0_combout\,
	combout => \L_Showkey|Decoder0~7_combout\);

-- Location: LCCOMB_X15_Y5_N0
\L_Showkey|COUNT:byte_current[6]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:byte_current[6]~0_combout\ = (\L_Showkey|Decoder0~7_combout\ & ((\L_ClockDomainCrossing|c1|Q~q\))) # (!\L_Showkey|Decoder0~7_combout\ & (\L_Showkey|COUNT:byte_current[6]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_Showkey|Decoder0~7_combout\,
	datac => \L_Showkey|COUNT:byte_current[6]~q\,
	datad => \L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_Showkey|COUNT:byte_current[6]~0_combout\);

-- Location: FF_X15_Y5_N1
\L_Showkey|COUNT:byte_current[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:byte_current[6]~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:byte_current[6]~q\);

-- Location: FF_X17_Y5_N21
\L_Showkey|scancode[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_Showkey|COUNT:byte_current[6]~q\,
	clrn => \reset~inputclkctrl_outclk\,
	sload => VCC,
	ena => \L_Showkey|scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|scancode\(6));

-- Location: LCCOMB_X16_Y5_N30
\L_Showkey|Decoder0~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|Decoder0~8_combout\ = (!\L_Showkey|COUNT:counter[0]~q\ & (!\L_Showkey|COUNT:counter[1]~q\ & (!\L_Showkey|COUNT:counter[2]~q\ & \L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|COUNT:counter[0]~q\,
	datab => \L_Showkey|COUNT:counter[1]~q\,
	datac => \L_Showkey|COUNT:counter[2]~q\,
	datad => \L_Showkey|Decoder0~0_combout\,
	combout => \L_Showkey|Decoder0~8_combout\);

-- Location: LCCOMB_X15_Y5_N26
\L_Showkey|COUNT:byte_current[7]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Showkey|COUNT:byte_current[7]~0_combout\ = (\L_Showkey|Decoder0~8_combout\ & ((\L_ClockDomainCrossing|c1|Q~q\))) # (!\L_Showkey|Decoder0~8_combout\ & (\L_Showkey|COUNT:byte_current[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|Decoder0~8_combout\,
	datac => \L_Showkey|COUNT:byte_current[7]~q\,
	datad => \L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_Showkey|COUNT:byte_current[7]~0_combout\);

-- Location: FF_X15_Y5_N27
\L_Showkey|COUNT:byte_current[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_Showkey|COUNT:byte_current[7]~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|COUNT:byte_current[7]~q\);

-- Location: FF_X17_Y5_N3
\L_Showkey|scancode[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_Showkey|COUNT:byte_current[7]~q\,
	clrn => \reset~inputclkctrl_outclk\,
	sload => VCC,
	ena => \L_Showkey|scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Showkey|scancode\(7));

-- Location: LCCOMB_X17_Y5_N16
\L_Constantkey|Equal0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Equal0~0_combout\ = (\L_Showkey|scancode\(5) & (\L_Showkey|scancode\(6) & (!\L_Showkey|scancode\(0) & \L_Showkey|scancode\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|scancode\(5),
	datab => \L_Showkey|scancode\(6),
	datac => \L_Showkey|scancode\(0),
	datad => \L_Showkey|scancode\(7),
	combout => \L_Constantkey|Equal0~0_combout\);

-- Location: LCCOMB_X17_Y5_N10
\L_Constantkey|Equal0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Equal0~2_combout\ = (\L_Constantkey|Equal0~1_combout\ & \L_Constantkey|Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_Constantkey|Equal0~1_combout\,
	datad => \L_Constantkey|Equal0~0_combout\,
	combout => \L_Constantkey|Equal0~2_combout\);

-- Location: LCCOMB_X17_Y5_N24
\L_Constantkey|current_state.state_key_released~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|current_state.state_key_released~0_combout\ = (\L_Showkey|byte_read~q\ & ((\L_Constantkey|Equal0~2_combout\ & (\L_Constantkey|current_state.state_key_pressed~q\)) # (!\L_Constantkey|Equal0~2_combout\ & 
-- ((\L_Constantkey|current_state.state_key_released~q\))))) # (!\L_Showkey|byte_read~q\ & (((\L_Constantkey|current_state.state_key_released~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_Showkey|byte_read~q\,
	datac => \L_Constantkey|current_state.state_key_released~q\,
	datad => \L_Constantkey|Equal0~2_combout\,
	combout => \L_Constantkey|current_state.state_key_released~0_combout\);

-- Location: FF_X17_Y5_N25
\L_Constantkey|current_state.state_key_released\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_Constantkey|current_state.state_key_released~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Constantkey|current_state.state_key_released~q\);

-- Location: LCCOMB_X17_Y5_N26
\L_Constantkey|Selector3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Selector3~0_combout\ = (\L_Constantkey|Equal0~2_combout\ & (\L_Showkey|byte_read~q\ & ((\L_Constantkey|current_state.state_key_reminder~q\) # (\L_Constantkey|current_state.state_key_released~q\)))) # (!\L_Constantkey|Equal0~2_combout\ & 
-- (((\L_Constantkey|current_state.state_key_reminder~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Constantkey|Equal0~2_combout\,
	datab => \L_Showkey|byte_read~q\,
	datac => \L_Constantkey|current_state.state_key_reminder~q\,
	datad => \L_Constantkey|current_state.state_key_released~q\,
	combout => \L_Constantkey|Selector3~0_combout\);

-- Location: FF_X17_Y5_N27
\L_Constantkey|current_state.state_key_reminder\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_Constantkey|Selector3~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Constantkey|current_state.state_key_reminder~q\);

-- Location: LCCOMB_X17_Y5_N18
\L_Constantkey|current_state.state_key_reminder_2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|current_state.state_key_reminder_2~0_combout\ = (\L_Showkey|byte_read~q\ & (((\L_Constantkey|current_state.state_key_reminder_2~q\)))) # (!\L_Showkey|byte_read~q\ & (\L_Constantkey|current_state.state_key_reminder~q\ & 
-- ((\L_Constantkey|Equal0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Constantkey|current_state.state_key_reminder~q\,
	datab => \L_Showkey|byte_read~q\,
	datac => \L_Constantkey|current_state.state_key_reminder_2~q\,
	datad => \L_Constantkey|Equal0~2_combout\,
	combout => \L_Constantkey|current_state.state_key_reminder_2~0_combout\);

-- Location: FF_X17_Y5_N19
\L_Constantkey|current_state.state_key_reminder_2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_Constantkey|current_state.state_key_reminder_2~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Constantkey|current_state.state_key_reminder_2~q\);

-- Location: LCCOMB_X17_Y5_N12
\L_Constantkey|Selector0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Selector0~0_combout\ = (\L_Showkey|byte_read~q\) # ((\L_Constantkey|current_state.state_no_key~q\ & !\L_Constantkey|current_state.state_key_reminder_2~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|byte_read~q\,
	datac => \L_Constantkey|current_state.state_no_key~q\,
	datad => \L_Constantkey|current_state.state_key_reminder_2~q\,
	combout => \L_Constantkey|Selector0~0_combout\);

-- Location: FF_X17_Y5_N13
\L_Constantkey|current_state.state_no_key\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_Constantkey|Selector0~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Constantkey|current_state.state_no_key~q\);

-- Location: LCCOMB_X17_Y5_N6
\L_Constantkey|Selector1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Selector1~0_combout\ = (\L_Showkey|byte_read~q\ & (((\L_Constantkey|current_state.state_key_pressed~q\ & !\L_Constantkey|Equal0~2_combout\)) # (!\L_Constantkey|current_state.state_no_key~q\))) # (!\L_Showkey|byte_read~q\ & 
-- (((\L_Constantkey|current_state.state_key_pressed~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Constantkey|current_state.state_no_key~q\,
	datab => \L_Showkey|byte_read~q\,
	datac => \L_Constantkey|current_state.state_key_pressed~q\,
	datad => \L_Constantkey|Equal0~2_combout\,
	combout => \L_Constantkey|Selector1~0_combout\);

-- Location: FF_X17_Y5_N7
\L_Constantkey|current_state.state_key_pressed\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \L_Constantkey|Selector1~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_Constantkey|current_state.state_key_pressed~q\);

-- Location: LCCOMB_X17_Y5_N8
\L_Constantkey|dig2[0]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|dig2[0]~0_combout\ = (\L_Showkey|scancode\(0) & \L_Constantkey|current_state.state_key_pressed~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_Showkey|scancode\(0),
	datad => \L_Constantkey|current_state.state_key_pressed~q\,
	combout => \L_Constantkey|dig2[0]~0_combout\);

-- Location: LCCOMB_X17_Y5_N2
\L_Constantkey|dig2[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|dig2[1]~1_combout\ = (\L_Constantkey|current_state.state_key_pressed~q\ & \L_Showkey|scancode\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_Constantkey|current_state.state_key_pressed~q\,
	datad => \L_Showkey|scancode\(1),
	combout => \L_Constantkey|dig2[1]~1_combout\);

-- Location: LCCOMB_X17_Y5_N28
\L_Constantkey|dig2[2]~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|dig2[2]~2_combout\ = (\L_Showkey|scancode\(2) & \L_Constantkey|current_state.state_key_pressed~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_Showkey|scancode\(2),
	datad => \L_Constantkey|current_state.state_key_pressed~q\,
	combout => \L_Constantkey|dig2[2]~2_combout\);

-- Location: LCCOMB_X17_Y5_N30
\L_Constantkey|dig2[3]~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|dig2[3]~3_combout\ = (\L_Showkey|scancode\(3) & \L_Constantkey|current_state.state_key_pressed~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_Showkey|scancode\(3),
	datad => \L_Constantkey|current_state.state_key_pressed~q\,
	combout => \L_Constantkey|dig2[3]~3_combout\);

-- Location: LCCOMB_X17_Y5_N0
\L_Constantkey|Selector8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Selector8~0_combout\ = (\L_Constantkey|current_state.state_key_released~q\) # ((\L_Constantkey|current_state.state_key_reminder~q\) # ((\L_Constantkey|current_state.state_key_pressed~q\ & \L_Showkey|scancode\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Constantkey|current_state.state_key_released~q\,
	datab => \L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_Showkey|scancode\(4),
	datad => \L_Constantkey|current_state.state_key_reminder~q\,
	combout => \L_Constantkey|Selector8~0_combout\);

-- Location: LCCOMB_X17_Y5_N22
\L_Constantkey|Selector7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Selector7~0_combout\ = (\L_Constantkey|current_state.state_key_released~q\) # ((\L_Constantkey|current_state.state_key_reminder~q\) # ((\L_Constantkey|current_state.state_key_pressed~q\ & \L_Showkey|scancode\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Constantkey|current_state.state_key_released~q\,
	datab => \L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_Showkey|scancode\(5),
	datad => \L_Constantkey|current_state.state_key_reminder~q\,
	combout => \L_Constantkey|Selector7~0_combout\);

-- Location: LCCOMB_X17_Y5_N20
\L_Constantkey|Selector6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Selector6~0_combout\ = (\L_Constantkey|current_state.state_key_released~q\) # ((\L_Constantkey|current_state.state_key_reminder~q\) # ((\L_Constantkey|current_state.state_key_pressed~q\ & \L_Showkey|scancode\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Constantkey|current_state.state_key_released~q\,
	datab => \L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_Showkey|scancode\(6),
	datad => \L_Constantkey|current_state.state_key_reminder~q\,
	combout => \L_Constantkey|Selector6~0_combout\);

-- Location: LCCOMB_X17_Y5_N14
\L_Constantkey|Selector5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_Constantkey|Selector5~0_combout\ = (\L_Constantkey|current_state.state_key_released~q\) # ((\L_Constantkey|current_state.state_key_reminder~q\) # ((\L_Showkey|scancode\(7) & \L_Constantkey|current_state.state_key_pressed~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_Showkey|scancode\(7),
	datab => \L_Constantkey|current_state.state_key_released~q\,
	datac => \L_Constantkey|current_state.state_key_pressed~q\,
	datad => \L_Constantkey|current_state.state_key_reminder~q\,
	combout => \L_Constantkey|Selector5~0_combout\);

-- Location: UNVM_X0_Y8_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

ww_key(0) <= \key[0]~output_o\;

ww_key(1) <= \key[1]~output_o\;

ww_key(2) <= \key[2]~output_o\;

ww_key(3) <= \key[3]~output_o\;

ww_key(4) <= \key[4]~output_o\;

ww_key(5) <= \key[5]~output_o\;

ww_key(6) <= \key[6]~output_o\;

ww_key(7) <= \key[7]~output_o\;

ww_dig2(0) <= \dig2[0]~output_o\;

ww_dig2(1) <= \dig2[1]~output_o\;

ww_dig2(2) <= \dig2[2]~output_o\;

ww_dig2(3) <= \dig2[3]~output_o\;

ww_dig2(4) <= \dig2[4]~output_o\;

ww_dig2(5) <= \dig2[5]~output_o\;

ww_dig2(6) <= \dig2[6]~output_o\;

ww_dig2(7) <= \dig2[7]~output_o\;

ww_dig3(0) <= \dig3[0]~output_o\;

ww_dig3(1) <= \dig3[1]~output_o\;

ww_dig3(2) <= \dig3[2]~output_o\;

ww_dig3(3) <= \dig3[3]~output_o\;

ww_dig3(4) <= \dig3[4]~output_o\;

ww_dig3(5) <= \dig3[5]~output_o\;

ww_dig3(6) <= \dig3[6]~output_o\;

ww_dig3(7) <= \dig3[7]~output_o\;
END structure;


