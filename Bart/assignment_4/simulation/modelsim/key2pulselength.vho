-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "05/26/2020 14:58:07"

-- 
-- Device: Altera 10M02DCU324A6G Package UFBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_J7,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_J8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_H3,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_H4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_H8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
LIBRARY STD;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE STD.STANDARD.ALL;

ENTITY 	key2pulselength IS
    PORT (
	reset : IN std_logic;
	key : IN std_logic_vector(7 DOWNTO 0);
	pulslength : OUT STD.STANDARD.integer range 0 TO 8191
	);
END key2pulselength;

-- Design Ports Information
-- reset	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[0]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[1]	=>  Location: PIN_K7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[2]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[3]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[4]	=>  Location: PIN_K4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[5]	=>  Location: PIN_J4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[6]	=>  Location: PIN_C6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[7]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[8]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[9]	=>  Location: PIN_K12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[10]	=>  Location: PIN_C5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[11]	=>  Location: PIN_H2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[12]	=>  Location: PIN_D4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[1]	=>  Location: PIN_K3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[6]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[5]	=>  Location: PIN_E5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[2]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[7]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[4]	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[0]	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[3]	=>  Location: PIN_G7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF key2pulselength IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_key : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_pulslength : std_logic_vector(12 DOWNTO 0);
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \pulslength[0]~output_o\ : std_logic;
SIGNAL \pulslength[1]~output_o\ : std_logic;
SIGNAL \pulslength[2]~output_o\ : std_logic;
SIGNAL \pulslength[3]~output_o\ : std_logic;
SIGNAL \pulslength[4]~output_o\ : std_logic;
SIGNAL \pulslength[5]~output_o\ : std_logic;
SIGNAL \pulslength[6]~output_o\ : std_logic;
SIGNAL \pulslength[7]~output_o\ : std_logic;
SIGNAL \pulslength[8]~output_o\ : std_logic;
SIGNAL \pulslength[9]~output_o\ : std_logic;
SIGNAL \pulslength[10]~output_o\ : std_logic;
SIGNAL \pulslength[11]~output_o\ : std_logic;
SIGNAL \pulslength[12]~output_o\ : std_logic;
SIGNAL \key[7]~input_o\ : std_logic;
SIGNAL \key[0]~input_o\ : std_logic;
SIGNAL \key[1]~input_o\ : std_logic;
SIGNAL \key[6]~input_o\ : std_logic;
SIGNAL \key[3]~input_o\ : std_logic;
SIGNAL \key[4]~input_o\ : std_logic;
SIGNAL \Mux12~5_combout\ : std_logic;
SIGNAL \Mux12~4_combout\ : std_logic;
SIGNAL \key[5]~input_o\ : std_logic;
SIGNAL \Mux12~6_combout\ : std_logic;
SIGNAL \key[2]~input_o\ : std_logic;
SIGNAL \Mux12~2_combout\ : std_logic;
SIGNAL \Mux2~0_combout\ : std_logic;
SIGNAL \Mux12~3_combout\ : std_logic;
SIGNAL \Mux12~7_combout\ : std_logic;
SIGNAL \Mux11~3_combout\ : std_logic;
SIGNAL \Mux11~4_combout\ : std_logic;
SIGNAL \Mux11~5_combout\ : std_logic;
SIGNAL \Mux11~6_combout\ : std_logic;
SIGNAL \Mux11~7_combout\ : std_logic;
SIGNAL \Mux11~8_combout\ : std_logic;
SIGNAL \Mux11~10_combout\ : std_logic;
SIGNAL \Mux10~0_combout\ : std_logic;
SIGNAL \Mux11~9_combout\ : std_logic;
SIGNAL \Mux10~1_combout\ : std_logic;
SIGNAL \Mux10~2_combout\ : std_logic;
SIGNAL \Mux10~3_combout\ : std_logic;
SIGNAL \Mux10~4_combout\ : std_logic;
SIGNAL \Mux11~2_combout\ : std_logic;
SIGNAL \Mux10~5_combout\ : std_logic;
SIGNAL \Mux9~2_combout\ : std_logic;
SIGNAL \Mux9~3_combout\ : std_logic;
SIGNAL \Mux9~4_combout\ : std_logic;
SIGNAL \Mux9~0_combout\ : std_logic;
SIGNAL \Mux9~1_combout\ : std_logic;
SIGNAL \Mux9~5_combout\ : std_logic;
SIGNAL \Mux9~6_combout\ : std_logic;
SIGNAL \Mux8~1_combout\ : std_logic;
SIGNAL \Mux8~0_combout\ : std_logic;
SIGNAL \Mux8~2_combout\ : std_logic;
SIGNAL \Mux8~3_combout\ : std_logic;
SIGNAL \Mux8~4_combout\ : std_logic;
SIGNAL \Mux7~3_combout\ : std_logic;
SIGNAL \Mux7~4_combout\ : std_logic;
SIGNAL \Mux7~2_combout\ : std_logic;
SIGNAL \Mux7~0_combout\ : std_logic;
SIGNAL \Mux7~1_combout\ : std_logic;
SIGNAL \Mux7~5_combout\ : std_logic;
SIGNAL \Mux6~2_combout\ : std_logic;
SIGNAL \Mux6~1_combout\ : std_logic;
SIGNAL \Mux6~3_combout\ : std_logic;
SIGNAL \Mux6~0_combout\ : std_logic;
SIGNAL \Mux6~4_combout\ : std_logic;
SIGNAL \Mux5~2_combout\ : std_logic;
SIGNAL \Mux5~3_combout\ : std_logic;
SIGNAL \Mux5~4_combout\ : std_logic;
SIGNAL \Mux5~5_combout\ : std_logic;
SIGNAL \Mux4~1_combout\ : std_logic;
SIGNAL \Mux4~0_combout\ : std_logic;
SIGNAL \Mux4~2_combout\ : std_logic;
SIGNAL \Mux4~3_combout\ : std_logic;
SIGNAL \Mux3~0_combout\ : std_logic;
SIGNAL \Mux3~2_combout\ : std_logic;
SIGNAL \Mux3~3_combout\ : std_logic;
SIGNAL \Mux3~1_combout\ : std_logic;
SIGNAL \Mux2~1_combout\ : std_logic;
SIGNAL \Mux2~2_combout\ : std_logic;
SIGNAL \Mux2~3_combout\ : std_logic;
SIGNAL \Mux1~0_combout\ : std_logic;
SIGNAL \Mux1~2_combout\ : std_logic;
SIGNAL \Mux1~3_combout\ : std_logic;
SIGNAL \Mux1~4_combout\ : std_logic;
SIGNAL \Mux1~5_combout\ : std_logic;
SIGNAL \Mux1~1_combout\ : std_logic;
SIGNAL \Mux1~6_combout\ : std_logic;
SIGNAL \Mux1~7_combout\ : std_logic;
SIGNAL \Mux0~0_combout\ : std_logic;
SIGNAL \Mux0~1_combout\ : std_logic;
SIGNAL \Mux0~2_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_reset <= reset;
ww_key <= key;
pulslength <= IEEE.STD_LOGIC_ARITH.CONV_INTEGER(UNSIGNED(ww_pulslength));
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X11_Y8_N0
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X10_Y10_N9
\pulslength[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux12~7_combout\,
	devoe => ww_devoe,
	o => \pulslength[0]~output_o\);

-- Location: IOOBUF_X10_Y11_N23
\pulslength[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux11~10_combout\,
	devoe => ww_devoe,
	o => \pulslength[1]~output_o\);

-- Location: IOOBUF_X10_Y10_N2
\pulslength[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux10~5_combout\,
	devoe => ww_devoe,
	o => \pulslength[2]~output_o\);

-- Location: IOOBUF_X18_Y11_N16
\pulslength[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux9~6_combout\,
	devoe => ww_devoe,
	o => \pulslength[3]~output_o\);

-- Location: IOOBUF_X10_Y10_N16
\pulslength[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux8~4_combout\,
	devoe => ww_devoe,
	o => \pulslength[4]~output_o\);

-- Location: IOOBUF_X10_Y13_N9
\pulslength[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux7~5_combout\,
	devoe => ww_devoe,
	o => \pulslength[5]~output_o\);

-- Location: IOOBUF_X11_Y17_N16
\pulslength[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux6~4_combout\,
	devoe => ww_devoe,
	o => \pulslength[6]~output_o\);

-- Location: IOOBUF_X18_Y10_N2
\pulslength[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux5~5_combout\,
	devoe => ww_devoe,
	o => \pulslength[7]~output_o\);

-- Location: IOOBUF_X6_Y7_N9
\pulslength[8]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux4~3_combout\,
	devoe => ww_devoe,
	o => \pulslength[8]~output_o\);

-- Location: IOOBUF_X18_Y6_N23
\pulslength[9]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux3~1_combout\,
	devoe => ww_devoe,
	o => \pulslength[9]~output_o\);

-- Location: IOOBUF_X6_Y7_N23
\pulslength[10]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux2~3_combout\,
	devoe => ww_devoe,
	o => \pulslength[10]~output_o\);

-- Location: IOOBUF_X10_Y14_N2
\pulslength[11]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux1~7_combout\,
	devoe => ww_devoe,
	o => \pulslength[11]~output_o\);

-- Location: IOOBUF_X10_Y16_N16
\pulslength[12]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Mux0~2_combout\,
	devoe => ww_devoe,
	o => \pulslength[12]~output_o\);

-- Location: IOIBUF_X10_Y11_N1
\key[7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(7),
	o => \key[7]~input_o\);

-- Location: IOIBUF_X11_Y17_N1
\key[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(0),
	o => \key[0]~input_o\);

-- Location: IOIBUF_X10_Y10_N22
\key[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(1),
	o => \key[1]~input_o\);

-- Location: IOIBUF_X10_Y13_N1
\key[6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(6),
	o => \key[6]~input_o\);

-- Location: IOIBUF_X10_Y14_N15
\key[3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(3),
	o => \key[3]~input_o\);

-- Location: IOIBUF_X10_Y11_N15
\key[4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(4),
	o => \key[4]~input_o\);

-- Location: LCCOMB_X10_Y6_N18
\Mux12~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~5_combout\ = (\key[1]~input_o\ & ((\key[6]~input_o\) # ((\key[3]~input_o\)))) # (!\key[1]~input_o\ & ((\key[4]~input_o\ & (\key[6]~input_o\)) # (!\key[4]~input_o\ & ((\key[3]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[4]~input_o\,
	combout => \Mux12~5_combout\);

-- Location: LCCOMB_X10_Y6_N8
\Mux12~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~4_combout\ = (\key[1]~input_o\ & (\key[6]~input_o\ & (\key[3]~input_o\ & !\key[4]~input_o\))) # (!\key[1]~input_o\ & (!\key[3]~input_o\ & (\key[6]~input_o\ $ (\key[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[4]~input_o\,
	combout => \Mux12~4_combout\);

-- Location: IOIBUF_X10_Y16_N8
\key[5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(5),
	o => \key[5]~input_o\);

-- Location: LCCOMB_X10_Y6_N28
\Mux12~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~6_combout\ = (\Mux12~4_combout\ & (!\key[5]~input_o\ & (\key[0]~input_o\ $ (\Mux12~5_combout\)))) # (!\Mux12~4_combout\ & (!\key[0]~input_o\ & (!\Mux12~5_combout\ & \key[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[0]~input_o\,
	datab => \Mux12~5_combout\,
	datac => \Mux12~4_combout\,
	datad => \key[5]~input_o\,
	combout => \Mux12~6_combout\);

-- Location: IOIBUF_X10_Y11_N8
\key[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(2),
	o => \key[2]~input_o\);

-- Location: LCCOMB_X12_Y10_N16
\Mux12~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~2_combout\ = (\key[6]~input_o\ & (!\key[2]~input_o\ & (!\key[7]~input_o\ & !\key[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \key[2]~input_o\,
	datac => \key[7]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux12~2_combout\);

-- Location: LCCOMB_X12_Y13_N0
\Mux2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux2~0_combout\ = (\key[4]~input_o\ & (\key[3]~input_o\ & \key[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux2~0_combout\);

-- Location: LCCOMB_X12_Y13_N2
\Mux12~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~3_combout\ = (\Mux12~2_combout\ & (\key[1]~input_o\ & \Mux2~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux12~2_combout\,
	datac => \key[1]~input_o\,
	datad => \Mux2~0_combout\,
	combout => \Mux12~3_combout\);

-- Location: LCCOMB_X12_Y10_N20
\Mux12~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~7_combout\ = (\Mux12~3_combout\) # ((!\key[7]~input_o\ & (\Mux12~6_combout\ & \key[2]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[7]~input_o\,
	datab => \Mux12~6_combout\,
	datac => \Mux12~3_combout\,
	datad => \key[2]~input_o\,
	combout => \Mux12~7_combout\);

-- Location: LCCOMB_X12_Y7_N8
\Mux11~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~3_combout\ = (\key[4]~input_o\ & (!\key[3]~input_o\ & (!\key[0]~input_o\ & \key[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux11~3_combout\);

-- Location: LCCOMB_X12_Y7_N26
\Mux11~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~4_combout\ = (\key[3]~input_o\) # ((\key[5]~input_o\) # ((\key[4]~input_o\ & \key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux11~4_combout\);

-- Location: LCCOMB_X12_Y7_N12
\Mux11~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~5_combout\ = (\key[3]~input_o\ & (\key[5]~input_o\ $ (((!\key[4]~input_o\ & \key[0]~input_o\))))) # (!\key[3]~input_o\ & (\key[0]~input_o\ & ((\key[4]~input_o\) # (\key[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux11~5_combout\);

-- Location: LCCOMB_X12_Y7_N14
\Mux11~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~6_combout\ = (\key[6]~input_o\ & ((\key[1]~input_o\) # ((!\Mux11~4_combout\)))) # (!\key[6]~input_o\ & (!\key[1]~input_o\ & ((\Mux11~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001101110001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \key[1]~input_o\,
	datac => \Mux11~4_combout\,
	datad => \Mux11~5_combout\,
	combout => \Mux11~6_combout\);

-- Location: LCCOMB_X12_Y7_N16
\Mux11~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~7_combout\ = (!\key[4]~input_o\ & (\key[3]~input_o\ & (!\key[0]~input_o\ & !\key[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux11~7_combout\);

-- Location: LCCOMB_X12_Y7_N2
\Mux11~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~8_combout\ = (\key[1]~input_o\ & ((\Mux11~6_combout\ & ((\Mux11~7_combout\))) # (!\Mux11~6_combout\ & (\Mux11~3_combout\)))) # (!\key[1]~input_o\ & (((\Mux11~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \Mux11~3_combout\,
	datac => \Mux11~6_combout\,
	datad => \Mux11~7_combout\,
	combout => \Mux11~8_combout\);

-- Location: LCCOMB_X11_Y11_N16
\Mux11~10\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~10_combout\ = (!\key[7]~input_o\ & (\key[2]~input_o\ & \Mux11~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[7]~input_o\,
	datac => \key[2]~input_o\,
	datad => \Mux11~8_combout\,
	combout => \Mux11~10_combout\);

-- Location: LCCOMB_X12_Y10_N12
\Mux10~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~0_combout\ = (\key[1]~input_o\ & (\key[0]~input_o\ & (\key[3]~input_o\ $ (!\key[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[4]~input_o\,
	combout => \Mux10~0_combout\);

-- Location: LCCOMB_X12_Y10_N22
\Mux11~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~9_combout\ = (\key[3]~input_o\ & (!\key[4]~input_o\ & !\key[5]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[4]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux11~9_combout\);

-- Location: LCCOMB_X12_Y7_N4
\Mux10~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~1_combout\ = (!\key[3]~input_o\ & (!\key[0]~input_o\ & \key[5]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key[3]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux10~1_combout\);

-- Location: LCCOMB_X12_Y7_N30
\Mux10~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~2_combout\ = (\key[3]~input_o\ & (\key[0]~input_o\ $ (((\key[4]~input_o\ & \key[5]~input_o\))))) # (!\key[3]~input_o\ & (\key[0]~input_o\ & (\key[4]~input_o\ $ (\key[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux10~2_combout\);

-- Location: LCCOMB_X12_Y7_N24
\Mux10~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~3_combout\ = (\key[6]~input_o\ & (((\key[1]~input_o\)))) # (!\key[6]~input_o\ & ((\key[1]~input_o\ & (\Mux10~1_combout\)) # (!\key[1]~input_o\ & ((\Mux10~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \Mux10~1_combout\,
	datac => \Mux10~2_combout\,
	datad => \key[1]~input_o\,
	combout => \Mux10~3_combout\);

-- Location: LCCOMB_X12_Y10_N0
\Mux10~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~4_combout\ = (\key[6]~input_o\ & (\Mux11~9_combout\ & (\key[0]~input_o\ $ (\Mux10~3_combout\)))) # (!\key[6]~input_o\ & (((\Mux10~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \key[0]~input_o\,
	datac => \Mux11~9_combout\,
	datad => \Mux10~3_combout\,
	combout => \Mux10~4_combout\);

-- Location: LCCOMB_X12_Y10_N2
\Mux11~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~2_combout\ = (!\key[7]~input_o\ & \key[2]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \key[7]~input_o\,
	datad => \key[2]~input_o\,
	combout => \Mux11~2_combout\);

-- Location: LCCOMB_X12_Y10_N18
\Mux10~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~5_combout\ = (\Mux10~0_combout\ & ((\Mux12~2_combout\) # ((\Mux10~4_combout\ & \Mux11~2_combout\)))) # (!\Mux10~0_combout\ & (\Mux10~4_combout\ & ((\Mux11~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux10~0_combout\,
	datab => \Mux10~4_combout\,
	datac => \Mux12~2_combout\,
	datad => \Mux11~2_combout\,
	combout => \Mux10~5_combout\);

-- Location: LCCOMB_X12_Y10_N14
\Mux9~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~2_combout\ = (\key[4]~input_o\) # ((\key[5]~input_o\) # (\key[3]~input_o\ $ (\key[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[1]~input_o\,
	combout => \Mux9~2_combout\);

-- Location: LCCOMB_X12_Y10_N8
\Mux9~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~3_combout\ = (\key[6]~input_o\ & (!\Mux9~2_combout\ & (!\key[0]~input_o\ & \Mux11~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \Mux9~2_combout\,
	datac => \key[0]~input_o\,
	datad => \Mux11~2_combout\,
	combout => \Mux9~3_combout\);

-- Location: LCCOMB_X11_Y16_N10
\Mux9~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~4_combout\ = (!\key[1]~input_o\ & (!\key[4]~input_o\ & (\key[3]~input_o\ $ (\key[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux9~4_combout\);

-- Location: LCCOMB_X11_Y16_N24
\Mux9~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~0_combout\ = (\key[5]~input_o\ & ((\key[1]~input_o\ & (\key[4]~input_o\)) # (!\key[1]~input_o\ & ((\key[3]~input_o\) # (!\key[4]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux9~0_combout\);

-- Location: LCCOMB_X12_Y10_N4
\Mux9~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~1_combout\ = (\key[2]~input_o\ & (!\key[7]~input_o\ & !\key[6]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key[2]~input_o\,
	datac => \key[7]~input_o\,
	datad => \key[6]~input_o\,
	combout => \Mux9~1_combout\);

-- Location: LCCOMB_X11_Y16_N18
\Mux9~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~5_combout\ = (\Mux9~1_combout\ & ((\key[0]~input_o\ & (\Mux9~4_combout\)) # (!\key[0]~input_o\ & ((\Mux9~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux9~4_combout\,
	datab => \Mux9~0_combout\,
	datac => \Mux9~1_combout\,
	datad => \key[0]~input_o\,
	combout => \Mux9~5_combout\);

-- Location: LCCOMB_X12_Y10_N30
\Mux9~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~6_combout\ = (\Mux9~3_combout\) # ((\Mux9~5_combout\) # ((\Mux10~0_combout\ & \Mux12~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux10~0_combout\,
	datab => \Mux9~3_combout\,
	datac => \Mux9~5_combout\,
	datad => \Mux12~2_combout\,
	combout => \Mux9~6_combout\);

-- Location: LCCOMB_X12_Y10_N28
\Mux8~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~1_combout\ = (!\key[6]~input_o\ & (\key[2]~input_o\ & (!\key[7]~input_o\ & \key[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \key[2]~input_o\,
	datac => \key[7]~input_o\,
	datad => \key[4]~input_o\,
	combout => \Mux8~1_combout\);

-- Location: LCCOMB_X12_Y10_N26
\Mux8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~0_combout\ = (\key[0]~input_o\ & (((!\key[1]~input_o\)))) # (!\key[0]~input_o\ & ((\key[3]~input_o\ & ((\key[5]~input_o\))) # (!\key[3]~input_o\ & (\key[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111000110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux8~0_combout\);

-- Location: LCCOMB_X12_Y10_N6
\Mux8~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~2_combout\ = (\Mux10~0_combout\ & ((\Mux12~2_combout\) # ((\Mux8~1_combout\ & \Mux8~0_combout\)))) # (!\Mux10~0_combout\ & (\Mux8~1_combout\ & (\Mux8~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux10~0_combout\,
	datab => \Mux8~1_combout\,
	datac => \Mux8~0_combout\,
	datad => \Mux12~2_combout\,
	combout => \Mux8~2_combout\);

-- Location: LCCOMB_X12_Y10_N24
\Mux8~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~3_combout\ = (!\key[1]~input_o\ & ((\key[3]~input_o\ & (\key[0]~input_o\ $ (\key[5]~input_o\))) # (!\key[3]~input_o\ & (\key[0]~input_o\ & \key[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux8~3_combout\);

-- Location: LCCOMB_X12_Y10_N10
\Mux8~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~4_combout\ = (\Mux8~2_combout\) # ((\Mux8~3_combout\ & (\Mux9~1_combout\ & !\key[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux8~2_combout\,
	datab => \Mux8~3_combout\,
	datac => \Mux9~1_combout\,
	datad => \key[4]~input_o\,
	combout => \Mux8~4_combout\);

-- Location: LCCOMB_X12_Y13_N26
\Mux7~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~3_combout\ = (\key[4]~input_o\) # ((\key[5]~input_o\) # (\key[3]~input_o\ $ (\key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux7~3_combout\);

-- Location: LCCOMB_X12_Y13_N12
\Mux7~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~4_combout\ = (!\key[1]~input_o\ & (\Mux11~2_combout\ & (!\Mux7~3_combout\ & \key[6]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \Mux11~2_combout\,
	datac => \Mux7~3_combout\,
	datad => \key[6]~input_o\,
	combout => \Mux7~4_combout\);

-- Location: LCCOMB_X12_Y13_N8
\Mux7~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~2_combout\ = (\key[1]~input_o\ & (!\key[3]~input_o\ & (\Mux8~1_combout\ & !\key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[3]~input_o\,
	datac => \Mux8~1_combout\,
	datad => \key[0]~input_o\,
	combout => \Mux7~2_combout\);

-- Location: LCCOMB_X12_Y13_N28
\Mux7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~0_combout\ = (\key[4]~input_o\ & (\key[0]~input_o\ & (\key[3]~input_o\ $ (!\key[5]~input_o\)))) # (!\key[4]~input_o\ & (\key[3]~input_o\ & (\key[5]~input_o\ $ (\key[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux7~0_combout\);

-- Location: LCCOMB_X12_Y13_N30
\Mux7~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~1_combout\ = (\Mux9~1_combout\ & (!\key[1]~input_o\ & \Mux7~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux9~1_combout\,
	datac => \key[1]~input_o\,
	datad => \Mux7~0_combout\,
	combout => \Mux7~1_combout\);

-- Location: LCCOMB_X12_Y13_N22
\Mux7~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~5_combout\ = (\Mux7~4_combout\) # ((\Mux7~2_combout\) # ((\Mux7~1_combout\) # (\Mux12~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux7~4_combout\,
	datab => \Mux7~2_combout\,
	datac => \Mux7~1_combout\,
	datad => \Mux12~3_combout\,
	combout => \Mux7~5_combout\);

-- Location: LCCOMB_X12_Y13_N4
\Mux6~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~2_combout\ = (\key[4]~input_o\ & (!\key[1]~input_o\ & (\key[3]~input_o\ $ (!\key[0]~input_o\)))) # (!\key[4]~input_o\ & (\key[3]~input_o\ & (\key[1]~input_o\ $ (\key[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[1]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux6~2_combout\);

-- Location: LCCOMB_X12_Y13_N10
\Mux6~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~1_combout\ = (\key[1]~input_o\ & ((\key[0]~input_o\) # (\key[4]~input_o\ $ (!\key[3]~input_o\)))) # (!\key[1]~input_o\ & (\key[0]~input_o\ & ((\key[4]~input_o\) # (!\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[1]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux6~1_combout\);

-- Location: LCCOMB_X12_Y13_N6
\Mux6~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~3_combout\ = (\key[5]~input_o\ & (!\Mux6~2_combout\ & (!\key[6]~input_o\ & !\Mux6~1_combout\))) # (!\key[5]~input_o\ & (\Mux6~2_combout\ & (\key[6]~input_o\ $ (\Mux6~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \Mux6~2_combout\,
	datac => \key[6]~input_o\,
	datad => \Mux6~1_combout\,
	combout => \Mux6~3_combout\);

-- Location: LCCOMB_X12_Y13_N24
\Mux6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~0_combout\ = (!\key[4]~input_o\ & (!\key[3]~input_o\ & (\Mux12~2_combout\ & \key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \Mux12~2_combout\,
	datad => \key[0]~input_o\,
	combout => \Mux6~0_combout\);

-- Location: LCCOMB_X12_Y13_N16
\Mux6~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~4_combout\ = (\Mux6~0_combout\) # ((\Mux6~3_combout\ & (!\key[7]~input_o\ & \key[2]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux6~3_combout\,
	datab => \key[7]~input_o\,
	datac => \key[2]~input_o\,
	datad => \Mux6~0_combout\,
	combout => \Mux6~4_combout\);

-- Location: LCCOMB_X12_Y7_N18
\Mux5~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux5~2_combout\ = (\key[4]~input_o\ & ((\key[6]~input_o\) # (!\key[3]~input_o\))) # (!\key[4]~input_o\ & (\key[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datad => \key[6]~input_o\,
	combout => \Mux5~2_combout\);

-- Location: LCCOMB_X12_Y7_N28
\Mux5~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux5~3_combout\ = (\Mux5~2_combout\ & ((\key[6]~input_o\) # ((\key[0]~input_o\)))) # (!\Mux5~2_combout\ & ((\key[5]~input_o\) # ((\key[6]~input_o\ & \key[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \key[5]~input_o\,
	datac => \key[0]~input_o\,
	datad => \Mux5~2_combout\,
	combout => \Mux5~3_combout\);

-- Location: LCCOMB_X12_Y7_N22
\Mux5~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux5~4_combout\ = (\key[1]~input_o\ & (\key[6]~input_o\ & (\Mux11~7_combout\))) # (!\key[1]~input_o\ & (\key[6]~input_o\ $ (((\Mux5~3_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \Mux11~7_combout\,
	datac => \Mux5~3_combout\,
	datad => \key[1]~input_o\,
	combout => \Mux5~4_combout\);

-- Location: LCCOMB_X11_Y10_N24
\Mux5~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux5~5_combout\ = (\Mux12~3_combout\) # ((!\key[7]~input_o\ & (\key[2]~input_o\ & \Mux5~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux12~3_combout\,
	datab => \key[7]~input_o\,
	datac => \key[2]~input_o\,
	datad => \Mux5~4_combout\,
	combout => \Mux5~5_combout\);

-- Location: LCCOMB_X12_Y7_N10
\Mux4~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux4~1_combout\ = (\key[1]~input_o\ & (!\key[0]~input_o\ & \key[4]~input_o\)) # (!\key[1]~input_o\ & (\key[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[4]~input_o\,
	combout => \Mux4~1_combout\);

-- Location: LCCOMB_X12_Y7_N0
\Mux4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux4~0_combout\ = (\key[6]~input_o\ & ((\key[1]~input_o\ & ((!\Mux11~7_combout\))) # (!\key[1]~input_o\ & (\Mux11~4_combout\)))) # (!\key[6]~input_o\ & (\key[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \key[1]~input_o\,
	datac => \Mux11~4_combout\,
	datad => \Mux11~7_combout\,
	combout => \Mux4~0_combout\);

-- Location: LCCOMB_X12_Y7_N20
\Mux4~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux4~2_combout\ = (\Mux4~1_combout\ & ((\key[5]~input_o\ & (\Mux4~0_combout\ $ (!\key[3]~input_o\))) # (!\key[5]~input_o\ & (!\Mux4~0_combout\ & \key[3]~input_o\)))) # (!\Mux4~1_combout\ & (\key[5]~input_o\ & (!\Mux4~0_combout\ & \key[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux4~1_combout\,
	datab => \key[5]~input_o\,
	datac => \Mux4~0_combout\,
	datad => \key[3]~input_o\,
	combout => \Mux4~2_combout\);

-- Location: LCCOMB_X12_Y7_N6
\Mux4~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux4~3_combout\ = (\Mux11~2_combout\ & ((\key[6]~input_o\ & ((!\Mux4~0_combout\))) # (!\key[6]~input_o\ & (\Mux4~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \Mux4~2_combout\,
	datac => \Mux11~2_combout\,
	datad => \Mux4~0_combout\,
	combout => \Mux4~3_combout\);

-- Location: LCCOMB_X10_Y6_N22
\Mux3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux3~0_combout\ = (!\key[1]~input_o\ & (\key[0]~input_o\ & ((\key[3]~input_o\) # (\key[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[5]~input_o\,
	datac => \key[1]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux3~0_combout\);

-- Location: LCCOMB_X10_Y6_N26
\Mux3~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux3~2_combout\ = (\key[1]~input_o\ & (!\key[6]~input_o\ & (\key[4]~input_o\ $ (\key[5]~input_o\)))) # (!\key[1]~input_o\ & (!\key[4]~input_o\ & (\key[6]~input_o\ & !\key[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux3~2_combout\);

-- Location: LCCOMB_X10_Y6_N20
\Mux3~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux3~3_combout\ = (\Mux3~2_combout\ & ((\key[0]~input_o\ & (\key[6]~input_o\)) # (!\key[0]~input_o\ & ((!\key[3]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux3~2_combout\,
	datab => \key[6]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux3~3_combout\);

-- Location: LCCOMB_X10_Y6_N0
\Mux3~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux3~1_combout\ = (\Mux11~2_combout\ & ((\Mux3~3_combout\) # ((\Mux3~0_combout\ & !\key[6]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux3~0_combout\,
	datab => \Mux3~3_combout\,
	datac => \key[6]~input_o\,
	datad => \Mux11~2_combout\,
	combout => \Mux3~1_combout\);

-- Location: LCCOMB_X12_Y13_N18
\Mux2~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux2~1_combout\ = (\key[0]~input_o\ & (((\key[1]~input_o\)) # (!\key[4]~input_o\))) # (!\key[0]~input_o\ & (\key[4]~input_o\ $ ((\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[3]~input_o\,
	datac => \key[1]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux2~1_combout\);

-- Location: LCCOMB_X12_Y13_N20
\Mux2~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux2~2_combout\ = (\key[5]~input_o\ & (!\Mux2~1_combout\)) # (!\key[5]~input_o\ & (((!\key[1]~input_o\ & \Mux2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \Mux2~1_combout\,
	datac => \key[1]~input_o\,
	datad => \Mux2~0_combout\,
	combout => \Mux2~2_combout\);

-- Location: LCCOMB_X12_Y13_N14
\Mux2~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux2~3_combout\ = (\Mux9~1_combout\ & ((\Mux2~2_combout\) # ((\key[1]~input_o\ & \Mux6~0_combout\)))) # (!\Mux9~1_combout\ & (((\key[1]~input_o\ & \Mux6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux9~1_combout\,
	datab => \Mux2~2_combout\,
	datac => \key[1]~input_o\,
	datad => \Mux6~0_combout\,
	combout => \Mux2~3_combout\);

-- Location: LCCOMB_X10_Y6_N2
\Mux1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~0_combout\ = (\key[0]~input_o\ & ((\key[3]~input_o\ & (\key[4]~input_o\ & \key[1]~input_o\)) # (!\key[3]~input_o\ & (!\key[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[1]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux1~0_combout\);

-- Location: LCCOMB_X10_Y6_N6
\Mux1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~2_combout\ = (\key[4]~input_o\ & (!\key[6]~input_o\ & ((\key[3]~input_o\) # (\key[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux1~2_combout\);

-- Location: LCCOMB_X10_Y6_N16
\Mux1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~3_combout\ = (\key[3]~input_o\ & (!\key[4]~input_o\ & ((\key[0]~input_o\)))) # (!\key[3]~input_o\ & (\key[6]~input_o\ $ (((\key[4]~input_o\ & \key[0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux1~3_combout\);

-- Location: LCCOMB_X10_Y6_N10
\Mux1~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~4_combout\ = (\key[1]~input_o\ & (((\key[5]~input_o\)))) # (!\key[1]~input_o\ & ((\key[5]~input_o\ & (\Mux1~2_combout\)) # (!\key[5]~input_o\ & ((\Mux1~3_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux1~2_combout\,
	datab => \Mux1~3_combout\,
	datac => \key[1]~input_o\,
	datad => \key[5]~input_o\,
	combout => \Mux1~4_combout\);

-- Location: LCCOMB_X10_Y6_N4
\Mux1~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~5_combout\ = (\key[3]~input_o\ & (\key[4]~input_o\ & (!\key[6]~input_o\ & !\key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux1~5_combout\);

-- Location: LCCOMB_X10_Y6_N12
\Mux1~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~1_combout\ = (!\key[0]~input_o\ & ((\key[3]~input_o\ & (!\key[4]~input_o\ & \key[6]~input_o\)) # (!\key[3]~input_o\ & (\key[4]~input_o\ & !\key[6]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux1~1_combout\);

-- Location: LCCOMB_X10_Y6_N30
\Mux1~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~6_combout\ = (\Mux1~4_combout\ & ((\Mux1~5_combout\) # ((!\key[1]~input_o\)))) # (!\Mux1~4_combout\ & (((\key[1]~input_o\ & \Mux1~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux1~4_combout\,
	datab => \Mux1~5_combout\,
	datac => \key[1]~input_o\,
	datad => \Mux1~1_combout\,
	combout => \Mux1~6_combout\);

-- Location: LCCOMB_X10_Y6_N24
\Mux1~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~7_combout\ = (\Mux11~2_combout\ & ((\Mux1~6_combout\) # ((\Mux1~0_combout\ & \Mux12~2_combout\)))) # (!\Mux11~2_combout\ & (\Mux1~0_combout\ & ((\Mux12~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux11~2_combout\,
	datab => \Mux1~0_combout\,
	datac => \Mux1~6_combout\,
	datad => \Mux12~2_combout\,
	combout => \Mux1~7_combout\);

-- Location: LCCOMB_X11_Y16_N12
\Mux0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~0_combout\ = (!\key[3]~input_o\ & (!\key[0]~input_o\ & ((\key[5]~input_o\) # (\key[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux0~0_combout\);

-- Location: LCCOMB_X11_Y16_N6
\Mux0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~1_combout\ = (\key[5]~input_o\ & (!\key[4]~input_o\)) # (!\key[5]~input_o\ & (\key[0]~input_o\ & ((\key[4]~input_o\) # (\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Mux0~1_combout\);

-- Location: LCCOMB_X11_Y16_N16
\Mux0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~2_combout\ = (\Mux9~1_combout\ & ((\key[1]~input_o\ & (\Mux0~0_combout\)) # (!\key[1]~input_o\ & ((\Mux0~1_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~0_combout\,
	datab => \Mux9~1_combout\,
	datac => \key[1]~input_o\,
	datad => \Mux0~1_combout\,
	combout => \Mux0~2_combout\);

-- Location: IOIBUF_X18_Y14_N22
\reset~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: UNVM_X0_Y8_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

ww_pulslength(0) <= \pulslength[0]~output_o\;

ww_pulslength(1) <= \pulslength[1]~output_o\;

ww_pulslength(2) <= \pulslength[2]~output_o\;

ww_pulslength(3) <= \pulslength[3]~output_o\;

ww_pulslength(4) <= \pulslength[4]~output_o\;

ww_pulslength(5) <= \pulslength[5]~output_o\;

ww_pulslength(6) <= \pulslength[6]~output_o\;

ww_pulslength(7) <= \pulslength[7]~output_o\;

ww_pulslength(8) <= \pulslength[8]~output_o\;

ww_pulslength(9) <= \pulslength[9]~output_o\;

ww_pulslength(10) <= \pulslength[10]~output_o\;

ww_pulslength(11) <= \pulslength[11]~output_o\;

ww_pulslength(12) <= \pulslength[12]~output_o\;
END structure;


