--------------------------------------------------------------------
--! \file      seg7dec.vhd
--! \date      see top of 'Version History'
--! \brief     7-segment driver without control of dot 
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |12-2-2015  |WLGRW   |Inital version
--! 002    |28-3-2015  |WLGRW   |Modification to set dot in display  
--! 003    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--! 004    |31-3-2020  |WLGRW   |Modification for assignment 0-a
--!
--! # 7 segment decoder
--!
--! Use constants:
--! - to make the VHDL code more readable
--! - for both numbers and alphanumeric characters
--! 
--! The following constants are defined to generate alle required characters.
--! Each led in the HEX_display has been given a number for the purpose of 
--! identification. See figure 1.
--!
--! \verbatim
--!
--!  Figure 1: 7-segments lay-out:
--!  
--!        -0-
--!       |   |
--!       5   1
--!       |   |
--!        -6-
--!       |   |
--!       4   2
--!       |   |
--!        -3-  7 (dot)
--!  
--! \endverbatim
--!
--! All LEDs are grouped in a STD_LOGIC_VECTOR where the index number is
--! equal to the LED number. 
--!
--! Because the LEDs are contolled using inverted-logic we have to apply a
--! '1' to switch the LED off. 
--!
--! \todo Complete documentation
--------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
--------------------------------------------------------------------
ENTITY seg7dec IS
	GENERIC (

   --! When used in the refactored 7-segement drive, 
   CONSTANT hex_zero:  STD_LOGIC_VECTOR(0 TO 6) := "0000001"; -- 0
            CONSTANT hex_one:   STD_LOGIC_VECTOR(0 TO 6) := "1001111"; -- 1
            CONSTANT hex_two:   STD_LOGIC_VECTOR(0 TO 6) := "0010010"; -- 2
            CONSTANT hex_three: STD_LOGIC_VECTOR(0 TO 6) := "0000110"; -- 3
            CONSTANT hex_four:  STD_LOGIC_VECTOR(0 TO 6) := "1001100"; -- 4
            CONSTANT hex_five:  STD_LOGIC_VECTOR(0 TO 6) := "0100100"; -- 5
            CONSTANT hex_six:   STD_LOGIC_VECTOR(0 TO 6) := "0100000"; -- 6
            CONSTANT hex_seven: STD_LOGIC_VECTOR(0 TO 6) := "0001111"; -- 7
            CONSTANT hex_eight: STD_LOGIC_VECTOR(0 TO 6) := "0000000"; -- 8
            CONSTANT hex_nine:  STD_LOGIC_VECTOR(0 TO 6) := "0000100"; -- 9
            CONSTANT hex_a:     STD_LOGIC_VECTOR(0 TO 6) := "0001000"; -- a
            CONSTANT hex_b:     STD_LOGIC_VECTOR(0 TO 6) := "1100000"; -- b
            CONSTANT hex_c:     STD_LOGIC_VECTOR(0 TO 6) := "0110001"; -- c
            CONSTANT hex_d:     STD_LOGIC_VECTOR(0 TO 6) := "1000010"; -- d
            CONSTANT hex_e:     STD_LOGIC_VECTOR(0 TO 6) := "0110000"; -- e
            CONSTANT hex_f:     STD_LOGIC_VECTOR(0 TO 6) := "0111000"  -- f
				);

   port(
      C       : IN  STD_LOGIC_VECTOR(3 downto 0);
      display : OUT STD_LOGIC_VECTOR(0 to 7) 
   );
END ENTITY seg7dec;
--------------------------------------------------------------------
ARCHITECTURE implementation OF seg7dec IS
BEGIN

	-- Segments use inverted logic: on = '0', off = '1'.


-- Display decoder. This code is using "WITH - SELECT" to encode 6 segments on
-- the HEX diplay. This  code is using the CONSTANTS that are defined at GENERIC.

    WITH C SELECT
         display(0 TO 6) <= 
            hex_zero  WHEN "0000", -- 0
            hex_one   WHEN "0001", -- 1
            hex_two   WHEN "0010", -- 2
            hex_three WHEN "0011", -- 3
            hex_four  WHEN "0100", -- 4
            hex_five  WHEN "0101", -- 5
            hex_six   WHEN "0110", -- 6
            hex_seven WHEN "0111", -- 7
            hex_eight WHEN "1000", -- 8
            hex_nine  WHEN "1001", -- 9
            hex_a     WHEN "1010", -- a
            hex_b     WHEN "1011", -- b
            hex_c     WHEN "1100", -- c
            hex_d     WHEN "1101", -- d
            hex_e     WHEN "1110", -- e
            hex_f     WHEN "1111", -- f
            (OTHERS =>'1') WHEN OTHERS;

END ARCHITECTURE implementation;
--------------------------------------------------------------------