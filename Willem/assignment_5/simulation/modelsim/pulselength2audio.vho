-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "05/26/2020 16:22:52"

-- 
-- Device: Altera 10M02DCU324A6G Package UFBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_J7,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_J8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_H3,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_H4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_H8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
LIBRARY STD;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE STD.STANDARD.ALL;

ENTITY 	pulselength2audio IS
    PORT (
	reset : IN std_logic;
	clk_dev : IN std_logic;
	pulslength : IN STD.STANDARD.integer range 0 TO 131071;
	audiol : OUT std_logic;
	audior : OUT std_logic
	);
END pulselength2audio;

-- Design Ports Information
-- audiol	=>  Location: PIN_T8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- audior	=>  Location: PIN_T5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk_dev	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_L8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[1]	=>  Location: PIN_T6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[0]	=>  Location: PIN_M8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[3]	=>  Location: PIN_U9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[2]	=>  Location: PIN_V8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[5]	=>  Location: PIN_U8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[4]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[7]	=>  Location: PIN_L16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[6]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[9]	=>  Location: PIN_R9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[8]	=>  Location: PIN_U7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[11]	=>  Location: PIN_T17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[10]	=>  Location: PIN_U6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[13]	=>  Location: PIN_V7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[12]	=>  Location: PIN_L11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[15]	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[14]	=>  Location: PIN_V6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- pulslength[16]	=>  Location: PIN_R8,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF pulselength2audio IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_clk_dev : std_logic;
SIGNAL ww_pulslength : std_logic_vector(16 DOWNTO 0);
SIGNAL ww_audiol : std_logic;
SIGNAL ww_audior : std_logic;
SIGNAL \reset~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk_dev~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \audiol~output_o\ : std_logic;
SIGNAL \audior~output_o\ : std_logic;
SIGNAL \clk_dev~input_o\ : std_logic;
SIGNAL \clk_dev~inputclkctrl_outclk\ : std_logic;
SIGNAL \COUNT:counter[0]~1_combout\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \reset~inputclkctrl_outclk\ : std_logic;
SIGNAL \pulslength[16]~input_o\ : std_logic;
SIGNAL \pulslength[15]~input_o\ : std_logic;
SIGNAL \pulslength[14]~input_o\ : std_logic;
SIGNAL \Equal1~8_combout\ : std_logic;
SIGNAL \pulslength[8]~input_o\ : std_logic;
SIGNAL \pulslength[9]~input_o\ : std_logic;
SIGNAL \Equal1~5_combout\ : std_logic;
SIGNAL \pulslength[10]~input_o\ : std_logic;
SIGNAL \pulslength[11]~input_o\ : std_logic;
SIGNAL \Equal1~6_combout\ : std_logic;
SIGNAL \pulslength[12]~input_o\ : std_logic;
SIGNAL \pulslength[13]~input_o\ : std_logic;
SIGNAL \Equal1~7_combout\ : std_logic;
SIGNAL \Equal1~9_combout\ : std_logic;
SIGNAL \pulslength[2]~input_o\ : std_logic;
SIGNAL \pulslength[3]~input_o\ : std_logic;
SIGNAL \Equal1~1_combout\ : std_logic;
SIGNAL \pulslength[7]~input_o\ : std_logic;
SIGNAL \pulslength[6]~input_o\ : std_logic;
SIGNAL \Equal1~3_combout\ : std_logic;
SIGNAL \pulslength[1]~input_o\ : std_logic;
SIGNAL \pulslength[0]~input_o\ : std_logic;
SIGNAL \Equal1~0_combout\ : std_logic;
SIGNAL \pulslength[4]~input_o\ : std_logic;
SIGNAL \pulslength[5]~input_o\ : std_logic;
SIGNAL \Equal1~2_combout\ : std_logic;
SIGNAL \Equal1~4_combout\ : std_logic;
SIGNAL \Equal1~10_combout\ : std_logic;
SIGNAL \COUNT:counter[0]~q\ : std_logic;
SIGNAL \COUNT:counter[0]~2\ : std_logic;
SIGNAL \COUNT:counter[1]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[1]~q\ : std_logic;
SIGNAL \COUNT:counter[1]~2\ : std_logic;
SIGNAL \COUNT:counter[2]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[2]~q\ : std_logic;
SIGNAL \COUNT:counter[2]~2\ : std_logic;
SIGNAL \COUNT:counter[3]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[3]~q\ : std_logic;
SIGNAL \COUNT:counter[3]~2\ : std_logic;
SIGNAL \COUNT:counter[4]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[4]~q\ : std_logic;
SIGNAL \COUNT:counter[4]~2\ : std_logic;
SIGNAL \COUNT:counter[5]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[5]~q\ : std_logic;
SIGNAL \COUNT:counter[5]~2\ : std_logic;
SIGNAL \COUNT:counter[6]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[6]~q\ : std_logic;
SIGNAL \COUNT:counter[6]~2\ : std_logic;
SIGNAL \COUNT:counter[7]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[7]~q\ : std_logic;
SIGNAL \COUNT:counter[7]~2\ : std_logic;
SIGNAL \COUNT:counter[8]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[8]~q\ : std_logic;
SIGNAL \COUNT:counter[8]~2\ : std_logic;
SIGNAL \COUNT:counter[9]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[9]~q\ : std_logic;
SIGNAL \COUNT:counter[9]~2\ : std_logic;
SIGNAL \COUNT:counter[10]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[10]~q\ : std_logic;
SIGNAL \COUNT:counter[10]~2\ : std_logic;
SIGNAL \COUNT:counter[11]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[11]~q\ : std_logic;
SIGNAL \COUNT:counter[11]~2\ : std_logic;
SIGNAL \COUNT:counter[12]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[12]~q\ : std_logic;
SIGNAL \COUNT:counter[12]~2\ : std_logic;
SIGNAL \COUNT:counter[13]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[13]~q\ : std_logic;
SIGNAL \COUNT:counter[13]~2\ : std_logic;
SIGNAL \COUNT:counter[14]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[14]~q\ : std_logic;
SIGNAL \COUNT:counter[14]~2\ : std_logic;
SIGNAL \COUNT:counter[15]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[15]~q\ : std_logic;
SIGNAL \COUNT:counter[15]~2\ : std_logic;
SIGNAL \COUNT:counter[16]~1_combout\ : std_logic;
SIGNAL \COUNT:counter[16]~q\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~4_combout\ : std_logic;
SIGNAL \audior~0_combout\ : std_logic;
SIGNAL \audior~reg0_q\ : std_logic;
SIGNAL \ALT_INV_clk_dev~inputclkctrl_outclk\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_reset <= reset;
ww_clk_dev <= clk_dev;
ww_pulslength <= IEEE.STD_LOGIC_ARITH.CONV_STD_LOGIC_VECTOR(pulslength, 17);
audiol <= ww_audiol;
audior <= ww_audior;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\reset~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \reset~input_o\);

\clk_dev~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk_dev~input_o\);
\ALT_INV_clk_dev~inputclkctrl_outclk\ <= NOT \clk_dev~inputclkctrl_outclk\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X11_Y8_N0
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X6_Y0_N9
\audiol~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \audior~reg0_q\,
	devoe => ww_devoe,
	o => \audiol~output_o\);

-- Location: IOOBUF_X6_Y0_N2
\audior~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \audior~reg0_q\,
	devoe => ww_devoe,
	o => \audior~output_o\);

-- Location: IOIBUF_X0_Y4_N15
\clk_dev~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk_dev,
	o => \clk_dev~input_o\);

-- Location: CLKCTRL_G3
\clk_dev~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk_dev~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk_dev~inputclkctrl_outclk\);

-- Location: LCCOMB_X12_Y4_N16
\COUNT:counter[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[0]~1_combout\ = \COUNT:counter[0]~q\ $ (VCC)
-- \COUNT:counter[0]~2\ = CARRY(\COUNT:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[0]~q\,
	datad => VCC,
	combout => \COUNT:counter[0]~1_combout\,
	cout => \COUNT:counter[0]~2\);

-- Location: IOIBUF_X0_Y4_N22
\reset~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: CLKCTRL_G4
\reset~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \reset~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \reset~inputclkctrl_outclk\);

-- Location: IOIBUF_X11_Y0_N8
\pulslength[16]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(16),
	o => \pulslength[16]~input_o\);

-- Location: IOIBUF_X9_Y0_N8
\pulslength[15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(15),
	o => \pulslength[15]~input_o\);

-- Location: IOIBUF_X9_Y0_N15
\pulslength[14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(14),
	o => \pulslength[14]~input_o\);

-- Location: LCCOMB_X12_Y3_N26
\Equal1~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~8_combout\ = (\pulslength[15]~input_o\ & (\COUNT:counter[15]~q\ & (\pulslength[14]~input_o\ $ (!\COUNT:counter[14]~q\)))) # (!\pulslength[15]~input_o\ & (!\COUNT:counter[15]~q\ & (\pulslength[14]~input_o\ $ (!\COUNT:counter[14]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[15]~input_o\,
	datab => \pulslength[14]~input_o\,
	datac => \COUNT:counter[15]~q\,
	datad => \COUNT:counter[14]~q\,
	combout => \Equal1~8_combout\);

-- Location: IOIBUF_X11_Y0_N22
\pulslength[8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(8),
	o => \pulslength[8]~input_o\);

-- Location: IOIBUF_X11_Y0_N1
\pulslength[9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(9),
	o => \pulslength[9]~input_o\);

-- Location: LCCOMB_X12_Y3_N28
\Equal1~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~5_combout\ = (\pulslength[8]~input_o\ & (\COUNT:counter[8]~q\ & (\pulslength[9]~input_o\ $ (!\COUNT:counter[9]~q\)))) # (!\pulslength[8]~input_o\ & (!\COUNT:counter[8]~q\ & (\pulslength[9]~input_o\ $ (!\COUNT:counter[9]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[8]~input_o\,
	datab => \COUNT:counter[8]~q\,
	datac => \pulslength[9]~input_o\,
	datad => \COUNT:counter[9]~q\,
	combout => \Equal1~5_combout\);

-- Location: IOIBUF_X9_Y0_N22
\pulslength[10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(10),
	o => \pulslength[10]~input_o\);

-- Location: IOIBUF_X18_Y3_N8
\pulslength[11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(11),
	o => \pulslength[11]~input_o\);

-- Location: LCCOMB_X12_Y3_N22
\Equal1~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~6_combout\ = (\pulslength[10]~input_o\ & (\COUNT:counter[10]~q\ & (\pulslength[11]~input_o\ $ (!\COUNT:counter[11]~q\)))) # (!\pulslength[10]~input_o\ & (!\COUNT:counter[10]~q\ & (\pulslength[11]~input_o\ $ (!\COUNT:counter[11]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[10]~input_o\,
	datab => \pulslength[11]~input_o\,
	datac => \COUNT:counter[10]~q\,
	datad => \COUNT:counter[11]~q\,
	combout => \Equal1~6_combout\);

-- Location: IOIBUF_X18_Y3_N15
\pulslength[12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(12),
	o => \pulslength[12]~input_o\);

-- Location: IOIBUF_X11_Y0_N15
\pulslength[13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(13),
	o => \pulslength[13]~input_o\);

-- Location: LCCOMB_X12_Y3_N24
\Equal1~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~7_combout\ = (\pulslength[12]~input_o\ & (\COUNT:counter[12]~q\ & (\pulslength[13]~input_o\ $ (!\COUNT:counter[13]~q\)))) # (!\pulslength[12]~input_o\ & (!\COUNT:counter[12]~q\ & (\pulslength[13]~input_o\ $ (!\COUNT:counter[13]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[12]~input_o\,
	datab => \pulslength[13]~input_o\,
	datac => \COUNT:counter[12]~q\,
	datad => \COUNT:counter[13]~q\,
	combout => \Equal1~7_combout\);

-- Location: LCCOMB_X12_Y3_N20
\Equal1~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~9_combout\ = (\Equal1~8_combout\ & (\Equal1~5_combout\ & (\Equal1~6_combout\ & \Equal1~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~8_combout\,
	datab => \Equal1~5_combout\,
	datac => \Equal1~6_combout\,
	datad => \Equal1~7_combout\,
	combout => \Equal1~9_combout\);

-- Location: IOIBUF_X14_Y0_N22
\pulslength[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(2),
	o => \pulslength[2]~input_o\);

-- Location: IOIBUF_X14_Y0_N8
\pulslength[3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(3),
	o => \pulslength[3]~input_o\);

-- Location: LCCOMB_X12_Y4_N6
\Equal1~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~1_combout\ = (\pulslength[2]~input_o\ & (\COUNT:counter[2]~q\ & (\pulslength[3]~input_o\ $ (!\COUNT:counter[3]~q\)))) # (!\pulslength[2]~input_o\ & (!\COUNT:counter[2]~q\ & (\pulslength[3]~input_o\ $ (!\COUNT:counter[3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[2]~input_o\,
	datab => \pulslength[3]~input_o\,
	datac => \COUNT:counter[3]~q\,
	datad => \COUNT:counter[2]~q\,
	combout => \Equal1~1_combout\);

-- Location: IOIBUF_X18_Y4_N8
\pulslength[7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(7),
	o => \pulslength[7]~input_o\);

-- Location: IOIBUF_X18_Y4_N1
\pulslength[6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(6),
	o => \pulslength[6]~input_o\);

-- Location: LCCOMB_X12_Y4_N14
\Equal1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~3_combout\ = (\pulslength[7]~input_o\ & (\COUNT:counter[7]~q\ & (\pulslength[6]~input_o\ $ (!\COUNT:counter[6]~q\)))) # (!\pulslength[7]~input_o\ & (!\COUNT:counter[7]~q\ & (\pulslength[6]~input_o\ $ (!\COUNT:counter[6]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[7]~input_o\,
	datab => \pulslength[6]~input_o\,
	datac => \COUNT:counter[7]~q\,
	datad => \COUNT:counter[6]~q\,
	combout => \Equal1~3_combout\);

-- Location: IOIBUF_X9_Y0_N29
\pulslength[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(1),
	o => \pulslength[1]~input_o\);

-- Location: IOIBUF_X14_Y0_N1
\pulslength[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(0),
	o => \pulslength[0]~input_o\);

-- Location: LCCOMB_X12_Y4_N4
\Equal1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~0_combout\ = (\pulslength[1]~input_o\ & (\COUNT:counter[1]~q\ & (\COUNT:counter[0]~q\ $ (!\pulslength[0]~input_o\)))) # (!\pulslength[1]~input_o\ & (!\COUNT:counter[1]~q\ & (\COUNT:counter[0]~q\ $ (!\pulslength[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[1]~input_o\,
	datab => \COUNT:counter[0]~q\,
	datac => \pulslength[0]~input_o\,
	datad => \COUNT:counter[1]~q\,
	combout => \Equal1~0_combout\);

-- Location: IOIBUF_X9_Y0_N1
\pulslength[4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(4),
	o => \pulslength[4]~input_o\);

-- Location: IOIBUF_X14_Y0_N29
\pulslength[5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pulslength(5),
	o => \pulslength[5]~input_o\);

-- Location: LCCOMB_X12_Y4_N0
\Equal1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~2_combout\ = (\pulslength[4]~input_o\ & (\COUNT:counter[4]~q\ & (\pulslength[5]~input_o\ $ (!\COUNT:counter[5]~q\)))) # (!\pulslength[4]~input_o\ & (!\COUNT:counter[4]~q\ & (\pulslength[5]~input_o\ $ (!\COUNT:counter[5]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[4]~input_o\,
	datab => \pulslength[5]~input_o\,
	datac => \COUNT:counter[5]~q\,
	datad => \COUNT:counter[4]~q\,
	combout => \Equal1~2_combout\);

-- Location: LCCOMB_X12_Y4_N12
\Equal1~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~4_combout\ = (\Equal1~1_combout\ & (\Equal1~3_combout\ & (\Equal1~0_combout\ & \Equal1~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~1_combout\,
	datab => \Equal1~3_combout\,
	datac => \Equal1~0_combout\,
	datad => \Equal1~2_combout\,
	combout => \Equal1~4_combout\);

-- Location: LCCOMB_X12_Y3_N18
\Equal1~10\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~10_combout\ = (\Equal1~9_combout\ & (\Equal1~4_combout\ & (\pulslength[16]~input_o\ $ (!\COUNT:counter[16]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pulslength[16]~input_o\,
	datab => \Equal1~9_combout\,
	datac => \COUNT:counter[16]~q\,
	datad => \Equal1~4_combout\,
	combout => \Equal1~10_combout\);

-- Location: FF_X12_Y4_N17
\COUNT:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[0]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[0]~q\);

-- Location: LCCOMB_X12_Y4_N18
\COUNT:counter[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[1]~1_combout\ = (\COUNT:counter[1]~q\ & (!\COUNT:counter[0]~2\)) # (!\COUNT:counter[1]~q\ & ((\COUNT:counter[0]~2\) # (GND)))
-- \COUNT:counter[1]~2\ = CARRY((!\COUNT:counter[0]~2\) # (!\COUNT:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[1]~q\,
	datad => VCC,
	cin => \COUNT:counter[0]~2\,
	combout => \COUNT:counter[1]~1_combout\,
	cout => \COUNT:counter[1]~2\);

-- Location: FF_X12_Y4_N19
\COUNT:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[1]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[1]~q\);

-- Location: LCCOMB_X12_Y4_N20
\COUNT:counter[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[2]~1_combout\ = (\COUNT:counter[2]~q\ & (\COUNT:counter[1]~2\ $ (GND))) # (!\COUNT:counter[2]~q\ & (!\COUNT:counter[1]~2\ & VCC))
-- \COUNT:counter[2]~2\ = CARRY((\COUNT:counter[2]~q\ & !\COUNT:counter[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[2]~q\,
	datad => VCC,
	cin => \COUNT:counter[1]~2\,
	combout => \COUNT:counter[2]~1_combout\,
	cout => \COUNT:counter[2]~2\);

-- Location: FF_X12_Y4_N21
\COUNT:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[2]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[2]~q\);

-- Location: LCCOMB_X12_Y4_N22
\COUNT:counter[3]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[3]~1_combout\ = (\COUNT:counter[3]~q\ & (!\COUNT:counter[2]~2\)) # (!\COUNT:counter[3]~q\ & ((\COUNT:counter[2]~2\) # (GND)))
-- \COUNT:counter[3]~2\ = CARRY((!\COUNT:counter[2]~2\) # (!\COUNT:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[3]~q\,
	datad => VCC,
	cin => \COUNT:counter[2]~2\,
	combout => \COUNT:counter[3]~1_combout\,
	cout => \COUNT:counter[3]~2\);

-- Location: FF_X12_Y4_N23
\COUNT:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[3]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[3]~q\);

-- Location: LCCOMB_X12_Y4_N24
\COUNT:counter[4]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[4]~1_combout\ = (\COUNT:counter[4]~q\ & (\COUNT:counter[3]~2\ $ (GND))) # (!\COUNT:counter[4]~q\ & (!\COUNT:counter[3]~2\ & VCC))
-- \COUNT:counter[4]~2\ = CARRY((\COUNT:counter[4]~q\ & !\COUNT:counter[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[4]~q\,
	datad => VCC,
	cin => \COUNT:counter[3]~2\,
	combout => \COUNT:counter[4]~1_combout\,
	cout => \COUNT:counter[4]~2\);

-- Location: FF_X12_Y4_N25
\COUNT:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[4]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[4]~q\);

-- Location: LCCOMB_X12_Y4_N26
\COUNT:counter[5]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[5]~1_combout\ = (\COUNT:counter[5]~q\ & (!\COUNT:counter[4]~2\)) # (!\COUNT:counter[5]~q\ & ((\COUNT:counter[4]~2\) # (GND)))
-- \COUNT:counter[5]~2\ = CARRY((!\COUNT:counter[4]~2\) # (!\COUNT:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[5]~q\,
	datad => VCC,
	cin => \COUNT:counter[4]~2\,
	combout => \COUNT:counter[5]~1_combout\,
	cout => \COUNT:counter[5]~2\);

-- Location: FF_X12_Y4_N27
\COUNT:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[5]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[5]~q\);

-- Location: LCCOMB_X12_Y4_N28
\COUNT:counter[6]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[6]~1_combout\ = (\COUNT:counter[6]~q\ & (\COUNT:counter[5]~2\ $ (GND))) # (!\COUNT:counter[6]~q\ & (!\COUNT:counter[5]~2\ & VCC))
-- \COUNT:counter[6]~2\ = CARRY((\COUNT:counter[6]~q\ & !\COUNT:counter[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[6]~q\,
	datad => VCC,
	cin => \COUNT:counter[5]~2\,
	combout => \COUNT:counter[6]~1_combout\,
	cout => \COUNT:counter[6]~2\);

-- Location: FF_X12_Y4_N29
\COUNT:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[6]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[6]~q\);

-- Location: LCCOMB_X12_Y4_N30
\COUNT:counter[7]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[7]~1_combout\ = (\COUNT:counter[7]~q\ & (!\COUNT:counter[6]~2\)) # (!\COUNT:counter[7]~q\ & ((\COUNT:counter[6]~2\) # (GND)))
-- \COUNT:counter[7]~2\ = CARRY((!\COUNT:counter[6]~2\) # (!\COUNT:counter[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[7]~q\,
	datad => VCC,
	cin => \COUNT:counter[6]~2\,
	combout => \COUNT:counter[7]~1_combout\,
	cout => \COUNT:counter[7]~2\);

-- Location: FF_X12_Y4_N31
\COUNT:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[7]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[7]~q\);

-- Location: LCCOMB_X12_Y3_N0
\COUNT:counter[8]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[8]~1_combout\ = (\COUNT:counter[8]~q\ & (\COUNT:counter[7]~2\ $ (GND))) # (!\COUNT:counter[8]~q\ & (!\COUNT:counter[7]~2\ & VCC))
-- \COUNT:counter[8]~2\ = CARRY((\COUNT:counter[8]~q\ & !\COUNT:counter[7]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[8]~q\,
	datad => VCC,
	cin => \COUNT:counter[7]~2\,
	combout => \COUNT:counter[8]~1_combout\,
	cout => \COUNT:counter[8]~2\);

-- Location: FF_X12_Y3_N1
\COUNT:counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[8]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[8]~q\);

-- Location: LCCOMB_X12_Y3_N2
\COUNT:counter[9]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[9]~1_combout\ = (\COUNT:counter[9]~q\ & (!\COUNT:counter[8]~2\)) # (!\COUNT:counter[9]~q\ & ((\COUNT:counter[8]~2\) # (GND)))
-- \COUNT:counter[9]~2\ = CARRY((!\COUNT:counter[8]~2\) # (!\COUNT:counter[9]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[9]~q\,
	datad => VCC,
	cin => \COUNT:counter[8]~2\,
	combout => \COUNT:counter[9]~1_combout\,
	cout => \COUNT:counter[9]~2\);

-- Location: FF_X12_Y3_N3
\COUNT:counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[9]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[9]~q\);

-- Location: LCCOMB_X12_Y3_N4
\COUNT:counter[10]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[10]~1_combout\ = (\COUNT:counter[10]~q\ & (\COUNT:counter[9]~2\ $ (GND))) # (!\COUNT:counter[10]~q\ & (!\COUNT:counter[9]~2\ & VCC))
-- \COUNT:counter[10]~2\ = CARRY((\COUNT:counter[10]~q\ & !\COUNT:counter[9]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[10]~q\,
	datad => VCC,
	cin => \COUNT:counter[9]~2\,
	combout => \COUNT:counter[10]~1_combout\,
	cout => \COUNT:counter[10]~2\);

-- Location: FF_X12_Y3_N5
\COUNT:counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[10]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[10]~q\);

-- Location: LCCOMB_X12_Y3_N6
\COUNT:counter[11]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[11]~1_combout\ = (\COUNT:counter[11]~q\ & (!\COUNT:counter[10]~2\)) # (!\COUNT:counter[11]~q\ & ((\COUNT:counter[10]~2\) # (GND)))
-- \COUNT:counter[11]~2\ = CARRY((!\COUNT:counter[10]~2\) # (!\COUNT:counter[11]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[11]~q\,
	datad => VCC,
	cin => \COUNT:counter[10]~2\,
	combout => \COUNT:counter[11]~1_combout\,
	cout => \COUNT:counter[11]~2\);

-- Location: FF_X12_Y3_N7
\COUNT:counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[11]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[11]~q\);

-- Location: LCCOMB_X12_Y3_N8
\COUNT:counter[12]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[12]~1_combout\ = (\COUNT:counter[12]~q\ & (\COUNT:counter[11]~2\ $ (GND))) # (!\COUNT:counter[12]~q\ & (!\COUNT:counter[11]~2\ & VCC))
-- \COUNT:counter[12]~2\ = CARRY((\COUNT:counter[12]~q\ & !\COUNT:counter[11]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[12]~q\,
	datad => VCC,
	cin => \COUNT:counter[11]~2\,
	combout => \COUNT:counter[12]~1_combout\,
	cout => \COUNT:counter[12]~2\);

-- Location: FF_X12_Y3_N9
\COUNT:counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[12]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[12]~q\);

-- Location: LCCOMB_X12_Y3_N10
\COUNT:counter[13]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[13]~1_combout\ = (\COUNT:counter[13]~q\ & (!\COUNT:counter[12]~2\)) # (!\COUNT:counter[13]~q\ & ((\COUNT:counter[12]~2\) # (GND)))
-- \COUNT:counter[13]~2\ = CARRY((!\COUNT:counter[12]~2\) # (!\COUNT:counter[13]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[13]~q\,
	datad => VCC,
	cin => \COUNT:counter[12]~2\,
	combout => \COUNT:counter[13]~1_combout\,
	cout => \COUNT:counter[13]~2\);

-- Location: FF_X12_Y3_N11
\COUNT:counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[13]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[13]~q\);

-- Location: LCCOMB_X12_Y3_N12
\COUNT:counter[14]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[14]~1_combout\ = (\COUNT:counter[14]~q\ & (\COUNT:counter[13]~2\ $ (GND))) # (!\COUNT:counter[14]~q\ & (!\COUNT:counter[13]~2\ & VCC))
-- \COUNT:counter[14]~2\ = CARRY((\COUNT:counter[14]~q\ & !\COUNT:counter[13]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[14]~q\,
	datad => VCC,
	cin => \COUNT:counter[13]~2\,
	combout => \COUNT:counter[14]~1_combout\,
	cout => \COUNT:counter[14]~2\);

-- Location: FF_X12_Y3_N13
\COUNT:counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[14]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[14]~q\);

-- Location: LCCOMB_X12_Y3_N14
\COUNT:counter[15]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[15]~1_combout\ = (\COUNT:counter[15]~q\ & (!\COUNT:counter[14]~2\)) # (!\COUNT:counter[15]~q\ & ((\COUNT:counter[14]~2\) # (GND)))
-- \COUNT:counter[15]~2\ = CARRY((!\COUNT:counter[14]~2\) # (!\COUNT:counter[15]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[15]~q\,
	datad => VCC,
	cin => \COUNT:counter[14]~2\,
	combout => \COUNT:counter[15]~1_combout\,
	cout => \COUNT:counter[15]~2\);

-- Location: FF_X12_Y3_N15
\COUNT:counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[15]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[15]~q\);

-- Location: LCCOMB_X12_Y3_N16
\COUNT:counter[16]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \COUNT:counter[16]~1_combout\ = \COUNT:counter[15]~2\ $ (!\COUNT:counter[16]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \COUNT:counter[16]~q\,
	cin => \COUNT:counter[15]~2\,
	combout => \COUNT:counter[16]~1_combout\);

-- Location: FF_X12_Y3_N17
\COUNT:counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \COUNT:counter[16]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \Equal1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[16]~q\);

-- Location: LCCOMB_X12_Y3_N30
\Equal0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (!\COUNT:counter[11]~q\ & (!\COUNT:counter[8]~q\ & (!\COUNT:counter[10]~q\ & !\COUNT:counter[9]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[11]~q\,
	datab => \COUNT:counter[8]~q\,
	datac => \COUNT:counter[10]~q\,
	datad => \COUNT:counter[9]~q\,
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X11_Y3_N18
\Equal0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = (!\COUNT:counter[12]~q\ & (!\COUNT:counter[14]~q\ & (!\COUNT:counter[13]~q\ & !\COUNT:counter[15]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[12]~q\,
	datab => \COUNT:counter[14]~q\,
	datac => \COUNT:counter[13]~q\,
	datad => \COUNT:counter[15]~q\,
	combout => \Equal0~3_combout\);

-- Location: LCCOMB_X12_Y4_N2
\Equal0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (!\COUNT:counter[6]~q\ & (!\COUNT:counter[5]~q\ & (!\COUNT:counter[4]~q\ & !\COUNT:counter[7]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[6]~q\,
	datab => \COUNT:counter[5]~q\,
	datac => \COUNT:counter[4]~q\,
	datad => \COUNT:counter[7]~q\,
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X12_Y4_N8
\Equal0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\COUNT:counter[2]~q\ & (!\COUNT:counter[3]~q\ & (!\COUNT:counter[1]~q\ & !\COUNT:counter[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \COUNT:counter[2]~q\,
	datab => \COUNT:counter[3]~q\,
	datac => \COUNT:counter[1]~q\,
	datad => \COUNT:counter[0]~q\,
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X11_Y3_N28
\Equal0~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal0~4_combout\ = (\Equal0~2_combout\ & (\Equal0~3_combout\ & (\Equal0~1_combout\ & \Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~2_combout\,
	datab => \Equal0~3_combout\,
	datac => \Equal0~1_combout\,
	datad => \Equal0~0_combout\,
	combout => \Equal0~4_combout\);

-- Location: LCCOMB_X11_Y3_N24
\audior~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \audior~0_combout\ = \audior~reg0_q\ $ (((!\COUNT:counter[16]~q\ & \Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \COUNT:counter[16]~q\,
	datac => \audior~reg0_q\,
	datad => \Equal0~4_combout\,
	combout => \audior~0_combout\);

-- Location: FF_X11_Y3_N25
\audior~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_clk_dev~inputclkctrl_outclk\,
	d => \audior~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \audior~reg0_q\);

-- Location: UNVM_X0_Y8_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

ww_audiol <= \audiol~output_o\;

ww_audior <= \audior~output_o\;
END structure;


