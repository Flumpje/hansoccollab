transcript on
if {[file exists gate_work]} {
	vdel -lib gate_work -all
}
vlib gate_work
vmap work gate_work

vcom -93 -work work {readkey.vho}

vcom -93 -work work {C:/HAN/hansoccollab/Willem/assignment_2_readKey/readkey_tb.vhd}

vsim -t 1ps +transport_int_delays +transport_path_delays -sdftyp /NA=readkey_vhd.sdo -L altera -L altera_lnsim -L fiftyfivenm -L gate_work -L work -voptargs="+acc"  readkey_tb

add wave *
view structure
view signals
run -all
