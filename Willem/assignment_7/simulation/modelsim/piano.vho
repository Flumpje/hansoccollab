-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "06/10/2020 21:58:42"

-- 
-- Device: Altera 10M50DAF484C7G Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_G2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_L4,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_F8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	piano IS
    PORT (
	MAX10_CLK1_50 : IN std_logic;
	arduino_io4 : IN std_logic;
	arduino_io5 : IN std_logic;
	arduino_io3 : OUT std_logic;
	HEX0 : OUT std_logic_vector(0 TO 7);
	HEX1 : OUT std_logic_vector(0 TO 7);
	HEX2 : OUT std_logic_vector(0 TO 7);
	HEX3 : OUT std_logic_vector(0 TO 7);
	HEX4 : OUT std_logic_vector(0 TO 7);
	HEX5 : OUT std_logic_vector(0 TO 7);
	KEY : IN std_logic_vector(0 TO 2);
	LEDR : OUT std_logic_vector(0 TO 9)
	);
END piano;

-- Design Ports Information
-- arduino_io3	=>  Location: PIN_AB8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[7]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[5]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[4]	=>  Location: PIN_E16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[3]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[2]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[1]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[7]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[6]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[5]	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[3]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[2]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[1]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[0]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[7]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[6]	=>  Location: PIN_B22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[5]	=>  Location: PIN_C22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[4]	=>  Location: PIN_B21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[3]	=>  Location: PIN_A21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[2]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[1]	=>  Location: PIN_A20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[0]	=>  Location: PIN_B20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[7]	=>  Location: PIN_D22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[6]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[5]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[4]	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[3]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[2]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[1]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[0]	=>  Location: PIN_F21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[7]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[6]	=>  Location: PIN_F20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[5]	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[4]	=>  Location: PIN_H19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[3]	=>  Location: PIN_J18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[2]	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[1]	=>  Location: PIN_E20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[0]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[7]	=>  Location: PIN_L19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[6]	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[5]	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[4]	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[3]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[2]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[1]	=>  Location: PIN_K20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[0]	=>  Location: PIN_J20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_R9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_B8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_A9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- MAX10_CLK1_50	=>  Location: PIN_N14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- arduino_io4	=>  Location: PIN_AB9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- arduino_io5	=>  Location: PIN_Y10,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF piano IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_MAX10_CLK1_50 : std_logic;
SIGNAL ww_arduino_io4 : std_logic;
SIGNAL ww_arduino_io5 : std_logic;
SIGNAL ww_arduino_io3 : std_logic;
SIGNAL ww_HEX0 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX1 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX2 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX3 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX4 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX5 : std_logic_vector(0 TO 7);
SIGNAL ww_KEY : std_logic_vector(0 TO 2);
SIGNAL ww_LEDR : std_logic_vector(0 TO 9);
SIGNAL \~QUARTUS_CREATED_ADC1~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC2~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \L_tone_generation|L_clock_generator|Equal0~19clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \L_readkey|L_ClockDomainCrossing|c3|Q~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \L_tone_generation|L_clock_generator|Selector9~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \L_tone_generation|L_clock_generator|Selector0~3clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \MAX10_CLK1_50~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC1~~eoc\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC2~~eoc\ : std_logic;
SIGNAL \arduino_io3~output_o\ : std_logic;
SIGNAL \HEX0[7]~output_o\ : std_logic;
SIGNAL \HEX0[6]~output_o\ : std_logic;
SIGNAL \HEX0[5]~output_o\ : std_logic;
SIGNAL \HEX0[4]~output_o\ : std_logic;
SIGNAL \HEX0[3]~output_o\ : std_logic;
SIGNAL \HEX0[2]~output_o\ : std_logic;
SIGNAL \HEX0[1]~output_o\ : std_logic;
SIGNAL \HEX0[0]~output_o\ : std_logic;
SIGNAL \HEX1[7]~output_o\ : std_logic;
SIGNAL \HEX1[6]~output_o\ : std_logic;
SIGNAL \HEX1[5]~output_o\ : std_logic;
SIGNAL \HEX1[4]~output_o\ : std_logic;
SIGNAL \HEX1[3]~output_o\ : std_logic;
SIGNAL \HEX1[2]~output_o\ : std_logic;
SIGNAL \HEX1[1]~output_o\ : std_logic;
SIGNAL \HEX1[0]~output_o\ : std_logic;
SIGNAL \HEX2[7]~output_o\ : std_logic;
SIGNAL \HEX2[6]~output_o\ : std_logic;
SIGNAL \HEX2[5]~output_o\ : std_logic;
SIGNAL \HEX2[4]~output_o\ : std_logic;
SIGNAL \HEX2[3]~output_o\ : std_logic;
SIGNAL \HEX2[2]~output_o\ : std_logic;
SIGNAL \HEX2[1]~output_o\ : std_logic;
SIGNAL \HEX2[0]~output_o\ : std_logic;
SIGNAL \HEX3[7]~output_o\ : std_logic;
SIGNAL \HEX3[6]~output_o\ : std_logic;
SIGNAL \HEX3[5]~output_o\ : std_logic;
SIGNAL \HEX3[4]~output_o\ : std_logic;
SIGNAL \HEX3[3]~output_o\ : std_logic;
SIGNAL \HEX3[2]~output_o\ : std_logic;
SIGNAL \HEX3[1]~output_o\ : std_logic;
SIGNAL \HEX3[0]~output_o\ : std_logic;
SIGNAL \HEX4[7]~output_o\ : std_logic;
SIGNAL \HEX4[6]~output_o\ : std_logic;
SIGNAL \HEX4[5]~output_o\ : std_logic;
SIGNAL \HEX4[4]~output_o\ : std_logic;
SIGNAL \HEX4[3]~output_o\ : std_logic;
SIGNAL \HEX4[2]~output_o\ : std_logic;
SIGNAL \HEX4[1]~output_o\ : std_logic;
SIGNAL \HEX4[0]~output_o\ : std_logic;
SIGNAL \HEX5[7]~output_o\ : std_logic;
SIGNAL \HEX5[6]~output_o\ : std_logic;
SIGNAL \HEX5[5]~output_o\ : std_logic;
SIGNAL \HEX5[4]~output_o\ : std_logic;
SIGNAL \HEX5[3]~output_o\ : std_logic;
SIGNAL \HEX5[2]~output_o\ : std_logic;
SIGNAL \HEX5[1]~output_o\ : std_logic;
SIGNAL \HEX5[0]~output_o\ : std_logic;
SIGNAL \LEDR[9]~output_o\ : std_logic;
SIGNAL \LEDR[8]~output_o\ : std_logic;
SIGNAL \LEDR[7]~output_o\ : std_logic;
SIGNAL \LEDR[6]~output_o\ : std_logic;
SIGNAL \LEDR[5]~output_o\ : std_logic;
SIGNAL \LEDR[4]~output_o\ : std_logic;
SIGNAL \LEDR[3]~output_o\ : std_logic;
SIGNAL \LEDR[2]~output_o\ : std_logic;
SIGNAL \LEDR[1]~output_o\ : std_logic;
SIGNAL \LEDR[0]~output_o\ : std_logic;
SIGNAL \MAX10_CLK1_50~input_o\ : std_logic;
SIGNAL \MAX10_CLK1_50~inputclkctrl_outclk\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[0]~1_combout\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[2]~2\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[3]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[3]~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[3]~2\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[4]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[4]~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[4]~2\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[5]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[5]~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[5]~2\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[6]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[6]~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[6]~2\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[7]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[7]~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[7]~2\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[8]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[8]~feeder_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[8]~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[0]~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[0]~2\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[1]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[1]~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[1]~2\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[2]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|devider:counter[2]~q\ : std_logic;
SIGNAL \arduino_io4~input_o\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c2|Q~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c2|Q~q\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c3|Q~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c3|Q~q\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c3|Q~clkctrl_outclk\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[0]~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[2]~2\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[3]~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Equal2~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[3]~2\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[4]~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[4]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[4]~2\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[5]~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[5]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[5]~2\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[6]~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[6]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[6]~2\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[7]~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[7]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Equal0~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|scancode[3]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[3]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|counter~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[0]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[0]~2\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[1]~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[1]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[1]~2\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[2]~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:counter[2]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~5_combout\ : std_logic;
SIGNAL \arduino_io5~input_o\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c0|Q~q\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c1|Q~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c1|Q~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[4]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[4]~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[4]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~4_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[3]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[3]~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[3]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~2_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[1]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[1]~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[1]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~3_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[2]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[2]~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[2]~q\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|Equal0~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~8_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[7]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[7]~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[7]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[0]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[0]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~7_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[6]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[6]~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[6]~q\ : std_logic;
SIGNAL \L_readkey|L_Showkey|Decoder0~6_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[5]~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[5]~feeder_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|COUNT:byte_current[5]~q\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|Equal0~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|Equal0~2_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|byte_read~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|byte_read~1_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|byte_read~q\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|Selector2~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|current_state.state_key_released~q\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|Selector3~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|current_state.state_key_reminder~q\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|Selector4~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|current_state.state_key_reminder_2~q\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|Selector0~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|current_state.state_no_key~q\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|Selector1~0_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|current_state.state_key_pressed~q\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|dig2[3]~2_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|dig2[5]~4_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal2~11_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|dig2[1]~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal2~9_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal2~10_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|dig2[4]~3_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|dig2[0]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector8~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal1~8_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal1~5_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal1~21_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector2~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|next_state.state_1_535~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal0~6_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal0~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal0~19_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Valid_key~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Valid_key~feeder_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Valid_key~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_1~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector1~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|next_state.state_0_563~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_0~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector0~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector0~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector0~2_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector0~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|next_state.state_7_367~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_7~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector7~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|next_state.state_6_395~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_6~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector6~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|next_state.state_5_423~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_5~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector5~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|next_state.state_4_451~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_4~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector4~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|next_state.state_3_479~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_3~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_3~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector3~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|next_state.state_2_507~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|tone_state.state_2~q\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector9~2_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector9~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector9~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector9~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector9~combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|Equal2~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|Equal2~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux12~9_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux12~8_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux12~5_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux12~16_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~24_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux5~22_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux5~19_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux5~37_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux5~9_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux5~8_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux5~5_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux5~36_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~18_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~27_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux5~35_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux4~7_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux4~6_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux4~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux4~24_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux4~18_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux4~25_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux4~23_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~9_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~28_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~29_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~10_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~23_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~26_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~27_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~11_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~25_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux7~4_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux7~11_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~25_combout\ : std_logic;
SIGNAL \L_tone_generation|L_clock_generator|Equal2~8_combout\ : std_logic;
SIGNAL \L_readkey|L_Constantkey|dig2[6]~5_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux8~19_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux7~9_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux7~10_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|LessThan0~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux8~11_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux8~21_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux8~22_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux8~6_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux8~20_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux9~9_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux9~18_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux9~19_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux9~4_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux9~17_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux11~20_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux11~21_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux11~22_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux11~23_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux11~18_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux11~14_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux11~19_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux11~17_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux10~16_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux10~13_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux10~25_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux10~24_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux6~24_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux10~23_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|LessThan0~2_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux0~13_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux0~14_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux0~15_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux0~16_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux0~12_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux0~11_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~9_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~8_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~11_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~28_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~29_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux2~26_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux3~6_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux3~7_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux3~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux3~34_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux3~20_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux3~17_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux3~35_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux3~33_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux1~20_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux1~21_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux1~11_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux1~6_combout\ : std_logic;
SIGNAL \L_tone_generation|L_key2pulselength|Mux1~19_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|LessThan0~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|Equal2~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|Equal2~2_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|Equal2~4_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~5_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~6_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~7_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~2_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~4_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~8_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~2\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|audiol~0_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|audiol~2_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|audiol~1_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|audiol~3_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|audiol~4_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|audiol~5_combout\ : std_logic;
SIGNAL \L_tone_generation|L_pulselength2audio|audiol~q\ : std_logic;
SIGNAL \Display0|Mux6~2_combout\ : std_logic;
SIGNAL \Display0|Mux6~3_combout\ : std_logic;
SIGNAL \Display0|Mux5~2_combout\ : std_logic;
SIGNAL \Display0|Mux5~3_combout\ : std_logic;
SIGNAL \Display0|Mux4~2_combout\ : std_logic;
SIGNAL \Display0|Mux4~3_combout\ : std_logic;
SIGNAL \Display0|Mux3~2_combout\ : std_logic;
SIGNAL \Display0|Mux3~3_combout\ : std_logic;
SIGNAL \Display0|Mux2~2_combout\ : std_logic;
SIGNAL \Display0|Mux2~3_combout\ : std_logic;
SIGNAL \Display0|Mux1~2_combout\ : std_logic;
SIGNAL \Display0|Mux1~3_combout\ : std_logic;
SIGNAL \Display0|Mux0~2_combout\ : std_logic;
SIGNAL \Display0|Mux0~3_combout\ : std_logic;
SIGNAL \Display1|Mux6~2_combout\ : std_logic;
SIGNAL \Display1|Mux6~3_combout\ : std_logic;
SIGNAL \Display1|Mux5~2_combout\ : std_logic;
SIGNAL \Display1|Mux5~3_combout\ : std_logic;
SIGNAL \Display1|Mux4~2_combout\ : std_logic;
SIGNAL \Display1|Mux4~3_combout\ : std_logic;
SIGNAL \Display1|Mux3~2_combout\ : std_logic;
SIGNAL \Display1|Mux3~3_combout\ : std_logic;
SIGNAL \Display1|Mux2~2_combout\ : std_logic;
SIGNAL \Display1|Mux2~3_combout\ : std_logic;
SIGNAL \Display1|Mux1~2_combout\ : std_logic;
SIGNAL \Display1|Mux1~3_combout\ : std_logic;
SIGNAL \Display1|Mux0~2_combout\ : std_logic;
SIGNAL \Display1|Mux0~3_combout\ : std_logic;
SIGNAL \L_readkey|L_Showkey|scancode\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \L_tone_generation|L_key2pulselength|pulslength\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\ : std_logic;
SIGNAL \Display0|ALT_INV_Mux6~3_combout\ : std_logic;
SIGNAL \Display1|ALT_INV_Mux6~3_combout\ : std_logic;
SIGNAL \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_MAX10_CLK1_50 <= MAX10_CLK1_50;
ww_arduino_io4 <= arduino_io4;
ww_arduino_io5 <= arduino_io5;
arduino_io3 <= ww_arduino_io3;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
ww_KEY <= KEY;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\~QUARTUS_CREATED_ADC1~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\~QUARTUS_CREATED_ADC2~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\L_tone_generation|L_clock_generator|Equal0~19clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \L_tone_generation|L_clock_generator|Equal0~19_combout\);

\L_readkey|L_ClockDomainCrossing|c3|Q~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \L_readkey|L_ClockDomainCrossing|c3|Q~q\);

\L_tone_generation|L_clock_generator|Selector9~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \L_tone_generation|L_clock_generator|Selector9~combout\);

\L_tone_generation|L_clock_generator|Selector0~3clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \L_tone_generation|L_clock_generator|Selector0~3_combout\);

\MAX10_CLK1_50~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \MAX10_CLK1_50~input_o\);
\L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\ <= NOT \L_readkey|L_ClockDomainCrossing|c3|Q~clkctrl_outclk\;
\Display0|ALT_INV_Mux6~3_combout\ <= NOT \Display0|Mux6~3_combout\;
\Display1|ALT_INV_Mux6~3_combout\ <= NOT \Display1|Mux6~3_combout\;
\L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\ <= NOT \L_readkey|L_ClockDomainCrossing|c3|Q~q\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X44_Y43_N16
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X31_Y0_N9
\arduino_io3~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_tone_generation|L_pulselength2audio|audiol~q\,
	devoe => ww_devoe,
	o => \arduino_io3~output_o\);

-- Location: IOOBUF_X66_Y54_N16
\HEX0[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX0[7]~output_o\);

-- Location: IOOBUF_X74_Y54_N23
\HEX0[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|ALT_INV_Mux6~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[6]~output_o\);

-- Location: IOOBUF_X74_Y54_N16
\HEX0[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux5~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[5]~output_o\);

-- Location: IOOBUF_X74_Y54_N2
\HEX0[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux4~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[4]~output_o\);

-- Location: IOOBUF_X62_Y54_N30
\HEX0[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux3~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[3]~output_o\);

-- Location: IOOBUF_X60_Y54_N2
\HEX0[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux2~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[2]~output_o\);

-- Location: IOOBUF_X74_Y54_N9
\HEX0[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux1~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[1]~output_o\);

-- Location: IOOBUF_X58_Y54_N16
\HEX0[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux0~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[0]~output_o\);

-- Location: IOOBUF_X60_Y54_N16
\HEX1[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX1[7]~output_o\);

-- Location: IOOBUF_X69_Y54_N30
\HEX1[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|ALT_INV_Mux6~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[6]~output_o\);

-- Location: IOOBUF_X66_Y54_N30
\HEX1[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux5~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[5]~output_o\);

-- Location: IOOBUF_X64_Y54_N2
\HEX1[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux4~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[4]~output_o\);

-- Location: IOOBUF_X60_Y54_N9
\HEX1[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux3~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[3]~output_o\);

-- Location: IOOBUF_X78_Y49_N2
\HEX1[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux2~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[2]~output_o\);

-- Location: IOOBUF_X78_Y49_N9
\HEX1[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux1~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[1]~output_o\);

-- Location: IOOBUF_X69_Y54_N23
\HEX1[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux0~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[0]~output_o\);

-- Location: IOOBUF_X66_Y54_N9
\HEX2[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX2[7]~output_o\);

-- Location: IOOBUF_X78_Y43_N9
\HEX2[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|ALT_INV_Mux6~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[6]~output_o\);

-- Location: IOOBUF_X78_Y35_N2
\HEX2[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux5~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[5]~output_o\);

-- Location: IOOBUF_X78_Y43_N2
\HEX2[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux4~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[4]~output_o\);

-- Location: IOOBUF_X78_Y44_N2
\HEX2[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux3~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[3]~output_o\);

-- Location: IOOBUF_X69_Y54_N16
\HEX2[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux2~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[2]~output_o\);

-- Location: IOOBUF_X66_Y54_N2
\HEX2[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux1~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[1]~output_o\);

-- Location: IOOBUF_X78_Y44_N9
\HEX2[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display0|Mux0~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[0]~output_o\);

-- Location: IOOBUF_X78_Y35_N9
\HEX3[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX3[7]~output_o\);

-- Location: IOOBUF_X78_Y43_N16
\HEX3[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|ALT_INV_Mux6~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[6]~output_o\);

-- Location: IOOBUF_X78_Y41_N2
\HEX3[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux5~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[5]~output_o\);

-- Location: IOOBUF_X78_Y41_N9
\HEX3[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux4~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[4]~output_o\);

-- Location: IOOBUF_X69_Y54_N9
\HEX3[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux3~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[3]~output_o\);

-- Location: IOOBUF_X78_Y33_N2
\HEX3[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux2~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[2]~output_o\);

-- Location: IOOBUF_X78_Y33_N9
\HEX3[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux1~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[1]~output_o\);

-- Location: IOOBUF_X78_Y35_N23
\HEX3[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Display1|Mux0~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[0]~output_o\);

-- Location: IOOBUF_X78_Y43_N23
\HEX4[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[7]~output_o\);

-- Location: IOOBUF_X78_Y35_N16
\HEX4[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[6]~output_o\);

-- Location: IOOBUF_X78_Y40_N9
\HEX4[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[5]~output_o\);

-- Location: IOOBUF_X78_Y45_N23
\HEX4[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[4]~output_o\);

-- Location: IOOBUF_X78_Y42_N16
\HEX4[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[3]~output_o\);

-- Location: IOOBUF_X78_Y40_N23
\HEX4[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[2]~output_o\);

-- Location: IOOBUF_X78_Y40_N2
\HEX4[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[1]~output_o\);

-- Location: IOOBUF_X78_Y40_N16
\HEX4[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[0]~output_o\);

-- Location: IOOBUF_X78_Y37_N9
\HEX5[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[7]~output_o\);

-- Location: IOOBUF_X78_Y34_N2
\HEX5[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[6]~output_o\);

-- Location: IOOBUF_X78_Y34_N16
\HEX5[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[5]~output_o\);

-- Location: IOOBUF_X78_Y34_N9
\HEX5[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[4]~output_o\);

-- Location: IOOBUF_X78_Y34_N24
\HEX5[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[3]~output_o\);

-- Location: IOOBUF_X78_Y37_N16
\HEX5[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[2]~output_o\);

-- Location: IOOBUF_X78_Y42_N2
\HEX5[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[1]~output_o\);

-- Location: IOOBUF_X78_Y45_N9
\HEX5[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[0]~output_o\);

-- Location: IOOBUF_X49_Y54_N9
\LEDR[9]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[9]~output_o\);

-- Location: IOOBUF_X51_Y54_N9
\LEDR[8]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[8]~output_o\);

-- Location: IOOBUF_X56_Y54_N9
\LEDR[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[7]~output_o\);

-- Location: IOOBUF_X66_Y54_N23
\LEDR[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[6]~output_o\);

-- Location: IOOBUF_X58_Y54_N23
\LEDR[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[5]~output_o\);

-- Location: IOOBUF_X56_Y54_N30
\LEDR[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[4]~output_o\);

-- Location: IOOBUF_X46_Y54_N9
\LEDR[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[3]~output_o\);

-- Location: IOOBUF_X51_Y54_N16
\LEDR[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[2]~output_o\);

-- Location: IOOBUF_X46_Y54_N23
\LEDR[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[1]~output_o\);

-- Location: IOOBUF_X46_Y54_N2
\LEDR[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \KEY[1]~input_o\,
	devoe => ww_devoe,
	o => \LEDR[0]~output_o\);

-- Location: IOIBUF_X78_Y29_N22
\MAX10_CLK1_50~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_MAX10_CLK1_50,
	o => \MAX10_CLK1_50~input_o\);

-- Location: CLKCTRL_G9
\MAX10_CLK1_50~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \MAX10_CLK1_50~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \MAX10_CLK1_50~inputclkctrl_outclk\);

-- Location: LCCOMB_X63_Y48_N6
\L_tone_generation|L_clock_generator|devider:counter[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[0]~1_combout\ = \L_tone_generation|L_clock_generator|devider:counter[0]~q\ $ (VCC)
-- \L_tone_generation|L_clock_generator|devider:counter[0]~2\ = CARRY(\L_tone_generation|L_clock_generator|devider:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|devider:counter[0]~q\,
	datad => VCC,
	combout => \L_tone_generation|L_clock_generator|devider:counter[0]~1_combout\,
	cout => \L_tone_generation|L_clock_generator|devider:counter[0]~2\);

-- Location: IOIBUF_X49_Y54_N29
\KEY[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: LCCOMB_X63_Y48_N10
\L_tone_generation|L_clock_generator|devider:counter[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[2]~1_combout\ = (\L_tone_generation|L_clock_generator|devider:counter[2]~q\ & (\L_tone_generation|L_clock_generator|devider:counter[1]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_clock_generator|devider:counter[2]~q\ & (!\L_tone_generation|L_clock_generator|devider:counter[1]~2\ & VCC))
-- \L_tone_generation|L_clock_generator|devider:counter[2]~2\ = CARRY((\L_tone_generation|L_clock_generator|devider:counter[2]~q\ & !\L_tone_generation|L_clock_generator|devider:counter[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|devider:counter[2]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_clock_generator|devider:counter[1]~2\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[2]~1_combout\,
	cout => \L_tone_generation|L_clock_generator|devider:counter[2]~2\);

-- Location: LCCOMB_X63_Y48_N12
\L_tone_generation|L_clock_generator|devider:counter[3]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[3]~1_combout\ = (\L_tone_generation|L_clock_generator|devider:counter[3]~q\ & (!\L_tone_generation|L_clock_generator|devider:counter[2]~2\)) # 
-- (!\L_tone_generation|L_clock_generator|devider:counter[3]~q\ & ((\L_tone_generation|L_clock_generator|devider:counter[2]~2\) # (GND)))
-- \L_tone_generation|L_clock_generator|devider:counter[3]~2\ = CARRY((!\L_tone_generation|L_clock_generator|devider:counter[2]~2\) # (!\L_tone_generation|L_clock_generator|devider:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|devider:counter[3]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_clock_generator|devider:counter[2]~2\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[3]~1_combout\,
	cout => \L_tone_generation|L_clock_generator|devider:counter[3]~2\);

-- Location: FF_X63_Y48_N13
\L_tone_generation|L_clock_generator|devider:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|devider:counter[3]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[3]~q\);

-- Location: LCCOMB_X63_Y48_N14
\L_tone_generation|L_clock_generator|devider:counter[4]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[4]~1_combout\ = (\L_tone_generation|L_clock_generator|devider:counter[4]~q\ & (\L_tone_generation|L_clock_generator|devider:counter[3]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_clock_generator|devider:counter[4]~q\ & (!\L_tone_generation|L_clock_generator|devider:counter[3]~2\ & VCC))
-- \L_tone_generation|L_clock_generator|devider:counter[4]~2\ = CARRY((\L_tone_generation|L_clock_generator|devider:counter[4]~q\ & !\L_tone_generation|L_clock_generator|devider:counter[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|devider:counter[4]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_clock_generator|devider:counter[3]~2\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[4]~1_combout\,
	cout => \L_tone_generation|L_clock_generator|devider:counter[4]~2\);

-- Location: FF_X63_Y48_N15
\L_tone_generation|L_clock_generator|devider:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|devider:counter[4]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[4]~q\);

-- Location: LCCOMB_X63_Y48_N16
\L_tone_generation|L_clock_generator|devider:counter[5]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[5]~1_combout\ = (\L_tone_generation|L_clock_generator|devider:counter[5]~q\ & (!\L_tone_generation|L_clock_generator|devider:counter[4]~2\)) # 
-- (!\L_tone_generation|L_clock_generator|devider:counter[5]~q\ & ((\L_tone_generation|L_clock_generator|devider:counter[4]~2\) # (GND)))
-- \L_tone_generation|L_clock_generator|devider:counter[5]~2\ = CARRY((!\L_tone_generation|L_clock_generator|devider:counter[4]~2\) # (!\L_tone_generation|L_clock_generator|devider:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|devider:counter[5]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_clock_generator|devider:counter[4]~2\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[5]~1_combout\,
	cout => \L_tone_generation|L_clock_generator|devider:counter[5]~2\);

-- Location: FF_X63_Y48_N17
\L_tone_generation|L_clock_generator|devider:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|devider:counter[5]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[5]~q\);

-- Location: LCCOMB_X63_Y48_N18
\L_tone_generation|L_clock_generator|devider:counter[6]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[6]~1_combout\ = (\L_tone_generation|L_clock_generator|devider:counter[6]~q\ & (\L_tone_generation|L_clock_generator|devider:counter[5]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_clock_generator|devider:counter[6]~q\ & (!\L_tone_generation|L_clock_generator|devider:counter[5]~2\ & VCC))
-- \L_tone_generation|L_clock_generator|devider:counter[6]~2\ = CARRY((\L_tone_generation|L_clock_generator|devider:counter[6]~q\ & !\L_tone_generation|L_clock_generator|devider:counter[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|devider:counter[6]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_clock_generator|devider:counter[5]~2\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[6]~1_combout\,
	cout => \L_tone_generation|L_clock_generator|devider:counter[6]~2\);

-- Location: FF_X63_Y48_N19
\L_tone_generation|L_clock_generator|devider:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|devider:counter[6]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[6]~q\);

-- Location: LCCOMB_X63_Y48_N20
\L_tone_generation|L_clock_generator|devider:counter[7]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[7]~1_combout\ = (\L_tone_generation|L_clock_generator|devider:counter[7]~q\ & (!\L_tone_generation|L_clock_generator|devider:counter[6]~2\)) # 
-- (!\L_tone_generation|L_clock_generator|devider:counter[7]~q\ & ((\L_tone_generation|L_clock_generator|devider:counter[6]~2\) # (GND)))
-- \L_tone_generation|L_clock_generator|devider:counter[7]~2\ = CARRY((!\L_tone_generation|L_clock_generator|devider:counter[6]~2\) # (!\L_tone_generation|L_clock_generator|devider:counter[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|devider:counter[7]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_clock_generator|devider:counter[6]~2\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[7]~1_combout\,
	cout => \L_tone_generation|L_clock_generator|devider:counter[7]~2\);

-- Location: FF_X63_Y48_N21
\L_tone_generation|L_clock_generator|devider:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|devider:counter[7]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[7]~q\);

-- Location: LCCOMB_X63_Y48_N22
\L_tone_generation|L_clock_generator|devider:counter[8]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[8]~1_combout\ = \L_tone_generation|L_clock_generator|devider:counter[7]~2\ $ (!\L_tone_generation|L_clock_generator|devider:counter[8]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	cin => \L_tone_generation|L_clock_generator|devider:counter[7]~2\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[8]~1_combout\);

-- Location: LCCOMB_X63_Y48_N2
\L_tone_generation|L_clock_generator|devider:counter[8]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[8]~feeder_combout\ = \L_tone_generation|L_clock_generator|devider:counter[8]~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_tone_generation|L_clock_generator|devider:counter[8]~1_combout\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[8]~feeder_combout\);

-- Location: FF_X63_Y48_N3
\L_tone_generation|L_clock_generator|devider:counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	d => \L_tone_generation|L_clock_generator|devider:counter[8]~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[8]~q\);

-- Location: FF_X64_Y48_N19
\L_tone_generation|L_clock_generator|devider:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	asdata => \L_tone_generation|L_clock_generator|devider:counter[0]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[0]~q\);

-- Location: LCCOMB_X63_Y48_N8
\L_tone_generation|L_clock_generator|devider:counter[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|devider:counter[1]~1_combout\ = (\L_tone_generation|L_clock_generator|devider:counter[1]~q\ & (!\L_tone_generation|L_clock_generator|devider:counter[0]~2\)) # 
-- (!\L_tone_generation|L_clock_generator|devider:counter[1]~q\ & ((\L_tone_generation|L_clock_generator|devider:counter[0]~2\) # (GND)))
-- \L_tone_generation|L_clock_generator|devider:counter[1]~2\ = CARRY((!\L_tone_generation|L_clock_generator|devider:counter[0]~2\) # (!\L_tone_generation|L_clock_generator|devider:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|devider:counter[1]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_clock_generator|devider:counter[0]~2\,
	combout => \L_tone_generation|L_clock_generator|devider:counter[1]~1_combout\,
	cout => \L_tone_generation|L_clock_generator|devider:counter[1]~2\);

-- Location: FF_X64_Y48_N21
\L_tone_generation|L_clock_generator|devider:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	asdata => \L_tone_generation|L_clock_generator|devider:counter[1]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[1]~q\);

-- Location: FF_X63_Y48_N11
\L_tone_generation|L_clock_generator|devider:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|devider:counter[2]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_clock_generator|devider:counter[8]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|devider:counter[2]~q\);

-- Location: IOIBUF_X34_Y0_N15
\arduino_io4~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_arduino_io4,
	o => \arduino_io4~input_o\);

-- Location: LCCOMB_X64_Y51_N2
\L_readkey|L_ClockDomainCrossing|c2|Q~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_ClockDomainCrossing|c2|Q~feeder_combout\ = \arduino_io4~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \arduino_io4~input_o\,
	combout => \L_readkey|L_ClockDomainCrossing|c2|Q~feeder_combout\);

-- Location: FF_X64_Y51_N3
\L_readkey|L_ClockDomainCrossing|c2|Q\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	d => \L_readkey|L_ClockDomainCrossing|c2|Q~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_ClockDomainCrossing|c2|Q~q\);

-- Location: LCCOMB_X64_Y51_N30
\L_readkey|L_ClockDomainCrossing|c3|Q~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_ClockDomainCrossing|c3|Q~feeder_combout\ = \L_readkey|L_ClockDomainCrossing|c2|Q~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_ClockDomainCrossing|c2|Q~q\,
	combout => \L_readkey|L_ClockDomainCrossing|c3|Q~feeder_combout\);

-- Location: FF_X64_Y51_N31
\L_readkey|L_ClockDomainCrossing|c3|Q\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	d => \L_readkey|L_ClockDomainCrossing|c3|Q~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_ClockDomainCrossing|c3|Q~q\);

-- Location: CLKCTRL_G10
\L_readkey|L_ClockDomainCrossing|c3|Q~clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \L_readkey|L_ClockDomainCrossing|c3|Q~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \L_readkey|L_ClockDomainCrossing|c3|Q~clkctrl_outclk\);

-- Location: LCCOMB_X66_Y50_N12
\L_readkey|L_Showkey|COUNT:counter[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[0]~1_combout\ = \L_readkey|L_Showkey|COUNT:counter[0]~q\ $ (VCC)
-- \L_readkey|L_Showkey|COUNT:counter[0]~2\ = CARRY(\L_readkey|L_Showkey|COUNT:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datad => VCC,
	combout => \L_readkey|L_Showkey|COUNT:counter[0]~1_combout\,
	cout => \L_readkey|L_Showkey|COUNT:counter[0]~2\);

-- Location: LCCOMB_X66_Y50_N16
\L_readkey|L_Showkey|COUNT:counter[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[2]~1_combout\ = (\L_readkey|L_Showkey|COUNT:counter[2]~q\ & (\L_readkey|L_Showkey|COUNT:counter[1]~2\ $ (GND))) # (!\L_readkey|L_Showkey|COUNT:counter[2]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[1]~2\ & VCC))
-- \L_readkey|L_Showkey|COUNT:counter[2]~2\ = CARRY((\L_readkey|L_Showkey|COUNT:counter[2]~q\ & !\L_readkey|L_Showkey|COUNT:counter[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	datad => VCC,
	cin => \L_readkey|L_Showkey|COUNT:counter[1]~2\,
	combout => \L_readkey|L_Showkey|COUNT:counter[2]~1_combout\,
	cout => \L_readkey|L_Showkey|COUNT:counter[2]~2\);

-- Location: LCCOMB_X66_Y50_N18
\L_readkey|L_Showkey|COUNT:counter[3]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[3]~1_combout\ = (\L_readkey|L_Showkey|COUNT:counter[3]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[2]~2\)) # (!\L_readkey|L_Showkey|COUNT:counter[3]~q\ & ((\L_readkey|L_Showkey|COUNT:counter[2]~2\) # (GND)))
-- \L_readkey|L_Showkey|COUNT:counter[3]~2\ = CARRY((!\L_readkey|L_Showkey|COUNT:counter[2]~2\) # (!\L_readkey|L_Showkey|COUNT:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|COUNT:counter[3]~q\,
	datad => VCC,
	cin => \L_readkey|L_Showkey|COUNT:counter[2]~2\,
	combout => \L_readkey|L_Showkey|COUNT:counter[3]~1_combout\,
	cout => \L_readkey|L_Showkey|COUNT:counter[3]~2\);

-- Location: LCCOMB_X66_Y50_N4
\L_readkey|L_Showkey|Equal2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Equal2~0_combout\ = (!\L_readkey|L_Showkey|COUNT:counter[0]~q\ & !\L_readkey|L_Showkey|COUNT:counter[2]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datad => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	combout => \L_readkey|L_Showkey|Equal2~0_combout\);

-- Location: LCCOMB_X66_Y50_N20
\L_readkey|L_Showkey|COUNT:counter[4]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[4]~1_combout\ = (\L_readkey|L_Showkey|COUNT:counter[4]~q\ & (\L_readkey|L_Showkey|COUNT:counter[3]~2\ $ (GND))) # (!\L_readkey|L_Showkey|COUNT:counter[4]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[3]~2\ & VCC))
-- \L_readkey|L_Showkey|COUNT:counter[4]~2\ = CARRY((\L_readkey|L_Showkey|COUNT:counter[4]~q\ & !\L_readkey|L_Showkey|COUNT:counter[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[4]~q\,
	datad => VCC,
	cin => \L_readkey|L_Showkey|COUNT:counter[3]~2\,
	combout => \L_readkey|L_Showkey|COUNT:counter[4]~1_combout\,
	cout => \L_readkey|L_Showkey|COUNT:counter[4]~2\);

-- Location: FF_X66_Y50_N21
\L_readkey|L_Showkey|COUNT:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:counter[4]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_readkey|L_Showkey|counter~0_combout\,
	ena => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:counter[4]~q\);

-- Location: LCCOMB_X66_Y50_N22
\L_readkey|L_Showkey|COUNT:counter[5]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[5]~1_combout\ = (\L_readkey|L_Showkey|COUNT:counter[5]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[4]~2\)) # (!\L_readkey|L_Showkey|COUNT:counter[5]~q\ & ((\L_readkey|L_Showkey|COUNT:counter[4]~2\) # (GND)))
-- \L_readkey|L_Showkey|COUNT:counter[5]~2\ = CARRY((!\L_readkey|L_Showkey|COUNT:counter[4]~2\) # (!\L_readkey|L_Showkey|COUNT:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|COUNT:counter[5]~q\,
	datad => VCC,
	cin => \L_readkey|L_Showkey|COUNT:counter[4]~2\,
	combout => \L_readkey|L_Showkey|COUNT:counter[5]~1_combout\,
	cout => \L_readkey|L_Showkey|COUNT:counter[5]~2\);

-- Location: FF_X66_Y50_N23
\L_readkey|L_Showkey|COUNT:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:counter[5]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_readkey|L_Showkey|counter~0_combout\,
	ena => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:counter[5]~q\);

-- Location: LCCOMB_X66_Y50_N24
\L_readkey|L_Showkey|COUNT:counter[6]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[6]~1_combout\ = (\L_readkey|L_Showkey|COUNT:counter[6]~q\ & (\L_readkey|L_Showkey|COUNT:counter[5]~2\ $ (GND))) # (!\L_readkey|L_Showkey|COUNT:counter[6]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[5]~2\ & VCC))
-- \L_readkey|L_Showkey|COUNT:counter[6]~2\ = CARRY((\L_readkey|L_Showkey|COUNT:counter[6]~q\ & !\L_readkey|L_Showkey|COUNT:counter[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[6]~q\,
	datad => VCC,
	cin => \L_readkey|L_Showkey|COUNT:counter[5]~2\,
	combout => \L_readkey|L_Showkey|COUNT:counter[6]~1_combout\,
	cout => \L_readkey|L_Showkey|COUNT:counter[6]~2\);

-- Location: FF_X66_Y50_N25
\L_readkey|L_Showkey|COUNT:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:counter[6]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_readkey|L_Showkey|counter~0_combout\,
	ena => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:counter[6]~q\);

-- Location: LCCOMB_X66_Y50_N26
\L_readkey|L_Showkey|COUNT:counter[7]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[7]~1_combout\ = \L_readkey|L_Showkey|COUNT:counter[7]~q\ $ (\L_readkey|L_Showkey|COUNT:counter[6]~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[7]~q\,
	cin => \L_readkey|L_Showkey|COUNT:counter[6]~2\,
	combout => \L_readkey|L_Showkey|COUNT:counter[7]~1_combout\);

-- Location: FF_X66_Y50_N27
\L_readkey|L_Showkey|COUNT:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:counter[7]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_readkey|L_Showkey|counter~0_combout\,
	ena => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:counter[7]~q\);

-- Location: LCCOMB_X66_Y50_N6
\L_readkey|L_Showkey|Equal0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Equal0~0_combout\ = (!\L_readkey|L_Showkey|COUNT:counter[5]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[6]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[7]~q\ & !\L_readkey|L_Showkey|COUNT:counter[4]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[5]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[6]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[7]~q\,
	datad => \L_readkey|L_Showkey|COUNT:counter[4]~q\,
	combout => \L_readkey|L_Showkey|Equal0~0_combout\);

-- Location: LCCOMB_X66_Y50_N28
\L_readkey|L_Showkey|scancode[3]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|scancode[3]~0_combout\ = (\L_readkey|L_Showkey|COUNT:counter[1]~q\ & (\L_readkey|L_Showkey|COUNT:counter[3]~q\ & (\L_readkey|L_Showkey|Equal2~0_combout\ & \L_readkey|L_Showkey|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[3]~q\,
	datac => \L_readkey|L_Showkey|Equal2~0_combout\,
	datad => \L_readkey|L_Showkey|Equal0~0_combout\,
	combout => \L_readkey|L_Showkey|scancode[3]~0_combout\);

-- Location: LCCOMB_X66_Y50_N0
\L_readkey|L_Showkey|COUNT:counter[3]~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\ = (\L_readkey|L_Showkey|scancode[3]~0_combout\) # (!\L_readkey|L_Showkey|counter~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	datad => \L_readkey|L_Showkey|counter~0_combout\,
	combout => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\);

-- Location: FF_X66_Y50_N19
\L_readkey|L_Showkey|COUNT:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:counter[3]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_readkey|L_Showkey|counter~0_combout\,
	ena => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:counter[3]~q\);

-- Location: LCCOMB_X67_Y50_N4
\L_readkey|L_Showkey|counter~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|counter~0_combout\ = ((\L_readkey|L_Showkey|COUNT:counter[3]~q\ & ((\L_readkey|L_Showkey|COUNT:counter[1]~q\) # (\L_readkey|L_Showkey|COUNT:counter[2]~q\)))) # (!\L_readkey|L_Showkey|Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[3]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	datad => \L_readkey|L_Showkey|Equal0~0_combout\,
	combout => \L_readkey|L_Showkey|counter~0_combout\);

-- Location: FF_X66_Y50_N13
\L_readkey|L_Showkey|COUNT:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:counter[0]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_readkey|L_Showkey|counter~0_combout\,
	ena => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:counter[0]~q\);

-- Location: LCCOMB_X66_Y50_N14
\L_readkey|L_Showkey|COUNT:counter[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:counter[1]~1_combout\ = (\L_readkey|L_Showkey|COUNT:counter[1]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[0]~2\)) # (!\L_readkey|L_Showkey|COUNT:counter[1]~q\ & ((\L_readkey|L_Showkey|COUNT:counter[0]~2\) # (GND)))
-- \L_readkey|L_Showkey|COUNT:counter[1]~2\ = CARRY((!\L_readkey|L_Showkey|COUNT:counter[0]~2\) # (!\L_readkey|L_Showkey|COUNT:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datad => VCC,
	cin => \L_readkey|L_Showkey|COUNT:counter[0]~2\,
	combout => \L_readkey|L_Showkey|COUNT:counter[1]~1_combout\,
	cout => \L_readkey|L_Showkey|COUNT:counter[1]~2\);

-- Location: FF_X66_Y50_N15
\L_readkey|L_Showkey|COUNT:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:counter[1]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_readkey|L_Showkey|counter~0_combout\,
	ena => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:counter[1]~q\);

-- Location: FF_X66_Y50_N17
\L_readkey|L_Showkey|COUNT:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:counter[2]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_readkey|L_Showkey|counter~0_combout\,
	ena => \L_readkey|L_Showkey|COUNT:counter[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:counter[2]~q\);

-- Location: LCCOMB_X66_Y50_N10
\L_readkey|L_Showkey|Decoder0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~0_combout\ = (\L_readkey|L_Showkey|Equal0~0_combout\ & (\L_readkey|L_Showkey|COUNT:counter[3]~q\ $ (((\L_readkey|L_Showkey|COUNT:counter[1]~q\) # (!\L_readkey|L_Showkey|Equal2~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|Equal0~0_combout\,
	datab => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[3]~q\,
	datad => \L_readkey|L_Showkey|Equal2~0_combout\,
	combout => \L_readkey|L_Showkey|Decoder0~0_combout\);

-- Location: LCCOMB_X64_Y51_N10
\L_readkey|L_Showkey|Decoder0~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~5_combout\ = (\L_readkey|L_Showkey|COUNT:counter[2]~q\ & (\L_readkey|L_Showkey|COUNT:counter[0]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[1]~q\ & \L_readkey|L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datad => \L_readkey|L_Showkey|Decoder0~0_combout\,
	combout => \L_readkey|L_Showkey|Decoder0~5_combout\);

-- Location: IOIBUF_X34_Y0_N8
\arduino_io5~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_arduino_io5,
	o => \arduino_io5~input_o\);

-- Location: FF_X63_Y51_N13
\L_readkey|L_ClockDomainCrossing|c0|Q\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	asdata => \arduino_io5~input_o\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_ClockDomainCrossing|c0|Q~q\);

-- Location: LCCOMB_X63_Y51_N14
\L_readkey|L_ClockDomainCrossing|c1|Q~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_ClockDomainCrossing|c1|Q~feeder_combout\ = \L_readkey|L_ClockDomainCrossing|c0|Q~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_ClockDomainCrossing|c0|Q~q\,
	combout => \L_readkey|L_ClockDomainCrossing|c1|Q~feeder_combout\);

-- Location: FF_X63_Y51_N15
\L_readkey|L_ClockDomainCrossing|c1|Q\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	d => \L_readkey|L_ClockDomainCrossing|c1|Q~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_ClockDomainCrossing|c1|Q~q\);

-- Location: LCCOMB_X64_Y51_N16
\L_readkey|L_Showkey|COUNT:byte_current[4]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[4]~0_combout\ = (\L_readkey|L_Showkey|Decoder0~5_combout\ & ((\L_readkey|L_ClockDomainCrossing|c1|Q~q\))) # (!\L_readkey|L_Showkey|Decoder0~5_combout\ & (\L_readkey|L_Showkey|COUNT:byte_current[4]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|Decoder0~5_combout\,
	datab => \L_readkey|L_Showkey|COUNT:byte_current[4]~q\,
	datad => \L_readkey|L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[4]~0_combout\);

-- Location: LCCOMB_X64_Y51_N28
\L_readkey|L_Showkey|COUNT:byte_current[4]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[4]~feeder_combout\ = \L_readkey|L_Showkey|COUNT:byte_current[4]~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_Showkey|COUNT:byte_current[4]~0_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[4]~feeder_combout\);

-- Location: FF_X64_Y51_N29
\L_readkey|L_Showkey|COUNT:byte_current[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:byte_current[4]~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:byte_current[4]~q\);

-- Location: FF_X65_Y50_N31
\L_readkey|L_Showkey|scancode[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[4]~q\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|scancode\(4));

-- Location: LCCOMB_X64_Y51_N24
\L_readkey|L_Showkey|Decoder0~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~4_combout\ = (\L_readkey|L_Showkey|COUNT:counter[2]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[0]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[1]~q\ & \L_readkey|L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datad => \L_readkey|L_Showkey|Decoder0~0_combout\,
	combout => \L_readkey|L_Showkey|Decoder0~4_combout\);

-- Location: LCCOMB_X64_Y51_N20
\L_readkey|L_Showkey|COUNT:byte_current[3]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[3]~0_combout\ = (\L_readkey|L_Showkey|Decoder0~4_combout\ & ((\L_readkey|L_ClockDomainCrossing|c1|Q~q\))) # (!\L_readkey|L_Showkey|Decoder0~4_combout\ & (\L_readkey|L_Showkey|COUNT:byte_current[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|COUNT:byte_current[3]~q\,
	datac => \L_readkey|L_Showkey|Decoder0~4_combout\,
	datad => \L_readkey|L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[3]~0_combout\);

-- Location: LCCOMB_X64_Y51_N14
\L_readkey|L_Showkey|COUNT:byte_current[3]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[3]~feeder_combout\ = \L_readkey|L_Showkey|COUNT:byte_current[3]~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_Showkey|COUNT:byte_current[3]~0_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[3]~feeder_combout\);

-- Location: FF_X64_Y51_N15
\L_readkey|L_Showkey|COUNT:byte_current[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:byte_current[3]~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:byte_current[3]~q\);

-- Location: FF_X65_Y50_N7
\L_readkey|L_Showkey|scancode[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[3]~q\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|scancode\(3));

-- Location: LCCOMB_X64_Y51_N8
\L_readkey|L_Showkey|Decoder0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~2_combout\ = (!\L_readkey|L_Showkey|COUNT:counter[2]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[0]~q\ & (\L_readkey|L_Showkey|COUNT:counter[1]~q\ & \L_readkey|L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datad => \L_readkey|L_Showkey|Decoder0~0_combout\,
	combout => \L_readkey|L_Showkey|Decoder0~2_combout\);

-- Location: LCCOMB_X64_Y51_N12
\L_readkey|L_Showkey|COUNT:byte_current[1]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[1]~0_combout\ = (\L_readkey|L_Showkey|Decoder0~2_combout\ & ((\L_readkey|L_ClockDomainCrossing|c1|Q~q\))) # (!\L_readkey|L_Showkey|Decoder0~2_combout\ & (\L_readkey|L_Showkey|COUNT:byte_current[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:byte_current[1]~q\,
	datac => \L_readkey|L_Showkey|Decoder0~2_combout\,
	datad => \L_readkey|L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[1]~0_combout\);

-- Location: LCCOMB_X64_Y51_N18
\L_readkey|L_Showkey|COUNT:byte_current[1]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[1]~feeder_combout\ = \L_readkey|L_Showkey|COUNT:byte_current[1]~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_Showkey|COUNT:byte_current[1]~0_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[1]~feeder_combout\);

-- Location: FF_X64_Y51_N19
\L_readkey|L_Showkey|COUNT:byte_current[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:byte_current[1]~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:byte_current[1]~q\);

-- Location: FF_X65_Y50_N11
\L_readkey|L_Showkey|scancode[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[1]~q\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|scancode\(1));

-- Location: LCCOMB_X64_Y51_N22
\L_readkey|L_Showkey|Decoder0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~3_combout\ = (!\L_readkey|L_Showkey|COUNT:counter[2]~q\ & (\L_readkey|L_Showkey|COUNT:counter[0]~q\ & (\L_readkey|L_Showkey|COUNT:counter[1]~q\ & \L_readkey|L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datad => \L_readkey|L_Showkey|Decoder0~0_combout\,
	combout => \L_readkey|L_Showkey|Decoder0~3_combout\);

-- Location: LCCOMB_X64_Y51_N0
\L_readkey|L_Showkey|COUNT:byte_current[2]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[2]~0_combout\ = (\L_readkey|L_Showkey|Decoder0~3_combout\ & ((\L_readkey|L_ClockDomainCrossing|c1|Q~q\))) # (!\L_readkey|L_Showkey|Decoder0~3_combout\ & (\L_readkey|L_Showkey|COUNT:byte_current[2]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:byte_current[2]~q\,
	datac => \L_readkey|L_Showkey|Decoder0~3_combout\,
	datad => \L_readkey|L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[2]~0_combout\);

-- Location: LCCOMB_X64_Y51_N6
\L_readkey|L_Showkey|COUNT:byte_current[2]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[2]~feeder_combout\ = \L_readkey|L_Showkey|COUNT:byte_current[2]~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_Showkey|COUNT:byte_current[2]~0_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[2]~feeder_combout\);

-- Location: FF_X64_Y51_N7
\L_readkey|L_Showkey|COUNT:byte_current[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:byte_current[2]~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:byte_current[2]~q\);

-- Location: FF_X65_Y50_N23
\L_readkey|L_Showkey|scancode[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[2]~q\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|scancode\(2));

-- Location: LCCOMB_X64_Y49_N26
\L_readkey|L_Constantkey|Equal0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|Equal0~1_combout\ = (\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(3) & (!\L_readkey|L_Showkey|scancode\(1) & !\L_readkey|L_Showkey|scancode\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(1),
	datad => \L_readkey|L_Showkey|scancode\(2),
	combout => \L_readkey|L_Constantkey|Equal0~1_combout\);

-- Location: LCCOMB_X63_Y51_N0
\L_readkey|L_Showkey|Decoder0~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~8_combout\ = (!\L_readkey|L_Showkey|COUNT:counter[1]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[0]~q\ & (\L_readkey|L_Showkey|Decoder0~0_combout\ & !\L_readkey|L_Showkey|COUNT:counter[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datac => \L_readkey|L_Showkey|Decoder0~0_combout\,
	datad => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	combout => \L_readkey|L_Showkey|Decoder0~8_combout\);

-- Location: LCCOMB_X63_Y51_N2
\L_readkey|L_Showkey|COUNT:byte_current[7]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[7]~0_combout\ = (\L_readkey|L_Showkey|Decoder0~8_combout\ & ((\L_readkey|L_ClockDomainCrossing|c1|Q~q\))) # (!\L_readkey|L_Showkey|Decoder0~8_combout\ & (\L_readkey|L_Showkey|COUNT:byte_current[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|COUNT:byte_current[7]~q\,
	datac => \L_readkey|L_ClockDomainCrossing|c1|Q~q\,
	datad => \L_readkey|L_Showkey|Decoder0~8_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[7]~0_combout\);

-- Location: LCCOMB_X63_Y51_N24
\L_readkey|L_Showkey|COUNT:byte_current[7]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[7]~feeder_combout\ = \L_readkey|L_Showkey|COUNT:byte_current[7]~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_Showkey|COUNT:byte_current[7]~0_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[7]~feeder_combout\);

-- Location: FF_X63_Y51_N25
\L_readkey|L_Showkey|COUNT:byte_current[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:byte_current[7]~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:byte_current[7]~q\);

-- Location: FF_X65_Y50_N29
\L_readkey|L_Showkey|scancode[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[7]~q\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|scancode\(7));

-- Location: LCCOMB_X64_Y51_N26
\L_readkey|L_Showkey|Decoder0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~1_combout\ = (!\L_readkey|L_Showkey|COUNT:counter[2]~q\ & (\L_readkey|L_Showkey|COUNT:counter[0]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[1]~q\ & \L_readkey|L_Showkey|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datad => \L_readkey|L_Showkey|Decoder0~0_combout\,
	combout => \L_readkey|L_Showkey|Decoder0~1_combout\);

-- Location: LCCOMB_X64_Y51_N4
\L_readkey|L_Showkey|COUNT:byte_current[0]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[0]~0_combout\ = (\L_readkey|L_Showkey|Decoder0~1_combout\ & ((\L_readkey|L_ClockDomainCrossing|c1|Q~q\))) # (!\L_readkey|L_Showkey|Decoder0~1_combout\ & (\L_readkey|L_Showkey|COUNT:byte_current[0]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:byte_current[0]~q\,
	datac => \L_readkey|L_Showkey|Decoder0~1_combout\,
	datad => \L_readkey|L_ClockDomainCrossing|c1|Q~q\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[0]~0_combout\);

-- Location: FF_X64_Y51_N17
\L_readkey|L_Showkey|COUNT:byte_current[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[0]~0_combout\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:byte_current[0]~q\);

-- Location: FF_X65_Y50_N27
\L_readkey|L_Showkey|scancode[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[0]~q\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|scancode\(0));

-- Location: LCCOMB_X63_Y51_N16
\L_readkey|L_Showkey|Decoder0~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~7_combout\ = (\L_readkey|L_Showkey|COUNT:counter[1]~q\ & (\L_readkey|L_Showkey|COUNT:counter[0]~q\ & (\L_readkey|L_Showkey|Decoder0~0_combout\ & \L_readkey|L_Showkey|COUNT:counter[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datac => \L_readkey|L_Showkey|Decoder0~0_combout\,
	datad => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	combout => \L_readkey|L_Showkey|Decoder0~7_combout\);

-- Location: LCCOMB_X63_Y51_N6
\L_readkey|L_Showkey|COUNT:byte_current[6]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[6]~0_combout\ = (\L_readkey|L_Showkey|Decoder0~7_combout\ & ((\L_readkey|L_ClockDomainCrossing|c1|Q~q\))) # (!\L_readkey|L_Showkey|Decoder0~7_combout\ & (\L_readkey|L_Showkey|COUNT:byte_current[6]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:byte_current[6]~q\,
	datac => \L_readkey|L_ClockDomainCrossing|c1|Q~q\,
	datad => \L_readkey|L_Showkey|Decoder0~7_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[6]~0_combout\);

-- Location: LCCOMB_X63_Y51_N30
\L_readkey|L_Showkey|COUNT:byte_current[6]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[6]~feeder_combout\ = \L_readkey|L_Showkey|COUNT:byte_current[6]~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_Showkey|COUNT:byte_current[6]~0_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[6]~feeder_combout\);

-- Location: FF_X63_Y51_N31
\L_readkey|L_Showkey|COUNT:byte_current[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:byte_current[6]~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:byte_current[6]~q\);

-- Location: FF_X65_Y50_N13
\L_readkey|L_Showkey|scancode[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[6]~q\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|scancode\(6));

-- Location: LCCOMB_X63_Y51_N28
\L_readkey|L_Showkey|Decoder0~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|Decoder0~6_combout\ = (\L_readkey|L_Showkey|COUNT:counter[1]~q\ & (!\L_readkey|L_Showkey|COUNT:counter[0]~q\ & (\L_readkey|L_Showkey|Decoder0~0_combout\ & \L_readkey|L_Showkey|COUNT:counter[2]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	datac => \L_readkey|L_Showkey|Decoder0~0_combout\,
	datad => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	combout => \L_readkey|L_Showkey|Decoder0~6_combout\);

-- Location: LCCOMB_X63_Y51_N18
\L_readkey|L_Showkey|COUNT:byte_current[5]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[5]~0_combout\ = (\L_readkey|L_Showkey|Decoder0~6_combout\ & ((\L_readkey|L_ClockDomainCrossing|c1|Q~q\))) # (!\L_readkey|L_Showkey|Decoder0~6_combout\ & (\L_readkey|L_Showkey|COUNT:byte_current[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|COUNT:byte_current[5]~q\,
	datac => \L_readkey|L_ClockDomainCrossing|c1|Q~q\,
	datad => \L_readkey|L_Showkey|Decoder0~6_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[5]~0_combout\);

-- Location: LCCOMB_X63_Y51_N4
\L_readkey|L_Showkey|COUNT:byte_current[5]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|COUNT:byte_current[5]~feeder_combout\ = \L_readkey|L_Showkey|COUNT:byte_current[5]~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_readkey|L_Showkey|COUNT:byte_current[5]~0_combout\,
	combout => \L_readkey|L_Showkey|COUNT:byte_current[5]~feeder_combout\);

-- Location: FF_X63_Y51_N5
\L_readkey|L_Showkey|COUNT:byte_current[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|COUNT:byte_current[5]~feeder_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|COUNT:byte_current[5]~q\);

-- Location: FF_X65_Y50_N21
\L_readkey|L_Showkey|scancode[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~q\,
	asdata => \L_readkey|L_Showkey|COUNT:byte_current[5]~q\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|scancode\(5));

-- Location: LCCOMB_X64_Y50_N20
\L_readkey|L_Constantkey|Equal0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|Equal0~0_combout\ = (\L_readkey|L_Showkey|scancode\(7) & (!\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(6) & \L_readkey|L_Showkey|scancode\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(7),
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_readkey|L_Showkey|scancode\(5),
	combout => \L_readkey|L_Constantkey|Equal0~0_combout\);

-- Location: LCCOMB_X65_Y49_N26
\L_readkey|L_Constantkey|Equal0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|Equal0~2_combout\ = (\L_readkey|L_Constantkey|Equal0~1_combout\ & \L_readkey|L_Constantkey|Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Constantkey|Equal0~1_combout\,
	datad => \L_readkey|L_Constantkey|Equal0~0_combout\,
	combout => \L_readkey|L_Constantkey|Equal0~2_combout\);

-- Location: LCCOMB_X67_Y50_N30
\L_readkey|L_Showkey|byte_read~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|byte_read~0_combout\ = (\L_readkey|L_Showkey|COUNT:counter[3]~q\ & ((\L_readkey|L_Showkey|COUNT:counter[1]~q\) # ((\L_readkey|L_Showkey|COUNT:counter[2]~q\) # (\L_readkey|L_Showkey|COUNT:counter[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|COUNT:counter[1]~q\,
	datab => \L_readkey|L_Showkey|COUNT:counter[3]~q\,
	datac => \L_readkey|L_Showkey|COUNT:counter[2]~q\,
	datad => \L_readkey|L_Showkey|COUNT:counter[0]~q\,
	combout => \L_readkey|L_Showkey|byte_read~0_combout\);

-- Location: LCCOMB_X65_Y49_N16
\L_readkey|L_Showkey|byte_read~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Showkey|byte_read~1_combout\ = (\L_readkey|L_Showkey|Equal0~0_combout\ & (\L_readkey|L_Showkey|byte_read~0_combout\ & ((\L_readkey|L_Showkey|byte_read~q\) # (\L_readkey|L_Showkey|scancode[3]~0_combout\)))) # 
-- (!\L_readkey|L_Showkey|Equal0~0_combout\ & (((\L_readkey|L_Showkey|byte_read~q\) # (\L_readkey|L_Showkey|scancode[3]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|Equal0~0_combout\,
	datab => \L_readkey|L_Showkey|byte_read~0_combout\,
	datac => \L_readkey|L_Showkey|byte_read~q\,
	datad => \L_readkey|L_Showkey|scancode[3]~0_combout\,
	combout => \L_readkey|L_Showkey|byte_read~1_combout\);

-- Location: FF_X65_Y49_N17
\L_readkey|L_Showkey|byte_read\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_readkey|L_ClockDomainCrossing|c3|ALT_INV_Q~clkctrl_outclk\,
	d => \L_readkey|L_Showkey|byte_read~1_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Showkey|byte_read~q\);

-- Location: LCCOMB_X65_Y49_N8
\L_readkey|L_Constantkey|Selector2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|Selector2~0_combout\ = (\L_readkey|L_Showkey|byte_read~q\ & (\L_readkey|L_Constantkey|Equal0~2_combout\ & ((\L_readkey|L_Constantkey|current_state.state_key_pressed~q\) # 
-- (\L_readkey|L_Constantkey|current_state.state_key_released~q\)))) # (!\L_readkey|L_Showkey|byte_read~q\ & (((\L_readkey|L_Constantkey|current_state.state_key_released~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Constantkey|current_state.state_key_released~q\,
	datac => \L_readkey|L_Constantkey|Equal0~2_combout\,
	datad => \L_readkey|L_Showkey|byte_read~q\,
	combout => \L_readkey|L_Constantkey|Selector2~0_combout\);

-- Location: FF_X65_Y49_N3
\L_readkey|L_Constantkey|current_state.state_key_released\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	asdata => \L_readkey|L_Constantkey|Selector2~0_combout\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Constantkey|current_state.state_key_released~q\);

-- Location: LCCOMB_X65_Y49_N24
\L_readkey|L_Constantkey|Selector3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|Selector3~0_combout\ = (!\L_readkey|L_Constantkey|Equal0~2_combout\ & ((\L_readkey|L_Showkey|byte_read~q\ & ((\L_readkey|L_Constantkey|current_state.state_key_released~q\))) # (!\L_readkey|L_Showkey|byte_read~q\ & 
-- (\L_readkey|L_Constantkey|current_state.state_key_reminder~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|Equal0~2_combout\,
	datab => \L_readkey|L_Showkey|byte_read~q\,
	datac => \L_readkey|L_Constantkey|current_state.state_key_reminder~q\,
	datad => \L_readkey|L_Constantkey|current_state.state_key_released~q\,
	combout => \L_readkey|L_Constantkey|Selector3~0_combout\);

-- Location: FF_X65_Y49_N25
\L_readkey|L_Constantkey|current_state.state_key_reminder\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	d => \L_readkey|L_Constantkey|Selector3~0_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Constantkey|current_state.state_key_reminder~q\);

-- Location: LCCOMB_X65_Y49_N20
\L_readkey|L_Constantkey|Selector4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|Selector4~0_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_reminder~q\ & ((\L_readkey|L_Constantkey|Equal0~2_combout\) # ((\L_readkey|L_Showkey|byte_read~q\)))) # 
-- (!\L_readkey|L_Constantkey|current_state.state_key_reminder~q\ & (((\L_readkey|L_Constantkey|current_state.state_key_reminder_2~q\ & \L_readkey|L_Showkey|byte_read~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|Equal0~2_combout\,
	datab => \L_readkey|L_Constantkey|current_state.state_key_reminder~q\,
	datac => \L_readkey|L_Constantkey|current_state.state_key_reminder_2~q\,
	datad => \L_readkey|L_Showkey|byte_read~q\,
	combout => \L_readkey|L_Constantkey|Selector4~0_combout\);

-- Location: FF_X65_Y49_N21
\L_readkey|L_Constantkey|current_state.state_key_reminder_2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	d => \L_readkey|L_Constantkey|Selector4~0_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Constantkey|current_state.state_key_reminder_2~q\);

-- Location: LCCOMB_X65_Y49_N14
\L_readkey|L_Constantkey|Selector0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|Selector0~0_combout\ = (\L_readkey|L_Showkey|byte_read~q\) # ((!\L_readkey|L_Constantkey|current_state.state_key_reminder_2~q\ & \L_readkey|L_Constantkey|current_state.state_no_key~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Constantkey|current_state.state_key_reminder_2~q\,
	datac => \L_readkey|L_Constantkey|current_state.state_no_key~q\,
	datad => \L_readkey|L_Showkey|byte_read~q\,
	combout => \L_readkey|L_Constantkey|Selector0~0_combout\);

-- Location: FF_X65_Y49_N27
\L_readkey|L_Constantkey|current_state.state_no_key\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	asdata => \L_readkey|L_Constantkey|Selector0~0_combout\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Constantkey|current_state.state_no_key~q\);

-- Location: LCCOMB_X65_Y49_N0
\L_readkey|L_Constantkey|Selector1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|Selector1~0_combout\ = (\L_readkey|L_Showkey|byte_read~q\ & (((\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & !\L_readkey|L_Constantkey|Equal0~2_combout\)) # 
-- (!\L_readkey|L_Constantkey|current_state.state_no_key~q\))) # (!\L_readkey|L_Showkey|byte_read~q\ & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Constantkey|Equal0~2_combout\,
	datac => \L_readkey|L_Constantkey|current_state.state_no_key~q\,
	datad => \L_readkey|L_Showkey|byte_read~q\,
	combout => \L_readkey|L_Constantkey|Selector1~0_combout\);

-- Location: FF_X64_Y50_N9
\L_readkey|L_Constantkey|current_state.state_key_pressed\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	asdata => \L_readkey|L_Constantkey|Selector1~0_combout\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\);

-- Location: LCCOMB_X64_Y50_N10
\L_readkey|L_Constantkey|dig2[3]~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|dig2[3]~2_combout\ = (\L_readkey|L_Showkey|scancode\(3) & \L_readkey|L_Constantkey|current_state.state_key_pressed~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_readkey|L_Showkey|scancode\(3),
	datad => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	combout => \L_readkey|L_Constantkey|dig2[3]~2_combout\);

-- Location: LCCOMB_X64_Y50_N8
\L_readkey|L_Constantkey|dig2[5]~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|dig2[5]~4_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \L_readkey|L_Showkey|scancode\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \L_readkey|L_Showkey|scancode\(5),
	combout => \L_readkey|L_Constantkey|dig2[5]~4_combout\);

-- Location: LCCOMB_X65_Y50_N12
\L_tone_generation|L_clock_generator|Equal2~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal2~11_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (!\L_readkey|L_Showkey|scancode\(7) & (!\L_readkey|L_Showkey|scancode\(6) & \L_readkey|L_Showkey|scancode\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Showkey|scancode\(7),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_readkey|L_Showkey|scancode\(2),
	combout => \L_tone_generation|L_clock_generator|Equal2~11_combout\);

-- Location: LCCOMB_X65_Y50_N10
\L_readkey|L_Constantkey|dig2[1]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|dig2[1]~0_combout\ = (\L_readkey|L_Showkey|scancode\(1) & \L_readkey|L_Constantkey|current_state.state_key_pressed~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_readkey|L_Showkey|scancode\(1),
	datad => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	combout => \L_readkey|L_Constantkey|dig2[1]~0_combout\);

-- Location: LCCOMB_X64_Y50_N0
\L_tone_generation|L_clock_generator|Equal2~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal2~9_combout\ = (\L_readkey|L_Constantkey|dig2[3]~2_combout\ & (!\L_readkey|L_Constantkey|dig2[5]~4_combout\ & (\L_tone_generation|L_clock_generator|Equal2~11_combout\ & 
-- !\L_readkey|L_Constantkey|dig2[1]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|dig2[3]~2_combout\,
	datab => \L_readkey|L_Constantkey|dig2[5]~4_combout\,
	datac => \L_tone_generation|L_clock_generator|Equal2~11_combout\,
	datad => \L_readkey|L_Constantkey|dig2[1]~0_combout\,
	combout => \L_tone_generation|L_clock_generator|Equal2~9_combout\);

-- Location: LCCOMB_X64_Y50_N30
\L_tone_generation|L_clock_generator|Equal2~10\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal2~10_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_tone_generation|L_clock_generator|Equal2~9_combout\ & (\L_readkey|L_Showkey|scancode\(4) & !\L_readkey|L_Showkey|scancode\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_tone_generation|L_clock_generator|Equal2~9_combout\,
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_clock_generator|Equal2~10_combout\);

-- Location: LCCOMB_X65_Y50_N28
\L_readkey|L_Constantkey|dig2[4]~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|dig2[4]~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \L_readkey|L_Showkey|scancode\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \L_readkey|L_Constantkey|dig2[4]~3_combout\);

-- Location: LCCOMB_X65_Y50_N26
\L_readkey|L_Constantkey|dig2[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|dig2[0]~1_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \L_readkey|L_Showkey|scancode\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_readkey|L_Constantkey|dig2[0]~1_combout\);

-- Location: LCCOMB_X63_Y50_N28
\L_tone_generation|L_clock_generator|Selector8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector8~0_combout\ = (\L_tone_generation|L_clock_generator|tone_state.state_6~q\ & (((\L_readkey|L_Constantkey|dig2[0]~1_combout\) # (!\L_tone_generation|L_clock_generator|Equal2~9_combout\)) # 
-- (!\L_readkey|L_Constantkey|dig2[4]~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_6~q\,
	datab => \L_readkey|L_Constantkey|dig2[4]~3_combout\,
	datac => \L_readkey|L_Constantkey|dig2[0]~1_combout\,
	datad => \L_tone_generation|L_clock_generator|Equal2~9_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector8~0_combout\);

-- Location: LCCOMB_X65_Y50_N20
\L_tone_generation|L_clock_generator|Equal1~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal1~8_combout\ = (\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(6) & !\L_readkey|L_Showkey|scancode\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_clock_generator|Equal1~8_combout\);

-- Location: LCCOMB_X64_Y49_N0
\L_tone_generation|L_clock_generator|Equal1~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal1~5_combout\ = (\L_readkey|L_Showkey|scancode\(3) & (\L_tone_generation|L_clock_generator|Equal1~8_combout\ & (!\L_readkey|L_Showkey|scancode\(5) & !\L_readkey|L_Showkey|scancode\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_tone_generation|L_clock_generator|Equal1~8_combout\,
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(7),
	combout => \L_tone_generation|L_clock_generator|Equal1~5_combout\);

-- Location: LCCOMB_X64_Y50_N2
\L_tone_generation|L_clock_generator|Equal1~21\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal1~21_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_tone_generation|L_clock_generator|Equal1~5_combout\ & (\L_readkey|L_Showkey|scancode\(1) & !\L_readkey|L_Showkey|scancode\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_tone_generation|L_clock_generator|Equal1~5_combout\,
	datac => \L_readkey|L_Showkey|scancode\(1),
	datad => \L_readkey|L_Showkey|scancode\(2),
	combout => \L_tone_generation|L_clock_generator|Equal1~21_combout\);

-- Location: LCCOMB_X64_Y50_N16
\L_tone_generation|L_clock_generator|Selector2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector2~0_combout\ = (\L_tone_generation|L_clock_generator|tone_state.state_0~q\) # ((\L_tone_generation|L_clock_generator|tone_state.state_2~q\ & \L_tone_generation|L_clock_generator|Equal2~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|tone_state.state_2~q\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_0~q\,
	datad => \L_tone_generation|L_clock_generator|Equal2~10_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector2~0_combout\);

-- Location: LCCOMB_X64_Y50_N12
\L_tone_generation|L_clock_generator|next_state.state_1_535\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|next_state.state_1_535~combout\ = (GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_tone_generation|L_clock_generator|Selector2~0_combout\))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & (\L_tone_generation|L_clock_generator|next_state.state_1_535~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|next_state.state_1_535~combout\,
	datab => \L_tone_generation|L_clock_generator|Selector2~0_combout\,
	datad => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_tone_generation|L_clock_generator|next_state.state_1_535~combout\);

-- Location: LCCOMB_X65_Y50_N6
\L_tone_generation|L_clock_generator|Equal0~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal0~6_combout\ = (!\L_readkey|L_Showkey|scancode\(2) & (!\L_readkey|L_Showkey|scancode\(3) & !\L_readkey|L_Showkey|scancode\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(2),
	datac => \L_readkey|L_Showkey|scancode\(3),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \L_tone_generation|L_clock_generator|Equal0~6_combout\);

-- Location: LCCOMB_X65_Y50_N24
\L_tone_generation|L_clock_generator|Equal0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal0~3_combout\ = (\L_tone_generation|L_clock_generator|Equal0~6_combout\ & (\L_readkey|L_Showkey|scancode\(7) & (!\L_readkey|L_Showkey|scancode\(0) & \L_readkey|L_Showkey|scancode\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|Equal0~6_combout\,
	datab => \L_readkey|L_Showkey|scancode\(7),
	datac => \L_readkey|L_Showkey|scancode\(0),
	datad => \L_readkey|L_Showkey|scancode\(6),
	combout => \L_tone_generation|L_clock_generator|Equal0~3_combout\);

-- Location: LCCOMB_X64_Y50_N14
\L_tone_generation|L_clock_generator|Equal0~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal0~19_combout\ = (\L_readkey|L_Showkey|scancode\(4) & (\L_readkey|L_Showkey|scancode\(5) & (\L_tone_generation|L_clock_generator|Equal0~3_combout\ & \L_readkey|L_Constantkey|current_state.state_key_pressed~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_tone_generation|L_clock_generator|Equal0~3_combout\,
	datad => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	combout => \L_tone_generation|L_clock_generator|Equal0~19_combout\);

-- Location: LCCOMB_X63_Y50_N0
\L_tone_generation|L_clock_generator|Valid_key~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Valid_key~0_combout\ = (\KEY[1]~input_o\ & (!\L_tone_generation|L_clock_generator|Valid_key~q\ & \L_tone_generation|L_clock_generator|Equal0~19_combout\)) # (!\KEY[1]~input_o\ & 
-- (\L_tone_generation|L_clock_generator|Valid_key~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY[1]~input_o\,
	datab => \L_tone_generation|L_clock_generator|Valid_key~q\,
	datad => \L_tone_generation|L_clock_generator|Equal0~19_combout\,
	combout => \L_tone_generation|L_clock_generator|Valid_key~0_combout\);

-- Location: LCCOMB_X63_Y50_N24
\L_tone_generation|L_clock_generator|Valid_key~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Valid_key~feeder_combout\ = \L_tone_generation|L_clock_generator|Valid_key~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|Valid_key~0_combout\,
	combout => \L_tone_generation|L_clock_generator|Valid_key~feeder_combout\);

-- Location: FF_X63_Y50_N25
\L_tone_generation|L_clock_generator|Valid_key\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	d => \L_tone_generation|L_clock_generator|Valid_key~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|Valid_key~q\);

-- Location: FF_X63_Y50_N1
\L_tone_generation|L_clock_generator|tone_state.state_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	asdata => \L_tone_generation|L_clock_generator|next_state.state_1_535~combout\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_tone_generation|L_clock_generator|Valid_key~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|tone_state.state_1~q\);

-- Location: LCCOMB_X64_Y50_N22
\L_tone_generation|L_clock_generator|Selector1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector1~0_combout\ = (\L_tone_generation|L_clock_generator|tone_state.state_1~q\ & (!\L_readkey|L_Constantkey|dig2[0]~1_combout\ & (\L_tone_generation|L_clock_generator|Equal2~9_combout\ & 
-- \L_readkey|L_Constantkey|dig2[4]~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_1~q\,
	datab => \L_readkey|L_Constantkey|dig2[0]~1_combout\,
	datac => \L_tone_generation|L_clock_generator|Equal2~9_combout\,
	datad => \L_readkey|L_Constantkey|dig2[4]~3_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector1~0_combout\);

-- Location: LCCOMB_X64_Y50_N26
\L_tone_generation|L_clock_generator|next_state.state_0_563\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|next_state.state_0_563~combout\ = (GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & (\L_tone_generation|L_clock_generator|Selector1~0_combout\)) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_tone_generation|L_clock_generator|next_state.state_0_563~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|Selector1~0_combout\,
	datac => \L_tone_generation|L_clock_generator|next_state.state_0_563~combout\,
	datad => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_tone_generation|L_clock_generator|next_state.state_0_563~combout\);

-- Location: FF_X64_Y50_N7
\L_tone_generation|L_clock_generator|tone_state.state_0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~inputclkctrl_outclk\,
	asdata => \L_tone_generation|L_clock_generator|next_state.state_0_563~combout\,
	clrn => \KEY[1]~input_o\,
	sload => VCC,
	ena => \L_tone_generation|L_clock_generator|Valid_key~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|tone_state.state_0~q\);

-- Location: LCCOMB_X62_Y50_N16
\L_tone_generation|L_clock_generator|Selector0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector0~0_combout\ = ((\L_tone_generation|L_clock_generator|tone_state.state_2~q\) # ((\L_tone_generation|L_clock_generator|tone_state.state_1~q\) # (\L_tone_generation|L_clock_generator|tone_state.state_4~q\))) # 
-- (!\L_tone_generation|L_clock_generator|tone_state.state_3~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_3~q\,
	datab => \L_tone_generation|L_clock_generator|tone_state.state_2~q\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_1~q\,
	datad => \L_tone_generation|L_clock_generator|tone_state.state_4~q\,
	combout => \L_tone_generation|L_clock_generator|Selector0~0_combout\);

-- Location: LCCOMB_X63_Y50_N4
\L_tone_generation|L_clock_generator|Selector0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector0~1_combout\ = (\L_tone_generation|L_clock_generator|tone_state.state_5~q\) # ((\L_tone_generation|L_clock_generator|Selector0~0_combout\) # (\L_tone_generation|L_clock_generator|tone_state.state_6~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_5~q\,
	datac => \L_tone_generation|L_clock_generator|Selector0~0_combout\,
	datad => \L_tone_generation|L_clock_generator|tone_state.state_6~q\,
	combout => \L_tone_generation|L_clock_generator|Selector0~1_combout\);

-- Location: LCCOMB_X64_Y50_N24
\L_tone_generation|L_clock_generator|Selector0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector0~2_combout\ = (\L_tone_generation|L_clock_generator|Equal2~10_combout\ & (((\L_tone_generation|L_clock_generator|Selector0~1_combout\) # (\L_tone_generation|L_clock_generator|tone_state.state_7~q\)))) # 
-- (!\L_tone_generation|L_clock_generator|Equal2~10_combout\ & (\L_tone_generation|L_clock_generator|Equal1~21_combout\ & (\L_tone_generation|L_clock_generator|Selector0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|Equal2~10_combout\,
	datab => \L_tone_generation|L_clock_generator|Equal1~21_combout\,
	datac => \L_tone_generation|L_clock_generator|Selector0~1_combout\,
	datad => \L_tone_generation|L_clock_generator|tone_state.state_7~q\,
	combout => \L_tone_generation|L_clock_generator|Selector0~2_combout\);

-- Location: LCCOMB_X64_Y50_N6
\L_tone_generation|L_clock_generator|Selector0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector0~3_combout\ = (\L_tone_generation|L_clock_generator|Selector0~2_combout\) # ((\L_tone_generation|L_clock_generator|Equal1~21_combout\ & \L_tone_generation|L_clock_generator|tone_state.state_0~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|Equal1~21_combout\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_0~q\,
	datad => \L_tone_generation|L_clock_generator|Selector0~2_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector0~3_combout\);

-- Location: CLKCTRL_G11
\L_tone_generation|L_clock_generator|Selector0~3clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\);

-- Location: LCCOMB_X63_Y50_N8
\L_tone_generation|L_clock_generator|next_state.state_7_367\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|next_state.state_7_367~combout\ = (GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_tone_generation|L_clock_generator|Selector8~0_combout\))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & (\L_tone_generation|L_clock_generator|next_state.state_7_367~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|next_state.state_7_367~combout\,
	datac => \L_tone_generation|L_clock_generator|Selector8~0_combout\,
	datad => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_tone_generation|L_clock_generator|next_state.state_7_367~combout\);

-- Location: FF_X63_Y50_N9
\L_tone_generation|L_clock_generator|tone_state.state_7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|next_state.state_7_367~combout\,
	clrn => \KEY[1]~input_o\,
	ena => \L_tone_generation|L_clock_generator|Valid_key~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|tone_state.state_7~q\);

-- Location: LCCOMB_X63_Y50_N26
\L_tone_generation|L_clock_generator|Selector7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector7~0_combout\ = (\L_tone_generation|L_clock_generator|tone_state.state_7~q\) # ((\L_tone_generation|L_clock_generator|tone_state.state_5~q\ & !\L_tone_generation|L_clock_generator|Equal2~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|tone_state.state_5~q\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_7~q\,
	datad => \L_tone_generation|L_clock_generator|Equal2~10_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector7~0_combout\);

-- Location: LCCOMB_X63_Y50_N20
\L_tone_generation|L_clock_generator|next_state.state_6_395\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|next_state.state_6_395~combout\ = (GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_tone_generation|L_clock_generator|Selector7~0_combout\))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & (\L_tone_generation|L_clock_generator|next_state.state_6_395~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|next_state.state_6_395~combout\,
	datac => \L_tone_generation|L_clock_generator|Selector7~0_combout\,
	datad => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_tone_generation|L_clock_generator|next_state.state_6_395~combout\);

-- Location: FF_X63_Y50_N21
\L_tone_generation|L_clock_generator|tone_state.state_6\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|next_state.state_6_395~combout\,
	clrn => \KEY[1]~input_o\,
	ena => \L_tone_generation|L_clock_generator|Valid_key~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|tone_state.state_6~q\);

-- Location: LCCOMB_X63_Y50_N6
\L_tone_generation|L_clock_generator|Selector6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector6~0_combout\ = (\L_tone_generation|L_clock_generator|Equal2~10_combout\ & ((\L_tone_generation|L_clock_generator|tone_state.state_6~q\))) # (!\L_tone_generation|L_clock_generator|Equal2~10_combout\ & 
-- (\L_tone_generation|L_clock_generator|tone_state.state_4~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_4~q\,
	datab => \L_tone_generation|L_clock_generator|tone_state.state_6~q\,
	datad => \L_tone_generation|L_clock_generator|Equal2~10_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector6~0_combout\);

-- Location: LCCOMB_X63_Y50_N30
\L_tone_generation|L_clock_generator|next_state.state_5_423\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|next_state.state_5_423~combout\ = (GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_tone_generation|L_clock_generator|Selector6~0_combout\))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & (\L_tone_generation|L_clock_generator|next_state.state_5_423~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|next_state.state_5_423~combout\,
	datac => \L_tone_generation|L_clock_generator|Selector6~0_combout\,
	datad => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_tone_generation|L_clock_generator|next_state.state_5_423~combout\);

-- Location: FF_X63_Y50_N31
\L_tone_generation|L_clock_generator|tone_state.state_5\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|next_state.state_5_423~combout\,
	clrn => \KEY[1]~input_o\,
	ena => \L_tone_generation|L_clock_generator|Valid_key~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|tone_state.state_5~q\);

-- Location: LCCOMB_X63_Y50_N16
\L_tone_generation|L_clock_generator|Selector5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector5~0_combout\ = (\L_tone_generation|L_clock_generator|Equal2~10_combout\ & ((\L_tone_generation|L_clock_generator|tone_state.state_5~q\))) # (!\L_tone_generation|L_clock_generator|Equal2~10_combout\ & 
-- (!\L_tone_generation|L_clock_generator|tone_state.state_3~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|Equal2~10_combout\,
	datab => \L_tone_generation|L_clock_generator|tone_state.state_3~q\,
	datad => \L_tone_generation|L_clock_generator|tone_state.state_5~q\,
	combout => \L_tone_generation|L_clock_generator|Selector5~0_combout\);

-- Location: LCCOMB_X63_Y50_N22
\L_tone_generation|L_clock_generator|next_state.state_4_451\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|next_state.state_4_451~combout\ = (GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_tone_generation|L_clock_generator|Selector5~0_combout\))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & (\L_tone_generation|L_clock_generator|next_state.state_4_451~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|next_state.state_4_451~combout\,
	datac => \L_tone_generation|L_clock_generator|Selector5~0_combout\,
	datad => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_tone_generation|L_clock_generator|next_state.state_4_451~combout\);

-- Location: FF_X63_Y50_N23
\L_tone_generation|L_clock_generator|tone_state.state_4\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|next_state.state_4_451~combout\,
	clrn => \KEY[1]~input_o\,
	ena => \L_tone_generation|L_clock_generator|Valid_key~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|tone_state.state_4~q\);

-- Location: LCCOMB_X63_Y50_N10
\L_tone_generation|L_clock_generator|Selector4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector4~0_combout\ = (\L_tone_generation|L_clock_generator|Equal2~10_combout\ & (\L_tone_generation|L_clock_generator|tone_state.state_4~q\)) # (!\L_tone_generation|L_clock_generator|Equal2~10_combout\ & 
-- ((\L_tone_generation|L_clock_generator|tone_state.state_2~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_4~q\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_2~q\,
	datad => \L_tone_generation|L_clock_generator|Equal2~10_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector4~0_combout\);

-- Location: LCCOMB_X63_Y50_N2
\L_tone_generation|L_clock_generator|next_state.state_3_479\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|next_state.state_3_479~combout\ = (GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_tone_generation|L_clock_generator|Selector4~0_combout\))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & (\L_tone_generation|L_clock_generator|next_state.state_3_479~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|next_state.state_3_479~combout\,
	datac => \L_tone_generation|L_clock_generator|Selector4~0_combout\,
	datad => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_tone_generation|L_clock_generator|next_state.state_3_479~combout\);

-- Location: LCCOMB_X63_Y50_N18
\L_tone_generation|L_clock_generator|tone_state.state_3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|tone_state.state_3~0_combout\ = !\L_tone_generation|L_clock_generator|next_state.state_3_479~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_tone_generation|L_clock_generator|next_state.state_3_479~combout\,
	combout => \L_tone_generation|L_clock_generator|tone_state.state_3~0_combout\);

-- Location: FF_X63_Y50_N19
\L_tone_generation|L_clock_generator|tone_state.state_3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|tone_state.state_3~0_combout\,
	clrn => \KEY[1]~input_o\,
	ena => \L_tone_generation|L_clock_generator|Valid_key~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|tone_state.state_3~q\);

-- Location: LCCOMB_X63_Y50_N12
\L_tone_generation|L_clock_generator|Selector3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector3~0_combout\ = (\L_tone_generation|L_clock_generator|Equal2~10_combout\ & (!\L_tone_generation|L_clock_generator|tone_state.state_3~q\)) # (!\L_tone_generation|L_clock_generator|Equal2~10_combout\ & 
-- ((\L_tone_generation|L_clock_generator|tone_state.state_1~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|tone_state.state_3~q\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_1~q\,
	datad => \L_tone_generation|L_clock_generator|Equal2~10_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector3~0_combout\);

-- Location: LCCOMB_X63_Y50_N14
\L_tone_generation|L_clock_generator|next_state.state_2_507\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|next_state.state_2_507~combout\ = (GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & (\L_tone_generation|L_clock_generator|Selector3~0_combout\)) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_tone_generation|L_clock_generator|next_state.state_2_507~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|Selector3~0_combout\,
	datac => \L_tone_generation|L_clock_generator|next_state.state_2_507~combout\,
	datad => \L_tone_generation|L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_tone_generation|L_clock_generator|next_state.state_2_507~combout\);

-- Location: FF_X63_Y50_N15
\L_tone_generation|L_clock_generator|tone_state.state_2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \MAX10_CLK1_50~input_o\,
	d => \L_tone_generation|L_clock_generator|next_state.state_2_507~combout\,
	clrn => \KEY[1]~input_o\,
	ena => \L_tone_generation|L_clock_generator|Valid_key~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_clock_generator|tone_state.state_2~q\);

-- Location: LCCOMB_X63_Y48_N26
\L_tone_generation|L_clock_generator|Selector9~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector9~2_combout\ = (\L_tone_generation|L_clock_generator|devider:counter[2]~q\ & ((\L_tone_generation|L_clock_generator|tone_state.state_2~q\) # ((!\L_tone_generation|L_clock_generator|tone_state.state_3~q\ & 
-- \L_tone_generation|L_clock_generator|devider:counter[3]~q\)))) # (!\L_tone_generation|L_clock_generator|devider:counter[2]~q\ & (((!\L_tone_generation|L_clock_generator|tone_state.state_3~q\ & \L_tone_generation|L_clock_generator|devider:counter[3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|devider:counter[2]~q\,
	datab => \L_tone_generation|L_clock_generator|tone_state.state_2~q\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_3~q\,
	datad => \L_tone_generation|L_clock_generator|devider:counter[3]~q\,
	combout => \L_tone_generation|L_clock_generator|Selector9~2_combout\);

-- Location: LCCOMB_X64_Y48_N28
\L_tone_generation|L_clock_generator|Selector9~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector9~3_combout\ = (\L_tone_generation|L_clock_generator|tone_state.state_1~q\ & ((\L_tone_generation|L_clock_generator|devider:counter[1]~q\) # ((\L_tone_generation|L_clock_generator|devider:counter[0]~q\ & 
-- \L_tone_generation|L_clock_generator|tone_state.state_0~q\)))) # (!\L_tone_generation|L_clock_generator|tone_state.state_1~q\ & (((\L_tone_generation|L_clock_generator|devider:counter[0]~q\ & \L_tone_generation|L_clock_generator|tone_state.state_0~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_1~q\,
	datab => \L_tone_generation|L_clock_generator|devider:counter[1]~q\,
	datac => \L_tone_generation|L_clock_generator|devider:counter[0]~q\,
	datad => \L_tone_generation|L_clock_generator|tone_state.state_0~q\,
	combout => \L_tone_generation|L_clock_generator|Selector9~3_combout\);

-- Location: LCCOMB_X63_Y48_N4
\L_tone_generation|L_clock_generator|Selector9~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector9~1_combout\ = (\L_tone_generation|L_clock_generator|tone_state.state_4~q\ & ((\L_tone_generation|L_clock_generator|devider:counter[4]~q\) # ((\L_tone_generation|L_clock_generator|tone_state.state_5~q\ & 
-- \L_tone_generation|L_clock_generator|devider:counter[5]~q\)))) # (!\L_tone_generation|L_clock_generator|tone_state.state_4~q\ & (((\L_tone_generation|L_clock_generator|tone_state.state_5~q\ & \L_tone_generation|L_clock_generator|devider:counter[5]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_4~q\,
	datab => \L_tone_generation|L_clock_generator|devider:counter[4]~q\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_5~q\,
	datad => \L_tone_generation|L_clock_generator|devider:counter[5]~q\,
	combout => \L_tone_generation|L_clock_generator|Selector9~1_combout\);

-- Location: LCCOMB_X63_Y48_N24
\L_tone_generation|L_clock_generator|Selector9~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector9~0_combout\ = (\L_tone_generation|L_clock_generator|tone_state.state_7~q\ & ((\L_tone_generation|L_clock_generator|devider:counter[7]~q\) # ((\L_tone_generation|L_clock_generator|tone_state.state_6~q\ & 
-- \L_tone_generation|L_clock_generator|devider:counter[6]~q\)))) # (!\L_tone_generation|L_clock_generator|tone_state.state_7~q\ & (((\L_tone_generation|L_clock_generator|tone_state.state_6~q\ & \L_tone_generation|L_clock_generator|devider:counter[6]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|tone_state.state_7~q\,
	datab => \L_tone_generation|L_clock_generator|devider:counter[7]~q\,
	datac => \L_tone_generation|L_clock_generator|tone_state.state_6~q\,
	datad => \L_tone_generation|L_clock_generator|devider:counter[6]~q\,
	combout => \L_tone_generation|L_clock_generator|Selector9~0_combout\);

-- Location: LCCOMB_X63_Y48_N0
\L_tone_generation|L_clock_generator|Selector9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Selector9~combout\ = LCELL((!\L_tone_generation|L_clock_generator|Selector9~2_combout\ & (!\L_tone_generation|L_clock_generator|Selector9~3_combout\ & (!\L_tone_generation|L_clock_generator|Selector9~1_combout\ & 
-- !\L_tone_generation|L_clock_generator|Selector9~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|Selector9~2_combout\,
	datab => \L_tone_generation|L_clock_generator|Selector9~3_combout\,
	datac => \L_tone_generation|L_clock_generator|Selector9~1_combout\,
	datad => \L_tone_generation|L_clock_generator|Selector9~0_combout\,
	combout => \L_tone_generation|L_clock_generator|Selector9~combout\);

-- Location: CLKCTRL_G13
\L_tone_generation|L_clock_generator|Selector9~clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\);

-- Location: LCCOMB_X67_Y49_N16
\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~1_combout\ = \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\ $ (VCC)
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~2\ = CARRY(\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\,
	datad => VCC,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~2\);

-- Location: LCCOMB_X67_Y49_N0
\L_tone_generation|L_pulselength2audio|Equal2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|Equal2~0_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\ & 
-- !\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\,
	combout => \L_tone_generation|L_pulselength2audio|Equal2~0_combout\);

-- Location: LCCOMB_X67_Y49_N14
\L_tone_generation|L_pulselength2audio|Equal2~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|Equal2~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\ & 
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\,
	combout => \L_tone_generation|L_pulselength2audio|Equal2~1_combout\);

-- Location: LCCOMB_X67_Y48_N12
\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~2\ & VCC))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~2\ = CARRY((\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\ & !\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~2\);

-- Location: LCCOMB_X67_Y48_N14
\L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~2\)) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\ & ((\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~2\) # (GND)))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~2\ = CARRY((!\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~2\) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~2\);

-- Location: LCCOMB_X65_Y51_N4
\L_tone_generation|L_key2pulselength|Mux12~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux12~9_combout\ = (!\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(3) & ((\L_readkey|L_Showkey|scancode\(4)) # (\L_readkey|L_Showkey|scancode\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux12~9_combout\);

-- Location: LCCOMB_X65_Y51_N26
\L_tone_generation|L_key2pulselength|Mux12~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux12~8_combout\ = (\L_readkey|L_Showkey|scancode\(5) & (((!\L_readkey|L_Showkey|scancode\(4))))) # (!\L_readkey|L_Showkey|scancode\(5) & (\L_readkey|L_Showkey|scancode\(0) & ((\L_readkey|L_Showkey|scancode\(4)) # 
-- (\L_readkey|L_Showkey|scancode\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux12~8_combout\);

-- Location: LCCOMB_X65_Y51_N8
\L_tone_generation|L_key2pulselength|Mux12~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux12~5_combout\ = (!\L_readkey|L_Showkey|scancode\(6) & ((\L_readkey|L_Showkey|scancode\(1) & (\L_tone_generation|L_key2pulselength|Mux12~9_combout\)) # (!\L_readkey|L_Showkey|scancode\(1) & 
-- ((\L_tone_generation|L_key2pulselength|Mux12~8_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_tone_generation|L_key2pulselength|Mux12~9_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux12~8_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux12~5_combout\);

-- Location: LCCOMB_X65_Y51_N22
\L_tone_generation|L_key2pulselength|Mux12~16\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux12~16_combout\ = (!\L_readkey|L_Showkey|scancode\(7) & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_readkey|L_Showkey|scancode\(2) & \L_tone_generation|L_key2pulselength|Mux12~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(7),
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_tone_generation|L_key2pulselength|Mux12~5_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux12~16_combout\);

-- Location: CLKCTRL_G14
\L_tone_generation|L_clock_generator|Equal0~19clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\);

-- Location: LCCOMB_X65_Y51_N6
\L_tone_generation|L_key2pulselength|pulslength[12]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(12) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(12))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & ((\L_tone_generation|L_key2pulselength|Mux12~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(12),
	datac => \L_tone_generation|L_key2pulselength|Mux12~16_combout\,
	datad => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(12));

-- Location: LCCOMB_X65_Y50_N4
\L_tone_generation|L_key2pulselength|Mux2~24\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~24_combout\ = (\L_readkey|L_Constantkey|dig2[3]~2_combout\ & (\L_readkey|L_Constantkey|dig2[0]~1_combout\ & (\L_readkey|L_Constantkey|dig2[4]~3_combout\ & \L_readkey|L_Constantkey|dig2[1]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|dig2[3]~2_combout\,
	datab => \L_readkey|L_Constantkey|dig2[0]~1_combout\,
	datac => \L_readkey|L_Constantkey|dig2[4]~3_combout\,
	datad => \L_readkey|L_Constantkey|dig2[1]~0_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux2~24_combout\);

-- Location: LCCOMB_X64_Y49_N20
\L_tone_generation|L_key2pulselength|Mux5~22\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux5~22_combout\ = (!\L_readkey|L_Showkey|scancode\(5) & (\L_readkey|L_Showkey|scancode\(3) $ (!\L_readkey|L_Showkey|scancode\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_key2pulselength|Mux5~22_combout\);

-- Location: LCCOMB_X64_Y49_N18
\L_tone_generation|L_key2pulselength|Mux5~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux5~19_combout\ = (!\L_readkey|L_Showkey|scancode\(7) & (!\L_readkey|L_Showkey|scancode\(1) & (!\L_readkey|L_Showkey|scancode\(4) & \L_tone_generation|L_key2pulselength|Mux5~22_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(7),
	datab => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_tone_generation|L_key2pulselength|Mux5~22_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux5~19_combout\);

-- Location: LCCOMB_X64_Y49_N24
\L_tone_generation|L_key2pulselength|Mux5~37\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux5~37_combout\ = (\L_readkey|L_Showkey|scancode\(6) & (\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \L_tone_generation|L_key2pulselength|Mux5~19_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(6),
	datab => \L_readkey|L_Showkey|scancode\(2),
	datac => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \L_tone_generation|L_key2pulselength|Mux5~19_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux5~37_combout\);

-- Location: LCCOMB_X64_Y49_N22
\L_tone_generation|L_key2pulselength|Mux5~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux5~9_combout\ = (\L_readkey|L_Showkey|scancode\(1) & (\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(0) & !\L_readkey|L_Showkey|scancode\(3)))) # (!\L_readkey|L_Showkey|scancode\(1) & 
-- (\L_readkey|L_Showkey|scancode\(3) & (\L_readkey|L_Showkey|scancode\(4) $ (!\L_readkey|L_Showkey|scancode\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(0),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux5~9_combout\);

-- Location: LCCOMB_X64_Y49_N12
\L_tone_generation|L_key2pulselength|Mux5~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux5~8_combout\ = (\L_readkey|L_Showkey|scancode\(1) & (\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(0) & !\L_readkey|L_Showkey|scancode\(3)))) # (!\L_readkey|L_Showkey|scancode\(1) & 
-- (\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(4) $ (\L_readkey|L_Showkey|scancode\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(0),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux5~8_combout\);

-- Location: LCCOMB_X64_Y49_N16
\L_tone_generation|L_key2pulselength|Mux5~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux5~5_combout\ = (!\L_readkey|L_Showkey|scancode\(7) & ((\L_readkey|L_Showkey|scancode\(5) & (\L_tone_generation|L_key2pulselength|Mux5~9_combout\)) # (!\L_readkey|L_Showkey|scancode\(5) & 
-- ((\L_tone_generation|L_key2pulselength|Mux5~8_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(7),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_tone_generation|L_key2pulselength|Mux5~9_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux5~8_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux5~5_combout\);

-- Location: LCCOMB_X64_Y49_N6
\L_tone_generation|L_key2pulselength|Mux5~36\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux5~36_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_readkey|L_Showkey|scancode\(2) & (!\L_readkey|L_Showkey|scancode\(6) & \L_tone_generation|L_key2pulselength|Mux5~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Showkey|scancode\(2),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux5~5_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux5~36_combout\);

-- Location: LCCOMB_X64_Y49_N10
\L_tone_generation|L_key2pulselength|Mux2~18\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~18_combout\ = (!\L_readkey|L_Showkey|scancode\(5) & !\L_readkey|L_Showkey|scancode\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(7),
	combout => \L_tone_generation|L_key2pulselength|Mux2~18_combout\);

-- Location: LCCOMB_X64_Y49_N4
\L_tone_generation|L_key2pulselength|Mux2~27\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~27_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (!\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Showkey|scancode\(6) & \L_tone_generation|L_key2pulselength|Mux2~18_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Showkey|scancode\(2),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux2~18_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux2~27_combout\);

-- Location: LCCOMB_X65_Y49_N18
\L_tone_generation|L_key2pulselength|Mux5~35\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux5~35_combout\ = (\L_tone_generation|L_key2pulselength|Mux5~37_combout\) # ((\L_tone_generation|L_key2pulselength|Mux5~36_combout\) # ((\L_tone_generation|L_key2pulselength|Mux2~24_combout\ & 
-- \L_tone_generation|L_key2pulselength|Mux2~27_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux2~24_combout\,
	datab => \L_tone_generation|L_key2pulselength|Mux5~37_combout\,
	datac => \L_tone_generation|L_key2pulselength|Mux5~36_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux2~27_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux5~35_combout\);

-- Location: LCCOMB_X66_Y49_N12
\L_tone_generation|L_key2pulselength|pulslength[5]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(5) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(5))) # (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) 
-- & ((\L_tone_generation|L_key2pulselength|Mux5~35_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(5),
	datab => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	datac => \L_tone_generation|L_key2pulselength|Mux5~35_combout\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(5));

-- Location: LCCOMB_X64_Y49_N14
\L_tone_generation|L_key2pulselength|Mux4~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux4~7_combout\ = (\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(1))) # (!\L_readkey|L_Showkey|scancode\(0) & ((\L_readkey|L_Showkey|scancode\(3) & ((\L_readkey|L_Showkey|scancode\(5)))) # 
-- (!\L_readkey|L_Showkey|scancode\(3) & (\L_readkey|L_Showkey|scancode\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(0),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux4~7_combout\);

-- Location: LCCOMB_X64_Y49_N2
\L_tone_generation|L_key2pulselength|Mux4~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux4~6_combout\ = (!\L_readkey|L_Showkey|scancode\(1) & ((\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(5) $ (\L_readkey|L_Showkey|scancode\(3)))) # (!\L_readkey|L_Showkey|scancode\(0) & 
-- (\L_readkey|L_Showkey|scancode\(5) & \L_readkey|L_Showkey|scancode\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux4~6_combout\);

-- Location: LCCOMB_X64_Y49_N28
\L_tone_generation|L_key2pulselength|Mux4~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux4~3_combout\ = (!\L_readkey|L_Showkey|scancode\(7) & ((\L_readkey|L_Showkey|scancode\(4) & (\L_tone_generation|L_key2pulselength|Mux4~7_combout\)) # (!\L_readkey|L_Showkey|scancode\(4) & 
-- ((\L_tone_generation|L_key2pulselength|Mux4~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(7),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_tone_generation|L_key2pulselength|Mux4~7_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux4~6_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux4~3_combout\);

-- Location: LCCOMB_X64_Y49_N30
\L_tone_generation|L_key2pulselength|Mux4~24\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux4~24_combout\ = (\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (!\L_readkey|L_Showkey|scancode\(6) & \L_tone_generation|L_key2pulselength|Mux4~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(2),
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux4~3_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux4~24_combout\);

-- Location: LCCOMB_X66_Y50_N8
\L_tone_generation|L_key2pulselength|Mux4~18\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux4~18_combout\ = \L_readkey|L_Showkey|scancode\(3) $ (!\L_readkey|L_Showkey|scancode\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_readkey|L_Showkey|scancode\(3),
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \L_tone_generation|L_key2pulselength|Mux4~18_combout\);

-- Location: LCCOMB_X66_Y50_N30
\L_tone_generation|L_key2pulselength|Mux4~25\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux4~25_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_readkey|L_Showkey|scancode\(0) & (\L_tone_generation|L_key2pulselength|Mux4~18_combout\ & \L_readkey|L_Showkey|scancode\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_tone_generation|L_key2pulselength|Mux4~18_combout\,
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \L_tone_generation|L_key2pulselength|Mux4~25_combout\);

-- Location: LCCOMB_X65_Y49_N6
\L_tone_generation|L_key2pulselength|Mux4~23\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux4~23_combout\ = (\L_tone_generation|L_key2pulselength|Mux4~24_combout\) # ((\L_tone_generation|L_key2pulselength|Mux2~27_combout\ & \L_tone_generation|L_key2pulselength|Mux4~25_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux2~27_combout\,
	datac => \L_tone_generation|L_key2pulselength|Mux4~24_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux4~25_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux4~23_combout\);

-- Location: LCCOMB_X66_Y49_N18
\L_tone_generation|L_key2pulselength|pulslength[4]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(4) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(4))) # (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) 
-- & ((\L_tone_generation|L_key2pulselength|Mux4~23_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_key2pulselength|pulslength\(4),
	datac => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	datad => \L_tone_generation|L_key2pulselength|Mux4~23_combout\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(4));

-- Location: LCCOMB_X65_Y49_N12
\L_tone_generation|L_key2pulselength|Mux6~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~9_combout\ = (!\L_readkey|L_Showkey|scancode\(2) & (!\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(5) & \L_readkey|L_Showkey|scancode\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(2),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(6),
	combout => \L_tone_generation|L_key2pulselength|Mux6~9_combout\);

-- Location: LCCOMB_X65_Y50_N18
\L_tone_generation|L_key2pulselength|Mux6~28\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~28_combout\ = (\L_readkey|L_Showkey|scancode\(5) & (!\L_readkey|L_Showkey|scancode\(6) & ((\L_readkey|L_Showkey|scancode\(3)) # (!\L_readkey|L_Showkey|scancode\(4))))) # (!\L_readkey|L_Showkey|scancode\(5) & 
-- (!\L_readkey|L_Showkey|scancode\(3) & (\L_readkey|L_Showkey|scancode\(4) & \L_readkey|L_Showkey|scancode\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(6),
	combout => \L_tone_generation|L_key2pulselength|Mux6~28_combout\);

-- Location: LCCOMB_X65_Y49_N22
\L_tone_generation|L_key2pulselength|Mux6~29\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~29_combout\ = (\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(3))) # (!\L_readkey|L_Showkey|scancode\(0) & (((\L_readkey|L_Showkey|scancode\(2) & 
-- \L_tone_generation|L_key2pulselength|Mux6~28_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(2),
	datac => \L_readkey|L_Showkey|scancode\(0),
	datad => \L_tone_generation|L_key2pulselength|Mux6~28_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux6~29_combout\);

-- Location: LCCOMB_X66_Y49_N26
\L_tone_generation|L_key2pulselength|Mux6~10\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~10_combout\ = (\L_readkey|L_Showkey|scancode\(2) & (!\L_readkey|L_Showkey|scancode\(5) & (\L_readkey|L_Showkey|scancode\(4) $ (\L_readkey|L_Showkey|scancode\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(2),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(6),
	combout => \L_tone_generation|L_key2pulselength|Mux6~10_combout\);

-- Location: LCCOMB_X65_Y49_N4
\L_tone_generation|L_key2pulselength|Mux6~23\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~23_combout\ = (\L_readkey|L_Showkey|scancode\(0) & ((\L_tone_generation|L_key2pulselength|Mux6~29_combout\ & ((\L_tone_generation|L_key2pulselength|Mux6~10_combout\))) # 
-- (!\L_tone_generation|L_key2pulselength|Mux6~29_combout\ & (\L_tone_generation|L_key2pulselength|Mux6~9_combout\)))) # (!\L_readkey|L_Showkey|scancode\(0) & (((\L_tone_generation|L_key2pulselength|Mux6~29_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux6~9_combout\,
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_tone_generation|L_key2pulselength|Mux6~29_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux6~10_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux6~23_combout\);

-- Location: LCCOMB_X65_Y50_N22
\L_tone_generation|L_key2pulselength|Mux6~26\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~26_combout\ = (\L_readkey|L_Showkey|scancode\(2) & ((\L_readkey|L_Showkey|scancode\(3) & (!\L_readkey|L_Showkey|scancode\(5) & \L_readkey|L_Showkey|scancode\(6))) # (!\L_readkey|L_Showkey|scancode\(3) & 
-- (\L_readkey|L_Showkey|scancode\(5) & !\L_readkey|L_Showkey|scancode\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_readkey|L_Showkey|scancode\(6),
	combout => \L_tone_generation|L_key2pulselength|Mux6~26_combout\);

-- Location: LCCOMB_X65_Y50_N2
\L_tone_generation|L_key2pulselength|Mux6~27\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~27_combout\ = (\L_tone_generation|L_key2pulselength|Mux6~26_combout\ & ((\L_readkey|L_Showkey|scancode\(4) & (\L_readkey|L_Showkey|scancode\(5) & !\L_readkey|L_Showkey|scancode\(6))) # 
-- (!\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(5) & \L_readkey|L_Showkey|scancode\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_tone_generation|L_key2pulselength|Mux6~26_combout\,
	datad => \L_readkey|L_Showkey|scancode\(6),
	combout => \L_tone_generation|L_key2pulselength|Mux6~27_combout\);

-- Location: LCCOMB_X65_Y49_N10
\L_tone_generation|L_key2pulselength|Mux6~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~11_combout\ = (\L_readkey|L_Showkey|scancode\(0) & (\L_tone_generation|L_key2pulselength|Mux6~9_combout\ & (!\L_readkey|L_Showkey|scancode\(3)))) # (!\L_readkey|L_Showkey|scancode\(0) & 
-- (((\L_tone_generation|L_key2pulselength|Mux6~27_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux6~9_combout\,
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_tone_generation|L_key2pulselength|Mux6~27_combout\,
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_key2pulselength|Mux6~11_combout\);

-- Location: LCCOMB_X65_Y49_N30
\L_tone_generation|L_key2pulselength|Mux6~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~3_combout\ = (\L_readkey|L_Showkey|scancode\(1) & ((\L_tone_generation|L_key2pulselength|Mux6~11_combout\))) # (!\L_readkey|L_Showkey|scancode\(1) & (\L_tone_generation|L_key2pulselength|Mux6~23_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_tone_generation|L_key2pulselength|Mux6~23_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux6~11_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux6~3_combout\);

-- Location: LCCOMB_X65_Y49_N28
\L_tone_generation|L_key2pulselength|Mux6~25\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~25_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (!\L_readkey|L_Showkey|scancode\(7) & \L_tone_generation|L_key2pulselength|Mux6~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Showkey|scancode\(7),
	datac => \L_tone_generation|L_key2pulselength|Mux6~3_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux6~25_combout\);

-- Location: LCCOMB_X65_Y49_N2
\L_tone_generation|L_key2pulselength|pulslength[6]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(6) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & ((\L_tone_generation|L_key2pulselength|pulslength\(6)))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|Mux6~25_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux6~25_combout\,
	datab => \L_tone_generation|L_key2pulselength|pulslength\(6),
	datad => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(6));

-- Location: LCCOMB_X62_Y50_N18
\L_tone_generation|L_key2pulselength|Mux7~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux7~4_combout\ = (\L_readkey|L_Showkey|scancode\(5) & ((\L_readkey|L_Showkey|scancode\(0)) # (\L_readkey|L_Showkey|scancode\(3) $ (!\L_readkey|L_Showkey|scancode\(4))))) # (!\L_readkey|L_Showkey|scancode\(5) & 
-- (\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(3) $ (\L_readkey|L_Showkey|scancode\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_readkey|L_Showkey|scancode\(3),
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \L_tone_generation|L_key2pulselength|Mux7~4_combout\);

-- Location: LCCOMB_X62_Y50_N14
\L_tone_generation|L_key2pulselength|Mux7~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux7~11_combout\ = (!\L_readkey|L_Showkey|scancode\(1) & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (!\L_readkey|L_Showkey|scancode\(6) & \L_tone_generation|L_key2pulselength|Mux7~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux7~4_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux7~11_combout\);

-- Location: LCCOMB_X65_Y50_N0
\L_tone_generation|L_key2pulselength|Mux2~25\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~25_combout\ = (\L_tone_generation|L_key2pulselength|Mux2~27_combout\ & \L_tone_generation|L_key2pulselength|Mux2~24_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_key2pulselength|Mux2~27_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux2~24_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux2~25_combout\);

-- Location: LCCOMB_X65_Y51_N28
\L_tone_generation|L_clock_generator|Equal2~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_clock_generator|Equal2~8_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (!\L_readkey|L_Showkey|scancode\(7) & \L_readkey|L_Showkey|scancode\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_readkey|L_Showkey|scancode\(2),
	combout => \L_tone_generation|L_clock_generator|Equal2~8_combout\);

-- Location: LCCOMB_X66_Y50_N2
\L_readkey|L_Constantkey|dig2[6]~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_readkey|L_Constantkey|dig2[6]~5_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \L_readkey|L_Showkey|scancode\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(6),
	combout => \L_readkey|L_Constantkey|dig2[6]~5_combout\);

-- Location: LCCOMB_X65_Y50_N30
\L_tone_generation|L_key2pulselength|Mux8~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux8~19_combout\ = ((!\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(5) & !\L_readkey|L_Showkey|scancode\(4)))) # (!\L_readkey|L_Constantkey|current_state.state_key_pressed~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	combout => \L_tone_generation|L_key2pulselength|Mux8~19_combout\);

-- Location: LCCOMB_X65_Y50_N8
\L_tone_generation|L_key2pulselength|Mux7~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux7~9_combout\ = (\L_readkey|L_Constantkey|dig2[6]~5_combout\ & (\L_tone_generation|L_key2pulselength|Mux8~19_combout\ & (\L_readkey|L_Constantkey|dig2[1]~0_combout\ $ (!\L_readkey|L_Constantkey|dig2[3]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|dig2[6]~5_combout\,
	datab => \L_readkey|L_Constantkey|dig2[1]~0_combout\,
	datac => \L_tone_generation|L_key2pulselength|Mux8~19_combout\,
	datad => \L_readkey|L_Constantkey|dig2[3]~2_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux7~9_combout\);

-- Location: LCCOMB_X65_Y50_N16
\L_tone_generation|L_key2pulselength|Mux7~10\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux7~10_combout\ = (\L_tone_generation|L_key2pulselength|Mux2~25_combout\) # ((\L_tone_generation|L_clock_generator|Equal2~8_combout\ & ((\L_tone_generation|L_key2pulselength|Mux7~11_combout\) # 
-- (\L_tone_generation|L_key2pulselength|Mux7~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux7~11_combout\,
	datab => \L_tone_generation|L_key2pulselength|Mux2~25_combout\,
	datac => \L_tone_generation|L_clock_generator|Equal2~8_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux7~9_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux7~10_combout\);

-- Location: LCCOMB_X65_Y50_N14
\L_tone_generation|L_key2pulselength|pulslength[7]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(7) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(7))) # (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) 
-- & ((\L_tone_generation|L_key2pulselength|Mux7~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_key2pulselength|pulslength\(7),
	datac => \L_tone_generation|L_key2pulselength|Mux7~10_combout\,
	datad => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(7));

-- Location: LCCOMB_X66_Y49_N24
\L_tone_generation|L_pulselength2audio|LessThan0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|LessThan0~1_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(5)) # ((\L_tone_generation|L_key2pulselength|pulslength\(4)) # ((\L_tone_generation|L_key2pulselength|pulslength\(6)) # 
-- (\L_tone_generation|L_key2pulselength|pulslength\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(5),
	datab => \L_tone_generation|L_key2pulselength|pulslength\(4),
	datac => \L_tone_generation|L_key2pulselength|pulslength\(6),
	datad => \L_tone_generation|L_key2pulselength|pulslength\(7),
	combout => \L_tone_generation|L_pulselength2audio|LessThan0~1_combout\);

-- Location: LCCOMB_X65_Y52_N4
\L_tone_generation|L_key2pulselength|Mux8~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux8~11_combout\ = (\L_readkey|L_Showkey|scancode\(3) & ((\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(6) & \L_readkey|L_Showkey|scancode\(5))) # (!\L_readkey|L_Showkey|scancode\(4) & 
-- (\L_readkey|L_Showkey|scancode\(6) & !\L_readkey|L_Showkey|scancode\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_readkey|L_Showkey|scancode\(5),
	combout => \L_tone_generation|L_key2pulselength|Mux8~11_combout\);

-- Location: LCCOMB_X65_Y52_N16
\L_tone_generation|L_key2pulselength|Mux8~21\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux8~21_combout\ = (\L_readkey|L_Showkey|scancode\(3) & ((\L_readkey|L_Showkey|scancode\(5) $ (\L_readkey|L_Showkey|scancode\(0))))) # (!\L_readkey|L_Showkey|scancode\(3) & ((\L_readkey|L_Showkey|scancode\(5) & 
-- ((\L_readkey|L_Showkey|scancode\(0)))) # (!\L_readkey|L_Showkey|scancode\(5) & ((!\L_readkey|L_Showkey|scancode\(0)) # (!\L_readkey|L_Showkey|scancode\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110111000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_key2pulselength|Mux8~21_combout\);

-- Location: LCCOMB_X65_Y52_N10
\L_tone_generation|L_key2pulselength|Mux8~22\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux8~22_combout\ = (\L_tone_generation|L_key2pulselength|Mux8~21_combout\ & (\L_readkey|L_Showkey|scancode\(6) $ (((\L_readkey|L_Showkey|scancode\(5)) # (\L_readkey|L_Showkey|scancode\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux8~21_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux8~22_combout\);

-- Location: LCCOMB_X65_Y52_N12
\L_tone_generation|L_key2pulselength|Mux8~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux8~6_combout\ = (\L_readkey|L_Showkey|scancode\(1) & (!\L_readkey|L_Showkey|scancode\(0) & (\L_tone_generation|L_key2pulselength|Mux8~11_combout\))) # (!\L_readkey|L_Showkey|scancode\(1) & 
-- (((\L_tone_generation|L_key2pulselength|Mux8~22_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_tone_generation|L_key2pulselength|Mux8~11_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux8~22_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux8~6_combout\);

-- Location: LCCOMB_X65_Y52_N6
\L_tone_generation|L_key2pulselength|Mux8~20\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux8~20_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_readkey|L_Showkey|scancode\(2) & (!\L_readkey|L_Showkey|scancode\(7) & \L_tone_generation|L_key2pulselength|Mux8~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Showkey|scancode\(2),
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_tone_generation|L_key2pulselength|Mux8~6_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux8~20_combout\);

-- Location: LCCOMB_X66_Y49_N10
\L_tone_generation|L_key2pulselength|pulslength[8]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(8) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(8))) # (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) 
-- & ((\L_tone_generation|L_key2pulselength|Mux8~20_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(8),
	datab => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	datac => \L_tone_generation|L_key2pulselength|Mux8~20_combout\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(8));

-- Location: LCCOMB_X63_Y49_N26
\L_tone_generation|L_key2pulselength|Mux9~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux9~9_combout\ = (\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(1) & ((\L_readkey|L_Showkey|scancode\(3)) # (\L_readkey|L_Showkey|scancode\(5))))) # (!\L_readkey|L_Showkey|scancode\(0) & 
-- (!\L_readkey|L_Showkey|scancode\(3) & (!\L_readkey|L_Showkey|scancode\(5) & \L_readkey|L_Showkey|scancode\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \L_tone_generation|L_key2pulselength|Mux9~9_combout\);

-- Location: LCCOMB_X63_Y49_N6
\L_tone_generation|L_key2pulselength|Mux9~18\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux9~18_combout\ = (\L_readkey|L_Showkey|scancode\(0) & (((!\L_readkey|L_Showkey|scancode\(1))))) # (!\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(3) & (\L_readkey|L_Showkey|scancode\(5) $ 
-- (!\L_readkey|L_Showkey|scancode\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001011001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_readkey|L_Showkey|scancode\(3),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \L_tone_generation|L_key2pulselength|Mux9~18_combout\);

-- Location: LCCOMB_X63_Y49_N12
\L_tone_generation|L_key2pulselength|Mux9~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux9~19_combout\ = (\L_tone_generation|L_key2pulselength|Mux9~18_combout\ & ((\L_readkey|L_Showkey|scancode\(6) & ((!\L_readkey|L_Showkey|scancode\(5)))) # (!\L_readkey|L_Showkey|scancode\(6) & 
-- ((\L_readkey|L_Showkey|scancode\(3)) # (\L_readkey|L_Showkey|scancode\(5))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(6),
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_tone_generation|L_key2pulselength|Mux9~18_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux9~19_combout\);

-- Location: LCCOMB_X63_Y49_N28
\L_tone_generation|L_key2pulselength|Mux9~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux9~4_combout\ = (\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(6) & (\L_tone_generation|L_key2pulselength|Mux9~9_combout\))) # (!\L_readkey|L_Showkey|scancode\(4) & 
-- (((\L_tone_generation|L_key2pulselength|Mux9~19_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_tone_generation|L_key2pulselength|Mux9~9_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux9~19_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux9~4_combout\);

-- Location: LCCOMB_X66_Y49_N8
\L_tone_generation|L_key2pulselength|Mux9~17\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux9~17_combout\ = (\L_readkey|L_Showkey|scancode\(2) & (!\L_readkey|L_Showkey|scancode\(7) & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \L_tone_generation|L_key2pulselength|Mux9~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(2),
	datab => \L_readkey|L_Showkey|scancode\(7),
	datac => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \L_tone_generation|L_key2pulselength|Mux9~4_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux9~17_combout\);

-- Location: LCCOMB_X66_Y49_N20
\L_tone_generation|L_key2pulselength|pulslength[9]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(9) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & ((\L_tone_generation|L_key2pulselength|pulslength\(9)))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|Mux9~17_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	datac => \L_tone_generation|L_key2pulselength|Mux9~17_combout\,
	datad => \L_tone_generation|L_key2pulselength|pulslength\(9),
	combout => \L_tone_generation|L_key2pulselength|pulslength\(9));

-- Location: LCCOMB_X63_Y49_N2
\L_tone_generation|L_key2pulselength|Mux11~20\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux11~20_combout\ = (!\L_readkey|L_Showkey|scancode\(6) & ((\L_readkey|L_Showkey|scancode\(0) & ((!\L_readkey|L_Showkey|scancode\(1)))) # (!\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \L_tone_generation|L_key2pulselength|Mux11~20_combout\);

-- Location: LCCOMB_X63_Y49_N8
\L_tone_generation|L_key2pulselength|Mux11~21\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux11~21_combout\ = (\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(6) & \L_tone_generation|L_key2pulselength|Mux11~20_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux11~20_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux11~21_combout\);

-- Location: LCCOMB_X63_Y49_N10
\L_tone_generation|L_key2pulselength|Mux11~22\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux11~22_combout\ = (\L_readkey|L_Showkey|scancode\(1) & (!\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(4) $ (\L_readkey|L_Showkey|scancode\(3))))) # (!\L_readkey|L_Showkey|scancode\(1) & 
-- (\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(4) $ (\L_readkey|L_Showkey|scancode\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(3),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_key2pulselength|Mux11~22_combout\);

-- Location: LCCOMB_X63_Y49_N20
\L_tone_generation|L_key2pulselength|Mux11~23\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux11~23_combout\ = (\L_readkey|L_Showkey|scancode\(1) & (\L_tone_generation|L_key2pulselength|Mux11~22_combout\ & (\L_readkey|L_Showkey|scancode\(3) $ (!\L_readkey|L_Showkey|scancode\(6))))) # 
-- (!\L_readkey|L_Showkey|scancode\(1) & (\L_tone_generation|L_key2pulselength|Mux11~22_combout\ $ (((!\L_readkey|L_Showkey|scancode\(3) & \L_readkey|L_Showkey|scancode\(6))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010011100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux11~22_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux11~23_combout\);

-- Location: LCCOMB_X63_Y49_N24
\L_tone_generation|L_key2pulselength|Mux11~18\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux11~18_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & ((\L_readkey|L_Showkey|scancode\(5) & (\L_tone_generation|L_key2pulselength|Mux11~21_combout\)) # (!\L_readkey|L_Showkey|scancode\(5) & 
-- ((\L_tone_generation|L_key2pulselength|Mux11~23_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_tone_generation|L_key2pulselength|Mux11~21_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux11~23_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux11~18_combout\);

-- Location: LCCOMB_X67_Y50_N18
\L_tone_generation|L_key2pulselength|Mux11~14\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux11~14_combout\ = (\L_readkey|L_Showkey|scancode\(4) & (\L_readkey|L_Showkey|scancode\(3) & \L_readkey|L_Showkey|scancode\(1))) # (!\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(3),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \L_tone_generation|L_key2pulselength|Mux11~14_combout\);

-- Location: LCCOMB_X67_Y50_N16
\L_tone_generation|L_key2pulselength|Mux11~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux11~19_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_readkey|L_Showkey|scancode\(0) & \L_tone_generation|L_key2pulselength|Mux11~14_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(0),
	datad => \L_tone_generation|L_key2pulselength|Mux11~14_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux11~19_combout\);

-- Location: LCCOMB_X66_Y49_N0
\L_tone_generation|L_key2pulselength|Mux11~17\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux11~17_combout\ = (\L_tone_generation|L_key2pulselength|Mux2~27_combout\ & ((\L_tone_generation|L_key2pulselength|Mux11~19_combout\) # ((\L_tone_generation|L_key2pulselength|Mux11~18_combout\ & 
-- \L_tone_generation|L_clock_generator|Equal2~8_combout\)))) # (!\L_tone_generation|L_key2pulselength|Mux2~27_combout\ & (\L_tone_generation|L_key2pulselength|Mux11~18_combout\ & (\L_tone_generation|L_clock_generator|Equal2~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux2~27_combout\,
	datab => \L_tone_generation|L_key2pulselength|Mux11~18_combout\,
	datac => \L_tone_generation|L_clock_generator|Equal2~8_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux11~19_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux11~17_combout\);

-- Location: LCCOMB_X66_Y49_N22
\L_tone_generation|L_key2pulselength|pulslength[11]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(11) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & ((\L_tone_generation|L_key2pulselength|pulslength\(11)))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|Mux11~17_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux11~17_combout\,
	datab => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	datac => \L_tone_generation|L_key2pulselength|pulslength\(11),
	combout => \L_tone_generation|L_key2pulselength|pulslength\(11));

-- Location: LCCOMB_X67_Y50_N2
\L_tone_generation|L_key2pulselength|Mux10~16\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux10~16_combout\ = (\L_readkey|L_Showkey|scancode\(0) & (((\L_readkey|L_Showkey|scancode\(4) & !\L_readkey|L_Showkey|scancode\(1))))) # (!\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(3) $ 
-- ((!\L_readkey|L_Showkey|scancode\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000111100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \L_tone_generation|L_key2pulselength|Mux10~16_combout\);

-- Location: LCCOMB_X67_Y50_N6
\L_tone_generation|L_key2pulselength|Mux10~13\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux10~13_combout\ = (!\L_readkey|L_Showkey|scancode\(7) & (!\L_readkey|L_Showkey|scancode\(6) & \L_tone_generation|L_key2pulselength|Mux10~16_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(7),
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux10~16_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux10~13_combout\);

-- Location: LCCOMB_X67_Y50_N8
\L_tone_generation|L_key2pulselength|Mux10~25\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux10~25_combout\ = (\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_readkey|L_Showkey|scancode\(5) & \L_tone_generation|L_key2pulselength|Mux10~13_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(2),
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_tone_generation|L_key2pulselength|Mux10~13_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux10~25_combout\);

-- Location: LCCOMB_X64_Y50_N28
\L_tone_generation|L_key2pulselength|Mux10~24\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux10~24_combout\ = (\L_tone_generation|L_clock_generator|Equal2~9_combout\ & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_readkey|L_Showkey|scancode\(4) & \L_readkey|L_Showkey|scancode\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|Equal2~9_combout\,
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_key2pulselength|Mux10~24_combout\);

-- Location: LCCOMB_X64_Y49_N8
\L_tone_generation|L_key2pulselength|Mux6~24\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux6~24_combout\ = (\L_readkey|L_Constantkey|dig2[0]~1_combout\ & (!\L_readkey|L_Constantkey|dig2[3]~2_combout\ & (\L_tone_generation|L_key2pulselength|Mux2~27_combout\ & !\L_readkey|L_Constantkey|dig2[4]~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|dig2[0]~1_combout\,
	datab => \L_readkey|L_Constantkey|dig2[3]~2_combout\,
	datac => \L_tone_generation|L_key2pulselength|Mux2~27_combout\,
	datad => \L_readkey|L_Constantkey|dig2[4]~3_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux6~24_combout\);

-- Location: LCCOMB_X64_Y50_N4
\L_tone_generation|L_key2pulselength|Mux10~23\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux10~23_combout\ = (\L_tone_generation|L_key2pulselength|Mux10~25_combout\) # ((\L_tone_generation|L_key2pulselength|Mux10~24_combout\) # ((\L_readkey|L_Constantkey|dig2[1]~0_combout\ & 
-- \L_tone_generation|L_key2pulselength|Mux6~24_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|dig2[1]~0_combout\,
	datab => \L_tone_generation|L_key2pulselength|Mux10~25_combout\,
	datac => \L_tone_generation|L_key2pulselength|Mux10~24_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux6~24_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux10~23_combout\);

-- Location: LCCOMB_X64_Y50_N18
\L_tone_generation|L_key2pulselength|pulslength[10]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(10) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(10))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & ((\L_tone_generation|L_key2pulselength|Mux10~23_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_key2pulselength|pulslength\(10),
	datac => \L_tone_generation|L_key2pulselength|Mux10~23_combout\,
	datad => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(10));

-- Location: LCCOMB_X66_Y49_N14
\L_tone_generation|L_pulselength2audio|LessThan0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|LessThan0~2_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(8)) # ((\L_tone_generation|L_key2pulselength|pulslength\(9)) # ((\L_tone_generation|L_key2pulselength|pulslength\(11)) # 
-- (\L_tone_generation|L_key2pulselength|pulslength\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(8),
	datab => \L_tone_generation|L_key2pulselength|pulslength\(9),
	datac => \L_tone_generation|L_key2pulselength|pulslength\(11),
	datad => \L_tone_generation|L_key2pulselength|pulslength\(10),
	combout => \L_tone_generation|L_pulselength2audio|LessThan0~2_combout\);

-- Location: LCCOMB_X66_Y51_N18
\L_tone_generation|L_key2pulselength|Mux0~13\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux0~13_combout\ = (!\L_readkey|L_Showkey|scancode\(0) & ((\L_readkey|L_Showkey|scancode\(6) & (!\L_readkey|L_Showkey|scancode\(5) & !\L_readkey|L_Showkey|scancode\(4))) # (!\L_readkey|L_Showkey|scancode\(6) & 
-- (\L_readkey|L_Showkey|scancode\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(6),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_key2pulselength|Mux0~13_combout\);

-- Location: LCCOMB_X66_Y51_N8
\L_tone_generation|L_key2pulselength|Mux0~14\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux0~14_combout\ = (\L_tone_generation|L_key2pulselength|Mux0~13_combout\ & ((\L_readkey|L_Showkey|scancode\(3) & (!\L_readkey|L_Showkey|scancode\(5) & !\L_readkey|L_Showkey|scancode\(4))) # 
-- (!\L_readkey|L_Showkey|scancode\(3) & (\L_readkey|L_Showkey|scancode\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_tone_generation|L_key2pulselength|Mux0~13_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux0~14_combout\);

-- Location: LCCOMB_X66_Y51_N10
\L_tone_generation|L_key2pulselength|Mux0~15\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux0~15_combout\ = (\L_readkey|L_Showkey|scancode\(6) & (!\L_readkey|L_Showkey|scancode\(5) & (!\L_readkey|L_Showkey|scancode\(4) & \L_readkey|L_Showkey|scancode\(0)))) # (!\L_readkey|L_Showkey|scancode\(6) & 
-- (\L_readkey|L_Showkey|scancode\(0) $ (((\L_readkey|L_Showkey|scancode\(5) & \L_readkey|L_Showkey|scancode\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(6),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_key2pulselength|Mux0~15_combout\);

-- Location: LCCOMB_X66_Y51_N16
\L_tone_generation|L_key2pulselength|Mux0~16\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux0~16_combout\ = (\L_tone_generation|L_key2pulselength|Mux0~15_combout\ & ((\L_readkey|L_Showkey|scancode\(3)) # (\L_readkey|L_Showkey|scancode\(5) $ (\L_readkey|L_Showkey|scancode\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(3),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_tone_generation|L_key2pulselength|Mux0~15_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux0~16_combout\);

-- Location: LCCOMB_X66_Y51_N30
\L_tone_generation|L_key2pulselength|Mux0~12\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux0~12_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & ((\L_readkey|L_Showkey|scancode\(1) & (\L_tone_generation|L_key2pulselength|Mux0~14_combout\)) # (!\L_readkey|L_Showkey|scancode\(1) & 
-- ((\L_tone_generation|L_key2pulselength|Mux0~16_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_tone_generation|L_key2pulselength|Mux0~14_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux0~16_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux0~12_combout\);

-- Location: LCCOMB_X66_Y51_N14
\L_tone_generation|L_key2pulselength|Mux0~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux0~11_combout\ = (\L_tone_generation|L_clock_generator|Equal2~8_combout\ & ((\L_tone_generation|L_key2pulselength|Mux0~12_combout\) # ((\L_tone_generation|L_key2pulselength|Mux2~27_combout\ & 
-- \L_tone_generation|L_key2pulselength|Mux4~25_combout\)))) # (!\L_tone_generation|L_clock_generator|Equal2~8_combout\ & (\L_tone_generation|L_key2pulselength|Mux2~27_combout\ & ((\L_tone_generation|L_key2pulselength|Mux4~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_clock_generator|Equal2~8_combout\,
	datab => \L_tone_generation|L_key2pulselength|Mux2~27_combout\,
	datac => \L_tone_generation|L_key2pulselength|Mux0~12_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux4~25_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux0~11_combout\);

-- Location: LCCOMB_X66_Y51_N12
\L_tone_generation|L_key2pulselength|pulslength[2]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(2) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(2))) # (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) 
-- & ((\L_tone_generation|L_key2pulselength|Mux0~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(2),
	datac => \L_tone_generation|L_key2pulselength|Mux0~11_combout\,
	datad => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(2));

-- Location: LCCOMB_X67_Y51_N12
\L_tone_generation|L_key2pulselength|Mux2~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~9_combout\ = (!\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(3) $ (((\L_readkey|L_Showkey|scancode\(1)) # (!\L_readkey|L_Showkey|scancode\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datab => \L_readkey|L_Showkey|scancode\(0),
	datac => \L_readkey|L_Showkey|scancode\(1),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux2~9_combout\);

-- Location: LCCOMB_X66_Y51_N4
\L_tone_generation|L_key2pulselength|Mux2~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~8_combout\ = (\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(3) & (\L_readkey|L_Showkey|scancode\(4) & !\L_readkey|L_Showkey|scancode\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \L_tone_generation|L_key2pulselength|Mux2~8_combout\);

-- Location: LCCOMB_X67_Y51_N26
\L_tone_generation|L_key2pulselength|Mux2~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~11_combout\ = (!\L_readkey|L_Showkey|scancode\(4) & ((\L_readkey|L_Showkey|scancode\(1) & (!\L_readkey|L_Showkey|scancode\(0) & \L_readkey|L_Showkey|scancode\(3))) # (!\L_readkey|L_Showkey|scancode\(1) & 
-- (\L_readkey|L_Showkey|scancode\(0) & !\L_readkey|L_Showkey|scancode\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(4),
	datab => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_readkey|L_Showkey|scancode\(0),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux2~11_combout\);

-- Location: LCCOMB_X66_Y51_N6
\L_tone_generation|L_key2pulselength|Mux2~28\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~28_combout\ = (\L_readkey|L_Showkey|scancode\(6) & (!\L_readkey|L_Showkey|scancode\(5) & ((\L_tone_generation|L_key2pulselength|Mux2~11_combout\)))) # (!\L_readkey|L_Showkey|scancode\(6) & 
-- ((\L_readkey|L_Showkey|scancode\(5)) # ((\L_tone_generation|L_key2pulselength|Mux2~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(6),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_tone_generation|L_key2pulselength|Mux2~8_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux2~11_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux2~28_combout\);

-- Location: LCCOMB_X66_Y51_N28
\L_tone_generation|L_key2pulselength|Mux2~29\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~29_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_tone_generation|L_key2pulselength|Mux2~28_combout\ & ((\L_tone_generation|L_key2pulselength|Mux2~9_combout\) # 
-- (!\L_readkey|L_Showkey|scancode\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_tone_generation|L_key2pulselength|Mux2~9_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux2~28_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux2~29_combout\);

-- Location: LCCOMB_X66_Y51_N24
\L_tone_generation|L_key2pulselength|Mux2~26\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux2~26_combout\ = (\L_tone_generation|L_key2pulselength|Mux2~24_combout\ & ((\L_tone_generation|L_key2pulselength|Mux2~27_combout\) # ((\L_tone_generation|L_key2pulselength|Mux2~29_combout\ & 
-- \L_tone_generation|L_clock_generator|Equal2~8_combout\)))) # (!\L_tone_generation|L_key2pulselength|Mux2~24_combout\ & (\L_tone_generation|L_key2pulselength|Mux2~29_combout\ & ((\L_tone_generation|L_clock_generator|Equal2~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux2~24_combout\,
	datab => \L_tone_generation|L_key2pulselength|Mux2~29_combout\,
	datac => \L_tone_generation|L_key2pulselength|Mux2~27_combout\,
	datad => \L_tone_generation|L_clock_generator|Equal2~8_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux2~26_combout\);

-- Location: LCCOMB_X66_Y51_N2
\L_tone_generation|L_key2pulselength|pulslength[0]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(0) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & ((\L_tone_generation|L_key2pulselength|pulslength\(0)))) # 
-- (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|Mux2~26_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_key2pulselength|Mux2~26_combout\,
	datac => \L_tone_generation|L_key2pulselength|pulslength\(0),
	datad => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(0));

-- Location: LCCOMB_X65_Y51_N30
\L_tone_generation|L_key2pulselength|Mux3~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux3~6_combout\ = (\L_readkey|L_Showkey|scancode\(5) & ((\L_readkey|L_Showkey|scancode\(1) & (\L_readkey|L_Showkey|scancode\(4))) # (!\L_readkey|L_Showkey|scancode\(1) & ((\L_readkey|L_Showkey|scancode\(3)) # 
-- (!\L_readkey|L_Showkey|scancode\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux3~6_combout\);

-- Location: LCCOMB_X65_Y51_N16
\L_tone_generation|L_key2pulselength|Mux3~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux3~7_combout\ = (!\L_readkey|L_Showkey|scancode\(1) & (!\L_readkey|L_Showkey|scancode\(4) & (\L_readkey|L_Showkey|scancode\(5) $ (\L_readkey|L_Showkey|scancode\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux3~7_combout\);

-- Location: LCCOMB_X65_Y51_N0
\L_tone_generation|L_key2pulselength|Mux3~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux3~3_combout\ = (!\L_readkey|L_Showkey|scancode\(7) & ((\L_readkey|L_Showkey|scancode\(0) & ((\L_tone_generation|L_key2pulselength|Mux3~7_combout\))) # (!\L_readkey|L_Showkey|scancode\(0) & 
-- (\L_tone_generation|L_key2pulselength|Mux3~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux3~6_combout\,
	datab => \L_tone_generation|L_key2pulselength|Mux3~7_combout\,
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_readkey|L_Showkey|scancode\(0),
	combout => \L_tone_generation|L_key2pulselength|Mux3~3_combout\);

-- Location: LCCOMB_X65_Y51_N18
\L_tone_generation|L_key2pulselength|Mux3~34\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux3~34_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (!\L_readkey|L_Showkey|scancode\(6) & (\L_readkey|L_Showkey|scancode\(2) & \L_tone_generation|L_key2pulselength|Mux3~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_tone_generation|L_key2pulselength|Mux3~3_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux3~34_combout\);

-- Location: LCCOMB_X67_Y50_N12
\L_tone_generation|L_key2pulselength|Mux3~20\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux3~20_combout\ = (!\L_readkey|L_Showkey|scancode\(5) & (\L_readkey|L_Showkey|scancode\(1) $ (!\L_readkey|L_Showkey|scancode\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(1),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux3~20_combout\);

-- Location: LCCOMB_X67_Y50_N10
\L_tone_generation|L_key2pulselength|Mux3~17\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux3~17_combout\ = (!\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(4) & (!\L_readkey|L_Showkey|scancode\(7) & \L_tone_generation|L_key2pulselength|Mux3~20_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_tone_generation|L_key2pulselength|Mux3~20_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux3~17_combout\);

-- Location: LCCOMB_X67_Y50_N0
\L_tone_generation|L_key2pulselength|Mux3~35\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux3~35_combout\ = (\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & (\L_readkey|L_Showkey|scancode\(6) & \L_tone_generation|L_key2pulselength|Mux3~17_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(2),
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datac => \L_readkey|L_Showkey|scancode\(6),
	datad => \L_tone_generation|L_key2pulselength|Mux3~17_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux3~35_combout\);

-- Location: LCCOMB_X66_Y51_N0
\L_tone_generation|L_key2pulselength|Mux3~33\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux3~33_combout\ = (\L_tone_generation|L_key2pulselength|Mux3~34_combout\) # ((\L_tone_generation|L_key2pulselength|Mux3~35_combout\) # ((\L_tone_generation|L_key2pulselength|Mux2~27_combout\ & 
-- \L_tone_generation|L_key2pulselength|Mux4~25_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux2~27_combout\,
	datab => \L_tone_generation|L_key2pulselength|Mux3~34_combout\,
	datac => \L_tone_generation|L_key2pulselength|Mux3~35_combout\,
	datad => \L_tone_generation|L_key2pulselength|Mux4~25_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux3~33_combout\);

-- Location: LCCOMB_X66_Y51_N22
\L_tone_generation|L_key2pulselength|pulslength[3]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(3) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(3))) # (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) 
-- & ((\L_tone_generation|L_key2pulselength|Mux3~33_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(3),
	datac => \L_tone_generation|L_key2pulselength|Mux3~33_combout\,
	datad => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(3));

-- Location: LCCOMB_X65_Y51_N12
\L_tone_generation|L_key2pulselength|Mux1~20\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux1~20_combout\ = (\L_readkey|L_Showkey|scancode\(3) & (\L_readkey|L_Showkey|scancode\(5) $ (((\L_readkey|L_Showkey|scancode\(0) & !\L_readkey|L_Showkey|scancode\(4)))))) # (!\L_readkey|L_Showkey|scancode\(3) & 
-- (\L_readkey|L_Showkey|scancode\(0) & ((\L_readkey|L_Showkey|scancode\(4)) # (\L_readkey|L_Showkey|scancode\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux1~20_combout\);

-- Location: LCCOMB_X65_Y51_N10
\L_tone_generation|L_key2pulselength|Mux1~21\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux1~21_combout\ = (\L_readkey|L_Showkey|scancode\(1) & (\L_readkey|L_Showkey|scancode\(4) & (\L_readkey|L_Showkey|scancode\(5) & !\L_tone_generation|L_key2pulselength|Mux1~20_combout\))) # 
-- (!\L_readkey|L_Showkey|scancode\(1) & (((\L_tone_generation|L_key2pulselength|Mux1~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(1),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(5),
	datad => \L_tone_generation|L_key2pulselength|Mux1~20_combout\,
	combout => \L_tone_generation|L_key2pulselength|Mux1~21_combout\);

-- Location: LCCOMB_X65_Y51_N2
\L_tone_generation|L_key2pulselength|Mux1~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux1~11_combout\ = (\L_readkey|L_Showkey|scancode\(1) & (!\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(4) & \L_readkey|L_Showkey|scancode\(3)))) # (!\L_readkey|L_Showkey|scancode\(1) & 
-- (!\L_readkey|L_Showkey|scancode\(3) & ((!\L_readkey|L_Showkey|scancode\(4)) # (!\L_readkey|L_Showkey|scancode\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(4),
	datac => \L_readkey|L_Showkey|scancode\(1),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \L_tone_generation|L_key2pulselength|Mux1~11_combout\);

-- Location: LCCOMB_X65_Y51_N14
\L_tone_generation|L_key2pulselength|Mux1~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux1~6_combout\ = (\L_readkey|L_Showkey|scancode\(6) & (((!\L_readkey|L_Showkey|scancode\(5) & \L_tone_generation|L_key2pulselength|Mux1~11_combout\)))) # (!\L_readkey|L_Showkey|scancode\(6) & 
-- (\L_tone_generation|L_key2pulselength|Mux1~21_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|Mux1~21_combout\,
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_tone_generation|L_key2pulselength|Mux1~11_combout\,
	datad => \L_readkey|L_Showkey|scancode\(6),
	combout => \L_tone_generation|L_key2pulselength|Mux1~6_combout\);

-- Location: LCCOMB_X65_Y51_N24
\L_tone_generation|L_key2pulselength|Mux1~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|Mux1~19_combout\ = (!\L_readkey|L_Showkey|scancode\(7) & (\L_readkey|L_Showkey|scancode\(2) & (\L_tone_generation|L_key2pulselength|Mux1~6_combout\ & \L_readkey|L_Constantkey|current_state.state_key_pressed~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(7),
	datab => \L_readkey|L_Showkey|scancode\(2),
	datac => \L_tone_generation|L_key2pulselength|Mux1~6_combout\,
	datad => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	combout => \L_tone_generation|L_key2pulselength|Mux1~19_combout\);

-- Location: LCCOMB_X65_Y51_N20
\L_tone_generation|L_key2pulselength|pulslength[1]\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_key2pulselength|pulslength\(1) = (GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) & (\L_tone_generation|L_key2pulselength|pulslength\(1))) # (!GLOBAL(\L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\) 
-- & ((\L_tone_generation|L_key2pulselength|Mux1~19_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_key2pulselength|pulslength\(1),
	datac => \L_tone_generation|L_key2pulselength|Mux1~19_combout\,
	datad => \L_tone_generation|L_clock_generator|Equal0~19clkctrl_outclk\,
	combout => \L_tone_generation|L_key2pulselength|pulslength\(1));

-- Location: LCCOMB_X66_Y51_N20
\L_tone_generation|L_pulselength2audio|LessThan0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|LessThan0~0_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(2)) # ((\L_tone_generation|L_key2pulselength|pulslength\(0)) # ((\L_tone_generation|L_key2pulselength|pulslength\(3)) # 
-- (\L_tone_generation|L_key2pulselength|pulslength\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(2),
	datab => \L_tone_generation|L_key2pulselength|pulslength\(0),
	datac => \L_tone_generation|L_key2pulselength|pulslength\(3),
	datad => \L_tone_generation|L_key2pulselength|pulslength\(1),
	combout => \L_tone_generation|L_pulselength2audio|LessThan0~0_combout\);

-- Location: LCCOMB_X66_Y49_N28
\L_tone_generation|L_pulselength2audio|LessThan0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(12)) # ((\L_tone_generation|L_pulselength2audio|LessThan0~1_combout\) # ((\L_tone_generation|L_pulselength2audio|LessThan0~2_combout\) # 
-- (\L_tone_generation|L_pulselength2audio|LessThan0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(12),
	datab => \L_tone_generation|L_pulselength2audio|LessThan0~1_combout\,
	datac => \L_tone_generation|L_pulselength2audio|LessThan0~2_combout\,
	datad => \L_tone_generation|L_pulselength2audio|LessThan0~0_combout\,
	combout => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\);

-- Location: FF_X67_Y48_N15
\L_tone_generation|L_pulselength2audio|COUNT2:counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\);

-- Location: LCCOMB_X67_Y48_N22
\L_tone_generation|L_pulselength2audio|Equal2~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|Equal2~3_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\ & 
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\,
	combout => \L_tone_generation|L_pulselength2audio|Equal2~3_combout\);

-- Location: LCCOMB_X67_Y48_N24
\L_tone_generation|L_pulselength2audio|Equal2~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|Equal2~2_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\ & 
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\,
	combout => \L_tone_generation|L_pulselength2audio|Equal2~2_combout\);

-- Location: LCCOMB_X67_Y49_N12
\L_tone_generation|L_pulselength2audio|Equal2~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|Equal2~4_combout\ = (\L_tone_generation|L_pulselength2audio|Equal2~0_combout\ & (\L_tone_generation|L_pulselength2audio|Equal2~1_combout\ & (\L_tone_generation|L_pulselength2audio|Equal2~3_combout\ & 
-- \L_tone_generation|L_pulselength2audio|Equal2~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|Equal2~0_combout\,
	datab => \L_tone_generation|L_pulselength2audio|Equal2~1_combout\,
	datac => \L_tone_generation|L_pulselength2audio|Equal2~3_combout\,
	datad => \L_tone_generation|L_pulselength2audio|Equal2~2_combout\,
	combout => \L_tone_generation|L_pulselength2audio|Equal2~4_combout\);

-- Location: LCCOMB_X67_Y48_N16
\L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~1_combout\ = \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~q\ $ (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~q\,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~1_combout\);

-- Location: FF_X67_Y48_N17
\L_tone_generation|L_pulselength2audio|COUNT2:counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~q\);

-- Location: LCCOMB_X66_Y49_N4
\L_tone_generation|L_pulselength2audio|COUNT2~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~5_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(8) & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(9) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\)))) # (!\L_tone_generation|L_key2pulselength|pulslength\(8) & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(9) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(8),
	datab => \L_tone_generation|L_key2pulselength|pulslength\(9),
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~5_combout\);

-- Location: LCCOMB_X66_Y49_N2
\L_tone_generation|L_pulselength2audio|COUNT2~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~6_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(11) & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\ $ 
-- (!\L_tone_generation|L_key2pulselength|pulslength\(10))))) # (!\L_tone_generation|L_key2pulselength|pulslength\(11) & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\ $ 
-- (!\L_tone_generation|L_key2pulselength|pulslength\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(11),
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\,
	datad => \L_tone_generation|L_key2pulselength|pulslength\(10),
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~6_combout\);

-- Location: LCCOMB_X66_Y49_N16
\L_tone_generation|L_pulselength2audio|COUNT2~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~7_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2~5_combout\ & (\L_tone_generation|L_pulselength2audio|COUNT2~6_combout\ & (\L_tone_generation|L_key2pulselength|pulslength\(12) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(12),
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2~5_combout\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2~6_combout\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~7_combout\);

-- Location: LCCOMB_X67_Y49_N10
\L_tone_generation|L_pulselength2audio|COUNT2~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~3_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(7) & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\ $ 
-- (!\L_tone_generation|L_key2pulselength|pulslength\(6))))) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\ & (!\L_tone_generation|L_key2pulselength|pulslength\(7) & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\ $ 
-- (!\L_tone_generation|L_key2pulselength|pulslength\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\,
	datac => \L_tone_generation|L_key2pulselength|pulslength\(6),
	datad => \L_tone_generation|L_key2pulselength|pulslength\(7),
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~3_combout\);

-- Location: LCCOMB_X66_Y49_N6
\L_tone_generation|L_pulselength2audio|COUNT2~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~2_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(5) & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(4) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\)))) # (!\L_tone_generation|L_key2pulselength|pulslength\(5) & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(4) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(5),
	datab => \L_tone_generation|L_key2pulselength|pulslength\(4),
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~2_combout\);

-- Location: LCCOMB_X67_Y49_N4
\L_tone_generation|L_pulselength2audio|COUNT2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~0_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(0) & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(1) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\)))) # (!\L_tone_generation|L_key2pulselength|pulslength\(0) & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(1) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(0),
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\,
	datac => \L_tone_generation|L_key2pulselength|pulslength\(1),
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~0_combout\);

-- Location: LCCOMB_X66_Y51_N26
\L_tone_generation|L_pulselength2audio|COUNT2~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~1_combout\ = (\L_tone_generation|L_key2pulselength|pulslength\(3) & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(2) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\)))) # (!\L_tone_generation|L_key2pulselength|pulslength\(3) & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\ & (\L_tone_generation|L_key2pulselength|pulslength\(2) $ 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_key2pulselength|pulslength\(3),
	datab => \L_tone_generation|L_key2pulselength|pulslength\(2),
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~1_combout\);

-- Location: LCCOMB_X67_Y49_N8
\L_tone_generation|L_pulselength2audio|COUNT2~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~4_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2~3_combout\ & (\L_tone_generation|L_pulselength2audio|COUNT2~2_combout\ & (\L_tone_generation|L_pulselength2audio|COUNT2~0_combout\ & 
-- \L_tone_generation|L_pulselength2audio|COUNT2~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2~3_combout\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2~2_combout\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2~0_combout\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2~1_combout\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~4_combout\);

-- Location: LCCOMB_X67_Y49_N2
\L_tone_generation|L_pulselength2audio|COUNT2~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~8_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2~7_combout\ & (\L_tone_generation|L_pulselength2audio|audiol~0_combout\ & (\L_tone_generation|L_pulselength2audio|COUNT2~4_combout\ & 
-- \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2~7_combout\,
	datab => \L_tone_generation|L_pulselength2audio|audiol~0_combout\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2~4_combout\,
	datad => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~8_combout\);

-- Location: LCCOMB_X67_Y49_N6
\L_tone_generation|L_pulselength2audio|COUNT2~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2~8_combout\) # ((\L_tone_generation|L_pulselength2audio|Equal2~4_combout\ & \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|Equal2~4_combout\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2~8_combout\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\);

-- Location: FF_X67_Y49_N17
\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\);

-- Location: LCCOMB_X67_Y49_N18
\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~2\)) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\ & ((\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~2\) # (GND)))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~2\ = CARRY((!\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~2\) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~2\);

-- Location: FF_X67_Y49_N19
\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\);

-- Location: LCCOMB_X67_Y49_N20
\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~2\ & VCC))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~2\ = CARRY((\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\ & !\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~2\);

-- Location: FF_X67_Y49_N21
\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\);

-- Location: LCCOMB_X67_Y49_N22
\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~2\)) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\ & ((\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~2\) # (GND)))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~2\ = CARRY((!\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~2\) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~2\);

-- Location: FF_X67_Y49_N23
\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\);

-- Location: LCCOMB_X67_Y49_N24
\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~2\ & VCC))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~2\ = CARRY((\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\ & !\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~2\);

-- Location: FF_X67_Y49_N25
\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\);

-- Location: LCCOMB_X67_Y49_N26
\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~2\)) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\ & ((\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~2\) # (GND)))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~2\ = CARRY((!\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~2\) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~2\);

-- Location: FF_X67_Y49_N27
\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\);

-- Location: LCCOMB_X67_Y49_N28
\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~2\ & VCC))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~2\ = CARRY((\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\ & !\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~2\);

-- Location: FF_X67_Y49_N29
\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\);

-- Location: LCCOMB_X67_Y49_N30
\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~2\)) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\ & ((\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~2\) # (GND)))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~2\ = CARRY((!\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~2\) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~2\);

-- Location: FF_X67_Y49_N31
\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\);

-- Location: LCCOMB_X67_Y48_N0
\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~2\ & VCC))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~2\ = CARRY((\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\ & !\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~2\);

-- Location: FF_X67_Y48_N1
\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\);

-- Location: LCCOMB_X67_Y48_N2
\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~2\)) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\ & ((\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~2\) # (GND)))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~2\ = CARRY((!\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~2\) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~2\);

-- Location: FF_X67_Y48_N3
\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\);

-- Location: LCCOMB_X67_Y48_N4
\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~2\ & VCC))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~2\ = CARRY((\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\ & !\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~2\);

-- Location: FF_X67_Y48_N5
\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\);

-- Location: LCCOMB_X67_Y48_N6
\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~2\)) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\ & ((\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~2\) # (GND)))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~2\ = CARRY((!\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~2\) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~2\);

-- Location: FF_X67_Y48_N7
\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\);

-- Location: LCCOMB_X67_Y48_N8
\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\ & (\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~2\ $ (GND))) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~2\ & VCC))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~2\ = CARRY((\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\ & !\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~2\);

-- Location: FF_X67_Y48_N9
\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\);

-- Location: LCCOMB_X67_Y48_N10
\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~1_combout\ = (\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~2\)) # 
-- (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\ & ((\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~2\) # (GND)))
-- \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~2\ = CARRY((!\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~2\) # (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\,
	datad => VCC,
	cin => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~2\,
	combout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~1_combout\,
	cout => \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~2\);

-- Location: FF_X67_Y48_N11
\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\);

-- Location: FF_X67_Y48_N13
\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~1_combout\,
	clrn => \KEY[1]~input_o\,
	sclr => \L_tone_generation|L_pulselength2audio|COUNT2~9_combout\,
	ena => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\);

-- Location: LCCOMB_X67_Y48_N20
\L_tone_generation|L_pulselength2audio|audiol~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|audiol~0_combout\ = (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\ & 
-- !\L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[14]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[15]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[13]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[16]~q\,
	combout => \L_tone_generation|L_pulselength2audio|audiol~0_combout\);

-- Location: LCCOMB_X67_Y48_N26
\L_tone_generation|L_pulselength2audio|audiol~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|audiol~2_combout\ = (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\ & 
-- !\L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[7]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[4]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[5]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[6]~q\,
	combout => \L_tone_generation|L_pulselength2audio|audiol~2_combout\);

-- Location: LCCOMB_X66_Y49_N30
\L_tone_generation|L_pulselength2audio|audiol~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|audiol~1_combout\ = (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\ & 
-- !\L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[1]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[3]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[2]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[0]~q\,
	combout => \L_tone_generation|L_pulselength2audio|audiol~1_combout\);

-- Location: LCCOMB_X67_Y48_N28
\L_tone_generation|L_pulselength2audio|audiol~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|audiol~3_combout\ = (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\ & 
-- !\L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|COUNT2:counter[11]~q\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[8]~q\,
	datac => \L_tone_generation|L_pulselength2audio|COUNT2:counter[10]~q\,
	datad => \L_tone_generation|L_pulselength2audio|COUNT2:counter[9]~q\,
	combout => \L_tone_generation|L_pulselength2audio|audiol~3_combout\);

-- Location: LCCOMB_X67_Y48_N18
\L_tone_generation|L_pulselength2audio|audiol~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|audiol~4_combout\ = (\L_tone_generation|L_pulselength2audio|audiol~2_combout\ & (!\L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\ & (\L_tone_generation|L_pulselength2audio|audiol~1_combout\ & 
-- \L_tone_generation|L_pulselength2audio|audiol~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|audiol~2_combout\,
	datab => \L_tone_generation|L_pulselength2audio|COUNT2:counter[12]~q\,
	datac => \L_tone_generation|L_pulselength2audio|audiol~1_combout\,
	datad => \L_tone_generation|L_pulselength2audio|audiol~3_combout\,
	combout => \L_tone_generation|L_pulselength2audio|audiol~4_combout\);

-- Location: LCCOMB_X67_Y48_N30
\L_tone_generation|L_pulselength2audio|audiol~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_tone_generation|L_pulselength2audio|audiol~5_combout\ = \L_tone_generation|L_pulselength2audio|audiol~q\ $ (((\L_tone_generation|L_pulselength2audio|audiol~0_combout\ & (\L_tone_generation|L_pulselength2audio|audiol~4_combout\ & 
-- \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_tone_generation|L_pulselength2audio|audiol~0_combout\,
	datab => \L_tone_generation|L_pulselength2audio|audiol~4_combout\,
	datac => \L_tone_generation|L_pulselength2audio|audiol~q\,
	datad => \L_tone_generation|L_pulselength2audio|LessThan0~3_combout\,
	combout => \L_tone_generation|L_pulselength2audio|audiol~5_combout\);

-- Location: FF_X67_Y48_N31
\L_tone_generation|L_pulselength2audio|audiol\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_tone_generation|L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_tone_generation|L_pulselength2audio|audiol~5_combout\,
	clrn => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_tone_generation|L_pulselength2audio|audiol~q\);

-- Location: LCCOMB_X67_Y50_N28
\Display0|Mux6~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux6~2_combout\ = (\L_readkey|L_Showkey|scancode\(0) & ((\L_readkey|L_Showkey|scancode\(3)) # (\L_readkey|L_Showkey|scancode\(2) $ (\L_readkey|L_Showkey|scancode\(1))))) # (!\L_readkey|L_Showkey|scancode\(0) & 
-- ((\L_readkey|L_Showkey|scancode\(1)) # (\L_readkey|L_Showkey|scancode\(3) $ (\L_readkey|L_Showkey|scancode\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111110111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \Display0|Mux6~2_combout\);

-- Location: LCCOMB_X69_Y50_N4
\Display0|Mux6~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux6~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display0|Mux6~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display0|Mux6~2_combout\,
	combout => \Display0|Mux6~3_combout\);

-- Location: LCCOMB_X67_Y51_N24
\Display0|Mux5~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux5~2_combout\ = (\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(3) $ (((\L_readkey|L_Showkey|scancode\(1)) # (!\L_readkey|L_Showkey|scancode\(2)))))) # (!\L_readkey|L_Showkey|scancode\(0) & 
-- (\L_readkey|L_Showkey|scancode\(1) & (!\L_readkey|L_Showkey|scancode\(2) & !\L_readkey|L_Showkey|scancode\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \Display0|Mux5~2_combout\);

-- Location: LCCOMB_X67_Y51_N10
\Display0|Mux5~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux5~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display0|Mux5~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display0|Mux5~2_combout\,
	combout => \Display0|Mux5~3_combout\);

-- Location: LCCOMB_X67_Y51_N4
\Display0|Mux4~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux4~2_combout\ = (\L_readkey|L_Showkey|scancode\(1) & (\L_readkey|L_Showkey|scancode\(0) & ((!\L_readkey|L_Showkey|scancode\(3))))) # (!\L_readkey|L_Showkey|scancode\(1) & ((\L_readkey|L_Showkey|scancode\(2) & 
-- ((!\L_readkey|L_Showkey|scancode\(3)))) # (!\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Showkey|scancode\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \Display0|Mux4~2_combout\);

-- Location: LCCOMB_X67_Y51_N22
\Display0|Mux4~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux4~3_combout\ = (\Display0|Mux4~2_combout\ & \L_readkey|L_Constantkey|current_state.state_key_pressed~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Display0|Mux4~2_combout\,
	datad => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	combout => \Display0|Mux4~3_combout\);

-- Location: LCCOMB_X67_Y51_N20
\Display0|Mux3~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux3~2_combout\ = (\L_readkey|L_Showkey|scancode\(1) & ((\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(2))) # (!\L_readkey|L_Showkey|scancode\(0) & (!\L_readkey|L_Showkey|scancode\(2) & \L_readkey|L_Showkey|scancode\(3))))) 
-- # (!\L_readkey|L_Showkey|scancode\(1) & (!\L_readkey|L_Showkey|scancode\(3) & (\L_readkey|L_Showkey|scancode\(0) $ (\L_readkey|L_Showkey|scancode\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \Display0|Mux3~2_combout\);

-- Location: LCCOMB_X67_Y51_N6
\Display0|Mux3~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux3~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display0|Mux3~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display0|Mux3~2_combout\,
	combout => \Display0|Mux3~3_combout\);

-- Location: LCCOMB_X67_Y51_N28
\Display0|Mux2~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux2~2_combout\ = (\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Showkey|scancode\(3) & ((\L_readkey|L_Showkey|scancode\(1)) # (!\L_readkey|L_Showkey|scancode\(0))))) # (!\L_readkey|L_Showkey|scancode\(2) & 
-- (!\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(1) & !\L_readkey|L_Showkey|scancode\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \Display0|Mux2~2_combout\);

-- Location: LCCOMB_X67_Y51_N18
\Display0|Mux2~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux2~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display0|Mux2~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display0|Mux2~2_combout\,
	combout => \Display0|Mux2~3_combout\);

-- Location: LCCOMB_X67_Y51_N16
\Display0|Mux1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux1~2_combout\ = (\L_readkey|L_Showkey|scancode\(1) & ((\L_readkey|L_Showkey|scancode\(0) & ((\L_readkey|L_Showkey|scancode\(3)))) # (!\L_readkey|L_Showkey|scancode\(0) & (\L_readkey|L_Showkey|scancode\(2))))) # 
-- (!\L_readkey|L_Showkey|scancode\(1) & (\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Showkey|scancode\(0) $ (\L_readkey|L_Showkey|scancode\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(0),
	datab => \L_readkey|L_Showkey|scancode\(1),
	datac => \L_readkey|L_Showkey|scancode\(2),
	datad => \L_readkey|L_Showkey|scancode\(3),
	combout => \Display0|Mux1~2_combout\);

-- Location: LCCOMB_X67_Y51_N30
\Display0|Mux1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux1~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display0|Mux1~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display0|Mux1~2_combout\,
	combout => \Display0|Mux1~3_combout\);

-- Location: LCCOMB_X65_Y52_N14
\Display0|Mux0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux0~2_combout\ = (\L_readkey|L_Showkey|scancode\(2) & (!\L_readkey|L_Showkey|scancode\(1) & (\L_readkey|L_Showkey|scancode\(3) $ (!\L_readkey|L_Showkey|scancode\(0))))) # (!\L_readkey|L_Showkey|scancode\(2) & (\L_readkey|L_Showkey|scancode\(0) 
-- & (\L_readkey|L_Showkey|scancode\(3) $ (!\L_readkey|L_Showkey|scancode\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(2),
	datab => \L_readkey|L_Showkey|scancode\(3),
	datac => \L_readkey|L_Showkey|scancode\(0),
	datad => \L_readkey|L_Showkey|scancode\(1),
	combout => \Display0|Mux0~2_combout\);

-- Location: LCCOMB_X66_Y52_N0
\Display0|Mux0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display0|Mux0~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display0|Mux0~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display0|Mux0~2_combout\,
	combout => \Display0|Mux0~3_combout\);

-- Location: LCCOMB_X69_Y50_N24
\Display1|Mux6~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux6~2_combout\ = (\L_readkey|L_Showkey|scancode\(4) & ((\L_readkey|L_Showkey|scancode\(7)) # (\L_readkey|L_Showkey|scancode\(5) $ (\L_readkey|L_Showkey|scancode\(6))))) # (!\L_readkey|L_Showkey|scancode\(4) & 
-- ((\L_readkey|L_Showkey|scancode\(5)) # (\L_readkey|L_Showkey|scancode\(6) $ (\L_readkey|L_Showkey|scancode\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011010111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \Display1|Mux6~2_combout\);

-- Location: LCCOMB_X69_Y50_N6
\Display1|Mux6~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux6~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display1|Mux6~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display1|Mux6~2_combout\,
	combout => \Display1|Mux6~3_combout\);

-- Location: LCCOMB_X69_Y50_N20
\Display1|Mux5~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux5~2_combout\ = (\L_readkey|L_Showkey|scancode\(5) & (!\L_readkey|L_Showkey|scancode\(7) & ((\L_readkey|L_Showkey|scancode\(4)) # (!\L_readkey|L_Showkey|scancode\(6))))) # (!\L_readkey|L_Showkey|scancode\(5) & 
-- (\L_readkey|L_Showkey|scancode\(4) & (\L_readkey|L_Showkey|scancode\(6) $ (!\L_readkey|L_Showkey|scancode\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \Display1|Mux5~2_combout\);

-- Location: LCCOMB_X69_Y50_N14
\Display1|Mux5~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux5~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display1|Mux5~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display1|Mux5~2_combout\,
	combout => \Display1|Mux5~3_combout\);

-- Location: LCCOMB_X69_Y50_N12
\Display1|Mux4~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux4~2_combout\ = (\L_readkey|L_Showkey|scancode\(5) & (((!\L_readkey|L_Showkey|scancode\(7) & \L_readkey|L_Showkey|scancode\(4))))) # (!\L_readkey|L_Showkey|scancode\(5) & ((\L_readkey|L_Showkey|scancode\(6) & 
-- (!\L_readkey|L_Showkey|scancode\(7))) # (!\L_readkey|L_Showkey|scancode\(6) & ((\L_readkey|L_Showkey|scancode\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \Display1|Mux4~2_combout\);

-- Location: LCCOMB_X69_Y50_N18
\Display1|Mux4~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux4~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display1|Mux4~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display1|Mux4~2_combout\,
	combout => \Display1|Mux4~3_combout\);

-- Location: LCCOMB_X69_Y50_N28
\Display1|Mux3~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux3~2_combout\ = (\L_readkey|L_Showkey|scancode\(5) & ((\L_readkey|L_Showkey|scancode\(6) & ((\L_readkey|L_Showkey|scancode\(4)))) # (!\L_readkey|L_Showkey|scancode\(6) & (\L_readkey|L_Showkey|scancode\(7) & 
-- !\L_readkey|L_Showkey|scancode\(4))))) # (!\L_readkey|L_Showkey|scancode\(5) & (!\L_readkey|L_Showkey|scancode\(7) & (\L_readkey|L_Showkey|scancode\(6) $ (\L_readkey|L_Showkey|scancode\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \Display1|Mux3~2_combout\);

-- Location: LCCOMB_X69_Y50_N22
\Display1|Mux3~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux3~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display1|Mux3~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display1|Mux3~2_combout\,
	combout => \Display1|Mux3~3_combout\);

-- Location: LCCOMB_X69_Y50_N16
\Display1|Mux2~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux2~2_combout\ = (\L_readkey|L_Showkey|scancode\(6) & (\L_readkey|L_Showkey|scancode\(7) & ((\L_readkey|L_Showkey|scancode\(5)) # (!\L_readkey|L_Showkey|scancode\(4))))) # (!\L_readkey|L_Showkey|scancode\(6) & (\L_readkey|L_Showkey|scancode\(5) 
-- & (!\L_readkey|L_Showkey|scancode\(7) & !\L_readkey|L_Showkey|scancode\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \Display1|Mux2~2_combout\);

-- Location: LCCOMB_X69_Y50_N30
\Display1|Mux2~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux2~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display1|Mux2~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display1|Mux2~2_combout\,
	combout => \Display1|Mux2~3_combout\);

-- Location: LCCOMB_X69_Y50_N8
\Display1|Mux1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux1~2_combout\ = (\L_readkey|L_Showkey|scancode\(5) & ((\L_readkey|L_Showkey|scancode\(4) & ((\L_readkey|L_Showkey|scancode\(7)))) # (!\L_readkey|L_Showkey|scancode\(4) & (\L_readkey|L_Showkey|scancode\(6))))) # 
-- (!\L_readkey|L_Showkey|scancode\(5) & (\L_readkey|L_Showkey|scancode\(6) & (\L_readkey|L_Showkey|scancode\(7) $ (\L_readkey|L_Showkey|scancode\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(5),
	datab => \L_readkey|L_Showkey|scancode\(6),
	datac => \L_readkey|L_Showkey|scancode\(7),
	datad => \L_readkey|L_Showkey|scancode\(4),
	combout => \Display1|Mux1~2_combout\);

-- Location: LCCOMB_X69_Y50_N26
\Display1|Mux1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux1~3_combout\ = (\Display1|Mux1~2_combout\ & \L_readkey|L_Constantkey|current_state.state_key_pressed~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Display1|Mux1~2_combout\,
	datad => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	combout => \Display1|Mux1~3_combout\);

-- Location: LCCOMB_X66_Y52_N28
\Display1|Mux0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux0~2_combout\ = (\L_readkey|L_Showkey|scancode\(7) & (\L_readkey|L_Showkey|scancode\(4) & (\L_readkey|L_Showkey|scancode\(5) $ (\L_readkey|L_Showkey|scancode\(6))))) # (!\L_readkey|L_Showkey|scancode\(7) & (!\L_readkey|L_Showkey|scancode\(5) & 
-- (\L_readkey|L_Showkey|scancode\(4) $ (\L_readkey|L_Showkey|scancode\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Showkey|scancode\(7),
	datab => \L_readkey|L_Showkey|scancode\(5),
	datac => \L_readkey|L_Showkey|scancode\(4),
	datad => \L_readkey|L_Showkey|scancode\(6),
	combout => \Display1|Mux0~2_combout\);

-- Location: LCCOMB_X66_Y52_N30
\Display1|Mux0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Display1|Mux0~3_combout\ = (\L_readkey|L_Constantkey|current_state.state_key_pressed~q\ & \Display1|Mux0~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_readkey|L_Constantkey|current_state.state_key_pressed~q\,
	datad => \Display1|Mux0~2_combout\,
	combout => \Display1|Mux0~3_combout\);

-- Location: IOIBUF_X22_Y0_N29
\KEY[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);

-- Location: IOIBUF_X46_Y54_N29
\KEY[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: UNVM_X0_Y40_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	xe_ye => \~QUARTUS_CREATED_GND~I_combout\,
	se => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

-- Location: ADCBLOCK_X43_Y52_N0
\~QUARTUS_CREATED_ADC1~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 1,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC1~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC1~~eoc\);

-- Location: ADCBLOCK_X43_Y51_N0
\~QUARTUS_CREATED_ADC2~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 2,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC2~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC2~~eoc\);

ww_arduino_io3 <= \arduino_io3~output_o\;

ww_HEX0(7) <= \HEX0[7]~output_o\;

ww_HEX0(6) <= \HEX0[6]~output_o\;

ww_HEX0(5) <= \HEX0[5]~output_o\;

ww_HEX0(4) <= \HEX0[4]~output_o\;

ww_HEX0(3) <= \HEX0[3]~output_o\;

ww_HEX0(2) <= \HEX0[2]~output_o\;

ww_HEX0(1) <= \HEX0[1]~output_o\;

ww_HEX0(0) <= \HEX0[0]~output_o\;

ww_HEX1(7) <= \HEX1[7]~output_o\;

ww_HEX1(6) <= \HEX1[6]~output_o\;

ww_HEX1(5) <= \HEX1[5]~output_o\;

ww_HEX1(4) <= \HEX1[4]~output_o\;

ww_HEX1(3) <= \HEX1[3]~output_o\;

ww_HEX1(2) <= \HEX1[2]~output_o\;

ww_HEX1(1) <= \HEX1[1]~output_o\;

ww_HEX1(0) <= \HEX1[0]~output_o\;

ww_HEX2(7) <= \HEX2[7]~output_o\;

ww_HEX2(6) <= \HEX2[6]~output_o\;

ww_HEX2(5) <= \HEX2[5]~output_o\;

ww_HEX2(4) <= \HEX2[4]~output_o\;

ww_HEX2(3) <= \HEX2[3]~output_o\;

ww_HEX2(2) <= \HEX2[2]~output_o\;

ww_HEX2(1) <= \HEX2[1]~output_o\;

ww_HEX2(0) <= \HEX2[0]~output_o\;

ww_HEX3(7) <= \HEX3[7]~output_o\;

ww_HEX3(6) <= \HEX3[6]~output_o\;

ww_HEX3(5) <= \HEX3[5]~output_o\;

ww_HEX3(4) <= \HEX3[4]~output_o\;

ww_HEX3(3) <= \HEX3[3]~output_o\;

ww_HEX3(2) <= \HEX3[2]~output_o\;

ww_HEX3(1) <= \HEX3[1]~output_o\;

ww_HEX3(0) <= \HEX3[0]~output_o\;

ww_HEX4(7) <= \HEX4[7]~output_o\;

ww_HEX4(6) <= \HEX4[6]~output_o\;

ww_HEX4(5) <= \HEX4[5]~output_o\;

ww_HEX4(4) <= \HEX4[4]~output_o\;

ww_HEX4(3) <= \HEX4[3]~output_o\;

ww_HEX4(2) <= \HEX4[2]~output_o\;

ww_HEX4(1) <= \HEX4[1]~output_o\;

ww_HEX4(0) <= \HEX4[0]~output_o\;

ww_HEX5(7) <= \HEX5[7]~output_o\;

ww_HEX5(6) <= \HEX5[6]~output_o\;

ww_HEX5(5) <= \HEX5[5]~output_o\;

ww_HEX5(4) <= \HEX5[4]~output_o\;

ww_HEX5(3) <= \HEX5[3]~output_o\;

ww_HEX5(2) <= \HEX5[2]~output_o\;

ww_HEX5(1) <= \HEX5[1]~output_o\;

ww_HEX5(0) <= \HEX5[0]~output_o\;

ww_LEDR(9) <= \LEDR[9]~output_o\;

ww_LEDR(8) <= \LEDR[8]~output_o\;

ww_LEDR(7) <= \LEDR[7]~output_o\;

ww_LEDR(6) <= \LEDR[6]~output_o\;

ww_LEDR(5) <= \LEDR[5]~output_o\;

ww_LEDR(4) <= \LEDR[4]~output_o\;

ww_LEDR(3) <= \LEDR[3]~output_o\;

ww_LEDR(2) <= \LEDR[2]~output_o\;

ww_LEDR(1) <= \LEDR[1]~output_o\;

ww_LEDR(0) <= \LEDR[0]~output_o\;
END structure;


