--------------------------------------------------------------------
--! \file      clock_generator.vhd
--! \date      see top of 'Version History'
--! \brief     Clock generator 
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |24-3-2015  |WLGRW   |Inital version
--! 002    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--! 003    |6-4-2020   |WLGRW   |Cleanup for assignment
--!
--! # Assignment  3: clock_generator
--!
--! This clock_generator wil devide the system clock of 50 MHz 1, 1/2, 1/4, 1/8, 1/16, 1/32 and 1/64 times
--! This will produce frequencies of: 50MHz, 25 MHz, 12.5MHz, 6,25MHz, 3,125MHz, 1,5625MHz or 0,78125MHz.
--! 
--! 
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
USE     ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
--------------------------------------------------------------------
ENTITY clock_generator IS
	GENERIC(
		N: INTEGER := 8
	);
   PORT(
      reset   : in  STD_LOGIC;                    -- Reset active is '0'
      clk     : in  STD_LOGIC;                    -- 50MHz clock
      key     : in  STD_LOGIC_VECTOR(7 DOWNTO 0);
      clk_dev : out STD_LOGIC
   );
END clock_generator;
--------------------------------------------------------------------
ARCHITECTURE LogicFunction OF clock_generator IS
   --! add a "typedef" for enum state_type.
	 TYPE state_type is (
      state_0,
      state_1,
		state_2,
		state_3,
      state_4,
		state_5,
		state_6,
      state_7
   );
   --! add SIGNALS 
	SIGNAL divider_output : STD_LOGIC_VECTOR(N DOWNTO 0);
	SIGNAL selector : INTEGER RANGE 0 TO 2 ** N := 0;
   --! add a signal of type state_type for the FSM
	SIGNAL tone_state, next_state : state_type := state_3; 
	SIGNAL Valid_key : STD_LOGIC;
BEGIN

--! #### clock devider based on a integer counter: #### 
--! The signal divider_output receives the integer
--! Per line on the vector a devided signal of the clock is avaialable
--! lsb is devide by 1, msb is devide by 64

   devider : PROCESS (clk, reset)
      --! use a variable for your counter
		VARIABLE counter : INTEGER RANGE 0 TO 2 ** N := 0;
   BEGIN
      
      --! Implement here the counter
		IF reset = '0' THEN -- Async reset
			counter := 0; -- set counter to 0
			divider_output <= (OTHERS => '0'); -- set counter to 0
		ELSIF rising_edge(clk) THEN -- on clock edge
			IF (counter < (2 ** N)) THEN -- When counter is < 2 power N
				counter := counter + 1; -- increment counter
			ELSE -- counter = 2 power N
				counter := 0; -- reset counter to 0
			END IF;
		END IF; -- put result on signal

		--divider_output <= STD_LOGIC_VECTOR(to_unsigned(counter, divider_output'length));
		divider_output <= conv_std_logic_vector(counter, divider_output'length);

     
   END PROCESS;

--! #### state_decoder ####
--! this PROCESS is PROCESSing state changes each clk and executing async reset.
--! the PROCESS will handle coninous pressing a key as well.

   state_decoder: PROCESS (clk, reset) is -- PROCESS watching reset and system clock
   BEGIN
      
      --! implement  here the state decoder
		
		IF reset = '0' THEN -- Reset (async).
			tone_state <= state_3;
		ELSIF rising_edge(clk) and Valid_key = '1' THEN
			tone_state <= next_state;
			Valid_key <= '0';
		ELSIF rising_edge(clk) and key = X"F0" THEN
			Valid_key <= '1';
		END IF;
		
     
   END PROCESS;                  -- END PROCESS input_decoder

--! #### input_decoder #### 
--! this PROCESS contains the tests and conditions for each state

   input_decoder : PROCESS ( key ) -- add state SIGNAL to watch-list
   BEGIN
      
      --! Implement here the input ecoder
		CASE tone_state IS 
			WHEN state_0 =>
				IF key = X"1A" THEN -- if Z
					next_state <= state_1;
				END IF;
			WHEN state_1 =>
				if key = X"1C" THEN -- if A
					next_state <= state_0;
				ELSIF key = X"1A" THEN -- if Z
					next_state <= state_2;
				END IF;
			WHEN state_2 =>
				IF key = X"1C" THEN -- if A
					next_state <= state_1;
				ELSIF key = X"1A" THEN -- if Z
					next_state <= state_3;
				END IF;
			WHEN state_3 =>
				if key = X"1C" THEN -- if A
					next_state <= state_2;
				elsif key = X"1A" THEN -- if Z
					next_state <= state_4;
				end if;
			WHEN state_4 =>
				if key = X"1C" THEN -- if A
					next_state <= state_3;
				elsif key = X"1A" THEN -- if Z
					next_state <= state_5;
				end if;
			WHEN state_5 =>
				if key = X"1C" THEN -- if A
					next_state <= state_4;
				elsif key = X"1A" THEN -- if Z
					next_state <= state_6;
				end if;
			WHEN state_6 =>
				if key = X"1C" THEN -- if A
					next_state <= state_5;
				elsif key = X"1A" THEN -- if Z
					next_state <= state_7;
				end if;
			WHEN state_7 =>
				if key = X"1C" THEN -- if A
					next_state <= state_6;
				end if;
		END CASE;			
   END PROCESS;
      
--! #### state_decoder ####
--! this PROCESS is performing actions that apply for each state
--! selector logic: select the divided clock signal from the clock devider.

   output_decoder : PROCESS ( clk) -- add divider_output SIGNAL to watch-list
   BEGIN   
	
      --! Implement here the output decoder	
		CASE tone_state IS
			WHEN state_0 =>
				clk_dev <= divider_output(0);
			WHEN state_1 =>
				clk_dev <= divider_output(1);
			WHEN state_2 =>
				clk_dev <= divider_output(2);
			WHEN state_3 =>
				clk_dev <= divider_output(3);
			WHEN state_4 =>
				clk_dev <= divider_output(4);
			WHEN state_5 =>
				clk_dev <= divider_output(5);
			WHEN state_6 =>
				clk_dev <= divider_output(6);
			WHEN OTHERS =>
				clk_dev <= divider_output(7);
		END CASE;
		  
   END PROCESS;
   
END LogicFunction;
--------------------------------------------------------------------