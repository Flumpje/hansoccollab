-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "05/26/2020 20:17:55"
                                                            
-- Vhdl Test Bench template for design  :  piano
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY piano_vhd_tst IS
END piano_vhd_tst;
ARCHITECTURE piano_arch OF piano_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL arduino_io3 : STD_LOGIC;
SIGNAL arduino_io4 : STD_LOGIC;
SIGNAL arduino_io5 : STD_LOGIC;
SIGNAL HEX0 : STD_LOGIC_VECTOR(0 TO 7);
SIGNAL HEX1 : STD_LOGIC_VECTOR(0 TO 7);
SIGNAL HEX2 : STD_LOGIC_VECTOR(0 TO 7);
SIGNAL HEX3 : STD_LOGIC_VECTOR(0 TO 7);
SIGNAL HEX4 : STD_LOGIC_VECTOR(0 TO 7);
SIGNAL HEX5 : STD_LOGIC_VECTOR(0 TO 7);
SIGNAL KEY : STD_LOGIC_VECTOR(0 TO 2);
SIGNAL LEDR : STD_LOGIC_VECTOR(0 TO 9);
SIGNAL MAX10_CLK1_50 : STD_LOGIC := '0';
-- Procedure for this example.
PROCEDURE send_byte(
	CONSTANT byte : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL pr_kbclock : OUT STD_LOGIC;
	SIGNAL pr_kbdata : OUT STD_LOGIC
)
IS
	VARIABLE odd_parity : STD_LOGIC;
	VARIABLE data : STD_LOGIC_VECTOR(10 DOWNTO 0);
BEGIN
	-- Generate paritybit
	odd_parity := '1';
	FOR i IN 7 DOWNTO 0 LOOP
		odd_parity := odd_parity XOR byte(i);
	END LOOP;
	data := '1' & odd_parity & byte & '0';
	-- Send off data
	FOR I IN 0 TO 10 LOOP
		pr_kbdata <= data(i);
		pr_kbclock <= '1';
		WAIT FOR 20 ns;
		pr_kbclock <= '0';
		WAIT FOR 20 ns;
	END LOOP;
	pr_kbclock <= '1';
END send_byte;

COMPONENT piano
	PORT (
	arduino_io3 : BUFFER STD_LOGIC;
	arduino_io4 : IN STD_LOGIC;
	arduino_io5 : IN STD_LOGIC;
	HEX0 : BUFFER STD_LOGIC_VECTOR(0 TO 7);
	HEX1 : BUFFER STD_LOGIC_VECTOR(0 TO 7);
	HEX2 : BUFFER STD_LOGIC_VECTOR(0 TO 7);
	HEX3 : BUFFER STD_LOGIC_VECTOR(0 TO 7);
	HEX4 : BUFFER STD_LOGIC_VECTOR(0 TO 7);
	HEX5 : BUFFER STD_LOGIC_VECTOR(0 TO 7);
	KEY : IN STD_LOGIC_VECTOR(0 TO 2);
	LEDR : BUFFER STD_LOGIC_VECTOR(0 TO 9);
	MAX10_CLK1_50 : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	MAX10_CLK1_50 <= not MAX10_CLK1_50 after 10 ns;

	i1 : piano
	PORT MAP (
-- list connections between master ports and signals
	arduino_io3 => arduino_io3,
	arduino_io4 => arduino_io4,
	arduino_io5 => arduino_io5,
	HEX0 => HEX0,
	HEX1 => HEX1,
	HEX2 => HEX2,
	HEX3 => HEX3,
	HEX4 => HEX4,
	HEX5 => HEX5,
	KEY => KEY,
	LEDR => LEDR,
	MAX10_CLK1_50 => MAX10_CLK1_50
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
       -- 4 times up
	KEY(1) <= '0';
	WAIT FOR 20 ns;
	KEY(1) <= '1';
	WAIT FOR 20 ns;
	-- send keystroke down (A)
	Send_byte( X"1C", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1C", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1C", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1C", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	

	
	-- down
	-- send keystroke down (A)
	Send_byte( X"1A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke down (A)
	Send_byte( X"1A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	




	
	-- 8 times down
	-- send keystroke up (Z)
	Send_byte( X"41", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 10 ms;
	-- send keystroke up (Z)
	Send_byte( X"5A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 2000 ns;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	-- send keystroke up (Z)
	Send_byte( X"5A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	-- send keystroke up (Z)
	Send_byte( X"5A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	Send_byte( X"F0", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	-- send keystroke up (Z)
	Send_byte( X"5A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	Send_byte( X"00", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	-- send keystroke up (Z)
	Send_byte( X"5A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	Send_byte( X"00", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	-- send keystroke up (Z)
	Send_byte( X"5A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	Send_byte( X"00", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	-- send keystroke up (Z)
	Send_byte( X"5A", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	Send_byte( X"00", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	
	-- 2 times down
	-- send keystroke down (A)
	Send_byte( X"52", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	Send_byte( X"00", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	-- send keystroke down (A)
	Send_byte( X"52", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;
	Send_byte( X"00", arduino_io4, arduino_io5 ); -- 'X' is to show that value is HEX
	wait for 1 sec;                          
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
WAIT;                                                        
END PROCESS always;                                          
END piano_arch;
