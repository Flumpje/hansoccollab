--------------------------------------------------------------------
--! \file      key2pulselength.vhd
--! \date      see top of 'Version History'
--! \brief     The masters orgeltje 
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |26-3-2015  |WLGRW   |Inital version
--! 002    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--! 003    |7-4-2020   |WLGRW   |Transformation to assignment
--!
--! # Assignment 4:key2pulselength
--!
--! 
--! 
--! \verbatim
--!
--! \endverbatim
--!
--!     |       |       |         |           | @50 MHz
--!  No | Toets | (Hex) | Note    | Freq (Hz) | freq/2 (Hz)
--!  ---|-------|------ |---------|-----------|------------
--!  1  | TAB   | 0D    | A       | 440       | 7102
--!  2  | 1!    | 16    |    Ais  | 466       | 6704
--!  3  | Q     | 15    | B       | 494       | 6327
--!  4  | W     | 1D    | C       | 523       | 5972
--!  5  | 3#    | 26    |    Cis  | 554       | 5637
--!  6  | E     | 24    | D       | 587       | 5321
--!  7  | 4$    | 25    |    Dis  | 622       | 5022
--!  8  | R     | 2D    | E       | 659       | 4740
--!  9  | T     | 2C    | F       | 698       | 4474
--!  10 | 6^    | 36    |    Fis  | 740       | 4223
--!  11 | Y     | 35    | G       | 784       | 3986
--!  12 | 7&    | 3D    |    Gis  | 831       | 3762
--!  13 | U     | 3C    | A+      | 880       | 3551
--!  14 | 8*    | 3E    |    Ais+ | 932       | 3352
--!  15 | I     | 43    | B+      | 988       | 3164
--!  16 | O     | 44    |    Cis+ | 1047      | 2986
--!  17 | 0)    | 45    | C+      | 1109      | 2819
--!  18 | P     | 4D    |    Dis+ | 1175      | 2660
--!  19 | -_    | 4E    | D+      | 1245      | 2511
--!  20 | [{    | 54    | E+      | 1319      | 2370
--!  21 | ]}    | 5B    | F+      | 1397      | 2237
--!  22 | Back  | 41    |    Fis+ | 1480      | 2112
--!  
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
USE     ieee.numeric_std.all;
--------------------------------------------------------------------
ENTITY key2pulselength is

   --! Use CONSTANTS here
   
   PORT(
      reset      : IN  STD_LOGIC;                    -- Reset active is '0'
      key        : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
      pulslength : OUT integer RANGE 0 TO 8191       -- 16 bit size to store up to maximal 56818
   );
END key2pulselength;
--------------------------------------------------------------------
ARCHITECTURE implementation1 OF key2pulselength IS
BEGIN
	PROCESS (key, reset)
	BEGIN
		IF reset = '0' THEN
			pulslength <= 0;
		END IF;

		CASE key IS
			WHEN X"0D" => pulslength <= 7102;
			WHEN X"16" => pulslength <= 6704;
			WHEN X"15" => pulslength <= 6327;
			WHEN X"1D" => pulslength <= 5972;
			WHEN X"26" => pulslength <= 5637;
			WHEN X"24" => pulslength <= 5321;
			WHEN X"25" => pulslength <= 5022;
			WHEN X"2D" => pulslength <= 4740;
			WHEN X"2C" => pulslength <= 4474;
			WHEN X"36" => pulslength <= 4223;
			WHEN X"35" => pulslength <= 3986;
			WHEN X"3D" => pulslength <= 3762;
			WHEN X"3C" => pulslength <= 3551;
			WHEN X"3E" => pulslength <= 3352;
			WHEN X"43" => pulslength <= 3164;
			WHEN X"44" => pulslength <= 2986;
			WHEN X"45" => pulslength <= 2819;
			WHEN X"4D" => pulslength <= 2660;
			WHEN X"4E" => pulslength <= 2511;
			WHEN X"54" => pulslength <= 2370;
			WHEN X"5B" => pulslength <= 2237;
			WHEN X"41" => pulslength <= 2112;
			WHEN X"F0" => pulslength <= pulslength;
			WHEN OTHERS => pulslength <= 0;
		END CASE;
		
	END PROCESS;
   
END implementation1;
--------------------------------------------------------------------
