--------------------------------------------------------------------
--! \file      tone_generation.vhd
--! \date      see top of 'Version History'
--! \brief     combination of components to tone_generation 
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |22-3-2015  |WLGRW   |Inital version
--! 002    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--! 003    |7-4-2020   |WLGRW   |Modification for use as assignment
--! 
--! # assignment 6: tone_generation
--!
--! tone_generation combines the following components:
--!  - clock_domain_crossing
--!  - showkey
--!  - constantkey
--! 
--!
--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
USE     ieee.STD_LOGIC_unsigned.all;
--------------------------------------------------------------------
ENTITY tone_generation IS
   PORT(
      clk     : IN  STD_LOGIC;                     -- 50 MHz clock.
      reset   : IN  STD_LOGIC;                     -- reset '0' active
      key     : IN  STD_LOGIC_VECTOR(7 downto 0);  -- kex character received from keyboard
      audiol,
      audior  : OUT STD_LOGIC                      -- Audio output
   );
END tone_generation;
--------------------------------------------------------------------
ARCHITECTURE tone_generation_struct OF tone_generation IS
   
   SIGNAL C_clk_dev        : STD_LOGIC; --! keyboard clock
	SIGNAL C_pulslength     : integer RANGE 0 TO 8191; 
BEGIN
   
   L_clock_generator: ENTITY work.clock_generator PORT MAP(
--    + component port
--    |              + Internal port or signal
--    |              |
      reset       => reset,
      clk         => clk,
      key     		=> key,
      clk_dev     => C_clk_dev
   );

   L_key2pulselength: ENTITY work.key2pulselength PORT MAP(
      reset       => reset, 
      key     		=> key,
      pulslength  => C_pulslength
   );
      
--! component constantkey that presents key data from keyboard as long as the key is pressed.
   L_pulselength2audio: ENTITY work.pulselength2audio PORT MAP(
      reset       => reset,
      clk_dev         => C_clk_dev,
      pulslength   => C_pulslength,
      audiol         => audiol,
      audior        => audior
   );
   
END tone_generation_struct;
--------------------------------------------------------------------
