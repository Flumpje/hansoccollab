transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -2008 -work work {C:/HAN/hansoccollab/Willem/assignment_6/pulselength2audio.vhd}
vcom -2008 -work work {C:/HAN/hansoccollab/Willem/assignment_6/key2pulselength.vhd}
vcom -2008 -work work {C:/HAN/hansoccollab/Willem/assignment_6/clock_generator.vhd}
vcom -2008 -work work {C:/HAN/hansoccollab/Willem/assignment_6/tone_generation.vhd}

vcom -2008 -work work {C:/HAN/hansoccollab/Willem/assignment_6/tone_generation_tb.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L fiftyfivenm -L rtl_work -L work -voptargs="+acc"  tone_generation_tb

add wave *
view structure
view signals
run -all
