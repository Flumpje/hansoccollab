-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "05/26/2020 21:40:19"

-- 
-- Device: Altera 10M02DCU324A6G Package UFBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_J7,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_J8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_H3,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_H4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_H8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	tone_generation IS
    PORT (
	clk : IN std_logic;
	reset : IN std_logic;
	key : IN std_logic_vector(7 DOWNTO 0);
	audiol : BUFFER std_logic;
	audior : BUFFER std_logic
	);
END tone_generation;

-- Design Ports Information
-- audiol	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- audior	=>  Location: PIN_V12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[2]	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[6]	=>  Location: PIN_P15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[7]	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[0]	=>  Location: PIN_R15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[4]	=>  Location: PIN_L11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[5]	=>  Location: PIN_T17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[3]	=>  Location: PIN_L12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[1]	=>  Location: PIN_P17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_K12,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF tone_generation IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_key : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_audiol : std_logic;
SIGNAL ww_audior : std_logic;
SIGNAL \reset~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \L_clock_generator|Selector0~3clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \L_clock_generator|Selector9~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \audiol~output_o\ : std_logic;
SIGNAL \audior~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[0]~1_combout\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[0]~feeder_combout\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \reset~inputclkctrl_outclk\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[3]~2\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[4]~1_combout\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[4]~q\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[4]~2\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[5]~1_combout\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[5]~q\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[5]~2\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[6]~1_combout\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[6]~q\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[6]~2\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[7]~1_combout\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[7]~q\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[0]~q\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[0]~2\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[1]~1_combout\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[1]~q\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[1]~2\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[2]~1_combout\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[2]~q\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[2]~2\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[3]~1_combout\ : std_logic;
SIGNAL \L_clock_generator|devider:counter[3]~q\ : std_logic;
SIGNAL \key[2]~input_o\ : std_logic;
SIGNAL \key[6]~input_o\ : std_logic;
SIGNAL \key[5]~input_o\ : std_logic;
SIGNAL \key[7]~input_o\ : std_logic;
SIGNAL \L_clock_generator|Equal1~0_combout\ : std_logic;
SIGNAL \key[1]~input_o\ : std_logic;
SIGNAL \key[3]~input_o\ : std_logic;
SIGNAL \key[0]~input_o\ : std_logic;
SIGNAL \key[4]~input_o\ : std_logic;
SIGNAL \L_clock_generator|Equal1~1_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector8~0_combout\ : std_logic;
SIGNAL \L_clock_generator|Equal1~2_combout\ : std_logic;
SIGNAL \L_clock_generator|Equal1~3_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector1~0_combout\ : std_logic;
SIGNAL \L_clock_generator|next_state.state_0_554~combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_0~q\ : std_logic;
SIGNAL \L_clock_generator|Selector3~0_combout\ : std_logic;
SIGNAL \L_clock_generator|next_state.state_2_498~combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_2~q\ : std_logic;
SIGNAL \L_clock_generator|Selector2~0_combout\ : std_logic;
SIGNAL \L_clock_generator|next_state.state_1_526~combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_1~q\ : std_logic;
SIGNAL \L_clock_generator|Selector0~1_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector0~0_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector0~2_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector0~3_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector0~3clkctrl_outclk\ : std_logic;
SIGNAL \L_clock_generator|next_state.state_7_358~combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_7~q\ : std_logic;
SIGNAL \L_clock_generator|Selector7~0_combout\ : std_logic;
SIGNAL \L_clock_generator|next_state.state_6_386~combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_6~q\ : std_logic;
SIGNAL \L_clock_generator|Selector6~0_combout\ : std_logic;
SIGNAL \L_clock_generator|next_state.state_5_414~combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_5~q\ : std_logic;
SIGNAL \L_clock_generator|Selector5~0_combout\ : std_logic;
SIGNAL \L_clock_generator|next_state.state_4_442~combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_4~q\ : std_logic;
SIGNAL \L_clock_generator|Selector4~0_combout\ : std_logic;
SIGNAL \L_clock_generator|next_state.state_3_470~combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_3~0_combout\ : std_logic;
SIGNAL \L_clock_generator|tone_state.state_3~q\ : std_logic;
SIGNAL \L_clock_generator|Selector9~2_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector9~1_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector9~3_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector9~0_combout\ : std_logic;
SIGNAL \L_clock_generator|Selector9~combout\ : std_logic;
SIGNAL \L_clock_generator|Selector9~clkctrl_outclk\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[0]~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux7~4_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux5~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux9~7_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux4~5_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux5~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux2~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux12~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux5~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux0~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux0~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux3~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux0~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux12~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux12~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux12~3_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux4~0_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~3_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux3~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux7~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux7~3_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux7~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux7~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~2_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~4_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux1~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux1~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux1~5_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux1~6_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux1~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux1~3_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux1~4_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux8~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux8~3_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux8~4_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux8~2_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~7_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux6~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux2~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux2~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux2~3_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~16_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux6~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux6~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux6~3_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux6~4_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux6~5_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~14_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux11~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux11~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux11~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux11~3_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~19_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux4~3_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux4~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux4~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux4~4_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[12]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[13]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[13]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[13]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[14]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[14]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[14]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[15]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[15]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[15]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[16]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[16]~q\ : std_logic;
SIGNAL \L_pulselength2audio|Equal0~0_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~8_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~9_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~10_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~11_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~12_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~13_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~15_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux9~8_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux9~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux9~3_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux9~4_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux9~5_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux9~6_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux8~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux10~1_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux10~0_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux10~2_combout\ : std_logic;
SIGNAL \L_key2pulselength|Mux10~3_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~5_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~6_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~17_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal1~18_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[0]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[0]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[1]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[1]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[1]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[2]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[2]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[2]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[3]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[3]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[3]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[4]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[4]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[4]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[5]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[5]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[5]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[6]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[6]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[6]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[7]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[7]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[7]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[8]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[8]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[8]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[9]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[9]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[9]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[10]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[10]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[10]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[11]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[11]~q\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[11]~2\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[12]~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|COUNT:counter[12]~q\ : std_logic;
SIGNAL \L_pulselength2audio|Equal0~3_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal0~1_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal0~2_combout\ : std_logic;
SIGNAL \L_pulselength2audio|Equal0~4_combout\ : std_logic;
SIGNAL \L_pulselength2audio|audior~0_combout\ : std_logic;
SIGNAL \L_pulselength2audio|audior~q\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_reset <= reset;
ww_key <= key;
audiol <= ww_audiol;
audior <= ww_audior;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\reset~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \reset~input_o\);

\L_clock_generator|Selector0~3clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \L_clock_generator|Selector0~3_combout\);

\L_clock_generator|Selector9~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \L_clock_generator|Selector9~combout\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X11_Y9_N16
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X16_Y0_N30
\audiol~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_pulselength2audio|audior~q\,
	devoe => ww_devoe,
	o => \audiol~output_o\);

-- Location: IOOBUF_X16_Y0_N23
\audior~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \L_pulselength2audio|audior~q\,
	devoe => ww_devoe,
	o => \audior~output_o\);

-- Location: IOIBUF_X18_Y6_N22
\clk~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: LCCOMB_X16_Y6_N0
\L_clock_generator|devider:counter[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[0]~1_combout\ = \L_clock_generator|devider:counter[0]~q\ $ (VCC)
-- \L_clock_generator|devider:counter[0]~2\ = CARRY(\L_clock_generator|devider:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|devider:counter[0]~q\,
	datad => VCC,
	combout => \L_clock_generator|devider:counter[0]~1_combout\,
	cout => \L_clock_generator|devider:counter[0]~2\);

-- Location: LCCOMB_X16_Y6_N28
\L_clock_generator|devider:counter[0]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[0]~feeder_combout\ = \L_clock_generator|devider:counter[0]~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_clock_generator|devider:counter[0]~1_combout\,
	combout => \L_clock_generator|devider:counter[0]~feeder_combout\);

-- Location: IOIBUF_X0_Y4_N15
\reset~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: CLKCTRL_G0
\reset~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \reset~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \reset~inputclkctrl_outclk\);

-- Location: LCCOMB_X16_Y6_N6
\L_clock_generator|devider:counter[3]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[3]~1_combout\ = (\L_clock_generator|devider:counter[3]~q\ & (!\L_clock_generator|devider:counter[2]~2\)) # (!\L_clock_generator|devider:counter[3]~q\ & ((\L_clock_generator|devider:counter[2]~2\) # (GND)))
-- \L_clock_generator|devider:counter[3]~2\ = CARRY((!\L_clock_generator|devider:counter[2]~2\) # (!\L_clock_generator|devider:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|devider:counter[3]~q\,
	datad => VCC,
	cin => \L_clock_generator|devider:counter[2]~2\,
	combout => \L_clock_generator|devider:counter[3]~1_combout\,
	cout => \L_clock_generator|devider:counter[3]~2\);

-- Location: LCCOMB_X16_Y6_N8
\L_clock_generator|devider:counter[4]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[4]~1_combout\ = (\L_clock_generator|devider:counter[4]~q\ & (\L_clock_generator|devider:counter[3]~2\ $ (GND))) # (!\L_clock_generator|devider:counter[4]~q\ & (!\L_clock_generator|devider:counter[3]~2\ & VCC))
-- \L_clock_generator|devider:counter[4]~2\ = CARRY((\L_clock_generator|devider:counter[4]~q\ & !\L_clock_generator|devider:counter[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|devider:counter[4]~q\,
	datad => VCC,
	cin => \L_clock_generator|devider:counter[3]~2\,
	combout => \L_clock_generator|devider:counter[4]~1_combout\,
	cout => \L_clock_generator|devider:counter[4]~2\);

-- Location: FF_X16_Y6_N9
\L_clock_generator|devider:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|devider:counter[4]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_clock_generator|devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|devider:counter[4]~q\);

-- Location: LCCOMB_X16_Y6_N10
\L_clock_generator|devider:counter[5]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[5]~1_combout\ = (\L_clock_generator|devider:counter[5]~q\ & (!\L_clock_generator|devider:counter[4]~2\)) # (!\L_clock_generator|devider:counter[5]~q\ & ((\L_clock_generator|devider:counter[4]~2\) # (GND)))
-- \L_clock_generator|devider:counter[5]~2\ = CARRY((!\L_clock_generator|devider:counter[4]~2\) # (!\L_clock_generator|devider:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|devider:counter[5]~q\,
	datad => VCC,
	cin => \L_clock_generator|devider:counter[4]~2\,
	combout => \L_clock_generator|devider:counter[5]~1_combout\,
	cout => \L_clock_generator|devider:counter[5]~2\);

-- Location: FF_X16_Y6_N11
\L_clock_generator|devider:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|devider:counter[5]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_clock_generator|devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|devider:counter[5]~q\);

-- Location: LCCOMB_X16_Y6_N12
\L_clock_generator|devider:counter[6]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[6]~1_combout\ = (\L_clock_generator|devider:counter[6]~q\ & (\L_clock_generator|devider:counter[5]~2\ $ (GND))) # (!\L_clock_generator|devider:counter[6]~q\ & (!\L_clock_generator|devider:counter[5]~2\ & VCC))
-- \L_clock_generator|devider:counter[6]~2\ = CARRY((\L_clock_generator|devider:counter[6]~q\ & !\L_clock_generator|devider:counter[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|devider:counter[6]~q\,
	datad => VCC,
	cin => \L_clock_generator|devider:counter[5]~2\,
	combout => \L_clock_generator|devider:counter[6]~1_combout\,
	cout => \L_clock_generator|devider:counter[6]~2\);

-- Location: FF_X16_Y6_N13
\L_clock_generator|devider:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|devider:counter[6]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_clock_generator|devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|devider:counter[6]~q\);

-- Location: LCCOMB_X16_Y6_N14
\L_clock_generator|devider:counter[7]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[7]~1_combout\ = \L_clock_generator|devider:counter[7]~q\ $ (\L_clock_generator|devider:counter[6]~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|devider:counter[7]~q\,
	cin => \L_clock_generator|devider:counter[6]~2\,
	combout => \L_clock_generator|devider:counter[7]~1_combout\);

-- Location: FF_X16_Y6_N15
\L_clock_generator|devider:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|devider:counter[7]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_clock_generator|devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|devider:counter[7]~q\);

-- Location: FF_X16_Y6_N29
\L_clock_generator|devider:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|devider:counter[0]~feeder_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_clock_generator|devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|devider:counter[0]~q\);

-- Location: LCCOMB_X16_Y6_N2
\L_clock_generator|devider:counter[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[1]~1_combout\ = (\L_clock_generator|devider:counter[1]~q\ & (!\L_clock_generator|devider:counter[0]~2\)) # (!\L_clock_generator|devider:counter[1]~q\ & ((\L_clock_generator|devider:counter[0]~2\) # (GND)))
-- \L_clock_generator|devider:counter[1]~2\ = CARRY((!\L_clock_generator|devider:counter[0]~2\) # (!\L_clock_generator|devider:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|devider:counter[1]~q\,
	datad => VCC,
	cin => \L_clock_generator|devider:counter[0]~2\,
	combout => \L_clock_generator|devider:counter[1]~1_combout\,
	cout => \L_clock_generator|devider:counter[1]~2\);

-- Location: FF_X17_Y6_N3
\L_clock_generator|devider:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	asdata => \L_clock_generator|devider:counter[1]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_clock_generator|devider:counter[7]~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|devider:counter[1]~q\);

-- Location: LCCOMB_X16_Y6_N4
\L_clock_generator|devider:counter[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|devider:counter[2]~1_combout\ = (\L_clock_generator|devider:counter[2]~q\ & (\L_clock_generator|devider:counter[1]~2\ $ (GND))) # (!\L_clock_generator|devider:counter[2]~q\ & (!\L_clock_generator|devider:counter[1]~2\ & VCC))
-- \L_clock_generator|devider:counter[2]~2\ = CARRY((\L_clock_generator|devider:counter[2]~q\ & !\L_clock_generator|devider:counter[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|devider:counter[2]~q\,
	datad => VCC,
	cin => \L_clock_generator|devider:counter[1]~2\,
	combout => \L_clock_generator|devider:counter[2]~1_combout\,
	cout => \L_clock_generator|devider:counter[2]~2\);

-- Location: FF_X16_Y6_N5
\L_clock_generator|devider:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|devider:counter[2]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_clock_generator|devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|devider:counter[2]~q\);

-- Location: FF_X16_Y6_N7
\L_clock_generator|devider:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|devider:counter[3]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_clock_generator|devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|devider:counter[3]~q\);

-- Location: IOIBUF_X18_Y2_N8
\key[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(2),
	o => \key[2]~input_o\);

-- Location: IOIBUF_X18_Y2_N15
\key[6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(6),
	o => \key[6]~input_o\);

-- Location: IOIBUF_X18_Y3_N8
\key[5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(5),
	o => \key[5]~input_o\);

-- Location: IOIBUF_X18_Y3_N1
\key[7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(7),
	o => \key[7]~input_o\);

-- Location: LCCOMB_X17_Y6_N6
\L_clock_generator|Equal1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Equal1~0_combout\ = (!\key[2]~input_o\ & (\key[6]~input_o\ & (!\key[5]~input_o\ & !\key[7]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[2]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[7]~input_o\,
	combout => \L_clock_generator|Equal1~0_combout\);

-- Location: IOIBUF_X18_Y2_N1
\key[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(1),
	o => \key[1]~input_o\);

-- Location: IOIBUF_X18_Y3_N22
\key[3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(3),
	o => \key[3]~input_o\);

-- Location: IOIBUF_X18_Y2_N22
\key[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(0),
	o => \key[0]~input_o\);

-- Location: IOIBUF_X18_Y3_N15
\key[4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(4),
	o => \key[4]~input_o\);

-- Location: LCCOMB_X15_Y6_N16
\L_clock_generator|Equal1~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Equal1~1_combout\ = (!\key[3]~input_o\ & (!\key[0]~input_o\ & \key[4]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[0]~input_o\,
	datad => \key[4]~input_o\,
	combout => \L_clock_generator|Equal1~1_combout\);

-- Location: LCCOMB_X15_Y6_N28
\L_clock_generator|Selector8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector8~0_combout\ = (\L_clock_generator|tone_state.state_6~q\ & (((!\L_clock_generator|Equal1~1_combout\) # (!\key[1]~input_o\)) # (!\L_clock_generator|Equal1~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_6~q\,
	datab => \L_clock_generator|Equal1~0_combout\,
	datac => \key[1]~input_o\,
	datad => \L_clock_generator|Equal1~1_combout\,
	combout => \L_clock_generator|Selector8~0_combout\);

-- Location: LCCOMB_X17_Y6_N14
\L_clock_generator|Equal1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Equal1~2_combout\ = (!\key[3]~input_o\ & \key[4]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \key[3]~input_o\,
	datad => \key[4]~input_o\,
	combout => \L_clock_generator|Equal1~2_combout\);

-- Location: LCCOMB_X15_Y6_N14
\L_clock_generator|Equal1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Equal1~3_combout\ = (\L_clock_generator|Equal1~0_combout\ & (!\key[0]~input_o\ & (\L_clock_generator|Equal1~2_combout\ & \key[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Equal1~0_combout\,
	datab => \key[0]~input_o\,
	datac => \L_clock_generator|Equal1~2_combout\,
	datad => \key[1]~input_o\,
	combout => \L_clock_generator|Equal1~3_combout\);

-- Location: LCCOMB_X15_Y6_N30
\L_clock_generator|Selector1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector1~0_combout\ = (\L_clock_generator|Equal1~1_combout\ & (\L_clock_generator|Equal1~0_combout\ & (\key[1]~input_o\ & \L_clock_generator|tone_state.state_1~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Equal1~1_combout\,
	datab => \L_clock_generator|Equal1~0_combout\,
	datac => \key[1]~input_o\,
	datad => \L_clock_generator|tone_state.state_1~q\,
	combout => \L_clock_generator|Selector1~0_combout\);

-- Location: LCCOMB_X15_Y6_N22
\L_clock_generator|next_state.state_0_554\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|next_state.state_0_554~combout\ = (GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_clock_generator|Selector1~0_combout\))) # (!GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & 
-- (\L_clock_generator|next_state.state_0_554~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|next_state.state_0_554~combout\,
	datac => \L_clock_generator|Selector1~0_combout\,
	datad => \L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_clock_generator|next_state.state_0_554~combout\);

-- Location: FF_X15_Y6_N23
\L_clock_generator|tone_state.state_0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|next_state.state_0_554~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|tone_state.state_0~q\);

-- Location: LCCOMB_X15_Y6_N8
\L_clock_generator|Selector3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector3~0_combout\ = (\L_clock_generator|Equal1~3_combout\ & (!\L_clock_generator|tone_state.state_3~q\)) # (!\L_clock_generator|Equal1~3_combout\ & ((\L_clock_generator|tone_state.state_1~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|tone_state.state_3~q\,
	datac => \L_clock_generator|tone_state.state_1~q\,
	datad => \L_clock_generator|Equal1~3_combout\,
	combout => \L_clock_generator|Selector3~0_combout\);

-- Location: LCCOMB_X15_Y6_N2
\L_clock_generator|next_state.state_2_498\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|next_state.state_2_498~combout\ = (GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_clock_generator|Selector3~0_combout\))) # (!GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & 
-- (\L_clock_generator|next_state.state_2_498~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|next_state.state_2_498~combout\,
	datac => \L_clock_generator|Selector3~0_combout\,
	datad => \L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_clock_generator|next_state.state_2_498~combout\);

-- Location: FF_X15_Y6_N3
\L_clock_generator|tone_state.state_2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|next_state.state_2_498~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|tone_state.state_2~q\);

-- Location: LCCOMB_X15_Y6_N4
\L_clock_generator|Selector2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector2~0_combout\ = (\L_clock_generator|tone_state.state_0~q\) # ((\L_clock_generator|Equal1~3_combout\ & \L_clock_generator|tone_state.state_2~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Equal1~3_combout\,
	datab => \L_clock_generator|tone_state.state_0~q\,
	datac => \L_clock_generator|tone_state.state_2~q\,
	combout => \L_clock_generator|Selector2~0_combout\);

-- Location: LCCOMB_X15_Y6_N6
\L_clock_generator|next_state.state_1_526\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|next_state.state_1_526~combout\ = (GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_clock_generator|Selector2~0_combout\))) # (!GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & 
-- (\L_clock_generator|next_state.state_1_526~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|next_state.state_1_526~combout\,
	datac => \L_clock_generator|Selector2~0_combout\,
	datad => \L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_clock_generator|next_state.state_1_526~combout\);

-- Location: FF_X15_Y6_N7
\L_clock_generator|tone_state.state_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|next_state.state_1_526~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|tone_state.state_1~q\);

-- Location: LCCOMB_X16_Y6_N26
\L_clock_generator|Selector0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector0~1_combout\ = (\L_clock_generator|tone_state.state_5~q\) # ((\L_clock_generator|tone_state.state_4~q\) # ((\L_clock_generator|tone_state.state_1~q\) # (\L_clock_generator|tone_state.state_2~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_5~q\,
	datab => \L_clock_generator|tone_state.state_4~q\,
	datac => \L_clock_generator|tone_state.state_1~q\,
	datad => \L_clock_generator|tone_state.state_2~q\,
	combout => \L_clock_generator|Selector0~1_combout\);

-- Location: LCCOMB_X17_Y6_N20
\L_clock_generator|Selector0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector0~0_combout\ = ((\key[3]~input_o\ & (\L_clock_generator|tone_state.state_0~q\)) # (!\key[3]~input_o\ & ((\L_clock_generator|tone_state.state_7~q\)))) # (!\L_clock_generator|tone_state.state_3~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_0~q\,
	datab => \key[3]~input_o\,
	datac => \L_clock_generator|tone_state.state_7~q\,
	datad => \L_clock_generator|tone_state.state_3~q\,
	combout => \L_clock_generator|Selector0~0_combout\);

-- Location: LCCOMB_X17_Y6_N12
\L_clock_generator|Selector0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector0~2_combout\ = (!\key[0]~input_o\ & ((\L_clock_generator|tone_state.state_6~q\) # ((\L_clock_generator|Selector0~1_combout\) # (\L_clock_generator|Selector0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_6~q\,
	datab => \key[0]~input_o\,
	datac => \L_clock_generator|Selector0~1_combout\,
	datad => \L_clock_generator|Selector0~0_combout\,
	combout => \L_clock_generator|Selector0~2_combout\);

-- Location: LCCOMB_X17_Y6_N16
\L_clock_generator|Selector0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector0~3_combout\ = (\L_clock_generator|Equal1~0_combout\ & (\key[4]~input_o\ & (\key[1]~input_o\ & \L_clock_generator|Selector0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Equal1~0_combout\,
	datab => \key[4]~input_o\,
	datac => \key[1]~input_o\,
	datad => \L_clock_generator|Selector0~2_combout\,
	combout => \L_clock_generator|Selector0~3_combout\);

-- Location: CLKCTRL_G6
\L_clock_generator|Selector0~3clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \L_clock_generator|Selector0~3clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \L_clock_generator|Selector0~3clkctrl_outclk\);

-- Location: LCCOMB_X15_Y6_N26
\L_clock_generator|next_state.state_7_358\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|next_state.state_7_358~combout\ = (GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_clock_generator|Selector8~0_combout\))) # (!GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & 
-- (\L_clock_generator|next_state.state_7_358~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|next_state.state_7_358~combout\,
	datac => \L_clock_generator|Selector8~0_combout\,
	datad => \L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_clock_generator|next_state.state_7_358~combout\);

-- Location: FF_X15_Y6_N27
\L_clock_generator|tone_state.state_7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|next_state.state_7_358~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|tone_state.state_7~q\);

-- Location: LCCOMB_X15_Y6_N20
\L_clock_generator|Selector7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector7~0_combout\ = (\L_clock_generator|tone_state.state_7~q\) # ((\L_clock_generator|tone_state.state_5~q\ & !\L_clock_generator|Equal1~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_5~q\,
	datac => \L_clock_generator|tone_state.state_7~q\,
	datad => \L_clock_generator|Equal1~3_combout\,
	combout => \L_clock_generator|Selector7~0_combout\);

-- Location: LCCOMB_X15_Y6_N10
\L_clock_generator|next_state.state_6_386\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|next_state.state_6_386~combout\ = (GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_clock_generator|Selector7~0_combout\))) # (!GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & 
-- (\L_clock_generator|next_state.state_6_386~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|next_state.state_6_386~combout\,
	datac => \L_clock_generator|Selector7~0_combout\,
	datad => \L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_clock_generator|next_state.state_6_386~combout\);

-- Location: FF_X15_Y6_N11
\L_clock_generator|tone_state.state_6\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|next_state.state_6_386~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|tone_state.state_6~q\);

-- Location: LCCOMB_X15_Y6_N18
\L_clock_generator|Selector6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector6~0_combout\ = (\L_clock_generator|Equal1~3_combout\ & (\L_clock_generator|tone_state.state_6~q\)) # (!\L_clock_generator|Equal1~3_combout\ & ((\L_clock_generator|tone_state.state_4~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_6~q\,
	datac => \L_clock_generator|tone_state.state_4~q\,
	datad => \L_clock_generator|Equal1~3_combout\,
	combout => \L_clock_generator|Selector6~0_combout\);

-- Location: LCCOMB_X15_Y6_N0
\L_clock_generator|next_state.state_5_414\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|next_state.state_5_414~combout\ = (GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_clock_generator|Selector6~0_combout\))) # (!GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & 
-- (\L_clock_generator|next_state.state_5_414~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|next_state.state_5_414~combout\,
	datac => \L_clock_generator|Selector6~0_combout\,
	datad => \L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_clock_generator|next_state.state_5_414~combout\);

-- Location: FF_X15_Y6_N1
\L_clock_generator|tone_state.state_5\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|next_state.state_5_414~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|tone_state.state_5~q\);

-- Location: LCCOMB_X15_Y6_N12
\L_clock_generator|Selector5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector5~0_combout\ = (\L_clock_generator|Equal1~3_combout\ & (\L_clock_generator|tone_state.state_5~q\)) # (!\L_clock_generator|Equal1~3_combout\ & ((!\L_clock_generator|tone_state.state_3~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_5~q\,
	datab => \L_clock_generator|tone_state.state_3~q\,
	datad => \L_clock_generator|Equal1~3_combout\,
	combout => \L_clock_generator|Selector5~0_combout\);

-- Location: LCCOMB_X15_Y6_N24
\L_clock_generator|next_state.state_4_442\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|next_state.state_4_442~combout\ = (GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_clock_generator|Selector5~0_combout\))) # (!GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & 
-- (\L_clock_generator|next_state.state_4_442~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|next_state.state_4_442~combout\,
	datac => \L_clock_generator|Selector5~0_combout\,
	datad => \L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_clock_generator|next_state.state_4_442~combout\);

-- Location: FF_X15_Y6_N25
\L_clock_generator|tone_state.state_4\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|next_state.state_4_442~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|tone_state.state_4~q\);

-- Location: LCCOMB_X16_Y6_N30
\L_clock_generator|Selector4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector4~0_combout\ = (\L_clock_generator|Equal1~3_combout\ & (\L_clock_generator|tone_state.state_4~q\)) # (!\L_clock_generator|Equal1~3_combout\ & ((\L_clock_generator|tone_state.state_2~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_4~q\,
	datab => \L_clock_generator|tone_state.state_2~q\,
	datac => \L_clock_generator|Equal1~3_combout\,
	combout => \L_clock_generator|Selector4~0_combout\);

-- Location: LCCOMB_X16_Y6_N18
\L_clock_generator|next_state.state_3_470\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|next_state.state_3_470~combout\ = (GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & ((\L_clock_generator|Selector4~0_combout\))) # (!GLOBAL(\L_clock_generator|Selector0~3clkctrl_outclk\) & 
-- (\L_clock_generator|next_state.state_3_470~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_clock_generator|next_state.state_3_470~combout\,
	datac => \L_clock_generator|Selector4~0_combout\,
	datad => \L_clock_generator|Selector0~3clkctrl_outclk\,
	combout => \L_clock_generator|next_state.state_3_470~combout\);

-- Location: LCCOMB_X16_Y6_N22
\L_clock_generator|tone_state.state_3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|tone_state.state_3~0_combout\ = !\L_clock_generator|next_state.state_3_470~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \L_clock_generator|next_state.state_3_470~combout\,
	combout => \L_clock_generator|tone_state.state_3~0_combout\);

-- Location: FF_X16_Y6_N23
\L_clock_generator|tone_state.state_3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \L_clock_generator|tone_state.state_3~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_clock_generator|tone_state.state_3~q\);

-- Location: LCCOMB_X16_Y6_N16
\L_clock_generator|Selector9~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector9~2_combout\ = (\L_clock_generator|devider:counter[3]~q\ & (((\L_clock_generator|devider:counter[2]~q\ & \L_clock_generator|tone_state.state_2~q\)) # (!\L_clock_generator|tone_state.state_3~q\))) # 
-- (!\L_clock_generator|devider:counter[3]~q\ & (\L_clock_generator|devider:counter[2]~q\ & ((\L_clock_generator|tone_state.state_2~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|devider:counter[3]~q\,
	datab => \L_clock_generator|devider:counter[2]~q\,
	datac => \L_clock_generator|tone_state.state_3~q\,
	datad => \L_clock_generator|tone_state.state_2~q\,
	combout => \L_clock_generator|Selector9~2_combout\);

-- Location: LCCOMB_X16_Y6_N20
\L_clock_generator|Selector9~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector9~1_combout\ = (\L_clock_generator|devider:counter[5]~q\ & ((\L_clock_generator|tone_state.state_5~q\) # ((\L_clock_generator|devider:counter[4]~q\ & \L_clock_generator|tone_state.state_4~q\)))) # 
-- (!\L_clock_generator|devider:counter[5]~q\ & (\L_clock_generator|devider:counter[4]~q\ & ((\L_clock_generator|tone_state.state_4~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|devider:counter[5]~q\,
	datab => \L_clock_generator|devider:counter[4]~q\,
	datac => \L_clock_generator|tone_state.state_5~q\,
	datad => \L_clock_generator|tone_state.state_4~q\,
	combout => \L_clock_generator|Selector9~1_combout\);

-- Location: LCCOMB_X17_Y6_N22
\L_clock_generator|Selector9~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector9~3_combout\ = (\L_clock_generator|tone_state.state_0~q\ & ((\L_clock_generator|devider:counter[0]~q\) # ((\L_clock_generator|devider:counter[1]~q\ & \L_clock_generator|tone_state.state_1~q\)))) # 
-- (!\L_clock_generator|tone_state.state_0~q\ & (\L_clock_generator|devider:counter[1]~q\ & (\L_clock_generator|tone_state.state_1~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|tone_state.state_0~q\,
	datab => \L_clock_generator|devider:counter[1]~q\,
	datac => \L_clock_generator|tone_state.state_1~q\,
	datad => \L_clock_generator|devider:counter[0]~q\,
	combout => \L_clock_generator|Selector9~3_combout\);

-- Location: LCCOMB_X16_Y6_N24
\L_clock_generator|Selector9~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector9~0_combout\ = (\L_clock_generator|devider:counter[6]~q\ & ((\L_clock_generator|tone_state.state_6~q\) # ((\L_clock_generator|devider:counter[7]~q\ & \L_clock_generator|tone_state.state_7~q\)))) # 
-- (!\L_clock_generator|devider:counter[6]~q\ & (\L_clock_generator|devider:counter[7]~q\ & (\L_clock_generator|tone_state.state_7~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|devider:counter[6]~q\,
	datab => \L_clock_generator|devider:counter[7]~q\,
	datac => \L_clock_generator|tone_state.state_7~q\,
	datad => \L_clock_generator|tone_state.state_6~q\,
	combout => \L_clock_generator|Selector9~0_combout\);

-- Location: LCCOMB_X17_Y6_N24
\L_clock_generator|Selector9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_clock_generator|Selector9~combout\ = LCELL((!\L_clock_generator|Selector9~2_combout\ & (!\L_clock_generator|Selector9~1_combout\ & (!\L_clock_generator|Selector9~3_combout\ & !\L_clock_generator|Selector9~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Selector9~2_combout\,
	datab => \L_clock_generator|Selector9~1_combout\,
	datac => \L_clock_generator|Selector9~3_combout\,
	datad => \L_clock_generator|Selector9~0_combout\,
	combout => \L_clock_generator|Selector9~combout\);

-- Location: CLKCTRL_G8
\L_clock_generator|Selector9~clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \L_clock_generator|Selector9~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \L_clock_generator|Selector9~clkctrl_outclk\);

-- Location: LCCOMB_X16_Y2_N16
\L_pulselength2audio|COUNT:counter[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[0]~1_combout\ = \L_pulselength2audio|COUNT:counter[0]~q\ $ (VCC)
-- \L_pulselength2audio|COUNT:counter[0]~2\ = CARRY(\L_pulselength2audio|COUNT:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[0]~q\,
	datad => VCC,
	combout => \L_pulselength2audio|COUNT:counter[0]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[0]~2\);

-- Location: LCCOMB_X17_Y3_N28
\L_key2pulselength|Mux7~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux7~4_combout\ = (!\key[1]~input_o\ & (\key[2]~input_o\ & (!\key[6]~input_o\ & !\key[7]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[2]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[7]~input_o\,
	combout => \L_key2pulselength|Mux7~4_combout\);

-- Location: LCCOMB_X17_Y2_N18
\L_key2pulselength|Mux5~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux5~1_combout\ = (\key[5]~input_o\ & ((\key[0]~input_o\) # (\key[3]~input_o\ $ (!\key[4]~input_o\)))) # (!\key[5]~input_o\ & (\key[0]~input_o\ & (\key[3]~input_o\ $ (\key[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[0]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[4]~input_o\,
	combout => \L_key2pulselength|Mux5~1_combout\);

-- Location: LCCOMB_X17_Y2_N14
\L_key2pulselength|Mux9~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux9~7_combout\ = (\key[6]~input_o\ & (\key[2]~input_o\ & !\key[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key[6]~input_o\,
	datac => \key[2]~input_o\,
	datad => \key[7]~input_o\,
	combout => \L_key2pulselength|Mux9~7_combout\);

-- Location: LCCOMB_X17_Y3_N12
\L_key2pulselength|Mux4~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux4~5_combout\ = (!\key[5]~input_o\ & (!\key[4]~input_o\ & !\key[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux4~5_combout\);

-- Location: LCCOMB_X17_Y2_N4
\L_key2pulselength|Mux5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux5~0_combout\ = (\L_key2pulselength|Mux9~7_combout\ & (\L_key2pulselength|Mux4~5_combout\ & (\key[3]~input_o\ $ (!\key[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[1]~input_o\,
	datac => \L_key2pulselength|Mux9~7_combout\,
	datad => \L_key2pulselength|Mux4~5_combout\,
	combout => \L_key2pulselength|Mux5~0_combout\);

-- Location: LCCOMB_X17_Y2_N8
\L_key2pulselength|Mux2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux2~0_combout\ = (\key[4]~input_o\ & (\key[0]~input_o\ & \key[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[0]~input_o\,
	datac => \key[3]~input_o\,
	combout => \L_key2pulselength|Mux2~0_combout\);

-- Location: LCCOMB_X17_Y2_N22
\L_key2pulselength|Mux12~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux12~0_combout\ = (\key[1]~input_o\ & (\L_key2pulselength|Mux2~0_combout\ & \L_clock_generator|Equal1~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key[1]~input_o\,
	datac => \L_key2pulselength|Mux2~0_combout\,
	datad => \L_clock_generator|Equal1~0_combout\,
	combout => \L_key2pulselength|Mux12~0_combout\);

-- Location: LCCOMB_X17_Y2_N12
\L_key2pulselength|Mux5~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux5~2_combout\ = (\L_key2pulselength|Mux5~0_combout\) # ((\L_key2pulselength|Mux12~0_combout\) # ((\L_key2pulselength|Mux7~4_combout\ & \L_key2pulselength|Mux5~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux7~4_combout\,
	datab => \L_key2pulselength|Mux5~1_combout\,
	datac => \L_key2pulselength|Mux5~0_combout\,
	datad => \L_key2pulselength|Mux12~0_combout\,
	combout => \L_key2pulselength|Mux5~2_combout\);

-- Location: LCCOMB_X15_Y2_N28
\L_key2pulselength|Mux0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux0~1_combout\ = (\key[5]~input_o\ & (!\key[4]~input_o\)) # (!\key[5]~input_o\ & (\key[0]~input_o\ & ((\key[4]~input_o\) # (\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux0~1_combout\);

-- Location: LCCOMB_X14_Y2_N0
\L_key2pulselength|Mux0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux0~0_combout\ = (!\key[0]~input_o\ & (!\key[3]~input_o\ & ((\key[4]~input_o\) # (\key[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[0]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[3]~input_o\,
	combout => \L_key2pulselength|Mux0~0_combout\);

-- Location: LCCOMB_X17_Y3_N0
\L_key2pulselength|Mux3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux3~0_combout\ = (\key[2]~input_o\ & (!\key[6]~input_o\ & !\key[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key[2]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[7]~input_o\,
	combout => \L_key2pulselength|Mux3~0_combout\);

-- Location: LCCOMB_X15_Y2_N6
\L_key2pulselength|Mux0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux0~2_combout\ = (\L_key2pulselength|Mux3~0_combout\ & ((\key[1]~input_o\ & ((\L_key2pulselength|Mux0~0_combout\))) # (!\key[1]~input_o\ & (\L_key2pulselength|Mux0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \L_key2pulselength|Mux0~1_combout\,
	datac => \L_key2pulselength|Mux0~0_combout\,
	datad => \L_key2pulselength|Mux3~0_combout\,
	combout => \L_key2pulselength|Mux0~2_combout\);

-- Location: LCCOMB_X17_Y3_N24
\L_key2pulselength|Mux12~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux12~1_combout\ = (\key[4]~input_o\ & (((!\key[6]~input_o\ & !\key[1]~input_o\)))) # (!\key[4]~input_o\ & (!\key[5]~input_o\ & (\key[6]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[1]~input_o\,
	combout => \L_key2pulselength|Mux12~1_combout\);

-- Location: LCCOMB_X17_Y3_N14
\L_key2pulselength|Mux12~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux12~2_combout\ = (\key[5]~input_o\ & (((!\key[6]~input_o\)))) # (!\key[5]~input_o\ & (!\key[4]~input_o\ & (\key[6]~input_o\ & \key[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[1]~input_o\,
	combout => \L_key2pulselength|Mux12~2_combout\);

-- Location: LCCOMB_X15_Y2_N22
\L_key2pulselength|Mux12~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux12~3_combout\ = (\key[0]~input_o\ & (\L_key2pulselength|Mux12~1_combout\ & (!\key[3]~input_o\ & !\L_key2pulselength|Mux12~2_combout\))) # (!\key[0]~input_o\ & (\L_key2pulselength|Mux12~2_combout\ & 
-- (\L_key2pulselength|Mux12~1_combout\ $ (!\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[0]~input_o\,
	datab => \L_key2pulselength|Mux12~1_combout\,
	datac => \key[3]~input_o\,
	datad => \L_key2pulselength|Mux12~2_combout\,
	combout => \L_key2pulselength|Mux12~3_combout\);

-- Location: LCCOMB_X17_Y3_N26
\L_key2pulselength|Mux4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux4~0_combout\ = (\key[2]~input_o\ & !\key[7]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key[2]~input_o\,
	datad => \key[7]~input_o\,
	combout => \L_key2pulselength|Mux4~0_combout\);

-- Location: LCCOMB_X15_Y2_N12
\L_pulselength2audio|Equal1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~3_combout\ = \L_pulselength2audio|COUNT:counter[0]~q\ $ (((\L_key2pulselength|Mux12~0_combout\) # ((\L_key2pulselength|Mux12~3_combout\ & \L_key2pulselength|Mux4~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux12~3_combout\,
	datab => \L_key2pulselength|Mux4~0_combout\,
	datac => \L_key2pulselength|Mux12~0_combout\,
	datad => \L_pulselength2audio|COUNT:counter[0]~q\,
	combout => \L_pulselength2audio|Equal1~3_combout\);

-- Location: LCCOMB_X17_Y6_N2
\L_key2pulselength|Mux3~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux3~1_combout\ = (\key[2]~input_o\ & (!\key[1]~input_o\ & !\key[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[2]~input_o\,
	datab => \key[1]~input_o\,
	datad => \key[7]~input_o\,
	combout => \L_key2pulselength|Mux3~1_combout\);

-- Location: LCCOMB_X15_Y2_N0
\L_key2pulselength|Mux7~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux7~2_combout\ = (\key[5]~input_o\) # ((\key[4]~input_o\) # (\key[3]~input_o\ $ (\key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux7~2_combout\);

-- Location: LCCOMB_X15_Y2_N18
\L_key2pulselength|Mux7~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux7~3_combout\ = (\key[6]~input_o\ & (\L_key2pulselength|Mux3~1_combout\ & !\L_key2pulselength|Mux7~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \L_key2pulselength|Mux3~1_combout\,
	datad => \L_key2pulselength|Mux7~2_combout\,
	combout => \L_key2pulselength|Mux7~3_combout\);

-- Location: LCCOMB_X15_Y2_N8
\L_key2pulselength|Mux7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux7~0_combout\ = (\key[5]~input_o\ & (\key[3]~input_o\ & (\key[4]~input_o\ $ (!\key[0]~input_o\)))) # (!\key[5]~input_o\ & (\key[0]~input_o\ & (\key[4]~input_o\ $ (\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux7~0_combout\);

-- Location: LCCOMB_X15_Y2_N2
\L_key2pulselength|Mux7~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux7~1_combout\ = (\L_key2pulselength|Mux3~0_combout\ & ((\key[1]~input_o\ & ((\L_clock_generator|Equal1~1_combout\))) # (!\key[1]~input_o\ & (\L_key2pulselength|Mux7~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \L_key2pulselength|Mux3~0_combout\,
	datac => \L_key2pulselength|Mux7~0_combout\,
	datad => \L_clock_generator|Equal1~1_combout\,
	combout => \L_key2pulselength|Mux7~1_combout\);

-- Location: LCCOMB_X15_Y2_N20
\L_pulselength2audio|Equal1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~2_combout\ = \L_pulselength2audio|COUNT:counter[5]~q\ $ (((\L_key2pulselength|Mux12~0_combout\) # ((\L_key2pulselength|Mux7~3_combout\) # (\L_key2pulselength|Mux7~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux12~0_combout\,
	datab => \L_key2pulselength|Mux7~3_combout\,
	datac => \L_pulselength2audio|COUNT:counter[5]~q\,
	datad => \L_key2pulselength|Mux7~1_combout\,
	combout => \L_pulselength2audio|Equal1~2_combout\);

-- Location: LCCOMB_X15_Y2_N26
\L_pulselength2audio|Equal1~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~4_combout\ = (!\L_pulselength2audio|Equal1~3_combout\ & (!\L_pulselength2audio|Equal1~2_combout\ & (\L_key2pulselength|Mux0~2_combout\ $ (!\L_pulselength2audio|COUNT:counter[12]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux0~2_combout\,
	datab => \L_pulselength2audio|COUNT:counter[12]~q\,
	datac => \L_pulselength2audio|Equal1~3_combout\,
	datad => \L_pulselength2audio|Equal1~2_combout\,
	combout => \L_pulselength2audio|Equal1~4_combout\);

-- Location: LCCOMB_X14_Y2_N4
\L_key2pulselength|Mux1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux1~0_combout\ = (\key[0]~input_o\ & ((\key[4]~input_o\ & (\key[1]~input_o\ & \key[3]~input_o\)) # (!\key[4]~input_o\ & ((!\key[3]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[3]~input_o\,
	combout => \L_key2pulselength|Mux1~0_combout\);

-- Location: LCCOMB_X14_Y2_N6
\L_key2pulselength|Mux1~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux1~1_combout\ = (\key[5]~input_o\ & (!\key[6]~input_o\ & ((\key[4]~input_o\)))) # (!\key[5]~input_o\ & (((\key[0]~input_o\ & !\key[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[4]~input_o\,
	combout => \L_key2pulselength|Mux1~1_combout\);

-- Location: LCCOMB_X14_Y2_N24
\L_key2pulselength|Mux1~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux1~5_combout\ = (\key[4]~input_o\ & (!\key[6]~input_o\ & (\key[5]~input_o\ $ (!\key[3]~input_o\)))) # (!\key[4]~input_o\ & (\key[6]~input_o\ & (!\key[5]~input_o\ & \key[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[3]~input_o\,
	combout => \L_key2pulselength|Mux1~5_combout\);

-- Location: LCCOMB_X14_Y2_N14
\L_key2pulselength|Mux1~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux1~6_combout\ = (\key[1]~input_o\ & (((\L_key2pulselength|Mux1~5_combout\ & !\key[0]~input_o\)))) # (!\key[1]~input_o\ & (\key[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \L_key2pulselength|Mux1~5_combout\,
	datac => \key[1]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux1~6_combout\);

-- Location: LCCOMB_X14_Y2_N28
\L_key2pulselength|Mux1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux1~2_combout\ = (\key[6]~input_o\ & (!\key[5]~input_o\ & ((!\key[4]~input_o\) # (!\key[0]~input_o\)))) # (!\key[6]~input_o\ & (((\key[0]~input_o\ & \key[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[4]~input_o\,
	combout => \L_key2pulselength|Mux1~2_combout\);

-- Location: LCCOMB_X14_Y2_N18
\L_key2pulselength|Mux1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux1~3_combout\ = (\key[1]~input_o\ & (((\L_key2pulselength|Mux1~6_combout\)))) # (!\key[1]~input_o\ & ((\L_key2pulselength|Mux1~6_combout\ & (\L_key2pulselength|Mux1~1_combout\)) # (!\L_key2pulselength|Mux1~6_combout\ & 
-- ((\L_key2pulselength|Mux1~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux1~1_combout\,
	datab => \key[1]~input_o\,
	datac => \L_key2pulselength|Mux1~6_combout\,
	datad => \L_key2pulselength|Mux1~2_combout\,
	combout => \L_key2pulselength|Mux1~3_combout\);

-- Location: LCCOMB_X14_Y2_N16
\L_key2pulselength|Mux1~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux1~4_combout\ = (\L_clock_generator|Equal1~0_combout\ & ((\L_key2pulselength|Mux1~0_combout\) # ((\L_key2pulselength|Mux4~0_combout\ & \L_key2pulselength|Mux1~3_combout\)))) # (!\L_clock_generator|Equal1~0_combout\ & 
-- (\L_key2pulselength|Mux4~0_combout\ & ((\L_key2pulselength|Mux1~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Equal1~0_combout\,
	datab => \L_key2pulselength|Mux4~0_combout\,
	datac => \L_key2pulselength|Mux1~0_combout\,
	datad => \L_key2pulselength|Mux1~3_combout\,
	combout => \L_key2pulselength|Mux1~4_combout\);

-- Location: LCCOMB_X14_Y2_N8
\L_key2pulselength|Mux8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux8~0_combout\ = (\key[1]~input_o\ & (\key[0]~input_o\ & (\key[4]~input_o\ $ (!\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[3]~input_o\,
	combout => \L_key2pulselength|Mux8~0_combout\);

-- Location: LCCOMB_X14_Y2_N12
\L_key2pulselength|Mux8~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux8~3_combout\ = (\key[0]~input_o\ & (!\key[1]~input_o\ & (\key[5]~input_o\ $ (\key[3]~input_o\)))) # (!\key[0]~input_o\ & ((\key[3]~input_o\ & (\key[5]~input_o\)) # (!\key[3]~input_o\ & ((\key[1]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[3]~input_o\,
	combout => \L_key2pulselength|Mux8~3_combout\);

-- Location: LCCOMB_X14_Y2_N10
\L_key2pulselength|Mux8~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux8~4_combout\ = (\key[4]~input_o\ & ((\key[0]~input_o\ & (!\key[1]~input_o\)) # (!\key[0]~input_o\ & ((\L_key2pulselength|Mux8~3_combout\))))) # (!\key[4]~input_o\ & (!\key[1]~input_o\ & ((\L_key2pulselength|Mux8~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \L_key2pulselength|Mux8~3_combout\,
	combout => \L_key2pulselength|Mux8~4_combout\);

-- Location: LCCOMB_X14_Y2_N30
\L_key2pulselength|Mux8~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux8~2_combout\ = (\L_clock_generator|Equal1~0_combout\ & ((\L_key2pulselength|Mux8~0_combout\) # ((\L_key2pulselength|Mux3~0_combout\ & \L_key2pulselength|Mux8~4_combout\)))) # (!\L_clock_generator|Equal1~0_combout\ & 
-- (\L_key2pulselength|Mux3~0_combout\ & ((\L_key2pulselength|Mux8~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Equal1~0_combout\,
	datab => \L_key2pulselength|Mux3~0_combout\,
	datac => \L_key2pulselength|Mux8~0_combout\,
	datad => \L_key2pulselength|Mux8~4_combout\,
	combout => \L_key2pulselength|Mux8~2_combout\);

-- Location: LCCOMB_X15_Y2_N16
\L_pulselength2audio|Equal1~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~7_combout\ = (\L_pulselength2audio|COUNT:counter[4]~q\ & (\L_key2pulselength|Mux8~2_combout\ & (\L_pulselength2audio|COUNT:counter[11]~q\ $ (!\L_key2pulselength|Mux1~4_combout\)))) # (!\L_pulselength2audio|COUNT:counter[4]~q\ & 
-- (!\L_key2pulselength|Mux8~2_combout\ & (\L_pulselength2audio|COUNT:counter[11]~q\ $ (!\L_key2pulselength|Mux1~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[4]~q\,
	datab => \L_pulselength2audio|COUNT:counter[11]~q\,
	datac => \L_key2pulselength|Mux1~4_combout\,
	datad => \L_key2pulselength|Mux8~2_combout\,
	combout => \L_pulselength2audio|Equal1~7_combout\);

-- Location: LCCOMB_X17_Y2_N24
\L_key2pulselength|Mux6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux6~0_combout\ = (\L_clock_generator|Equal1~0_combout\ & (\key[0]~input_o\ & (!\key[3]~input_o\ & !\key[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Equal1~0_combout\,
	datab => \key[0]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[4]~input_o\,
	combout => \L_key2pulselength|Mux6~0_combout\);

-- Location: LCCOMB_X17_Y2_N20
\L_key2pulselength|Mux2~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux2~1_combout\ = (\key[0]~input_o\ & (((\key[1]~input_o\)) # (!\key[4]~input_o\))) # (!\key[0]~input_o\ & (\key[4]~input_o\ $ (((\key[3]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux2~1_combout\);

-- Location: LCCOMB_X17_Y2_N2
\L_key2pulselength|Mux2~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux2~2_combout\ = (\key[5]~input_o\ & (\L_key2pulselength|Mux3~0_combout\ & !\L_key2pulselength|Mux2~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \L_key2pulselength|Mux3~0_combout\,
	datad => \L_key2pulselength|Mux2~1_combout\,
	combout => \L_key2pulselength|Mux2~2_combout\);

-- Location: LCCOMB_X17_Y2_N0
\L_key2pulselength|Mux2~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux2~3_combout\ = (\L_key2pulselength|Mux2~2_combout\) # ((\L_key2pulselength|Mux7~4_combout\ & (\L_key2pulselength|Mux2~0_combout\ & !\key[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux7~4_combout\,
	datab => \L_key2pulselength|Mux2~0_combout\,
	datac => \key[5]~input_o\,
	datad => \L_key2pulselength|Mux2~2_combout\,
	combout => \L_key2pulselength|Mux2~3_combout\);

-- Location: LCCOMB_X16_Y2_N14
\L_pulselength2audio|Equal1~16\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~16_combout\ = \L_pulselength2audio|COUNT:counter[10]~q\ $ (((\L_key2pulselength|Mux2~3_combout\) # ((\key[1]~input_o\ & \L_key2pulselength|Mux6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \L_key2pulselength|Mux6~0_combout\,
	datac => \L_key2pulselength|Mux2~3_combout\,
	datad => \L_pulselength2audio|COUNT:counter[10]~q\,
	combout => \L_pulselength2audio|Equal1~16_combout\);

-- Location: LCCOMB_X17_Y6_N10
\L_key2pulselength|Mux6~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux6~1_combout\ = (!\key[5]~input_o\ & (\key[3]~input_o\ & (\key[4]~input_o\ $ (\key[6]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[5]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[6]~input_o\,
	combout => \L_key2pulselength|Mux6~1_combout\);

-- Location: LCCOMB_X17_Y6_N28
\L_key2pulselength|Mux6~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux6~2_combout\ = (\key[5]~input_o\ & (!\key[6]~input_o\ & ((\key[3]~input_o\) # (!\key[4]~input_o\)))) # (!\key[5]~input_o\ & (\key[4]~input_o\ & (!\key[3]~input_o\ & \key[6]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[5]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[6]~input_o\,
	combout => \L_key2pulselength|Mux6~2_combout\);

-- Location: LCCOMB_X17_Y6_N18
\L_key2pulselength|Mux6~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux6~3_combout\ = (\L_key2pulselength|Mux3~1_combout\ & ((\key[0]~input_o\ & (\L_key2pulselength|Mux6~1_combout\)) # (!\key[0]~input_o\ & ((\L_key2pulselength|Mux6~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux6~1_combout\,
	datab => \L_key2pulselength|Mux6~2_combout\,
	datac => \L_key2pulselength|Mux3~1_combout\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux6~3_combout\);

-- Location: LCCOMB_X17_Y3_N18
\L_key2pulselength|Mux6~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux6~4_combout\ = (\key[5]~input_o\ & (!\key[6]~input_o\ & (!\key[3]~input_o\ & \key[4]~input_o\))) # (!\key[5]~input_o\ & (\key[6]~input_o\ & (\key[3]~input_o\ & !\key[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[4]~input_o\,
	combout => \L_key2pulselength|Mux6~4_combout\);

-- Location: LCCOMB_X17_Y2_N6
\L_key2pulselength|Mux6~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux6~5_combout\ = (\L_key2pulselength|Mux4~0_combout\ & (\key[1]~input_o\ & (\L_key2pulselength|Mux6~4_combout\ & !\key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux4~0_combout\,
	datab => \key[1]~input_o\,
	datac => \L_key2pulselength|Mux6~4_combout\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux6~5_combout\);

-- Location: LCCOMB_X16_Y2_N6
\L_pulselength2audio|Equal1~14\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~14_combout\ = \L_pulselength2audio|COUNT:counter[6]~q\ $ (((\L_key2pulselength|Mux6~0_combout\) # ((\L_key2pulselength|Mux6~3_combout\) # (\L_key2pulselength|Mux6~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[6]~q\,
	datab => \L_key2pulselength|Mux6~0_combout\,
	datac => \L_key2pulselength|Mux6~3_combout\,
	datad => \L_key2pulselength|Mux6~5_combout\,
	combout => \L_pulselength2audio|Equal1~14_combout\);

-- Location: LCCOMB_X17_Y2_N26
\L_key2pulselength|Mux11~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux11~1_combout\ = (\key[6]~input_o\ & ((\key[5]~input_o\ & ((\key[0]~input_o\))) # (!\key[5]~input_o\ & (\key[3]~input_o\)))) # (!\key[6]~input_o\ & (!\key[3]~input_o\ & (\key[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux11~1_combout\);

-- Location: LCCOMB_X17_Y2_N16
\L_key2pulselength|Mux11~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux11~0_combout\ = (\key[6]~input_o\ & (((\key[0]~input_o\) # (!\key[5]~input_o\)))) # (!\key[6]~input_o\ & (\key[3]~input_o\ & (\key[5]~input_o\ $ (\key[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux11~0_combout\);

-- Location: LCCOMB_X17_Y2_N28
\L_key2pulselength|Mux11~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux11~2_combout\ = \L_key2pulselength|Mux11~0_combout\ $ (((\key[4]~input_o\ & ((\key[0]~input_o\) # (\L_key2pulselength|Mux11~1_combout\))) # (!\key[4]~input_o\ & (\key[0]~input_o\ & \L_key2pulselength|Mux11~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011111101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[0]~input_o\,
	datac => \L_key2pulselength|Mux11~1_combout\,
	datad => \L_key2pulselength|Mux11~0_combout\,
	combout => \L_key2pulselength|Mux11~2_combout\);

-- Location: LCCOMB_X17_Y2_N30
\L_key2pulselength|Mux11~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux11~3_combout\ = (\L_key2pulselength|Mux11~2_combout\ & (\key[1]~input_o\ $ (((\key[0]~input_o\) # (!\L_key2pulselength|Mux11~1_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux11~1_combout\,
	datab => \key[0]~input_o\,
	datac => \key[1]~input_o\,
	datad => \L_key2pulselength|Mux11~2_combout\,
	combout => \L_key2pulselength|Mux11~3_combout\);

-- Location: LCCOMB_X17_Y2_N10
\L_pulselength2audio|Equal1~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~19_combout\ = \L_pulselength2audio|COUNT:counter[1]~q\ $ (((!\key[7]~input_o\ & (\key[2]~input_o\ & \L_key2pulselength|Mux11~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[7]~input_o\,
	datab => \key[2]~input_o\,
	datac => \L_key2pulselength|Mux11~3_combout\,
	datad => \L_pulselength2audio|COUNT:counter[1]~q\,
	combout => \L_pulselength2audio|Equal1~19_combout\);

-- Location: LCCOMB_X17_Y3_N8
\L_key2pulselength|Mux4~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux4~3_combout\ = (\key[5]~input_o\) # ((\key[4]~input_o\ & ((\key[3]~input_o\) # (\key[0]~input_o\))) # (!\key[4]~input_o\ & (\key[3]~input_o\ & \key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux4~3_combout\);

-- Location: LCCOMB_X17_Y3_N16
\L_key2pulselength|Mux4~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux4~1_combout\ = (\key[0]~input_o\ & (!\key[1]~input_o\ & (\key[5]~input_o\ $ (\key[3]~input_o\)))) # (!\key[0]~input_o\ & (((\key[5]~input_o\ & \key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[5]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux4~1_combout\);

-- Location: LCCOMB_X17_Y3_N6
\L_key2pulselength|Mux4~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux4~2_combout\ = (\key[6]~input_o\ & (\key[1]~input_o\)) # (!\key[6]~input_o\ & (\L_key2pulselength|Mux4~1_combout\ & ((\key[4]~input_o\) # (!\key[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \L_key2pulselength|Mux4~1_combout\,
	combout => \L_key2pulselength|Mux4~2_combout\);

-- Location: LCCOMB_X17_Y3_N30
\L_key2pulselength|Mux4~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux4~4_combout\ = (\key[6]~input_o\ & (!\L_key2pulselength|Mux4~3_combout\ & (\key[3]~input_o\ $ (!\L_key2pulselength|Mux4~2_combout\)))) # (!\key[6]~input_o\ & (((\L_key2pulselength|Mux4~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \key[6]~input_o\,
	datac => \L_key2pulselength|Mux4~3_combout\,
	datad => \L_key2pulselength|Mux4~2_combout\,
	combout => \L_key2pulselength|Mux4~4_combout\);

-- Location: LCCOMB_X16_Y1_N8
\L_pulselength2audio|COUNT:counter[12]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[12]~1_combout\ = (\L_pulselength2audio|COUNT:counter[12]~q\ & (\L_pulselength2audio|COUNT:counter[11]~2\ $ (GND))) # (!\L_pulselength2audio|COUNT:counter[12]~q\ & (!\L_pulselength2audio|COUNT:counter[11]~2\ & VCC))
-- \L_pulselength2audio|COUNT:counter[12]~2\ = CARRY((\L_pulselength2audio|COUNT:counter[12]~q\ & !\L_pulselength2audio|COUNT:counter[11]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[12]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[11]~2\,
	combout => \L_pulselength2audio|COUNT:counter[12]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[12]~2\);

-- Location: LCCOMB_X16_Y1_N10
\L_pulselength2audio|COUNT:counter[13]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[13]~1_combout\ = (\L_pulselength2audio|COUNT:counter[13]~q\ & (!\L_pulselength2audio|COUNT:counter[12]~2\)) # (!\L_pulselength2audio|COUNT:counter[13]~q\ & ((\L_pulselength2audio|COUNT:counter[12]~2\) # (GND)))
-- \L_pulselength2audio|COUNT:counter[13]~2\ = CARRY((!\L_pulselength2audio|COUNT:counter[12]~2\) # (!\L_pulselength2audio|COUNT:counter[13]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[13]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[12]~2\,
	combout => \L_pulselength2audio|COUNT:counter[13]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[13]~2\);

-- Location: FF_X16_Y1_N11
\L_pulselength2audio|COUNT:counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[13]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[13]~q\);

-- Location: LCCOMB_X16_Y1_N12
\L_pulselength2audio|COUNT:counter[14]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[14]~1_combout\ = (\L_pulselength2audio|COUNT:counter[14]~q\ & (\L_pulselength2audio|COUNT:counter[13]~2\ $ (GND))) # (!\L_pulselength2audio|COUNT:counter[14]~q\ & (!\L_pulselength2audio|COUNT:counter[13]~2\ & VCC))
-- \L_pulselength2audio|COUNT:counter[14]~2\ = CARRY((\L_pulselength2audio|COUNT:counter[14]~q\ & !\L_pulselength2audio|COUNT:counter[13]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[14]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[13]~2\,
	combout => \L_pulselength2audio|COUNT:counter[14]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[14]~2\);

-- Location: FF_X16_Y1_N13
\L_pulselength2audio|COUNT:counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[14]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[14]~q\);

-- Location: LCCOMB_X16_Y1_N14
\L_pulselength2audio|COUNT:counter[15]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[15]~1_combout\ = (\L_pulselength2audio|COUNT:counter[15]~q\ & (!\L_pulselength2audio|COUNT:counter[14]~2\)) # (!\L_pulselength2audio|COUNT:counter[15]~q\ & ((\L_pulselength2audio|COUNT:counter[14]~2\) # (GND)))
-- \L_pulselength2audio|COUNT:counter[15]~2\ = CARRY((!\L_pulselength2audio|COUNT:counter[14]~2\) # (!\L_pulselength2audio|COUNT:counter[15]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[15]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[14]~2\,
	combout => \L_pulselength2audio|COUNT:counter[15]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[15]~2\);

-- Location: FF_X16_Y1_N15
\L_pulselength2audio|COUNT:counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[15]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[15]~q\);

-- Location: LCCOMB_X16_Y1_N16
\L_pulselength2audio|COUNT:counter[16]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[16]~1_combout\ = \L_pulselength2audio|COUNT:counter[15]~2\ $ (!\L_pulselength2audio|COUNT:counter[16]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \L_pulselength2audio|COUNT:counter[16]~q\,
	cin => \L_pulselength2audio|COUNT:counter[15]~2\,
	combout => \L_pulselength2audio|COUNT:counter[16]~1_combout\);

-- Location: FF_X16_Y1_N17
\L_pulselength2audio|COUNT:counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[16]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[16]~q\);

-- Location: LCCOMB_X16_Y1_N28
\L_pulselength2audio|Equal0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal0~0_combout\ = (!\L_pulselength2audio|COUNT:counter[13]~q\ & (!\L_pulselength2audio|COUNT:counter[16]~q\ & (!\L_pulselength2audio|COUNT:counter[15]~q\ & !\L_pulselength2audio|COUNT:counter[14]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[13]~q\,
	datab => \L_pulselength2audio|COUNT:counter[16]~q\,
	datac => \L_pulselength2audio|COUNT:counter[15]~q\,
	datad => \L_pulselength2audio|COUNT:counter[14]~q\,
	combout => \L_pulselength2audio|Equal0~0_combout\);

-- Location: LCCOMB_X16_Y2_N8
\L_pulselength2audio|Equal1~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~8_combout\ = (\L_pulselength2audio|Equal0~0_combout\ & (\L_pulselength2audio|COUNT:counter[8]~q\ $ (((!\L_key2pulselength|Mux4~4_combout\) # (!\L_key2pulselength|Mux4~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_key2pulselength|Mux4~0_combout\,
	datab => \L_pulselength2audio|COUNT:counter[8]~q\,
	datac => \L_key2pulselength|Mux4~4_combout\,
	datad => \L_pulselength2audio|Equal0~0_combout\,
	combout => \L_pulselength2audio|Equal1~8_combout\);

-- Location: LCCOMB_X15_Y2_N10
\L_pulselength2audio|Equal1~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~9_combout\ = (\key[0]~input_o\ & (((\L_key2pulselength|Mux7~4_combout\)))) # (!\key[0]~input_o\ & (\key[1]~input_o\ & (\L_key2pulselength|Mux3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \L_key2pulselength|Mux3~0_combout\,
	datac => \L_key2pulselength|Mux7~4_combout\,
	datad => \key[0]~input_o\,
	combout => \L_pulselength2audio|Equal1~9_combout\);

-- Location: LCCOMB_X15_Y2_N24
\L_pulselength2audio|Equal1~10\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~10_combout\ = (\key[3]~input_o\ & (((\key[0]~input_o\)))) # (!\key[3]~input_o\ & (\key[5]~input_o\ $ (((\key[4]~input_o\ & !\key[0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_pulselength2audio|Equal1~10_combout\);

-- Location: LCCOMB_X15_Y2_N30
\L_pulselength2audio|Equal1~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~11_combout\ = (\key[5]~input_o\ & (!\key[0]~input_o\ & ((\key[4]~input_o\) # (\key[3]~input_o\)))) # (!\key[5]~input_o\ & (\key[3]~input_o\ $ (((\key[4]~input_o\ & \key[0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_pulselength2audio|Equal1~11_combout\);

-- Location: LCCOMB_X15_Y2_N4
\L_pulselength2audio|Equal1~12\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~12_combout\ = (\key[6]~input_o\ & (\L_key2pulselength|Mux3~1_combout\ & (\L_pulselength2audio|Equal1~11_combout\ $ (!\L_pulselength2audio|Equal1~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \L_key2pulselength|Mux3~1_combout\,
	datac => \L_pulselength2audio|Equal1~11_combout\,
	datad => \L_pulselength2audio|Equal1~10_combout\,
	combout => \L_pulselength2audio|Equal1~12_combout\);

-- Location: LCCOMB_X15_Y2_N14
\L_pulselength2audio|Equal1~13\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~13_combout\ = \L_pulselength2audio|COUNT:counter[9]~q\ $ (((\L_pulselength2audio|Equal1~12_combout\) # ((\L_pulselength2audio|Equal1~9_combout\ & \L_pulselength2audio|Equal1~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|Equal1~9_combout\,
	datab => \L_pulselength2audio|Equal1~10_combout\,
	datac => \L_pulselength2audio|Equal1~12_combout\,
	datad => \L_pulselength2audio|COUNT:counter[9]~q\,
	combout => \L_pulselength2audio|Equal1~13_combout\);

-- Location: LCCOMB_X16_Y2_N4
\L_pulselength2audio|Equal1~15\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~15_combout\ = (!\L_pulselength2audio|Equal1~14_combout\ & (!\L_pulselength2audio|Equal1~19_combout\ & (\L_pulselength2audio|Equal1~8_combout\ & !\L_pulselength2audio|Equal1~13_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|Equal1~14_combout\,
	datab => \L_pulselength2audio|Equal1~19_combout\,
	datac => \L_pulselength2audio|Equal1~8_combout\,
	datad => \L_pulselength2audio|Equal1~13_combout\,
	combout => \L_pulselength2audio|Equal1~15_combout\);

-- Location: LCCOMB_X17_Y6_N4
\L_key2pulselength|Mux9~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux9~8_combout\ = (\key[2]~input_o\ & (!\key[6]~input_o\ & (\key[5]~input_o\ & !\key[7]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[2]~input_o\,
	datab => \key[6]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[7]~input_o\,
	combout => \L_key2pulselength|Mux9~8_combout\);

-- Location: LCCOMB_X17_Y6_N26
\L_key2pulselength|Mux9~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux9~2_combout\ = (\key[1]~input_o\ & (\key[4]~input_o\ $ (((!\key[0]~input_o\) # (!\key[3]~input_o\))))) # (!\key[1]~input_o\ & (\key[4]~input_o\ & (!\key[3]~input_o\ & !\key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010001000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[3]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux9~2_combout\);

-- Location: LCCOMB_X17_Y6_N8
\L_key2pulselength|Mux9~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux9~3_combout\ = (\L_key2pulselength|Mux9~2_combout\ & (\L_clock_generator|Equal1~0_combout\ & ((\key[0]~input_o\)))) # (!\L_key2pulselength|Mux9~2_combout\ & (((\L_key2pulselength|Mux9~8_combout\ & !\key[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_clock_generator|Equal1~0_combout\,
	datab => \L_key2pulselength|Mux9~8_combout\,
	datac => \L_key2pulselength|Mux9~2_combout\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux9~3_combout\);

-- Location: LCCOMB_X14_Y2_N2
\L_key2pulselength|Mux9~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux9~4_combout\ = (\key[0]~input_o\ & (!\key[1]~input_o\ & (\key[5]~input_o\ $ (\key[3]~input_o\)))) # (!\key[0]~input_o\ & ((\key[5]~input_o\) # (\key[1]~input_o\ $ (\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101100101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[1]~input_o\,
	datac => \key[0]~input_o\,
	datad => \key[3]~input_o\,
	combout => \L_key2pulselength|Mux9~4_combout\);

-- Location: LCCOMB_X14_Y2_N20
\L_key2pulselength|Mux9~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux9~5_combout\ = (\key[4]~input_o\ & (((!\key[0]~input_o\)))) # (!\key[4]~input_o\ & (\L_key2pulselength|Mux9~4_combout\ & ((\L_key2pulselength|Mux3~0_combout\) # (!\key[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \L_key2pulselength|Mux3~0_combout\,
	datac => \key[0]~input_o\,
	datad => \L_key2pulselength|Mux9~4_combout\,
	combout => \L_key2pulselength|Mux9~5_combout\);

-- Location: LCCOMB_X14_Y2_N26
\L_key2pulselength|Mux9~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux9~6_combout\ = (\key[0]~input_o\ & (((\L_key2pulselength|Mux9~5_combout\)))) # (!\key[0]~input_o\ & (\key[6]~input_o\ & (\L_key2pulselength|Mux4~0_combout\ & !\L_key2pulselength|Mux9~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[0]~input_o\,
	datab => \key[6]~input_o\,
	datac => \L_key2pulselength|Mux4~0_combout\,
	datad => \L_key2pulselength|Mux9~5_combout\,
	combout => \L_key2pulselength|Mux9~6_combout\);

-- Location: LCCOMB_X14_Y2_N22
\L_key2pulselength|Mux8~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux8~1_combout\ = (\L_key2pulselength|Mux8~0_combout\ & \L_clock_generator|Equal1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \L_key2pulselength|Mux8~0_combout\,
	datad => \L_clock_generator|Equal1~0_combout\,
	combout => \L_key2pulselength|Mux8~1_combout\);

-- Location: LCCOMB_X17_Y3_N22
\L_key2pulselength|Mux10~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux10~1_combout\ = (\key[6]~input_o\ & (\key[0]~input_o\ $ (((!\key[5]~input_o\ & !\key[4]~input_o\))))) # (!\key[6]~input_o\ & (\key[5]~input_o\ & (\key[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux10~1_combout\);

-- Location: LCCOMB_X17_Y3_N20
\L_key2pulselength|Mux10~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux10~0_combout\ = (\key[0]~input_o\ & (!\key[6]~input_o\ & (\key[5]~input_o\ $ (\key[4]~input_o\)))) # (!\key[0]~input_o\ & (!\key[4]~input_o\ & (\key[5]~input_o\ $ (\key[6]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[5]~input_o\,
	datab => \key[4]~input_o\,
	datac => \key[6]~input_o\,
	datad => \key[0]~input_o\,
	combout => \L_key2pulselength|Mux10~0_combout\);

-- Location: LCCOMB_X17_Y3_N10
\L_key2pulselength|Mux10~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux10~2_combout\ = (\key[3]~input_o\ & (\L_key2pulselength|Mux10~1_combout\ & (\key[1]~input_o\ $ (!\L_key2pulselength|Mux10~0_combout\)))) # (!\key[3]~input_o\ & ((\L_key2pulselength|Mux10~1_combout\ & (\key[1]~input_o\ & 
-- !\L_key2pulselength|Mux10~0_combout\)) # (!\L_key2pulselength|Mux10~1_combout\ & ((\L_key2pulselength|Mux10~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[3]~input_o\,
	datac => \L_key2pulselength|Mux10~1_combout\,
	datad => \L_key2pulselength|Mux10~0_combout\,
	combout => \L_key2pulselength|Mux10~2_combout\);

-- Location: LCCOMB_X17_Y3_N4
\L_key2pulselength|Mux10~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_key2pulselength|Mux10~3_combout\ = (\key[0]~input_o\ & (!\key[1]~input_o\ & (\key[3]~input_o\ $ (\L_key2pulselength|Mux10~2_combout\)))) # (!\key[0]~input_o\ & (\L_key2pulselength|Mux10~2_combout\ & ((\key[1]~input_o\) # (\key[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[1]~input_o\,
	datab => \key[0]~input_o\,
	datac => \key[3]~input_o\,
	datad => \L_key2pulselength|Mux10~2_combout\,
	combout => \L_key2pulselength|Mux10~3_combout\);

-- Location: LCCOMB_X16_Y2_N0
\L_pulselength2audio|Equal1~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~5_combout\ = \L_pulselength2audio|COUNT:counter[2]~q\ $ (((\L_key2pulselength|Mux8~1_combout\) # ((\L_key2pulselength|Mux4~0_combout\ & \L_key2pulselength|Mux10~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[2]~q\,
	datab => \L_key2pulselength|Mux4~0_combout\,
	datac => \L_key2pulselength|Mux8~1_combout\,
	datad => \L_key2pulselength|Mux10~3_combout\,
	combout => \L_pulselength2audio|Equal1~5_combout\);

-- Location: LCCOMB_X16_Y2_N10
\L_pulselength2audio|Equal1~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~6_combout\ = (!\L_pulselength2audio|Equal1~5_combout\ & (\L_pulselength2audio|COUNT:counter[3]~q\ $ (((!\L_key2pulselength|Mux9~3_combout\ & !\L_key2pulselength|Mux9~6_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[3]~q\,
	datab => \L_key2pulselength|Mux9~3_combout\,
	datac => \L_key2pulselength|Mux9~6_combout\,
	datad => \L_pulselength2audio|Equal1~5_combout\,
	combout => \L_pulselength2audio|Equal1~6_combout\);

-- Location: LCCOMB_X16_Y2_N12
\L_pulselength2audio|Equal1~17\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~17_combout\ = (\L_pulselength2audio|Equal1~7_combout\ & (!\L_pulselength2audio|Equal1~16_combout\ & (\L_pulselength2audio|Equal1~15_combout\ & \L_pulselength2audio|Equal1~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|Equal1~7_combout\,
	datab => \L_pulselength2audio|Equal1~16_combout\,
	datac => \L_pulselength2audio|Equal1~15_combout\,
	datad => \L_pulselength2audio|Equal1~6_combout\,
	combout => \L_pulselength2audio|Equal1~17_combout\);

-- Location: LCCOMB_X16_Y2_N2
\L_pulselength2audio|Equal1~18\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal1~18_combout\ = (\L_pulselength2audio|Equal1~4_combout\ & (\L_pulselength2audio|Equal1~17_combout\ & (\L_pulselength2audio|COUNT:counter[7]~q\ $ (!\L_key2pulselength|Mux5~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[7]~q\,
	datab => \L_key2pulselength|Mux5~2_combout\,
	datac => \L_pulselength2audio|Equal1~4_combout\,
	datad => \L_pulselength2audio|Equal1~17_combout\,
	combout => \L_pulselength2audio|Equal1~18_combout\);

-- Location: FF_X16_Y2_N17
\L_pulselength2audio|COUNT:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[0]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[0]~q\);

-- Location: LCCOMB_X16_Y2_N18
\L_pulselength2audio|COUNT:counter[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[1]~1_combout\ = (\L_pulselength2audio|COUNT:counter[1]~q\ & (!\L_pulselength2audio|COUNT:counter[0]~2\)) # (!\L_pulselength2audio|COUNT:counter[1]~q\ & ((\L_pulselength2audio|COUNT:counter[0]~2\) # (GND)))
-- \L_pulselength2audio|COUNT:counter[1]~2\ = CARRY((!\L_pulselength2audio|COUNT:counter[0]~2\) # (!\L_pulselength2audio|COUNT:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[1]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[0]~2\,
	combout => \L_pulselength2audio|COUNT:counter[1]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[1]~2\);

-- Location: FF_X16_Y2_N19
\L_pulselength2audio|COUNT:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[1]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[1]~q\);

-- Location: LCCOMB_X16_Y2_N20
\L_pulselength2audio|COUNT:counter[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[2]~1_combout\ = (\L_pulselength2audio|COUNT:counter[2]~q\ & (\L_pulselength2audio|COUNT:counter[1]~2\ $ (GND))) # (!\L_pulselength2audio|COUNT:counter[2]~q\ & (!\L_pulselength2audio|COUNT:counter[1]~2\ & VCC))
-- \L_pulselength2audio|COUNT:counter[2]~2\ = CARRY((\L_pulselength2audio|COUNT:counter[2]~q\ & !\L_pulselength2audio|COUNT:counter[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[2]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[1]~2\,
	combout => \L_pulselength2audio|COUNT:counter[2]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[2]~2\);

-- Location: FF_X16_Y2_N21
\L_pulselength2audio|COUNT:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[2]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[2]~q\);

-- Location: LCCOMB_X16_Y2_N22
\L_pulselength2audio|COUNT:counter[3]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[3]~1_combout\ = (\L_pulselength2audio|COUNT:counter[3]~q\ & (!\L_pulselength2audio|COUNT:counter[2]~2\)) # (!\L_pulselength2audio|COUNT:counter[3]~q\ & ((\L_pulselength2audio|COUNT:counter[2]~2\) # (GND)))
-- \L_pulselength2audio|COUNT:counter[3]~2\ = CARRY((!\L_pulselength2audio|COUNT:counter[2]~2\) # (!\L_pulselength2audio|COUNT:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[3]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[2]~2\,
	combout => \L_pulselength2audio|COUNT:counter[3]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[3]~2\);

-- Location: FF_X16_Y2_N23
\L_pulselength2audio|COUNT:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[3]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[3]~q\);

-- Location: LCCOMB_X16_Y2_N24
\L_pulselength2audio|COUNT:counter[4]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[4]~1_combout\ = (\L_pulselength2audio|COUNT:counter[4]~q\ & (\L_pulselength2audio|COUNT:counter[3]~2\ $ (GND))) # (!\L_pulselength2audio|COUNT:counter[4]~q\ & (!\L_pulselength2audio|COUNT:counter[3]~2\ & VCC))
-- \L_pulselength2audio|COUNT:counter[4]~2\ = CARRY((\L_pulselength2audio|COUNT:counter[4]~q\ & !\L_pulselength2audio|COUNT:counter[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[4]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[3]~2\,
	combout => \L_pulselength2audio|COUNT:counter[4]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[4]~2\);

-- Location: FF_X16_Y2_N25
\L_pulselength2audio|COUNT:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[4]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[4]~q\);

-- Location: LCCOMB_X16_Y2_N26
\L_pulselength2audio|COUNT:counter[5]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[5]~1_combout\ = (\L_pulselength2audio|COUNT:counter[5]~q\ & (!\L_pulselength2audio|COUNT:counter[4]~2\)) # (!\L_pulselength2audio|COUNT:counter[5]~q\ & ((\L_pulselength2audio|COUNT:counter[4]~2\) # (GND)))
-- \L_pulselength2audio|COUNT:counter[5]~2\ = CARRY((!\L_pulselength2audio|COUNT:counter[4]~2\) # (!\L_pulselength2audio|COUNT:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[5]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[4]~2\,
	combout => \L_pulselength2audio|COUNT:counter[5]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[5]~2\);

-- Location: FF_X16_Y2_N27
\L_pulselength2audio|COUNT:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[5]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[5]~q\);

-- Location: LCCOMB_X16_Y2_N28
\L_pulselength2audio|COUNT:counter[6]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[6]~1_combout\ = (\L_pulselength2audio|COUNT:counter[6]~q\ & (\L_pulselength2audio|COUNT:counter[5]~2\ $ (GND))) # (!\L_pulselength2audio|COUNT:counter[6]~q\ & (!\L_pulselength2audio|COUNT:counter[5]~2\ & VCC))
-- \L_pulselength2audio|COUNT:counter[6]~2\ = CARRY((\L_pulselength2audio|COUNT:counter[6]~q\ & !\L_pulselength2audio|COUNT:counter[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[6]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[5]~2\,
	combout => \L_pulselength2audio|COUNT:counter[6]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[6]~2\);

-- Location: FF_X16_Y2_N29
\L_pulselength2audio|COUNT:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[6]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[6]~q\);

-- Location: LCCOMB_X16_Y2_N30
\L_pulselength2audio|COUNT:counter[7]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[7]~1_combout\ = (\L_pulselength2audio|COUNT:counter[7]~q\ & (!\L_pulselength2audio|COUNT:counter[6]~2\)) # (!\L_pulselength2audio|COUNT:counter[7]~q\ & ((\L_pulselength2audio|COUNT:counter[6]~2\) # (GND)))
-- \L_pulselength2audio|COUNT:counter[7]~2\ = CARRY((!\L_pulselength2audio|COUNT:counter[6]~2\) # (!\L_pulselength2audio|COUNT:counter[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[7]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[6]~2\,
	combout => \L_pulselength2audio|COUNT:counter[7]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[7]~2\);

-- Location: FF_X16_Y2_N31
\L_pulselength2audio|COUNT:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[7]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[7]~q\);

-- Location: LCCOMB_X16_Y1_N0
\L_pulselength2audio|COUNT:counter[8]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[8]~1_combout\ = (\L_pulselength2audio|COUNT:counter[8]~q\ & (\L_pulselength2audio|COUNT:counter[7]~2\ $ (GND))) # (!\L_pulselength2audio|COUNT:counter[8]~q\ & (!\L_pulselength2audio|COUNT:counter[7]~2\ & VCC))
-- \L_pulselength2audio|COUNT:counter[8]~2\ = CARRY((\L_pulselength2audio|COUNT:counter[8]~q\ & !\L_pulselength2audio|COUNT:counter[7]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[8]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[7]~2\,
	combout => \L_pulselength2audio|COUNT:counter[8]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[8]~2\);

-- Location: FF_X16_Y1_N1
\L_pulselength2audio|COUNT:counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[8]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[8]~q\);

-- Location: LCCOMB_X16_Y1_N2
\L_pulselength2audio|COUNT:counter[9]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[9]~1_combout\ = (\L_pulselength2audio|COUNT:counter[9]~q\ & (!\L_pulselength2audio|COUNT:counter[8]~2\)) # (!\L_pulselength2audio|COUNT:counter[9]~q\ & ((\L_pulselength2audio|COUNT:counter[8]~2\) # (GND)))
-- \L_pulselength2audio|COUNT:counter[9]~2\ = CARRY((!\L_pulselength2audio|COUNT:counter[8]~2\) # (!\L_pulselength2audio|COUNT:counter[9]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[9]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[8]~2\,
	combout => \L_pulselength2audio|COUNT:counter[9]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[9]~2\);

-- Location: FF_X16_Y1_N3
\L_pulselength2audio|COUNT:counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[9]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[9]~q\);

-- Location: LCCOMB_X16_Y1_N4
\L_pulselength2audio|COUNT:counter[10]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[10]~1_combout\ = (\L_pulselength2audio|COUNT:counter[10]~q\ & (\L_pulselength2audio|COUNT:counter[9]~2\ $ (GND))) # (!\L_pulselength2audio|COUNT:counter[10]~q\ & (!\L_pulselength2audio|COUNT:counter[9]~2\ & VCC))
-- \L_pulselength2audio|COUNT:counter[10]~2\ = CARRY((\L_pulselength2audio|COUNT:counter[10]~q\ & !\L_pulselength2audio|COUNT:counter[9]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[10]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[9]~2\,
	combout => \L_pulselength2audio|COUNT:counter[10]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[10]~2\);

-- Location: FF_X16_Y1_N5
\L_pulselength2audio|COUNT:counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[10]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[10]~q\);

-- Location: LCCOMB_X16_Y1_N6
\L_pulselength2audio|COUNT:counter[11]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|COUNT:counter[11]~1_combout\ = (\L_pulselength2audio|COUNT:counter[11]~q\ & (!\L_pulselength2audio|COUNT:counter[10]~2\)) # (!\L_pulselength2audio|COUNT:counter[11]~q\ & ((\L_pulselength2audio|COUNT:counter[10]~2\) # (GND)))
-- \L_pulselength2audio|COUNT:counter[11]~2\ = CARRY((!\L_pulselength2audio|COUNT:counter[10]~2\) # (!\L_pulselength2audio|COUNT:counter[11]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[11]~q\,
	datad => VCC,
	cin => \L_pulselength2audio|COUNT:counter[10]~2\,
	combout => \L_pulselength2audio|COUNT:counter[11]~1_combout\,
	cout => \L_pulselength2audio|COUNT:counter[11]~2\);

-- Location: FF_X16_Y1_N7
\L_pulselength2audio|COUNT:counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[11]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[11]~q\);

-- Location: FF_X16_Y1_N9
\L_pulselength2audio|COUNT:counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|COUNT:counter[12]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \L_pulselength2audio|Equal1~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|COUNT:counter[12]~q\);

-- Location: LCCOMB_X16_Y1_N22
\L_pulselength2audio|Equal0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal0~3_combout\ = (!\L_pulselength2audio|COUNT:counter[11]~q\ & (!\L_pulselength2audio|COUNT:counter[9]~q\ & (!\L_pulselength2audio|COUNT:counter[10]~q\ & !\L_pulselength2audio|COUNT:counter[8]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[11]~q\,
	datab => \L_pulselength2audio|COUNT:counter[9]~q\,
	datac => \L_pulselength2audio|COUNT:counter[10]~q\,
	datad => \L_pulselength2audio|COUNT:counter[8]~q\,
	combout => \L_pulselength2audio|Equal0~3_combout\);

-- Location: LCCOMB_X16_Y1_N26
\L_pulselength2audio|Equal0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal0~1_combout\ = (!\L_pulselength2audio|COUNT:counter[2]~q\ & (!\L_pulselength2audio|COUNT:counter[3]~q\ & (!\L_pulselength2audio|COUNT:counter[1]~q\ & !\L_pulselength2audio|COUNT:counter[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[2]~q\,
	datab => \L_pulselength2audio|COUNT:counter[3]~q\,
	datac => \L_pulselength2audio|COUNT:counter[1]~q\,
	datad => \L_pulselength2audio|COUNT:counter[0]~q\,
	combout => \L_pulselength2audio|Equal0~1_combout\);

-- Location: LCCOMB_X16_Y1_N20
\L_pulselength2audio|Equal0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal0~2_combout\ = (!\L_pulselength2audio|COUNT:counter[5]~q\ & (!\L_pulselength2audio|COUNT:counter[7]~q\ & (!\L_pulselength2audio|COUNT:counter[6]~q\ & !\L_pulselength2audio|COUNT:counter[4]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|COUNT:counter[5]~q\,
	datab => \L_pulselength2audio|COUNT:counter[7]~q\,
	datac => \L_pulselength2audio|COUNT:counter[6]~q\,
	datad => \L_pulselength2audio|COUNT:counter[4]~q\,
	combout => \L_pulselength2audio|Equal0~2_combout\);

-- Location: LCCOMB_X16_Y1_N24
\L_pulselength2audio|Equal0~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|Equal0~4_combout\ = (\L_pulselength2audio|Equal0~3_combout\ & (\L_pulselength2audio|Equal0~0_combout\ & (\L_pulselength2audio|Equal0~1_combout\ & \L_pulselength2audio|Equal0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \L_pulselength2audio|Equal0~3_combout\,
	datab => \L_pulselength2audio|Equal0~0_combout\,
	datac => \L_pulselength2audio|Equal0~1_combout\,
	datad => \L_pulselength2audio|Equal0~2_combout\,
	combout => \L_pulselength2audio|Equal0~4_combout\);

-- Location: LCCOMB_X16_Y1_N30
\L_pulselength2audio|audior~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \L_pulselength2audio|audior~0_combout\ = \L_pulselength2audio|audior~q\ $ (((!\L_pulselength2audio|COUNT:counter[12]~q\ & \L_pulselength2audio|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \L_pulselength2audio|COUNT:counter[12]~q\,
	datac => \L_pulselength2audio|audior~q\,
	datad => \L_pulselength2audio|Equal0~4_combout\,
	combout => \L_pulselength2audio|audior~0_combout\);

-- Location: FF_X16_Y1_N31
\L_pulselength2audio|audior\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \L_clock_generator|Selector9~clkctrl_outclk\,
	d => \L_pulselength2audio|audior~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \L_pulselength2audio|audior~q\);

-- Location: UNVM_X0_Y8_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

ww_audiol <= \audiol~output_o\;

ww_audior <= \audior~output_o\;
END structure;


