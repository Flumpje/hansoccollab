-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "05/19/2020 21:00:05"

-- 
-- Device: Altera 5CGXFC7C7F23C8 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	constantkey IS
    PORT (
	reset : IN std_logic;
	clk : IN std_logic;
	scancode : IN std_logic_vector(7 DOWNTO 0);
	byte_read : IN std_logic;
	key : BUFFER std_logic_vector(7 DOWNTO 0);
	dig2 : BUFFER std_logic_vector(7 DOWNTO 0);
	dig3 : BUFFER std_logic_vector(7 DOWNTO 0)
	);
END constantkey;

-- Design Ports Information
-- key[0]	=>  Location: PIN_Y20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[1]	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[2]	=>  Location: PIN_U15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[3]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[4]	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[5]	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[6]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[7]	=>  Location: PIN_V21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[0]	=>  Location: PIN_Y22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[1]	=>  Location: PIN_AB18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[2]	=>  Location: PIN_AA17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[3]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[4]	=>  Location: PIN_R14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[5]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[6]	=>  Location: PIN_Y17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig2[7]	=>  Location: PIN_V19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[0]	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[1]	=>  Location: PIN_V15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[2]	=>  Location: PIN_T14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[3]	=>  Location: PIN_AB22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[4]	=>  Location: PIN_P14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[5]	=>  Location: PIN_Y15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[6]	=>  Location: PIN_AB20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig3[7]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[0]	=>  Location: PIN_W19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[1]	=>  Location: PIN_AA20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[2]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[3]	=>  Location: PIN_AA18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[4]	=>  Location: PIN_V20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[5]	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[6]	=>  Location: PIN_AB21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[7]	=>  Location: PIN_AA19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- byte_read	=>  Location: PIN_V14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF constantkey IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_scancode : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_byte_read : std_logic;
SIGNAL ww_key : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_dig2 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_dig3 : std_logic_vector(7 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputCLKENA0_outclk\ : std_logic;
SIGNAL \scancode[7]~input_o\ : std_logic;
SIGNAL \scancode[6]~input_o\ : std_logic;
SIGNAL \byte_read~input_o\ : std_logic;
SIGNAL \scancode[0]~input_o\ : std_logic;
SIGNAL \scancode[1]~input_o\ : std_logic;
SIGNAL \scancode[3]~input_o\ : std_logic;
SIGNAL \scancode[4]~input_o\ : std_logic;
SIGNAL \scancode[2]~input_o\ : std_logic;
SIGNAL \scancode[5]~input_o\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \current_state.state_key_released~0_combout\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \current_state.state_key_released~q\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \current_state.state_key_reminder~q\ : std_logic;
SIGNAL \current_state.state_key_reminder_2~0_combout\ : std_logic;
SIGNAL \current_state.state_key_reminder_2~q\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \current_state.state_no_key~q\ : std_logic;
SIGNAL \Selector1~0_combout\ : std_logic;
SIGNAL \current_state.state_key_pressed~q\ : std_logic;
SIGNAL \byte_current~0_combout\ : std_logic;
SIGNAL \byte_current~1_combout\ : std_logic;
SIGNAL \byte_current~2_combout\ : std_logic;
SIGNAL \byte_current~3_combout\ : std_logic;
SIGNAL \Selector8~0_combout\ : std_logic;
SIGNAL \Selector7~0_combout\ : std_logic;
SIGNAL \Selector6~0_combout\ : std_logic;
SIGNAL \Selector5~0_combout\ : std_logic;
SIGNAL \ALT_INV_scancode[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_scancode[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_scancode[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_scancode[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_scancode[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_scancode[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_current_state.state_key_reminder_2~q\ : std_logic;
SIGNAL \ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \ALT_INV_current_state.state_no_key~q\ : std_logic;
SIGNAL \ALT_INV_current_state.state_key_reminder~q\ : std_logic;
SIGNAL \ALT_INV_current_state.state_key_released~q\ : std_logic;
SIGNAL \ALT_INV_current_state.state_key_pressed~q\ : std_logic;
SIGNAL \ALT_INV_byte_read~input_o\ : std_logic;
SIGNAL \ALT_INV_scancode[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_scancode[6]~input_o\ : std_logic;

BEGIN

ww_reset <= reset;
ww_clk <= clk;
ww_scancode <= scancode;
ww_byte_read <= byte_read;
key <= ww_key;
dig2 <= ww_dig2;
dig3 <= ww_dig3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_scancode[5]~input_o\ <= NOT \scancode[5]~input_o\;
\ALT_INV_scancode[4]~input_o\ <= NOT \scancode[4]~input_o\;
\ALT_INV_scancode[3]~input_o\ <= NOT \scancode[3]~input_o\;
\ALT_INV_scancode[2]~input_o\ <= NOT \scancode[2]~input_o\;
\ALT_INV_scancode[1]~input_o\ <= NOT \scancode[1]~input_o\;
\ALT_INV_scancode[0]~input_o\ <= NOT \scancode[0]~input_o\;
\ALT_INV_current_state.state_key_reminder_2~q\ <= NOT \current_state.state_key_reminder_2~q\;
\ALT_INV_Equal0~0_combout\ <= NOT \Equal0~0_combout\;
\ALT_INV_current_state.state_no_key~q\ <= NOT \current_state.state_no_key~q\;
\ALT_INV_current_state.state_key_reminder~q\ <= NOT \current_state.state_key_reminder~q\;
\ALT_INV_current_state.state_key_released~q\ <= NOT \current_state.state_key_released~q\;
\ALT_INV_current_state.state_key_pressed~q\ <= NOT \current_state.state_key_pressed~q\;
\ALT_INV_byte_read~input_o\ <= NOT \byte_read~input_o\;
\ALT_INV_scancode[7]~input_o\ <= NOT \scancode[7]~input_o\;
\ALT_INV_scancode[6]~input_o\ <= NOT \scancode[6]~input_o\;

-- Location: IOOBUF_X66_Y0_N59
\key[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~0_combout\,
	devoe => ww_devoe,
	o => ww_key(0));

-- Location: IOOBUF_X56_Y0_N53
\key[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~1_combout\,
	devoe => ww_devoe,
	o => ww_key(1));

-- Location: IOOBUF_X60_Y0_N2
\key[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~2_combout\,
	devoe => ww_devoe,
	o => ww_key(2));

-- Location: IOOBUF_X64_Y0_N19
\key[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~3_combout\,
	devoe => ww_devoe,
	o => ww_key(3));

-- Location: IOOBUF_X68_Y0_N53
\key[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector8~0_combout\,
	devoe => ww_devoe,
	o => ww_key(4));

-- Location: IOOBUF_X54_Y0_N53
\key[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector7~0_combout\,
	devoe => ww_devoe,
	o => ww_key(5));

-- Location: IOOBUF_X58_Y0_N59
\key[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector6~0_combout\,
	devoe => ww_devoe,
	o => ww_key(6));

-- Location: IOOBUF_X70_Y0_N36
\key[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector5~0_combout\,
	devoe => ww_devoe,
	o => ww_key(7));

-- Location: IOOBUF_X66_Y0_N93
\dig2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~0_combout\,
	devoe => ww_devoe,
	o => ww_dig2(0));

-- Location: IOOBUF_X56_Y0_N36
\dig2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~1_combout\,
	devoe => ww_devoe,
	o => ww_dig2(1));

-- Location: IOOBUF_X60_Y0_N53
\dig2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~2_combout\,
	devoe => ww_devoe,
	o => ww_dig2(2));

-- Location: IOOBUF_X64_Y0_N36
\dig2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~3_combout\,
	devoe => ww_devoe,
	o => ww_dig2(3));

-- Location: IOOBUF_X68_Y0_N2
\dig2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector8~0_combout\,
	devoe => ww_devoe,
	o => ww_dig2(4));

-- Location: IOOBUF_X54_Y0_N36
\dig2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector7~0_combout\,
	devoe => ww_devoe,
	o => ww_dig2(5));

-- Location: IOOBUF_X58_Y0_N42
\dig2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector6~0_combout\,
	devoe => ww_devoe,
	o => ww_dig2(6));

-- Location: IOOBUF_X70_Y0_N19
\dig2[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector5~0_combout\,
	devoe => ww_devoe,
	o => ww_dig2(7));

-- Location: IOOBUF_X66_Y0_N42
\dig3[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~0_combout\,
	devoe => ww_devoe,
	o => ww_dig3(0));

-- Location: IOOBUF_X56_Y0_N2
\dig3[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~1_combout\,
	devoe => ww_devoe,
	o => ww_dig3(1));

-- Location: IOOBUF_X60_Y0_N19
\dig3[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~2_combout\,
	devoe => ww_devoe,
	o => ww_dig3(2));

-- Location: IOOBUF_X64_Y0_N53
\dig3[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_current~3_combout\,
	devoe => ww_devoe,
	o => ww_dig3(3));

-- Location: IOOBUF_X68_Y0_N19
\dig3[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector8~0_combout\,
	devoe => ww_devoe,
	o => ww_dig3(4));

-- Location: IOOBUF_X54_Y0_N2
\dig3[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector7~0_combout\,
	devoe => ww_devoe,
	o => ww_dig3(5));

-- Location: IOOBUF_X58_Y0_N93
\dig3[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector6~0_combout\,
	devoe => ww_devoe,
	o => ww_dig3(6));

-- Location: IOOBUF_X70_Y0_N2
\dig3[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector5~0_combout\,
	devoe => ww_devoe,
	o => ww_dig3(7));

-- Location: IOIBUF_X89_Y35_N61
\clk~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G10
\clk~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \clk~input_o\,
	outclk => \clk~inputCLKENA0_outclk\);

-- Location: IOIBUF_X62_Y0_N52
\scancode[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_scancode(7),
	o => \scancode[7]~input_o\);

-- Location: IOIBUF_X58_Y0_N75
\scancode[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_scancode(6),
	o => \scancode[6]~input_o\);

-- Location: IOIBUF_X56_Y0_N18
\byte_read~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_byte_read,
	o => \byte_read~input_o\);

-- Location: IOIBUF_X62_Y0_N1
\scancode[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_scancode(0),
	o => \scancode[0]~input_o\);

-- Location: IOIBUF_X62_Y0_N35
\scancode[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_scancode(1),
	o => \scancode[1]~input_o\);

-- Location: IOIBUF_X60_Y0_N35
\scancode[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_scancode(3),
	o => \scancode[3]~input_o\);

-- Location: IOIBUF_X62_Y0_N18
\scancode[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_scancode(4),
	o => \scancode[4]~input_o\);

-- Location: IOIBUF_X64_Y0_N1
\scancode[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_scancode(2),
	o => \scancode[2]~input_o\);

-- Location: IOIBUF_X66_Y0_N75
\scancode[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_scancode(5),
	o => \scancode[5]~input_o\);

-- Location: LABCELL_X62_Y1_N12
\Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = ( !\scancode[2]~input_o\ & ( \scancode[5]~input_o\ & ( (!\scancode[0]~input_o\ & (!\scancode[1]~input_o\ & (!\scancode[3]~input_o\ & \scancode[4]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000100000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_scancode[0]~input_o\,
	datab => \ALT_INV_scancode[1]~input_o\,
	datac => \ALT_INV_scancode[3]~input_o\,
	datad => \ALT_INV_scancode[4]~input_o\,
	datae => \ALT_INV_scancode[2]~input_o\,
	dataf => \ALT_INV_scancode[5]~input_o\,
	combout => \Equal0~0_combout\);

-- Location: LABCELL_X62_Y1_N54
\current_state.state_key_released~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \current_state.state_key_released~0_combout\ = ( \current_state.state_key_released~q\ & ( \current_state.state_key_pressed~q\ ) ) # ( !\current_state.state_key_released~q\ & ( \current_state.state_key_pressed~q\ & ( (\scancode[7]~input_o\ & 
-- (\scancode[6]~input_o\ & (\byte_read~input_o\ & \Equal0~0_combout\))) ) ) ) # ( \current_state.state_key_released~q\ & ( !\current_state.state_key_pressed~q\ & ( (!\scancode[7]~input_o\) # ((!\scancode[6]~input_o\) # ((!\byte_read~input_o\) # 
-- (!\Equal0~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111000000000000000011111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_scancode[7]~input_o\,
	datab => \ALT_INV_scancode[6]~input_o\,
	datac => \ALT_INV_byte_read~input_o\,
	datad => \ALT_INV_Equal0~0_combout\,
	datae => \ALT_INV_current_state.state_key_released~q\,
	dataf => \ALT_INV_current_state.state_key_pressed~q\,
	combout => \current_state.state_key_released~0_combout\);

-- Location: IOIBUF_X68_Y0_N35
\reset~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: FF_X62_Y1_N56
\current_state.state_key_released\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputCLKENA0_outclk\,
	d => \current_state.state_key_released~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \current_state.state_key_released~q\);

-- Location: LABCELL_X62_Y1_N42
\Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = ( \current_state.state_key_reminder~q\ & ( \current_state.state_key_released~q\ & ( (!\scancode[7]~input_o\) # ((!\scancode[6]~input_o\) # ((!\Equal0~0_combout\) # (\byte_read~input_o\))) ) ) ) # ( 
-- !\current_state.state_key_reminder~q\ & ( \current_state.state_key_released~q\ & ( (\scancode[7]~input_o\ & (\scancode[6]~input_o\ & (\byte_read~input_o\ & \Equal0~0_combout\))) ) ) ) # ( \current_state.state_key_reminder~q\ & ( 
-- !\current_state.state_key_released~q\ & ( (!\scancode[7]~input_o\) # ((!\scancode[6]~input_o\) # ((!\Equal0~0_combout\) # (\byte_read~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111110111100000000000000011111111111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_scancode[7]~input_o\,
	datab => \ALT_INV_scancode[6]~input_o\,
	datac => \ALT_INV_byte_read~input_o\,
	datad => \ALT_INV_Equal0~0_combout\,
	datae => \ALT_INV_current_state.state_key_reminder~q\,
	dataf => \ALT_INV_current_state.state_key_released~q\,
	combout => \Selector3~0_combout\);

-- Location: FF_X62_Y1_N44
\current_state.state_key_reminder\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputCLKENA0_outclk\,
	d => \Selector3~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \current_state.state_key_reminder~q\);

-- Location: LABCELL_X62_Y1_N24
\current_state.state_key_reminder_2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \current_state.state_key_reminder_2~0_combout\ = ( \current_state.state_key_reminder_2~q\ & ( \current_state.state_key_reminder~q\ & ( ((\scancode[7]~input_o\ & (\scancode[6]~input_o\ & \Equal0~0_combout\))) # (\byte_read~input_o\) ) ) ) # ( 
-- !\current_state.state_key_reminder_2~q\ & ( \current_state.state_key_reminder~q\ & ( (\scancode[7]~input_o\ & (\scancode[6]~input_o\ & (!\byte_read~input_o\ & \Equal0~0_combout\))) ) ) ) # ( \current_state.state_key_reminder_2~q\ & ( 
-- !\current_state.state_key_reminder~q\ & ( \byte_read~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000100000000111100011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_scancode[7]~input_o\,
	datab => \ALT_INV_scancode[6]~input_o\,
	datac => \ALT_INV_byte_read~input_o\,
	datad => \ALT_INV_Equal0~0_combout\,
	datae => \ALT_INV_current_state.state_key_reminder_2~q\,
	dataf => \ALT_INV_current_state.state_key_reminder~q\,
	combout => \current_state.state_key_reminder_2~0_combout\);

-- Location: FF_X62_Y1_N25
\current_state.state_key_reminder_2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputCLKENA0_outclk\,
	d => \current_state.state_key_reminder_2~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \current_state.state_key_reminder_2~q\);

-- Location: LABCELL_X62_Y1_N30
\Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = ( \current_state.state_key_reminder_2~q\ & ( \byte_read~input_o\ ) ) # ( !\current_state.state_key_reminder_2~q\ & ( (\current_state.state_no_key~q\) # (\byte_read~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111000011111111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_byte_read~input_o\,
	datad => \ALT_INV_current_state.state_no_key~q\,
	dataf => \ALT_INV_current_state.state_key_reminder_2~q\,
	combout => \Selector0~0_combout\);

-- Location: FF_X62_Y1_N31
\current_state.state_no_key\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputCLKENA0_outclk\,
	d => \Selector0~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \current_state.state_no_key~q\);

-- Location: LABCELL_X62_Y1_N6
\Selector1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector1~0_combout\ = ( \current_state.state_key_pressed~q\ & ( \current_state.state_no_key~q\ & ( (!\scancode[7]~input_o\) # ((!\scancode[6]~input_o\) # ((!\byte_read~input_o\) # (!\Equal0~0_combout\))) ) ) ) # ( \current_state.state_key_pressed~q\ & ( 
-- !\current_state.state_no_key~q\ ) ) # ( !\current_state.state_key_pressed~q\ & ( !\current_state.state_no_key~q\ & ( \byte_read~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111111111111111111100000000000000001111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_scancode[7]~input_o\,
	datab => \ALT_INV_scancode[6]~input_o\,
	datac => \ALT_INV_byte_read~input_o\,
	datad => \ALT_INV_Equal0~0_combout\,
	datae => \ALT_INV_current_state.state_key_pressed~q\,
	dataf => \ALT_INV_current_state.state_no_key~q\,
	combout => \Selector1~0_combout\);

-- Location: FF_X62_Y1_N8
\current_state.state_key_pressed\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputCLKENA0_outclk\,
	d => \Selector1~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \current_state.state_key_pressed~q\);

-- Location: LABCELL_X62_Y1_N0
\byte_current~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \byte_current~0_combout\ = ( \scancode[0]~input_o\ & ( \current_state.state_key_pressed~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_current_state.state_key_pressed~q\,
	dataf => \ALT_INV_scancode[0]~input_o\,
	combout => \byte_current~0_combout\);

-- Location: LABCELL_X62_Y1_N18
\byte_current~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \byte_current~1_combout\ = (\current_state.state_key_pressed~q\ & \scancode[1]~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001100000000001100110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_current_state.state_key_pressed~q\,
	datad => \ALT_INV_scancode[1]~input_o\,
	combout => \byte_current~1_combout\);

-- Location: LABCELL_X62_Y1_N51
\byte_current~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \byte_current~2_combout\ = (\current_state.state_key_pressed~q\ & \scancode[2]~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_current_state.state_key_pressed~q\,
	datac => \ALT_INV_scancode[2]~input_o\,
	combout => \byte_current~2_combout\);

-- Location: LABCELL_X62_Y1_N21
\byte_current~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \byte_current~3_combout\ = ( \scancode[3]~input_o\ & ( \current_state.state_key_pressed~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_current_state.state_key_pressed~q\,
	dataf => \ALT_INV_scancode[3]~input_o\,
	combout => \byte_current~3_combout\);

-- Location: LABCELL_X62_Y1_N39
\Selector8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector8~0_combout\ = ( \current_state.state_key_reminder~q\ ) # ( !\current_state.state_key_reminder~q\ & ( ((\current_state.state_key_pressed~q\ & \scancode[4]~input_o\)) # (\current_state.state_key_released~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011101010111010101110101011111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_current_state.state_key_released~q\,
	datab => \ALT_INV_current_state.state_key_pressed~q\,
	datac => \ALT_INV_scancode[4]~input_o\,
	dataf => \ALT_INV_current_state.state_key_reminder~q\,
	combout => \Selector8~0_combout\);

-- Location: LABCELL_X62_Y1_N48
\Selector7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector7~0_combout\ = ( \current_state.state_key_reminder~q\ ) # ( !\current_state.state_key_reminder~q\ & ( ((\current_state.state_key_pressed~q\ & \scancode[5]~input_o\)) # (\current_state.state_key_released~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111111111000000111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_current_state.state_key_pressed~q\,
	datac => \ALT_INV_scancode[5]~input_o\,
	datad => \ALT_INV_current_state.state_key_released~q\,
	dataf => \ALT_INV_current_state.state_key_reminder~q\,
	combout => \Selector7~0_combout\);

-- Location: LABCELL_X62_Y1_N33
\Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector6~0_combout\ = ( \scancode[6]~input_o\ & ( ((\current_state.state_key_reminder~q\) # (\current_state.state_key_pressed~q\)) # (\current_state.state_key_released~q\) ) ) # ( !\scancode[6]~input_o\ & ( (\current_state.state_key_reminder~q\) # 
-- (\current_state.state_key_released~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111010111110101111101111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_current_state.state_key_released~q\,
	datab => \ALT_INV_current_state.state_key_pressed~q\,
	datac => \ALT_INV_current_state.state_key_reminder~q\,
	dataf => \ALT_INV_scancode[6]~input_o\,
	combout => \Selector6~0_combout\);

-- Location: LABCELL_X62_Y1_N36
\Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Selector5~0_combout\ = ( \current_state.state_key_reminder~q\ ) # ( !\current_state.state_key_reminder~q\ & ( ((\current_state.state_key_pressed~q\ & \scancode[7]~input_o\)) # (\current_state.state_key_released~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011101010111010101110101011111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_current_state.state_key_released~q\,
	datab => \ALT_INV_current_state.state_key_pressed~q\,
	datac => \ALT_INV_scancode[7]~input_o\,
	dataf => \ALT_INV_current_state.state_key_reminder~q\,
	combout => \Selector5~0_combout\);

-- Location: MLABCELL_X84_Y77_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


