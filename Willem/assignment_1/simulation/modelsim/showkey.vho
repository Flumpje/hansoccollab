-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "05/19/2020 21:35:18"

-- 
-- Device: Altera 5CGXFC7C7F23C8 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	showkey IS
    PORT (
	reset : IN std_logic;
	kbclock : IN std_logic;
	kbdata : IN std_logic;
	dig0 : OUT std_logic_vector(7 DOWNTO 0);
	dig1 : OUT std_logic_vector(7 DOWNTO 0);
	scancode : OUT std_logic_vector(7 DOWNTO 0);
	byte_read : OUT std_logic
	);
END showkey;

-- Design Ports Information
-- dig0[0]	=>  Location: PIN_N16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig0[1]	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig0[2]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig0[3]	=>  Location: PIN_R21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig0[4]	=>  Location: PIN_K17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig0[5]	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig0[6]	=>  Location: PIN_M21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig0[7]	=>  Location: PIN_P17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig1[0]	=>  Location: PIN_N21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig1[1]	=>  Location: PIN_K21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig1[2]	=>  Location: PIN_K22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig1[3]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig1[4]	=>  Location: PIN_L17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig1[5]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig1[6]	=>  Location: PIN_B21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dig1[7]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[0]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[1]	=>  Location: PIN_P19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[2]	=>  Location: PIN_L22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[3]	=>  Location: PIN_M22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[4]	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[5]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[6]	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- scancode[7]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- byte_read	=>  Location: PIN_L19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- kbclock	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- kbdata	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF showkey IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_kbclock : std_logic;
SIGNAL ww_kbdata : std_logic;
SIGNAL ww_dig0 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_dig1 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_scancode : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_byte_read : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \kbclock~input_o\ : std_logic;
SIGNAL \kbclock~inputCLKENA0_outclk\ : std_logic;
SIGNAL \kbdata~input_o\ : std_logic;
SIGNAL \Add1~9_sumout\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \COUNT:counter[3]~q\ : std_logic;
SIGNAL \Add1~10\ : std_logic;
SIGNAL \Add1~5_sumout\ : std_logic;
SIGNAL \COUNT:counter[1]~q\ : std_logic;
SIGNAL \Add1~6\ : std_logic;
SIGNAL \Add1~14\ : std_logic;
SIGNAL \Add1~1_sumout\ : std_logic;
SIGNAL \COUNT:counter[3]~DUPLICATE_q\ : std_logic;
SIGNAL \COUNT:counter[0]~DUPLICATE_q\ : std_logic;
SIGNAL \Add1~2\ : std_logic;
SIGNAL \Add1~29_sumout\ : std_logic;
SIGNAL \COUNT:counter[4]~q\ : std_logic;
SIGNAL \Add1~30\ : std_logic;
SIGNAL \Add1~25_sumout\ : std_logic;
SIGNAL \COUNT:counter[5]~q\ : std_logic;
SIGNAL \Add1~26\ : std_logic;
SIGNAL \Add1~21_sumout\ : std_logic;
SIGNAL \COUNT:counter[6]~q\ : std_logic;
SIGNAL \Add1~22\ : std_logic;
SIGNAL \Add1~17_sumout\ : std_logic;
SIGNAL \COUNT:counter[7]~q\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \COUNT:counter[7]~0_combout\ : std_logic;
SIGNAL \COUNT:counter[2]~q\ : std_logic;
SIGNAL \Add1~13_sumout\ : std_logic;
SIGNAL \COUNT:counter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \COUNT:counter[1]~DUPLICATE_q\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \byte_read~1_combout\ : std_logic;
SIGNAL \COUNT:counter[0]~q\ : std_logic;
SIGNAL \COUNT:byte_current[0]~0_combout\ : std_logic;
SIGNAL \COUNT:byte_current[0]~1_combout\ : std_logic;
SIGNAL \COUNT:byte_current[0]~q\ : std_logic;
SIGNAL \dig0[0]~reg0feeder_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \dig0[0]~reg0_q\ : std_logic;
SIGNAL \COUNT:byte_current[1]~0_combout\ : std_logic;
SIGNAL \COUNT:byte_current[1]~q\ : std_logic;
SIGNAL \dig0[1]~reg0_q\ : std_logic;
SIGNAL \COUNT:byte_current[2]~0_combout\ : std_logic;
SIGNAL \COUNT:byte_current[2]~q\ : std_logic;
SIGNAL \dig0[2]~reg0feeder_combout\ : std_logic;
SIGNAL \dig0[2]~reg0_q\ : std_logic;
SIGNAL \COUNT:byte_current[3]~0_combout\ : std_logic;
SIGNAL \COUNT:byte_current[3]~q\ : std_logic;
SIGNAL \dig0[3]~reg0_q\ : std_logic;
SIGNAL \COUNT:byte_current[3]~1_combout\ : std_logic;
SIGNAL \COUNT:byte_current[4]~0_combout\ : std_logic;
SIGNAL \COUNT:byte_current[4]~q\ : std_logic;
SIGNAL \dig0[4]~reg0_q\ : std_logic;
SIGNAL \COUNT:byte_current[5]~0_combout\ : std_logic;
SIGNAL \COUNT:byte_current[5]~q\ : std_logic;
SIGNAL \dig0[5]~reg0feeder_combout\ : std_logic;
SIGNAL \dig0[5]~reg0_q\ : std_logic;
SIGNAL \COUNT:byte_current[6]~0_combout\ : std_logic;
SIGNAL \COUNT:byte_current[6]~q\ : std_logic;
SIGNAL \dig0[6]~reg0_q\ : std_logic;
SIGNAL \COUNT:byte_current[7]~0_combout\ : std_logic;
SIGNAL \COUNT:byte_current[7]~q\ : std_logic;
SIGNAL \dig0[7]~reg0_q\ : std_logic;
SIGNAL \COUNT:dig_buffer[0]~feeder_combout\ : std_logic;
SIGNAL \COUNT:dig_buffer[0]~q\ : std_logic;
SIGNAL \dig1[0]~reg0_q\ : std_logic;
SIGNAL \COUNT:dig_buffer[1]~q\ : std_logic;
SIGNAL \dig1[1]~reg0_q\ : std_logic;
SIGNAL \COUNT:dig_buffer[2]~q\ : std_logic;
SIGNAL \dig1[2]~reg0feeder_combout\ : std_logic;
SIGNAL \dig1[2]~reg0_q\ : std_logic;
SIGNAL \COUNT:dig_buffer[3]~q\ : std_logic;
SIGNAL \dig1[3]~reg0feeder_combout\ : std_logic;
SIGNAL \dig1[3]~reg0_q\ : std_logic;
SIGNAL \COUNT:dig_buffer[4]~q\ : std_logic;
SIGNAL \dig1[4]~reg0feeder_combout\ : std_logic;
SIGNAL \dig1[4]~reg0_q\ : std_logic;
SIGNAL \COUNT:dig_buffer[5]~q\ : std_logic;
SIGNAL \dig1[5]~reg0feeder_combout\ : std_logic;
SIGNAL \dig1[5]~reg0_q\ : std_logic;
SIGNAL \COUNT:dig_buffer[6]~q\ : std_logic;
SIGNAL \dig1[6]~reg0feeder_combout\ : std_logic;
SIGNAL \dig1[6]~reg0_q\ : std_logic;
SIGNAL \COUNT:dig_buffer[7]~q\ : std_logic;
SIGNAL \dig1[7]~reg0feeder_combout\ : std_logic;
SIGNAL \dig1[7]~reg0_q\ : std_logic;
SIGNAL \scancode[0]~0_combout\ : std_logic;
SIGNAL \scancode[0]~reg0_q\ : std_logic;
SIGNAL \scancode[1]~reg0_q\ : std_logic;
SIGNAL \scancode[2]~reg0_q\ : std_logic;
SIGNAL \scancode[3]~reg0_q\ : std_logic;
SIGNAL \scancode[4]~reg0_q\ : std_logic;
SIGNAL \scancode[5]~reg0_q\ : std_logic;
SIGNAL \scancode[6]~reg0_q\ : std_logic;
SIGNAL \scancode[7]~reg0_q\ : std_logic;
SIGNAL \byte_read~0_combout\ : std_logic;
SIGNAL \byte_read~reg0_q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[2]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[0]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[1]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[3]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_kbclock~inputCLKENA0_outclk\ : std_logic;
SIGNAL \ALT_INV_kbdata~input_o\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[3]~1_combout\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[0]~0_combout\ : std_logic;
SIGNAL \ALT_INV_COUNT:dig_buffer[7]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:dig_buffer[6]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:dig_buffer[5]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:dig_buffer[4]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:dig_buffer[3]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:dig_buffer[2]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[7]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[6]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[5]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[4]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[3]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[2]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[1]~q\ : std_logic;
SIGNAL \ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \ALT_INV_COUNT:byte_current[0]~q\ : std_logic;
SIGNAL \ALT_INV_byte_read~reg0_q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[4]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[5]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[6]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[7]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[2]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[0]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[1]~q\ : std_logic;
SIGNAL \ALT_INV_COUNT:counter[3]~q\ : std_logic;

BEGIN

ww_reset <= reset;
ww_kbclock <= kbclock;
ww_kbdata <= kbdata;
dig0 <= ww_dig0;
dig1 <= ww_dig1;
scancode <= ww_scancode;
byte_read <= ww_byte_read;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_COUNT:counter[2]~DUPLICATE_q\ <= NOT \COUNT:counter[2]~DUPLICATE_q\;
\ALT_INV_COUNT:counter[0]~DUPLICATE_q\ <= NOT \COUNT:counter[0]~DUPLICATE_q\;
\ALT_INV_COUNT:counter[1]~DUPLICATE_q\ <= NOT \COUNT:counter[1]~DUPLICATE_q\;
\ALT_INV_COUNT:counter[3]~DUPLICATE_q\ <= NOT \COUNT:counter[3]~DUPLICATE_q\;
\ALT_INV_kbclock~inputCLKENA0_outclk\ <= NOT \kbclock~inputCLKENA0_outclk\;
\ALT_INV_kbdata~input_o\ <= NOT \kbdata~input_o\;
\ALT_INV_COUNT:byte_current[3]~1_combout\ <= NOT \COUNT:byte_current[3]~1_combout\;
\ALT_INV_COUNT:byte_current[0]~0_combout\ <= NOT \COUNT:byte_current[0]~0_combout\;
\ALT_INV_COUNT:dig_buffer[7]~q\ <= NOT \COUNT:dig_buffer[7]~q\;
\ALT_INV_COUNT:dig_buffer[6]~q\ <= NOT \COUNT:dig_buffer[6]~q\;
\ALT_INV_COUNT:dig_buffer[5]~q\ <= NOT \COUNT:dig_buffer[5]~q\;
\ALT_INV_COUNT:dig_buffer[4]~q\ <= NOT \COUNT:dig_buffer[4]~q\;
\ALT_INV_COUNT:dig_buffer[3]~q\ <= NOT \COUNT:dig_buffer[3]~q\;
\ALT_INV_COUNT:dig_buffer[2]~q\ <= NOT \COUNT:dig_buffer[2]~q\;
\ALT_INV_COUNT:byte_current[7]~q\ <= NOT \COUNT:byte_current[7]~q\;
\ALT_INV_COUNT:byte_current[6]~q\ <= NOT \COUNT:byte_current[6]~q\;
\ALT_INV_COUNT:byte_current[5]~q\ <= NOT \COUNT:byte_current[5]~q\;
\ALT_INV_COUNT:byte_current[4]~q\ <= NOT \COUNT:byte_current[4]~q\;
\ALT_INV_COUNT:byte_current[3]~q\ <= NOT \COUNT:byte_current[3]~q\;
\ALT_INV_COUNT:byte_current[2]~q\ <= NOT \COUNT:byte_current[2]~q\;
\ALT_INV_COUNT:byte_current[1]~q\ <= NOT \COUNT:byte_current[1]~q\;
\ALT_INV_Equal0~1_combout\ <= NOT \Equal0~1_combout\;
\ALT_INV_Equal0~0_combout\ <= NOT \Equal0~0_combout\;
\ALT_INV_COUNT:byte_current[0]~q\ <= NOT \COUNT:byte_current[0]~q\;
\ALT_INV_byte_read~reg0_q\ <= NOT \byte_read~reg0_q\;
\ALT_INV_COUNT:counter[4]~q\ <= NOT \COUNT:counter[4]~q\;
\ALT_INV_COUNT:counter[5]~q\ <= NOT \COUNT:counter[5]~q\;
\ALT_INV_COUNT:counter[6]~q\ <= NOT \COUNT:counter[6]~q\;
\ALT_INV_COUNT:counter[7]~q\ <= NOT \COUNT:counter[7]~q\;
\ALT_INV_COUNT:counter[2]~q\ <= NOT \COUNT:counter[2]~q\;
\ALT_INV_COUNT:counter[0]~q\ <= NOT \COUNT:counter[0]~q\;
\ALT_INV_COUNT:counter[1]~q\ <= NOT \COUNT:counter[1]~q\;
\ALT_INV_COUNT:counter[3]~q\ <= NOT \COUNT:counter[3]~q\;

-- Location: IOOBUF_X89_Y35_N45
\dig0[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig0[0]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig0(0));

-- Location: IOOBUF_X89_Y37_N39
\dig0[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig0[1]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig0(1));

-- Location: IOOBUF_X89_Y38_N22
\dig0[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig0[2]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig0(2));

-- Location: IOOBUF_X89_Y8_N39
\dig0[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig0[3]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig0(3));

-- Location: IOOBUF_X89_Y37_N5
\dig0[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig0[4]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig0(4));

-- Location: IOOBUF_X89_Y35_N79
\dig0[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig0[5]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig0(5));

-- Location: IOOBUF_X89_Y37_N56
\dig0[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig0[6]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig0(6));

-- Location: IOOBUF_X89_Y9_N22
\dig0[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig0[7]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig0(7));

-- Location: IOOBUF_X89_Y35_N96
\dig1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig1[0]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig1(0));

-- Location: IOOBUF_X89_Y38_N39
\dig1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig1[1]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig1(1));

-- Location: IOOBUF_X89_Y38_N56
\dig1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig1[2]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig1(2));

-- Location: IOOBUF_X82_Y81_N76
\dig1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig1[3]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig1(3));

-- Location: IOOBUF_X89_Y37_N22
\dig1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig1[4]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig1(4));

-- Location: IOOBUF_X82_Y81_N93
\dig1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig1[5]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig1(5));

-- Location: IOOBUF_X82_Y81_N59
\dig1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig1[6]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig1(6));

-- Location: IOOBUF_X89_Y8_N5
\dig1[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \dig1[7]~reg0_q\,
	devoe => ww_devoe,
	o => ww_dig1(7));

-- Location: IOOBUF_X89_Y36_N22
\scancode[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \scancode[0]~reg0_q\,
	devoe => ww_devoe,
	o => ww_scancode(0));

-- Location: IOOBUF_X89_Y9_N39
\scancode[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \scancode[1]~reg0_q\,
	devoe => ww_devoe,
	o => ww_scancode(1));

-- Location: IOOBUF_X89_Y36_N56
\scancode[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \scancode[2]~reg0_q\,
	devoe => ww_devoe,
	o => ww_scancode(2));

-- Location: IOOBUF_X89_Y36_N39
\scancode[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \scancode[3]~reg0_q\,
	devoe => ww_devoe,
	o => ww_scancode(3));

-- Location: IOOBUF_X89_Y9_N56
\scancode[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \scancode[4]~reg0_q\,
	devoe => ww_devoe,
	o => ww_scancode(4));

-- Location: IOOBUF_X84_Y81_N36
\scancode[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \scancode[5]~reg0_q\,
	devoe => ww_devoe,
	o => ww_scancode(5));

-- Location: IOOBUF_X89_Y36_N5
\scancode[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \scancode[6]~reg0_q\,
	devoe => ww_devoe,
	o => ww_scancode(6));

-- Location: IOOBUF_X84_Y81_N53
\scancode[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \scancode[7]~reg0_q\,
	devoe => ww_devoe,
	o => ww_scancode(7));

-- Location: IOOBUF_X89_Y38_N5
\byte_read~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \byte_read~reg0_q\,
	devoe => ww_devoe,
	o => ww_byte_read);

-- Location: IOIBUF_X89_Y35_N61
\kbclock~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_kbclock,
	o => \kbclock~input_o\);

-- Location: CLKCTRL_G10
\kbclock~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \kbclock~input_o\,
	outclk => \kbclock~inputCLKENA0_outclk\);

-- Location: IOIBUF_X89_Y9_N4
\kbdata~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_kbdata,
	o => \kbdata~input_o\);

-- Location: MLABCELL_X84_Y39_N0
\Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~9_sumout\ = SUM(( \COUNT:counter[0]~q\ ) + ( VCC ) + ( !VCC ))
-- \Add1~10\ = CARRY(( \COUNT:counter[0]~q\ ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_COUNT:counter[0]~q\,
	cin => GND,
	sumout => \Add1~9_sumout\,
	cout => \Add1~10\);

-- Location: IOIBUF_X84_Y81_N18
\reset~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: FF_X84_Y39_N11
\COUNT:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~1_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[3]~q\);

-- Location: MLABCELL_X84_Y39_N3
\Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~5_sumout\ = SUM(( \COUNT:counter[1]~q\ ) + ( GND ) + ( \Add1~10\ ))
-- \Add1~6\ = CARRY(( \COUNT:counter[1]~q\ ) + ( GND ) + ( \Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_COUNT:counter[1]~q\,
	cin => \Add1~10\,
	sumout => \Add1~5_sumout\,
	cout => \Add1~6\);

-- Location: FF_X84_Y39_N5
\COUNT:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~5_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[1]~q\);

-- Location: MLABCELL_X84_Y39_N6
\Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~13_sumout\ = SUM(( \COUNT:counter[2]~q\ ) + ( GND ) + ( \Add1~6\ ))
-- \Add1~14\ = CARRY(( \COUNT:counter[2]~q\ ) + ( GND ) + ( \Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_COUNT:counter[2]~q\,
	cin => \Add1~6\,
	sumout => \Add1~13_sumout\,
	cout => \Add1~14\);

-- Location: MLABCELL_X84_Y39_N9
\Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~1_sumout\ = SUM(( \COUNT:counter[3]~q\ ) + ( GND ) + ( \Add1~14\ ))
-- \Add1~2\ = CARRY(( \COUNT:counter[3]~q\ ) + ( GND ) + ( \Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_COUNT:counter[3]~q\,
	cin => \Add1~14\,
	sumout => \Add1~1_sumout\,
	cout => \Add1~2\);

-- Location: FF_X84_Y39_N10
\COUNT:counter[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~1_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[3]~DUPLICATE_q\);

-- Location: FF_X84_Y39_N2
\COUNT:counter[0]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~9_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[0]~DUPLICATE_q\);

-- Location: MLABCELL_X84_Y39_N12
\Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~29_sumout\ = SUM(( \COUNT:counter[4]~q\ ) + ( GND ) + ( \Add1~2\ ))
-- \Add1~30\ = CARRY(( \COUNT:counter[4]~q\ ) + ( GND ) + ( \Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_COUNT:counter[4]~q\,
	cin => \Add1~2\,
	sumout => \Add1~29_sumout\,
	cout => \Add1~30\);

-- Location: FF_X84_Y39_N14
\COUNT:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~29_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[4]~q\);

-- Location: MLABCELL_X84_Y39_N15
\Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~25_sumout\ = SUM(( \COUNT:counter[5]~q\ ) + ( GND ) + ( \Add1~30\ ))
-- \Add1~26\ = CARRY(( \COUNT:counter[5]~q\ ) + ( GND ) + ( \Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_COUNT:counter[5]~q\,
	cin => \Add1~30\,
	sumout => \Add1~25_sumout\,
	cout => \Add1~26\);

-- Location: FF_X84_Y39_N17
\COUNT:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~25_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[5]~q\);

-- Location: MLABCELL_X84_Y39_N18
\Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~21_sumout\ = SUM(( \COUNT:counter[6]~q\ ) + ( GND ) + ( \Add1~26\ ))
-- \Add1~22\ = CARRY(( \COUNT:counter[6]~q\ ) + ( GND ) + ( \Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_COUNT:counter[6]~q\,
	cin => \Add1~26\,
	sumout => \Add1~21_sumout\,
	cout => \Add1~22\);

-- Location: FF_X84_Y39_N20
\COUNT:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~21_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[6]~q\);

-- Location: MLABCELL_X84_Y39_N21
\Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~17_sumout\ = SUM(( \COUNT:counter[7]~q\ ) + ( GND ) + ( \Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_COUNT:counter[7]~q\,
	cin => \Add1~22\,
	sumout => \Add1~17_sumout\);

-- Location: FF_X84_Y39_N23
\COUNT:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~17_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[7]~q\);

-- Location: MLABCELL_X84_Y39_N42
\Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = ( !\COUNT:counter[7]~q\ & ( !\COUNT:counter[5]~q\ & ( (!\COUNT:counter[6]~q\ & !\COUNT:counter[4]~q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_COUNT:counter[6]~q\,
	datad => \ALT_INV_COUNT:counter[4]~q\,
	datae => \ALT_INV_COUNT:counter[7]~q\,
	dataf => \ALT_INV_COUNT:counter[5]~q\,
	combout => \Equal0~1_combout\);

-- Location: MLABCELL_X84_Y39_N57
\COUNT:counter[7]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:counter[7]~0_combout\ = ( \COUNT:counter[0]~DUPLICATE_q\ & ( \Equal0~1_combout\ & ( !\COUNT:counter[3]~DUPLICATE_q\ ) ) ) # ( !\COUNT:counter[0]~DUPLICATE_q\ & ( \Equal0~1_combout\ & ( (!\COUNT:counter[2]~DUPLICATE_q\) # 
-- (!\COUNT:counter[3]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111100001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_COUNT:counter[2]~DUPLICATE_q\,
	datad => \ALT_INV_COUNT:counter[3]~DUPLICATE_q\,
	datae => \ALT_INV_COUNT:counter[0]~DUPLICATE_q\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \COUNT:counter[7]~0_combout\);

-- Location: FF_X84_Y39_N7
\COUNT:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~13_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[2]~q\);

-- Location: FF_X84_Y39_N8
\COUNT:counter[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~13_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[2]~DUPLICATE_q\);

-- Location: FF_X84_Y39_N4
\COUNT:counter[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~5_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[1]~DUPLICATE_q\);

-- Location: MLABCELL_X84_Y39_N51
\Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = ( !\COUNT:counter[0]~DUPLICATE_q\ & ( !\COUNT:counter[1]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \ALT_INV_COUNT:counter[0]~DUPLICATE_q\,
	dataf => \ALT_INV_COUNT:counter[1]~DUPLICATE_q\,
	combout => \Equal0~0_combout\);

-- Location: MLABCELL_X84_Y39_N33
\byte_read~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \byte_read~1_combout\ = ( \Equal0~0_combout\ & ( \Equal0~1_combout\ & ( (\COUNT:counter[2]~DUPLICATE_q\ & \COUNT:counter[3]~DUPLICATE_q\) ) ) ) # ( !\Equal0~0_combout\ & ( \Equal0~1_combout\ & ( \COUNT:counter[3]~DUPLICATE_q\ ) ) ) # ( \Equal0~0_combout\ 
-- & ( !\Equal0~1_combout\ ) ) # ( !\Equal0~0_combout\ & ( !\Equal0~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000111111110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_COUNT:counter[2]~DUPLICATE_q\,
	datad => \ALT_INV_COUNT:counter[3]~DUPLICATE_q\,
	datae => \ALT_INV_Equal0~0_combout\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \byte_read~1_combout\);

-- Location: FF_X84_Y39_N1
\COUNT:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \Add1~9_sumout\,
	clrn => \reset~input_o\,
	sclr => \byte_read~1_combout\,
	ena => \COUNT:counter[7]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:counter[0]~q\);

-- Location: LABCELL_X85_Y39_N33
\COUNT:byte_current[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[0]~0_combout\ = ( \Equal0~0_combout\ & ( (\Equal0~1_combout\ & (\COUNT:counter[3]~DUPLICATE_q\ & !\COUNT:counter[2]~q\)) ) ) # ( !\Equal0~0_combout\ & ( (\Equal0~1_combout\ & (!\COUNT:counter[3]~DUPLICATE_q\ & !\COUNT:counter[2]~q\)) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000000011000000000000001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_Equal0~1_combout\,
	datac => \ALT_INV_COUNT:counter[3]~DUPLICATE_q\,
	datad => \ALT_INV_COUNT:counter[2]~q\,
	dataf => \ALT_INV_Equal0~0_combout\,
	combout => \COUNT:byte_current[0]~0_combout\);

-- Location: LABCELL_X85_Y39_N54
\COUNT:byte_current[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[0]~1_combout\ = ( \COUNT:byte_current[0]~0_combout\ & ( (!\COUNT:counter[0]~q\ & (((\COUNT:byte_current[0]~q\)))) # (\COUNT:counter[0]~q\ & ((!\COUNT:counter[1]~q\ & (\kbdata~input_o\)) # (\COUNT:counter[1]~q\ & 
-- ((\COUNT:byte_current[0]~q\))))) ) ) # ( !\COUNT:byte_current[0]~0_combout\ & ( \COUNT:byte_current[0]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100010000110111110001000011011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_kbdata~input_o\,
	datab => \ALT_INV_COUNT:counter[0]~q\,
	datac => \ALT_INV_COUNT:counter[1]~q\,
	datad => \ALT_INV_COUNT:byte_current[0]~q\,
	dataf => \ALT_INV_COUNT:byte_current[0]~0_combout\,
	combout => \COUNT:byte_current[0]~1_combout\);

-- Location: FF_X85_Y39_N56
\COUNT:byte_current[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:byte_current[0]~1_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:byte_current[0]~q\);

-- Location: LABCELL_X85_Y39_N0
\dig0[0]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig0[0]~reg0feeder_combout\ = \COUNT:byte_current[0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:byte_current[0]~q\,
	combout => \dig0[0]~reg0feeder_combout\);

-- Location: MLABCELL_X84_Y39_N27
\Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = ( \Equal0~0_combout\ & ( \Equal0~1_combout\ & ( (!\COUNT:counter[3]~q\ & !\COUNT:counter[2]~DUPLICATE_q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001100000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_COUNT:counter[3]~q\,
	datac => \ALT_INV_COUNT:counter[2]~DUPLICATE_q\,
	datae => \ALT_INV_Equal0~0_combout\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \Equal0~2_combout\);

-- Location: FF_X85_Y39_N1
\dig0[0]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig0[0]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig0[0]~reg0_q\);

-- Location: LABCELL_X85_Y39_N39
\COUNT:byte_current[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[1]~0_combout\ = ( \COUNT:byte_current[0]~0_combout\ & ( (!\COUNT:counter[0]~q\ & ((!\COUNT:counter[1]~q\ & ((\COUNT:byte_current[1]~q\))) # (\COUNT:counter[1]~q\ & (\kbdata~input_o\)))) # (\COUNT:counter[0]~q\ & 
-- (((\COUNT:byte_current[1]~q\)))) ) ) # ( !\COUNT:byte_current[0]~0_combout\ & ( \COUNT:byte_current[1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000100111101110000010011110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_kbdata~input_o\,
	datab => \ALT_INV_COUNT:counter[0]~q\,
	datac => \ALT_INV_COUNT:counter[1]~q\,
	datad => \ALT_INV_COUNT:byte_current[1]~q\,
	dataf => \ALT_INV_COUNT:byte_current[0]~0_combout\,
	combout => \COUNT:byte_current[1]~0_combout\);

-- Location: FF_X85_Y39_N41
\COUNT:byte_current[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:byte_current[1]~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:byte_current[1]~q\);

-- Location: FF_X85_Y39_N32
\dig0[1]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[1]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig0[1]~reg0_q\);

-- Location: LABCELL_X85_Y39_N42
\COUNT:byte_current[2]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[2]~0_combout\ = ( \COUNT:byte_current[0]~0_combout\ & ( (!\COUNT:counter[0]~q\ & (((\COUNT:byte_current[2]~q\)))) # (\COUNT:counter[0]~q\ & ((!\COUNT:counter[1]~q\ & ((\COUNT:byte_current[2]~q\))) # (\COUNT:counter[1]~q\ & 
-- (\kbdata~input_o\)))) ) ) # ( !\COUNT:byte_current[0]~0_combout\ & ( \COUNT:byte_current[2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111010000000111111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_kbdata~input_o\,
	datab => \ALT_INV_COUNT:counter[0]~q\,
	datac => \ALT_INV_COUNT:counter[1]~q\,
	datad => \ALT_INV_COUNT:byte_current[2]~q\,
	dataf => \ALT_INV_COUNT:byte_current[0]~0_combout\,
	combout => \COUNT:byte_current[2]~0_combout\);

-- Location: FF_X85_Y39_N44
\COUNT:byte_current[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:byte_current[2]~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:byte_current[2]~q\);

-- Location: LABCELL_X85_Y39_N15
\dig0[2]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig0[2]~reg0feeder_combout\ = \COUNT:byte_current[2]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_COUNT:byte_current[2]~q\,
	combout => \dig0[2]~reg0feeder_combout\);

-- Location: FF_X85_Y39_N16
\dig0[2]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig0[2]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig0[2]~reg0_q\);

-- Location: LABCELL_X85_Y39_N24
\COUNT:byte_current[3]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[3]~0_combout\ = ( \COUNT:byte_current[3]~q\ & ( \Equal0~0_combout\ & ( (((!\COUNT:counter[2]~q\) # (!\Equal0~1_combout\)) # (\COUNT:counter[3]~DUPLICATE_q\)) # (\kbdata~input_o\) ) ) ) # ( !\COUNT:byte_current[3]~q\ & ( 
-- \Equal0~0_combout\ & ( (\kbdata~input_o\ & (!\COUNT:counter[3]~DUPLICATE_q\ & (\COUNT:counter[2]~q\ & \Equal0~1_combout\))) ) ) ) # ( \COUNT:byte_current[3]~q\ & ( !\Equal0~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000001001111111111110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_kbdata~input_o\,
	datab => \ALT_INV_COUNT:counter[3]~DUPLICATE_q\,
	datac => \ALT_INV_COUNT:counter[2]~q\,
	datad => \ALT_INV_Equal0~1_combout\,
	datae => \ALT_INV_COUNT:byte_current[3]~q\,
	dataf => \ALT_INV_Equal0~0_combout\,
	combout => \COUNT:byte_current[3]~0_combout\);

-- Location: FF_X85_Y39_N26
\COUNT:byte_current[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:byte_current[3]~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:byte_current[3]~q\);

-- Location: FF_X83_Y39_N1
\dig0[3]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[3]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig0[3]~reg0_q\);

-- Location: LABCELL_X85_Y39_N9
\COUNT:byte_current[3]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[3]~1_combout\ = ( !\COUNT:counter[3]~DUPLICATE_q\ & ( (\Equal0~1_combout\ & \COUNT:counter[2]~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Equal0~1_combout\,
	datad => \ALT_INV_COUNT:counter[2]~q\,
	dataf => \ALT_INV_COUNT:counter[3]~DUPLICATE_q\,
	combout => \COUNT:byte_current[3]~1_combout\);

-- Location: LABCELL_X85_Y39_N45
\COUNT:byte_current[4]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[4]~0_combout\ = ( \COUNT:byte_current[3]~1_combout\ & ( (!\COUNT:counter[0]~q\ & (((\COUNT:byte_current[4]~q\)))) # (\COUNT:counter[0]~q\ & ((!\COUNT:counter[1]~q\ & (\kbdata~input_o\)) # (\COUNT:counter[1]~q\ & 
-- ((\COUNT:byte_current[4]~q\))))) ) ) # ( !\COUNT:byte_current[3]~1_combout\ & ( \COUNT:byte_current[4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100010000110111110001000011011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_kbdata~input_o\,
	datab => \ALT_INV_COUNT:counter[0]~q\,
	datac => \ALT_INV_COUNT:counter[1]~q\,
	datad => \ALT_INV_COUNT:byte_current[4]~q\,
	dataf => \ALT_INV_COUNT:byte_current[3]~1_combout\,
	combout => \COUNT:byte_current[4]~0_combout\);

-- Location: FF_X85_Y39_N47
\COUNT:byte_current[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:byte_current[4]~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:byte_current[4]~q\);

-- Location: FF_X85_Y39_N5
\dig0[4]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[4]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig0[4]~reg0_q\);

-- Location: LABCELL_X85_Y39_N36
\COUNT:byte_current[5]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[5]~0_combout\ = ( \COUNT:byte_current[3]~1_combout\ & ( (!\COUNT:counter[0]~q\ & ((!\COUNT:counter[1]~q\ & ((\COUNT:byte_current[5]~q\))) # (\COUNT:counter[1]~q\ & (\kbdata~input_o\)))) # (\COUNT:counter[0]~q\ & 
-- (((\COUNT:byte_current[5]~q\)))) ) ) # ( !\COUNT:byte_current[3]~1_combout\ & ( \COUNT:byte_current[5]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000100111101110000010011110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_kbdata~input_o\,
	datab => \ALT_INV_COUNT:counter[0]~q\,
	datac => \ALT_INV_COUNT:counter[1]~q\,
	datad => \ALT_INV_COUNT:byte_current[5]~q\,
	dataf => \ALT_INV_COUNT:byte_current[3]~1_combout\,
	combout => \COUNT:byte_current[5]~0_combout\);

-- Location: FF_X85_Y39_N38
\COUNT:byte_current[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:byte_current[5]~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:byte_current[5]~q\);

-- Location: LABCELL_X85_Y39_N30
\dig0[5]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig0[5]~reg0feeder_combout\ = \COUNT:byte_current[5]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:byte_current[5]~q\,
	combout => \dig0[5]~reg0feeder_combout\);

-- Location: FF_X85_Y39_N31
\dig0[5]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig0[5]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig0[5]~reg0_q\);

-- Location: LABCELL_X85_Y39_N57
\COUNT:byte_current[6]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[6]~0_combout\ = ( \COUNT:byte_current[3]~1_combout\ & ( (!\COUNT:counter[0]~q\ & (((\COUNT:byte_current[6]~q\)))) # (\COUNT:counter[0]~q\ & ((!\COUNT:counter[1]~q\ & ((\COUNT:byte_current[6]~q\))) # (\COUNT:counter[1]~q\ & 
-- (\kbdata~input_o\)))) ) ) # ( !\COUNT:byte_current[3]~1_combout\ & ( \COUNT:byte_current[6]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111010000000111111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_kbdata~input_o\,
	datab => \ALT_INV_COUNT:counter[0]~q\,
	datac => \ALT_INV_COUNT:counter[1]~q\,
	datad => \ALT_INV_COUNT:byte_current[6]~q\,
	dataf => \ALT_INV_COUNT:byte_current[3]~1_combout\,
	combout => \COUNT:byte_current[6]~0_combout\);

-- Location: FF_X85_Y39_N59
\COUNT:byte_current[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:byte_current[6]~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:byte_current[6]~q\);

-- Location: FF_X85_Y39_N10
\dig0[6]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[6]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig0[6]~reg0_q\);

-- Location: LABCELL_X85_Y39_N18
\COUNT:byte_current[7]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:byte_current[7]~0_combout\ = ( \COUNT:byte_current[7]~q\ & ( \Equal0~1_combout\ & ( ((!\COUNT:counter[3]~DUPLICATE_q\) # ((!\Equal0~0_combout\) # (\kbdata~input_o\))) # (\COUNT:counter[2]~q\) ) ) ) # ( !\COUNT:byte_current[7]~q\ & ( 
-- \Equal0~1_combout\ & ( (!\COUNT:counter[2]~q\ & (\COUNT:counter[3]~DUPLICATE_q\ & (\Equal0~0_combout\ & \kbdata~input_o\))) ) ) ) # ( \COUNT:byte_current[7]~q\ & ( !\Equal0~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000101111110111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:counter[2]~q\,
	datab => \ALT_INV_COUNT:counter[3]~DUPLICATE_q\,
	datac => \ALT_INV_Equal0~0_combout\,
	datad => \ALT_INV_kbdata~input_o\,
	datae => \ALT_INV_COUNT:byte_current[7]~q\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \COUNT:byte_current[7]~0_combout\);

-- Location: FF_X85_Y39_N20
\COUNT:byte_current[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:byte_current[7]~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:byte_current[7]~q\);

-- Location: FF_X83_Y39_N58
\dig0[7]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[7]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig0[7]~reg0_q\);

-- Location: LABCELL_X85_Y39_N3
\COUNT:dig_buffer[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \COUNT:dig_buffer[0]~feeder_combout\ = \COUNT:byte_current[0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:byte_current[0]~q\,
	combout => \COUNT:dig_buffer[0]~feeder_combout\);

-- Location: FF_X85_Y39_N4
\COUNT:dig_buffer[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \COUNT:dig_buffer[0]~feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:dig_buffer[0]~q\);

-- Location: FF_X85_Y39_N8
\dig1[0]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:dig_buffer[0]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig1[0]~reg0_q\);

-- Location: FF_X85_Y39_N34
\COUNT:dig_buffer[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[1]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:dig_buffer[1]~q\);

-- Location: FF_X85_Y39_N13
\dig1[1]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:dig_buffer[1]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig1[1]~reg0_q\);

-- Location: FF_X85_Y39_N2
\COUNT:dig_buffer[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[2]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:dig_buffer[2]~q\);

-- Location: LABCELL_X85_Y39_N12
\dig1[2]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig1[2]~reg0feeder_combout\ = \COUNT:dig_buffer[2]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:dig_buffer[2]~q\,
	combout => \dig1[2]~reg0feeder_combout\);

-- Location: FF_X85_Y39_N14
\dig1[2]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig1[2]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig1[2]~reg0_q\);

-- Location: FF_X83_Y39_N29
\COUNT:dig_buffer[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[3]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:dig_buffer[3]~q\);

-- Location: LABCELL_X83_Y39_N36
\dig1[3]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig1[3]~reg0feeder_combout\ = \COUNT:dig_buffer[3]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:dig_buffer[3]~q\,
	combout => \dig1[3]~reg0feeder_combout\);

-- Location: FF_X83_Y39_N37
\dig1[3]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig1[3]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig1[3]~reg0_q\);

-- Location: FF_X85_Y39_N17
\COUNT:dig_buffer[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[4]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:dig_buffer[4]~q\);

-- Location: LABCELL_X85_Y39_N6
\dig1[4]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig1[4]~reg0feeder_combout\ = \COUNT:dig_buffer[4]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_COUNT:dig_buffer[4]~q\,
	combout => \dig1[4]~reg0feeder_combout\);

-- Location: FF_X85_Y39_N7
\dig1[4]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig1[4]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig1[4]~reg0_q\);

-- Location: FF_X83_Y39_N8
\COUNT:dig_buffer[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[5]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:dig_buffer[5]~q\);

-- Location: LABCELL_X83_Y39_N45
\dig1[5]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig1[5]~reg0feeder_combout\ = \COUNT:dig_buffer[5]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_COUNT:dig_buffer[5]~q\,
	combout => \dig1[5]~reg0feeder_combout\);

-- Location: FF_X83_Y39_N46
\dig1[5]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig1[5]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig1[5]~reg0_q\);

-- Location: FF_X83_Y39_N50
\COUNT:dig_buffer[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[6]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:dig_buffer[6]~q\);

-- Location: LABCELL_X83_Y39_N15
\dig1[6]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig1[6]~reg0feeder_combout\ = \COUNT:dig_buffer[6]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:dig_buffer[6]~q\,
	combout => \dig1[6]~reg0feeder_combout\);

-- Location: FF_X83_Y39_N16
\dig1[6]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig1[6]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig1[6]~reg0_q\);

-- Location: FF_X83_Y39_N35
\COUNT:dig_buffer[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[7]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \COUNT:dig_buffer[7]~q\);

-- Location: LABCELL_X83_Y39_N18
\dig1[7]~reg0feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \dig1[7]~reg0feeder_combout\ = \COUNT:dig_buffer[7]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:dig_buffer[7]~q\,
	combout => \dig1[7]~reg0feeder_combout\);

-- Location: FF_X83_Y39_N19
\dig1[7]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \dig1[7]~reg0feeder_combout\,
	clrn => \reset~input_o\,
	ena => \Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dig1[7]~reg0_q\);

-- Location: MLABCELL_X84_Y39_N36
\scancode[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \scancode[0]~0_combout\ = ( \COUNT:counter[1]~q\ & ( \Equal0~1_combout\ & ( (!\COUNT:counter[0]~DUPLICATE_q\ & (!\COUNT:counter[2]~DUPLICATE_q\ & \COUNT:counter[3]~DUPLICATE_q\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000100000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:counter[0]~DUPLICATE_q\,
	datab => \ALT_INV_COUNT:counter[2]~DUPLICATE_q\,
	datac => \ALT_INV_COUNT:counter[3]~DUPLICATE_q\,
	datae => \ALT_INV_COUNT:counter[1]~q\,
	dataf => \ALT_INV_Equal0~1_combout\,
	combout => \scancode[0]~0_combout\);

-- Location: FF_X84_Y39_N37
\scancode[0]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[0]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \scancode[0]~reg0_q\);

-- Location: FF_X84_Y39_N25
\scancode[1]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[1]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \scancode[1]~reg0_q\);

-- Location: FF_X84_Y39_N34
\scancode[2]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[2]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \scancode[2]~reg0_q\);

-- Location: FF_X84_Y39_N55
\scancode[3]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[3]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \scancode[3]~reg0_q\);

-- Location: FF_X84_Y39_N46
\scancode[4]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[4]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \scancode[4]~reg0_q\);

-- Location: FF_X84_Y39_N49
\scancode[5]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[5]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \scancode[5]~reg0_q\);

-- Location: FF_X84_Y39_N32
\scancode[6]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[6]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \scancode[6]~reg0_q\);

-- Location: FF_X84_Y39_N40
\scancode[7]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	asdata => \COUNT:byte_current[7]~q\,
	clrn => \reset~input_o\,
	sload => VCC,
	ena => \scancode[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \scancode[7]~reg0_q\);

-- Location: LABCELL_X85_Y39_N48
\byte_read~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \byte_read~0_combout\ = ( \byte_read~reg0_q\ & ( \COUNT:counter[1]~q\ & ( (!\Equal0~1_combout\) # (\COUNT:counter[3]~DUPLICATE_q\) ) ) ) # ( !\byte_read~reg0_q\ & ( \COUNT:counter[1]~q\ & ( (!\COUNT:counter[2]~q\ & (\COUNT:counter[3]~DUPLICATE_q\ & 
-- (\Equal0~1_combout\ & !\COUNT:counter[0]~q\))) ) ) ) # ( \byte_read~reg0_q\ & ( !\COUNT:counter[1]~q\ & ( (!\Equal0~1_combout\) # ((\COUNT:counter[3]~DUPLICATE_q\ & ((\COUNT:counter[0]~q\) # (\COUNT:counter[2]~q\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100011111001100000010000000001111001111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_COUNT:counter[2]~q\,
	datab => \ALT_INV_COUNT:counter[3]~DUPLICATE_q\,
	datac => \ALT_INV_Equal0~1_combout\,
	datad => \ALT_INV_COUNT:counter[0]~q\,
	datae => \ALT_INV_byte_read~reg0_q\,
	dataf => \ALT_INV_COUNT:counter[1]~q\,
	combout => \byte_read~0_combout\);

-- Location: FF_X85_Y39_N49
\byte_read~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_kbclock~inputCLKENA0_outclk\,
	d => \byte_read~0_combout\,
	clrn => \reset~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \byte_read~reg0_q\);

-- Location: LABCELL_X55_Y61_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


