transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {C:/HAN/hansoccollab/Willem/assignment_3/clock_generator.vhd}

vcom -93 -work work {C:/HAN/hansoccollab/Willem/assignment_3/tb_clock_generator.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L fiftyfivenm -L rtl_work -L work -voptargs="+acc"  tb_clock_generator

add wave *
view structure
view signals
run -all
