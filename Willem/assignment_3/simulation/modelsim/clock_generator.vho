-- Copyright (C) 2017  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 17.1.0 Build 590 10/25/2017 SJ Lite Edition"

-- DATE "05/22/2020 22:47:33"

-- 
-- Device: Altera 10M02DCU324A6G Package UFBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_J7,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_J8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_H3,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_H4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_H8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	clock_generator IS
    PORT (
	reset : IN std_logic;
	clk : IN std_logic;
	key : IN std_logic_vector(7 DOWNTO 0);
	clk_div : OUT std_logic
	);
END clock_generator;

-- Design Ports Information
-- clk_div	=>  Location: PIN_V4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_L8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[3]	=>  Location: PIN_D5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[1]	=>  Location: PIN_R1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[4]	=>  Location: PIN_R2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[0]	=>  Location: PIN_N3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[2]	=>  Location: PIN_R4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[6]	=>  Location: PIN_M4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[5]	=>  Location: PIN_V2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- key[7]	=>  Location: PIN_U3,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF clock_generator IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_key : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_clk_div : std_logic;
SIGNAL \Selector0~2clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \reset~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \clk_div~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \devider:counter[0]~1_combout\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \reset~inputclkctrl_outclk\ : std_logic;
SIGNAL \devider:counter[1]~2\ : std_logic;
SIGNAL \devider:counter[2]~1_combout\ : std_logic;
SIGNAL \devider:counter[2]~q\ : std_logic;
SIGNAL \devider:counter[2]~2\ : std_logic;
SIGNAL \devider:counter[3]~1_combout\ : std_logic;
SIGNAL \devider:counter[3]~q\ : std_logic;
SIGNAL \devider:counter[3]~2\ : std_logic;
SIGNAL \devider:counter[4]~1_combout\ : std_logic;
SIGNAL \devider:counter[4]~q\ : std_logic;
SIGNAL \devider:counter[4]~2\ : std_logic;
SIGNAL \devider:counter[5]~1_combout\ : std_logic;
SIGNAL \devider:counter[5]~q\ : std_logic;
SIGNAL \devider:counter[5]~2\ : std_logic;
SIGNAL \devider:counter[6]~1_combout\ : std_logic;
SIGNAL \devider:counter[6]~q\ : std_logic;
SIGNAL \devider:counter[6]~2\ : std_logic;
SIGNAL \devider:counter[7]~1_combout\ : std_logic;
SIGNAL \devider:counter[7]~q\ : std_logic;
SIGNAL \devider:counter[0]~q\ : std_logic;
SIGNAL \devider:counter[0]~2\ : std_logic;
SIGNAL \devider:counter[1]~1_combout\ : std_logic;
SIGNAL \devider:counter[1]~q\ : std_logic;
SIGNAL \key[4]~input_o\ : std_logic;
SIGNAL \key[1]~input_o\ : std_logic;
SIGNAL \key[2]~input_o\ : std_logic;
SIGNAL \Equal1~4_combout\ : std_logic;
SIGNAL \key[0]~input_o\ : std_logic;
SIGNAL \key[3]~input_o\ : std_logic;
SIGNAL \key[6]~input_o\ : std_logic;
SIGNAL \key[5]~input_o\ : std_logic;
SIGNAL \key[7]~input_o\ : std_logic;
SIGNAL \Equal1~3_combout\ : std_logic;
SIGNAL \Equal1~5_combout\ : std_logic;
SIGNAL \Selector0~1_combout\ : std_logic;
SIGNAL \Equal1~2_combout\ : std_logic;
SIGNAL \Equal1~6_combout\ : std_logic;
SIGNAL \Selector1~0_combout\ : std_logic;
SIGNAL \next_state.state_0_554~combout\ : std_logic;
SIGNAL \tone_state.state_0~q\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \Selector0~2_combout\ : std_logic;
SIGNAL \Selector0~2clkctrl_outclk\ : std_logic;
SIGNAL \Selector8~0_combout\ : std_logic;
SIGNAL \next_state.state_7_358~combout\ : std_logic;
SIGNAL \tone_state.state_7~q\ : std_logic;
SIGNAL \Selector7~0_combout\ : std_logic;
SIGNAL \next_state.state_6_386~combout\ : std_logic;
SIGNAL \tone_state.state_6~q\ : std_logic;
SIGNAL \Selector6~0_combout\ : std_logic;
SIGNAL \next_state.state_5_414~combout\ : std_logic;
SIGNAL \tone_state.state_5~q\ : std_logic;
SIGNAL \Selector5~0_combout\ : std_logic;
SIGNAL \next_state.state_4_442~combout\ : std_logic;
SIGNAL \tone_state.state_4~q\ : std_logic;
SIGNAL \Selector4~0_combout\ : std_logic;
SIGNAL \next_state.state_3_470~combout\ : std_logic;
SIGNAL \tone_state.state_3~0_combout\ : std_logic;
SIGNAL \tone_state.state_3~q\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \next_state.state_2_498~combout\ : std_logic;
SIGNAL \tone_state.state_2~q\ : std_logic;
SIGNAL \Selector2~0_combout\ : std_logic;
SIGNAL \next_state.state_1_526~combout\ : std_logic;
SIGNAL \tone_state.state_1~q\ : std_logic;
SIGNAL \Selector9~3_combout\ : std_logic;
SIGNAL \Selector9~0_combout\ : std_logic;
SIGNAL \Selector9~1_combout\ : std_logic;
SIGNAL \Selector9~2_combout\ : std_logic;
SIGNAL \Selector9~4_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_reset <= reset;
ww_clk <= clk;
ww_key <= key;
clk_div <= ww_clk_div;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\Selector0~2clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \Selector0~2_combout\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);

\reset~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \reset~input_o\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X11_Y7_N16
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X3_Y0_N30
\clk_div~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Selector9~4_combout\,
	devoe => ww_devoe,
	o => \clk_div~output_o\);

-- Location: IOIBUF_X0_Y4_N15
\clk~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G3
\clk~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: LCCOMB_X3_Y3_N14
\devider:counter[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \devider:counter[0]~1_combout\ = \devider:counter[0]~q\ $ (VCC)
-- \devider:counter[0]~2\ = CARRY(\devider:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \devider:counter[0]~q\,
	datad => VCC,
	combout => \devider:counter[0]~1_combout\,
	cout => \devider:counter[0]~2\);

-- Location: IOIBUF_X0_Y4_N22
\reset~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: CLKCTRL_G1
\reset~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \reset~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \reset~inputclkctrl_outclk\);

-- Location: LCCOMB_X3_Y3_N16
\devider:counter[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \devider:counter[1]~1_combout\ = (\devider:counter[1]~q\ & (!\devider:counter[0]~2\)) # (!\devider:counter[1]~q\ & ((\devider:counter[0]~2\) # (GND)))
-- \devider:counter[1]~2\ = CARRY((!\devider:counter[0]~2\) # (!\devider:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \devider:counter[1]~q\,
	datad => VCC,
	cin => \devider:counter[0]~2\,
	combout => \devider:counter[1]~1_combout\,
	cout => \devider:counter[1]~2\);

-- Location: LCCOMB_X3_Y3_N18
\devider:counter[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \devider:counter[2]~1_combout\ = (\devider:counter[2]~q\ & (\devider:counter[1]~2\ $ (GND))) # (!\devider:counter[2]~q\ & (!\devider:counter[1]~2\ & VCC))
-- \devider:counter[2]~2\ = CARRY((\devider:counter[2]~q\ & !\devider:counter[1]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \devider:counter[2]~q\,
	datad => VCC,
	cin => \devider:counter[1]~2\,
	combout => \devider:counter[2]~1_combout\,
	cout => \devider:counter[2]~2\);

-- Location: FF_X3_Y3_N19
\devider:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \devider:counter[2]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \devider:counter[2]~q\);

-- Location: LCCOMB_X3_Y3_N20
\devider:counter[3]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \devider:counter[3]~1_combout\ = (\devider:counter[3]~q\ & (!\devider:counter[2]~2\)) # (!\devider:counter[3]~q\ & ((\devider:counter[2]~2\) # (GND)))
-- \devider:counter[3]~2\ = CARRY((!\devider:counter[2]~2\) # (!\devider:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \devider:counter[3]~q\,
	datad => VCC,
	cin => \devider:counter[2]~2\,
	combout => \devider:counter[3]~1_combout\,
	cout => \devider:counter[3]~2\);

-- Location: FF_X3_Y3_N21
\devider:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \devider:counter[3]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \devider:counter[3]~q\);

-- Location: LCCOMB_X3_Y3_N22
\devider:counter[4]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \devider:counter[4]~1_combout\ = (\devider:counter[4]~q\ & (\devider:counter[3]~2\ $ (GND))) # (!\devider:counter[4]~q\ & (!\devider:counter[3]~2\ & VCC))
-- \devider:counter[4]~2\ = CARRY((\devider:counter[4]~q\ & !\devider:counter[3]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \devider:counter[4]~q\,
	datad => VCC,
	cin => \devider:counter[3]~2\,
	combout => \devider:counter[4]~1_combout\,
	cout => \devider:counter[4]~2\);

-- Location: FF_X3_Y3_N23
\devider:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \devider:counter[4]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \devider:counter[4]~q\);

-- Location: LCCOMB_X3_Y3_N24
\devider:counter[5]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \devider:counter[5]~1_combout\ = (\devider:counter[5]~q\ & (!\devider:counter[4]~2\)) # (!\devider:counter[5]~q\ & ((\devider:counter[4]~2\) # (GND)))
-- \devider:counter[5]~2\ = CARRY((!\devider:counter[4]~2\) # (!\devider:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \devider:counter[5]~q\,
	datad => VCC,
	cin => \devider:counter[4]~2\,
	combout => \devider:counter[5]~1_combout\,
	cout => \devider:counter[5]~2\);

-- Location: FF_X3_Y3_N25
\devider:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \devider:counter[5]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \devider:counter[5]~q\);

-- Location: LCCOMB_X3_Y3_N26
\devider:counter[6]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \devider:counter[6]~1_combout\ = (\devider:counter[6]~q\ & (\devider:counter[5]~2\ $ (GND))) # (!\devider:counter[6]~q\ & (!\devider:counter[5]~2\ & VCC))
-- \devider:counter[6]~2\ = CARRY((\devider:counter[6]~q\ & !\devider:counter[5]~2\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \devider:counter[6]~q\,
	datad => VCC,
	cin => \devider:counter[5]~2\,
	combout => \devider:counter[6]~1_combout\,
	cout => \devider:counter[6]~2\);

-- Location: FF_X3_Y3_N27
\devider:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \devider:counter[6]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \devider:counter[6]~q\);

-- Location: LCCOMB_X3_Y3_N28
\devider:counter[7]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \devider:counter[7]~1_combout\ = \devider:counter[6]~2\ $ (\devider:counter[7]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \devider:counter[7]~q\,
	cin => \devider:counter[6]~2\,
	combout => \devider:counter[7]~1_combout\);

-- Location: FF_X3_Y3_N29
\devider:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \devider:counter[7]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \devider:counter[7]~q\);

-- Location: FF_X3_Y3_N15
\devider:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \devider:counter[0]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \devider:counter[0]~q\);

-- Location: FF_X3_Y3_N17
\devider:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \devider:counter[1]~1_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	sclr => \devider:counter[7]~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \devider:counter[1]~q\);

-- Location: IOIBUF_X0_Y3_N8
\key[4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(4),
	o => \key[4]~input_o\);

-- Location: IOIBUF_X0_Y3_N1
\key[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(1),
	o => \key[1]~input_o\);

-- Location: IOIBUF_X1_Y0_N15
\key[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(2),
	o => \key[2]~input_o\);

-- Location: LCCOMB_X1_Y3_N18
\Equal1~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~4_combout\ = (\key[4]~input_o\ & (\key[1]~input_o\ & !\key[2]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \key[4]~input_o\,
	datac => \key[1]~input_o\,
	datad => \key[2]~input_o\,
	combout => \Equal1~4_combout\);

-- Location: IOIBUF_X0_Y3_N22
\key[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(0),
	o => \key[0]~input_o\);

-- Location: IOIBUF_X1_Y7_N15
\key[3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(3),
	o => \key[3]~input_o\);

-- Location: IOIBUF_X0_Y3_N15
\key[6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(6),
	o => \key[6]~input_o\);

-- Location: IOIBUF_X1_Y0_N22
\key[5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(5),
	o => \key[5]~input_o\);

-- Location: IOIBUF_X1_Y0_N29
\key[7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_key(7),
	o => \key[7]~input_o\);

-- Location: LCCOMB_X1_Y3_N14
\Equal1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~3_combout\ = (\key[6]~input_o\ & (!\key[5]~input_o\ & !\key[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datac => \key[5]~input_o\,
	datad => \key[7]~input_o\,
	combout => \Equal1~3_combout\);

-- Location: LCCOMB_X1_Y3_N20
\Equal1~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~5_combout\ = (\Equal1~4_combout\ & (!\key[0]~input_o\ & (!\key[3]~input_o\ & \Equal1~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~4_combout\,
	datab => \key[0]~input_o\,
	datac => \key[3]~input_o\,
	datad => \Equal1~3_combout\,
	combout => \Equal1~5_combout\);

-- Location: LCCOMB_X2_Y3_N4
\Selector0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector0~1_combout\ = (\tone_state.state_5~q\) # ((\tone_state.state_2~q\) # ((\tone_state.state_1~q\) # (\tone_state.state_4~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tone_state.state_5~q\,
	datab => \tone_state.state_2~q\,
	datac => \tone_state.state_1~q\,
	datad => \tone_state.state_4~q\,
	combout => \Selector0~1_combout\);

-- Location: LCCOMB_X1_Y3_N16
\Equal1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~2_combout\ = (\key[4]~input_o\ & (!\key[2]~input_o\ & (\key[1]~input_o\ & !\key[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[4]~input_o\,
	datab => \key[2]~input_o\,
	datac => \key[1]~input_o\,
	datad => \key[0]~input_o\,
	combout => \Equal1~2_combout\);

-- Location: LCCOMB_X1_Y3_N24
\Equal1~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Equal1~6_combout\ = (\key[6]~input_o\ & (!\key[7]~input_o\ & (!\key[5]~input_o\ & \Equal1~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[6]~input_o\,
	datab => \key[7]~input_o\,
	datac => \key[5]~input_o\,
	datad => \Equal1~2_combout\,
	combout => \Equal1~6_combout\);

-- Location: LCCOMB_X1_Y3_N28
\Selector1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector1~0_combout\ = (!\key[3]~input_o\ & (\tone_state.state_1~q\ & (\Equal1~3_combout\ & \Equal1~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \tone_state.state_1~q\,
	datac => \Equal1~3_combout\,
	datad => \Equal1~2_combout\,
	combout => \Selector1~0_combout\);

-- Location: LCCOMB_X1_Y3_N8
\next_state.state_0_554\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \next_state.state_0_554~combout\ = (GLOBAL(\Selector0~2clkctrl_outclk\) & (\Selector1~0_combout\)) # (!GLOBAL(\Selector0~2clkctrl_outclk\) & ((\next_state.state_0_554~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector1~0_combout\,
	datac => \next_state.state_0_554~combout\,
	datad => \Selector0~2clkctrl_outclk\,
	combout => \next_state.state_0_554~combout\);

-- Location: FF_X1_Y3_N9
\tone_state.state_0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \next_state.state_0_554~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tone_state.state_0~q\);

-- Location: LCCOMB_X1_Y3_N30
\Selector0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = ((\key[3]~input_o\ & (\tone_state.state_0~q\)) # (!\key[3]~input_o\ & ((\tone_state.state_7~q\)))) # (!\tone_state.state_3~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111110001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \key[3]~input_o\,
	datab => \tone_state.state_0~q\,
	datac => \tone_state.state_3~q\,
	datad => \tone_state.state_7~q\,
	combout => \Selector0~0_combout\);

-- Location: LCCOMB_X1_Y3_N12
\Selector0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector0~2_combout\ = (\Equal1~6_combout\ & ((\Selector0~1_combout\) # ((\Selector0~0_combout\) # (\tone_state.state_6~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector0~1_combout\,
	datab => \Equal1~6_combout\,
	datac => \Selector0~0_combout\,
	datad => \tone_state.state_6~q\,
	combout => \Selector0~2_combout\);

-- Location: CLKCTRL_G2
\Selector0~2clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \Selector0~2clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \Selector0~2clkctrl_outclk\);

-- Location: LCCOMB_X1_Y3_N26
\Selector8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector8~0_combout\ = (\tone_state.state_6~q\ & (((\key[3]~input_o\) # (!\Equal1~3_combout\)) # (!\Equal1~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~2_combout\,
	datab => \tone_state.state_6~q\,
	datac => \key[3]~input_o\,
	datad => \Equal1~3_combout\,
	combout => \Selector8~0_combout\);

-- Location: LCCOMB_X1_Y3_N10
\next_state.state_7_358\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \next_state.state_7_358~combout\ = (GLOBAL(\Selector0~2clkctrl_outclk\) & ((\Selector8~0_combout\))) # (!GLOBAL(\Selector0~2clkctrl_outclk\) & (\next_state.state_7_358~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \next_state.state_7_358~combout\,
	datac => \Selector0~2clkctrl_outclk\,
	datad => \Selector8~0_combout\,
	combout => \next_state.state_7_358~combout\);

-- Location: FF_X1_Y3_N11
\tone_state.state_7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \next_state.state_7_358~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tone_state.state_7~q\);

-- Location: LCCOMB_X2_Y3_N18
\Selector7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector7~0_combout\ = (\tone_state.state_7~q\) # ((!\Equal1~5_combout\ & \tone_state.state_5~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tone_state.state_7~q\,
	datab => \Equal1~5_combout\,
	datac => \tone_state.state_5~q\,
	combout => \Selector7~0_combout\);

-- Location: LCCOMB_X2_Y3_N28
\next_state.state_6_386\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \next_state.state_6_386~combout\ = (GLOBAL(\Selector0~2clkctrl_outclk\) & (\Selector7~0_combout\)) # (!GLOBAL(\Selector0~2clkctrl_outclk\) & ((\next_state.state_6_386~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector7~0_combout\,
	datab => \next_state.state_6_386~combout\,
	datad => \Selector0~2clkctrl_outclk\,
	combout => \next_state.state_6_386~combout\);

-- Location: FF_X2_Y3_N29
\tone_state.state_6\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \next_state.state_6_386~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tone_state.state_6~q\);

-- Location: LCCOMB_X2_Y3_N24
\Selector6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector6~0_combout\ = (\Equal1~5_combout\ & (\tone_state.state_6~q\)) # (!\Equal1~5_combout\ & ((\tone_state.state_4~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal1~5_combout\,
	datac => \tone_state.state_6~q\,
	datad => \tone_state.state_4~q\,
	combout => \Selector6~0_combout\);

-- Location: LCCOMB_X2_Y3_N26
\next_state.state_5_414\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \next_state.state_5_414~combout\ = (GLOBAL(\Selector0~2clkctrl_outclk\) & (\Selector6~0_combout\)) # (!GLOBAL(\Selector0~2clkctrl_outclk\) & ((\next_state.state_5_414~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Selector6~0_combout\,
	datac => \next_state.state_5_414~combout\,
	datad => \Selector0~2clkctrl_outclk\,
	combout => \next_state.state_5_414~combout\);

-- Location: FF_X2_Y3_N27
\tone_state.state_5\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \next_state.state_5_414~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tone_state.state_5~q\);

-- Location: LCCOMB_X2_Y3_N30
\Selector5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector5~0_combout\ = (\Equal1~5_combout\ & (\tone_state.state_5~q\)) # (!\Equal1~5_combout\ & ((!\tone_state.state_3~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal1~5_combout\,
	datac => \tone_state.state_5~q\,
	datad => \tone_state.state_3~q\,
	combout => \Selector5~0_combout\);

-- Location: LCCOMB_X2_Y3_N20
\next_state.state_4_442\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \next_state.state_4_442~combout\ = (GLOBAL(\Selector0~2clkctrl_outclk\) & (\Selector5~0_combout\)) # (!GLOBAL(\Selector0~2clkctrl_outclk\) & ((\next_state.state_4_442~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector5~0_combout\,
	datab => \next_state.state_4_442~combout\,
	datad => \Selector0~2clkctrl_outclk\,
	combout => \next_state.state_4_442~combout\);

-- Location: FF_X2_Y3_N21
\tone_state.state_4\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \next_state.state_4_442~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tone_state.state_4~q\);

-- Location: LCCOMB_X2_Y3_N14
\Selector4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector4~0_combout\ = (\Equal1~5_combout\ & (\tone_state.state_4~q\)) # (!\Equal1~5_combout\ & ((\tone_state.state_2~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal1~5_combout\,
	datac => \tone_state.state_4~q\,
	datad => \tone_state.state_2~q\,
	combout => \Selector4~0_combout\);

-- Location: LCCOMB_X2_Y3_N16
\next_state.state_3_470\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \next_state.state_3_470~combout\ = (GLOBAL(\Selector0~2clkctrl_outclk\) & (\Selector4~0_combout\)) # (!GLOBAL(\Selector0~2clkctrl_outclk\) & ((\next_state.state_3_470~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector4~0_combout\,
	datac => \next_state.state_3_470~combout\,
	datad => \Selector0~2clkctrl_outclk\,
	combout => \next_state.state_3_470~combout\);

-- Location: LCCOMB_X2_Y3_N12
\tone_state.state_3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tone_state.state_3~0_combout\ = !\next_state.state_3_470~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \next_state.state_3_470~combout\,
	combout => \tone_state.state_3~0_combout\);

-- Location: FF_X2_Y3_N13
\tone_state.state_3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \tone_state.state_3~0_combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tone_state.state_3~q\);

-- Location: LCCOMB_X2_Y3_N22
\Selector3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = (\Equal1~5_combout\ & ((!\tone_state.state_3~q\))) # (!\Equal1~5_combout\ & (\tone_state.state_1~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal1~5_combout\,
	datac => \tone_state.state_1~q\,
	datad => \tone_state.state_3~q\,
	combout => \Selector3~0_combout\);

-- Location: LCCOMB_X2_Y3_N8
\next_state.state_2_498\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \next_state.state_2_498~combout\ = (GLOBAL(\Selector0~2clkctrl_outclk\) & ((\Selector3~0_combout\))) # (!GLOBAL(\Selector0~2clkctrl_outclk\) & (\next_state.state_2_498~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \next_state.state_2_498~combout\,
	datab => \Selector3~0_combout\,
	datad => \Selector0~2clkctrl_outclk\,
	combout => \next_state.state_2_498~combout\);

-- Location: FF_X2_Y3_N9
\tone_state.state_2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \next_state.state_2_498~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tone_state.state_2~q\);

-- Location: LCCOMB_X1_Y3_N22
\Selector2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector2~0_combout\ = (\tone_state.state_0~q\) # ((\tone_state.state_2~q\ & \Equal1~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \tone_state.state_2~q\,
	datac => \Equal1~5_combout\,
	datad => \tone_state.state_0~q\,
	combout => \Selector2~0_combout\);

-- Location: LCCOMB_X1_Y3_N6
\next_state.state_1_526\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \next_state.state_1_526~combout\ = (GLOBAL(\Selector0~2clkctrl_outclk\) & ((\Selector2~0_combout\))) # (!GLOBAL(\Selector0~2clkctrl_outclk\) & (\next_state.state_1_526~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \next_state.state_1_526~combout\,
	datac => \Selector2~0_combout\,
	datad => \Selector0~2clkctrl_outclk\,
	combout => \next_state.state_1_526~combout\);

-- Location: FF_X1_Y3_N7
\tone_state.state_1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~input_o\,
	d => \next_state.state_1_526~combout\,
	clrn => \reset~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tone_state.state_1~q\);

-- Location: LCCOMB_X3_Y3_N10
\Selector9~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector9~3_combout\ = (\devider:counter[1]~q\ & ((\tone_state.state_1~q\) # ((\tone_state.state_0~q\ & \devider:counter[0]~q\)))) # (!\devider:counter[1]~q\ & (((\tone_state.state_0~q\ & \devider:counter[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \devider:counter[1]~q\,
	datab => \tone_state.state_1~q\,
	datac => \tone_state.state_0~q\,
	datad => \devider:counter[0]~q\,
	combout => \Selector9~3_combout\);

-- Location: LCCOMB_X3_Y3_N4
\Selector9~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector9~0_combout\ = (\tone_state.state_6~q\ & ((\devider:counter[6]~q\) # ((\tone_state.state_7~q\ & \devider:counter[7]~q\)))) # (!\tone_state.state_6~q\ & (\tone_state.state_7~q\ & ((\devider:counter[7]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tone_state.state_6~q\,
	datab => \tone_state.state_7~q\,
	datac => \devider:counter[6]~q\,
	datad => \devider:counter[7]~q\,
	combout => \Selector9~0_combout\);

-- Location: LCCOMB_X3_Y3_N30
\Selector9~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector9~1_combout\ = (\devider:counter[4]~q\ & ((\tone_state.state_4~q\) # ((\tone_state.state_5~q\ & \devider:counter[5]~q\)))) # (!\devider:counter[4]~q\ & (\tone_state.state_5~q\ & ((\devider:counter[5]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \devider:counter[4]~q\,
	datab => \tone_state.state_5~q\,
	datac => \tone_state.state_4~q\,
	datad => \devider:counter[5]~q\,
	combout => \Selector9~1_combout\);

-- Location: LCCOMB_X3_Y3_N12
\Selector9~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector9~2_combout\ = (\devider:counter[2]~q\ & ((\tone_state.state_2~q\) # ((\devider:counter[3]~q\ & !\tone_state.state_3~q\)))) # (!\devider:counter[2]~q\ & (\devider:counter[3]~q\ & ((!\tone_state.state_3~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \devider:counter[2]~q\,
	datab => \devider:counter[3]~q\,
	datac => \tone_state.state_2~q\,
	datad => \tone_state.state_3~q\,
	combout => \Selector9~2_combout\);

-- Location: LCCOMB_X3_Y3_N8
\Selector9~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Selector9~4_combout\ = (\Selector9~3_combout\) # ((\Selector9~0_combout\) # ((\Selector9~1_combout\) # (\Selector9~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector9~3_combout\,
	datab => \Selector9~0_combout\,
	datac => \Selector9~1_combout\,
	datad => \Selector9~2_combout\,
	combout => \Selector9~4_combout\);

-- Location: UNVM_X0_Y8_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

ww_clk_div <= \clk_div~output_o\;
END structure;


